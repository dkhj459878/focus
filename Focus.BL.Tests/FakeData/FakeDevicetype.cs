﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных типов ТСТК
    /// </summary>
    public class FakeDevicetype
    {

        /// <summary>
        /// Возвращает коллекцию сгенерированных типов ТСТК.
        /// </summary>
        public List<DEVICETYPE> Devicetypes { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных типов ТСТК.
        /// </summary>
        public FakeDevicetype()
        {
            Devicetypes = new List<DEVICETYPE>()
                {new DEVICETYPE() { DEVICETYPEID = 1, NAME = "ВЕСЫ", LAST_UPDATE = DateTime.Now },
                 new DEVICETYPE() { DEVICETYPEID = 2, NAME = "ФОТОАППАРАТ", LAST_UPDATE = DateTime.Now},
                 new DEVICETYPE() { DEVICETYPEID = 3, NAME = "ВИДЕОКАМЕРА", LAST_UPDATE = DateTime.Now }
           };
        }
    }
}