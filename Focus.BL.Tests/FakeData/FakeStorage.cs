﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о СВХ.
    /// </summary>
    public class FakeStorage
    {
        /// <summary>
        /// Возвращает генератор данных о СВХ.
        /// </summary>
        public Faker<STORAGE> StorageFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных СВХ.
        /// </summary>
        public List<STORAGE> Storages { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных СВХ.
        /// </summary>
        /// <param name="storagesCount">Необходимое количество СВХ.</param>
        public FakeStorage(int storagesCount)
        {

            StorageFaker = new Faker<STORAGE>()
                .RuleFor(c => c.ADDRESS, f => f.Address.FullAddress())
                .RuleFor(c => c.ID, f => (byte)f.IndexFaker)
                .RuleFor(c => c.NAME, f => f.Random.Words(2))
                .RuleFor(c => c.NUMBER_, f => f.Random.AlphaNumeric(5))
                .RuleFor(c => c.POST, f => new FakePost(5).Posts.ToArray());

            

            Storages = StorageFaker.Generate(storagesCount);



        }
    }
}
