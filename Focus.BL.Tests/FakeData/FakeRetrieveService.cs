﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о таможенных представителях.
    /// </summary>
    public class FakeAudit_Trail
    {
        /// <summary>
        /// Возвращает генератор данных о документах.
        /// </summary>
        public Faker<AUDIT_TRAIL> Audit_TrailFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных документов.
        /// </summary>
        public List<AUDIT_TRAIL> Audit_Trails { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных документов.
        /// </summary>
        /// <param name="docsCount">Необходимое количество документов.</param>
        public FakeAudit_Trail(int audit_TrailsCount)
        {

            Audit_TrailFaker = new Faker<AUDIT_TRAIL>()
                .RuleFor(c => c.ID, f => (short)f.IndexFaker)
                .RuleForType(typeof(string), f => f.Random.Word())
                .RuleForType(typeof(DateTime), f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleForType(typeof(DateTime?), f => (DateTime?)f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleForType(typeof(decimal), f => f.Random.Decimal(min: (decimal)0.35, max: 1500))
                .RuleForType(typeof(sbyte), f => f.PickRandomParam(new sbyte[] { 0, 1 }))
                .RuleForType(typeof(sbyte?), f => f.PickRandomParam(new sbyte?[] { 0, 1 }))
                .RuleForType(typeof(short?), f => (short?)f.Random.Short());


            Audit_Trails = Audit_TrailFaker.Generate(audit_TrailsCount);



        }
    }
}
