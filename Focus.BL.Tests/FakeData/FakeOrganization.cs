﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных об организациях.
    /// </summary>
    public class FakeOrganization
    {
        /// <summary>
        /// Возвращает генератор данных об организациях.
        /// </summary>
        public Faker<ORGANIZATION> OrganizationFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных организаций.
        /// </summary>
        public List<ORGANIZATION> Organizations { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных организаций.
        /// </summary>
        /// <param name="organizationsCount">Необходимое количество организаций.</param>
        public FakeOrganization(int organizationsCount)
        {

            OrganizationFaker = new Faker<ORGANIZATION>()
                .RuleFor(c => c.ORGANIZATIONID, f => (short)f.IndexFaker)
                .RuleFor(c => c.ADDRESS, f   => f.Address.FullAddress())
                .RuleFor(c => c.ADDRESSFULL, f => f.Address.FullAddress())
                .RuleFor(c => c.BUILDING, f => f.Address.BuildingNumber())
                .RuleFor(c => c.STREET_TYPE, f => f.Address.StreetSuffix())
                .RuleFor(c => c.STREET, f => f.Address.StreetName())
                .RuleFor(c => c.COUNTRYID, f => f.PickRandomParam(new FakeCountry(15).Countries.ToArray()).ABBR2)
                .RuleFor(c => c.BUILDING_BLOCK, f => f.Address.BuildingNumber())
                .RuleFor(c => c.LAST_UPDATE, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.CITY, f => f.Address.City())
                .RuleFor(c => c.CITY_TYPE, f => f.Address.CityPrefix())
                .RuleFor(c => c.CUSTOM_ZONE, f => "06")
                .RuleFor(c => c.DISTRICT, f => f.Address.State())
                .RuleFor(c => c.EMAIL, f => f.Internet.Email())
                .RuleFor(c => c.NAME, f => f.Company.CompanyName())
                .RuleFor(c => c.UNP, f => f.Random.Digits(9).ToString())
                .RuleFor(c => c.FAX, f => f.Person.Phone)
                
                .RuleFor(c => c.OFFICE, f => f.Address.SecondaryAddress())
                
                .RuleFor(c => c.POST_INDEX, f => f.Address.ZipCode());

            

            Organizations = OrganizationFaker.Generate(organizationsCount);



        }
    }
}
