﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о видах транспортных средств.
    /// </summary>
    public class FakeVehiclekind
    {

        /// <summary>
        /// Возвращает коллекцию сгенерированных видов транспортных средств.
        /// </summary>
        public List<VEHICLEKIND> Vehiclekinds { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированых данных о видах транспортных средств.
        /// </summary>
        public FakeVehiclekind()
        {
            Vehiclekinds = new List<VEHICLEKIND>()
            { new VEHICLEKIND() { VEHICLEKINDID = 1,  NAME= "ЛЕГКОВАЯ" , VEHICLETYPEID = 1, VEHICLETYPE = new VEHICLETYPE() { VEHICLETYPEID = 1, NAME = "ЛЕГКОВАЯ"} },
              new VEHICLEKIND() { VEHICLEKINDID = 2, NAME = "ТЯГАЧ", VEHICLETYPEID = 2, VEHICLETYPE = new VEHICLETYPE() { VEHICLETYPEID = 2, NAME = "ЛЕГКОВАЯ"} },
              new VEHICLEKIND() { VEHICLEKINDID = 3, NAME = "ПОЛУПРИЦЕП", VEHICLETYPEID = 2, VEHICLETYPE = new VEHICLETYPE() { VEHICLETYPEID = 2, NAME = "ГРУЗОВАЯ"}  }
           };
        }
    }
}