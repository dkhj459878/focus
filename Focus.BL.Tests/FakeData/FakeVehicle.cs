﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о транспортных средствах.
    /// </summary>
    public class FakeVehicle
    {
        /// <summary>
        /// Возвращает генератор данных о транспортных средствах.
        /// </summary>
        public Faker<VEHICLE> VehicleFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных транспортных средств.
        /// </summary>
        public List<VEHICLE> Vehicles { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных транспортных средств.
        /// </summary>
        /// <param name="vehiclesCount">Необходимое количество транспортных средств.</param>
        public FakeVehicle(int vehiclesCount)
        {

            VehicleFaker = new Faker<VEHICLE>()
                .RuleFor(c => c.VEHICLEID, f => (short)f.IndexFaker)
                .RuleFor(c => c.COUNTRY, f => f.PickRandomParam(new FakeCountry(15).Countries.ToArray()))
                .RuleFor(c => c.COUNTRYCODE, (f, c) => c.COUNTRY.ABBR2)
                .RuleFor(c => c.DESCRIPTION, f => f.Random.Word())
                .RuleFor(c => c.VEHICLEKIND, f => f.PickRandomParam(new FakeVehiclekind().Vehiclekinds.ToArray()[new Random().Next(0, 2)]))
                .RuleFor(c => c.MARK, f => f.Random.Word())
                .RuleFor(c => c.MODEL, f => f.Random.Word())
                .RuleFor(c => c.VEHICLEKINDID, (f, c) => c.VEHICLEKIND.VEHICLEKINDID)
                .RuleFor(c => c.LAST_UPDATE, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.DESCRIPTION, f => f.Random.Word())
                .RuleFor(c => c.REGNUMBEDIGITSAFTER, f => f.Random.Word())
                .RuleFor(c => c.REGNUMBER, f => f.UniqueIndex.ToString())
                .RuleFor(c => c.REGNUMBERDIGITS, f => f.Random.Word())
                .RuleFor(c => c.REGNUMBERLETTERS, f => f.Random.Word());

            Vehicles = VehicleFaker.Generate(vehiclesCount);



        }
    }
}
