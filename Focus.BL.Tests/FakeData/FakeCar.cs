﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных об автомобилях.
    /// </summary>
    public class FakeCar
    {
        /// <summary>
        /// Возвращает генератор данных об автомобилях.
        /// </summary>
        public Faker<CAR> CarFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных автомобилей.
        /// </summary>
        public List<CAR> Cars { get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных автомобилей.
        /// </summary>
        /// <param name="carsCount">Необходимое количество автомобилей.</param>
        public FakeCar(int carsCount)
        {
            
            CarFaker = new Faker<CAR>()
                .RuleFor(c => c.CARID, f => (short)f.IndexFaker)
                .RuleFor(c => c.COUNTRY, f => f.PickRandomParam(new FakeCountry(15).Countries.ToArray()))
                .RuleFor(c => c.COUNTRYCODE, (f, c) => c.COUNTRY.ABBR2)
                .RuleFor(c => c.DESCRIPTION, f => f.Random.Word())
                .RuleFor(c => c.GOODSMARK, f => f.Random.Word())
                .RuleFor(c => c.GOODSMODEL, f => f.Random.Word())
                .RuleFor(c => c.LAST_UPDATE, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.MOTORFUELTYPE, f => f.PickRandomParam("БЕНЗИНОВЫЙ", "ДИЗЕЛЬНЫЙ", "ГИБРИДНЫЙ"))
                .RuleFor(c => c.VEHICLEKIND, f => f.PickRandom(new FakeVehiclekind().Vehiclekinds))
                .RuleFor(c => c.VEHICLEKINDID, (f, c) => c.VEHICLEKIND.VEHICLEKINDID)
                .RuleFor(c => c.VIN, f => f.Random.AlphaNumeric(17));

            Cars = CarFaker.Generate(carsCount);



        }
    }
}
