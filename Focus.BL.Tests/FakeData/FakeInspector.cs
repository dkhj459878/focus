﻿using Bogus;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Tests.FakeData
{
    /// <summary>
    /// Представляет набор сгенерированных данных о таможенных представителях.
    /// </summary>
    public class FakeInspector
    {
        /// <summary>
        /// Возвращает генератор данных о таможенных представителях.
        /// </summary>
        public Faker<INSPECTOR> InspectorFaker { get; }

        /// <summary>
        /// Возвращает коллекцию сгенерированных таможенных представителей.
        /// </summary>
        public List<INSPECTOR> Inspectors{ get; }

        /// <summary>
        /// Создает новый экземпляр сгенерированных таможенных представителей.
        /// </summary>
        /// <param name="inspectorsCount">Необходимое количество таможенных представителей.</param>
        public FakeInspector(int inspectorsCount)
        {

            InspectorFaker = new Faker<INSPECTOR>()
                .RuleFor(c => c.ID, f => (short)f.IndexFaker)
                .RuleFor(c => c.FATHERNAME, f => f.Person.FirstName.ToString())
                .RuleFor(c => c.FIRSTNAME, f => f.Person.FirstName.ToString())
                .RuleFor(c => c.LASTNAME, f => f.Person.LastName.ToString())
                .RuleFor(c => c.LNP, f => f.Random.Digits(3).ToString())
                .RuleFor(c => c.PHONENUMBER, f => f.Person.Phone.ToString())
                .RuleFor(c => c.POST, f => f.PickRandomParam(new FakePost(15).Posts.ToArray()))
                .RuleFor(c => c.POSTID, (f, c) => c.POST.ID)
                .RuleFor(c => c.LAST_UPDATE, f => f.Date.Past(2, DateTime.Now.AddMonths(-2)))
                .RuleFor(c => c.SEAL, f => f.Random.Chars(count: 2) + f.Random.Digits(2).ToString())
                .RuleFor(c => c.SHORTNAME, f => f.Person.FullName.ToString())
                .RuleFor(c => c.TITLE, (f, c) => f.PickRandom("инспектор", "старший инспектор", "главный инспектор"));


            Inspectors = InspectorFaker.Generate(inspectorsCount);
        }
    }
}
