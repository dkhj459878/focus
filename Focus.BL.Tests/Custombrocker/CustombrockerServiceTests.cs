﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Custombrocker
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="CustombrockerService"/>
    /// </summary>
    [TestFixture]
    public class CustombrockerServiceTests
    {
        private const int CustombrockersCount = 300;
        private ICustombrockerService _custombrockerService;
        private FakeCustombrocker _fakeCustombrockers;
        private Mock<IDbCustombrockerRepository> mockcustombrockerRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeCustombrockers = new FakeCustombrocker(CustombrockersCount);

            mockcustombrockerRepository = new Mock<IDbCustombrockerRepository>();
            mockcustombrockerRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<CUSTOMBROCKER, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<CUSTOMBROCKER, bool>> filter,
                    Func<IQueryable<CUSTOMBROCKER>, IOrderedQueryable<CUSTOMBROCKER>> orderBy,
                    string includeProperties) => _fakeCustombrockers.Custombrockers.Where(filter.Compile()));
            mockcustombrockerRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<CUSTOMBROCKER, bool>> filter,
                    Func<IQueryable<CUSTOMBROCKER>, IOrderedQueryable<CUSTOMBROCKER>> orderBy,
                    string includeProperties) => _fakeCustombrockers.Custombrockers);
            mockcustombrockerRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<CUSTOMBROCKER, bool>> filter,
                    Func<IQueryable<CUSTOMBROCKER>, IOrderedQueryable<CUSTOMBROCKER>> orderBy,
                    string includeProperties) => _fakeCustombrockers.Custombrockers.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockcustombrockerRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeCustombrockers.Custombrockers.Find(f => f.CUSTOMBROCKERID == id));
            mockcustombrockerRepository.Setup(mock => mock.Insert(It.IsAny<CUSTOMBROCKER>()))
                .Callback((CUSTOMBROCKER f) => _fakeCustombrockers.Custombrockers.Add(f));
            mockcustombrockerRepository.Setup(mock => mock.Update(It.IsAny<CUSTOMBROCKER>()))
                .Callback((CUSTOMBROCKER f) => _fakeCustombrockers.Custombrockers.Find(d => d.CUSTOMBROCKERID == f.CUSTOMBROCKERID).FIRSTNAME = f.FIRSTNAME);
            mockcustombrockerRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeCustombrockers.Custombrockers
                    .Remove(_fakeCustombrockers.Custombrockers.Find(f => f.CUSTOMBROCKERID == id)));

            var mockcustombrockerUnitOfWork = new Mock<IUnitOfWork>();
            mockcustombrockerUnitOfWork.Setup(mock => mock.CustombrockerRepository)
                .Returns(() => mockcustombrockerRepository.Object); //todo - закоментил, не знаю в чем дело

            _custombrockerService = new CustombrockerService(mockcustombrockerUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров таможенных представителей
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedCustombrockersPageTest([Random(1, 14, 1)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            var custombrockers = _fakeCustombrockers.Custombrockers;

            var custombrockersPageIndexes = new List<int>();

            var startCustombrockerIndexInPage = pagesize * (pageNumber - 1);

            var finishCustombrockerIndexInPage = startCustombrockerIndexInPage + pagesize - 1;

            for (var i = startCustombrockerIndexInPage; i <= finishCustombrockerIndexInPage; i++)
            {
                custombrockersPageIndexes.Add(i);
            }

            List<CUSTOMBROCKER> custombrockersPage;
            custombrockersPage = custombrockersPageIndexes.Select(custombrockersPageIndex => custombrockers[custombrockersPageIndex]).ToList();


            // Act

            var actualCustombrockersPage = _custombrockerService.GetCustombrockersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            custombrockersPage.Should().BeEquivalentTo(actualCustombrockersPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров таможенных представителей
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedCustombrockersLastPageTest()
        {
            const int pagesize = 16;
            const int pageNumber = 19;

            // Arrange
            var dbCustombrockers = _fakeCustombrockers.Custombrockers;

            var custombrockersPageIndexes = new List<int>();

            var startCustombrockerIndexInPage = pagesize * (pageNumber - 1);

            var finishCustombrockerIndexInPage = startCustombrockerIndexInPage + CustombrockersCount % pagesize - 1;

            for (var i = startCustombrockerIndexInPage; i <= finishCustombrockerIndexInPage; i++)
            {
                custombrockersPageIndexes.Add(i);
            }

            var dbCustombrockersPage = custombrockersPageIndexes.Select(custombrockersPageIndex => dbCustombrockers[custombrockersPageIndex]).ToList();

            // Act
            var actualCustombrockersPage = _custombrockerService.GetCustombrockersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbCustombrockersPage.Should().BeEquivalentTo(actualCustombrockersPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров таможенных представителей
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedCustombrockersPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<CUSTOMBROCKER> actualCustombrockersPage = _custombrockerService.GetCustombrockersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<CUSTOMBROCKER> expectedPage = _custombrockerService.GetCustombrockersPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualCustombrockersPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров таможенных представителей
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedCustombrockersPageWithValueIfPageNumbersMoreThanTotalCustombrockersPagesTest([Random(CustombrockersCount, CustombrockersCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<CUSTOMBROCKER> actualCustombrockersPage = _custombrockerService.GetCustombrockersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualCustombrockersPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров таможенных представителей
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedCustombrockersPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeCustombrockers.Custombrockers.Clear();

            // Act
            ICollection<CUSTOMBROCKER> actualCustombrockersPage = _custombrockerService.GetCustombrockersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualCustombrockersPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров таможенных представителей
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllCustombrockersFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeCustombrockers.Custombrockers.Clear();

            // Act
            var actualCustombrockers = _custombrockerService.GetAllCustombrockers(filter: null);

            // Assert
            actualCustombrockers.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров таможенных представителей
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedCustombrockersTest()
        {
            // Arrange
            var expectedCustombrockers = _fakeCustombrockers.Custombrockers;

            // Act
            var actualCustombrockers = _custombrockerService.GetAllCustombrockers().ToList();

            // Assert
            foreach (var expectedCustombrocker in expectedCustombrockers)
            {
                var i = expectedCustombrocker.CUSTOMBROCKERID;

                expectedCustombrockers[i].COUNTRY_ID.Should().Be(actualCustombrockers[i].COUNTRY_ID);
                expectedCustombrockers[i].FIRSTNAME.Should().Be(actualCustombrockers[i].FIRSTNAME);
            }

            expectedCustombrockers.Should().BeEquivalentTo(actualCustombrockers,
                options => options.ExcludingMissingMembers());

            expectedCustombrockers.Count.Should().Be(actualCustombrockers.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра таможенного представителя по заданному идентификатору.
        /// таможенный представитель с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор таможенного представителя</param>
        [Test, Order(1)]
        public void GetExistedCustombrockerTest([Random(0, CustombrockersCount - 1, 5)] short id)
        {
            // Arrange
            var expectedCustombrocker = _fakeCustombrockers.Custombrockers.Find(f => f.CUSTOMBROCKERID == id);

            // Act
            var actualCustombrocker = _custombrockerService.GetCustombrocker(id);

            // Assert


            actualCustombrocker.Should().BeEquivalentTo(expectedCustombrocker,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра таможенного представителя по заданному идентификатору.
        /// таможенный представитель с передаваемым идентификатором не существует
        /// </summary>
        /// <param name="id">Идентификатор таможенного представителя</param>
        [Test, Order(1)]
        public void GetNotExistedCustombrockerTest([Random(CustombrockersCount, CustombrockersCount + 300, 5)]
                    short id)
        {
            var actualCustombrocker = _custombrockerService.GetCustombrocker(id);

            actualCustombrocker.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления таможенного представителя в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddCustombrockerTest()
        {

            short custombrockerId = 15;
            
            // Arrange
            var custombrockersFake = _fakeCustombrockers.Custombrockers;

            var custombrockerFake = custombrockersFake.Where(c => c.CUSTOMBROCKERID == custombrockerId).FirstOrDefault();
            

            // Act

            var actualCustombrocker = _custombrockerService.GetCustombrocker(custombrockerId);

            // Assert

            actualCustombrocker.Should().BeEquivalentTo(actualCustombrocker,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления таможенного представителя в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddCustombrockerIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var custombrocker = new FakeCustombrockers(1, false, false, false, false, 5, 5).Custombrockers[0];
            //var custombrockerManagementService = new CustombrockerManagementService(_unitOfWork);
            //custombrocker.Id = _fakeCustombrockers.Custombrockers.Last().Id + 1;

            //// Act
            //var actualId = _custombrockerService.AddCustombrocker(custombrocker);

            //var actualCustombrocker = _custombrockerService.GetCustombrocker(actualId);

            //// Assert
            //actualCustombrocker.CustombrockerDetail.EMailAddresses.Should().BeEquivalentTo(custombrocker.CustombrockerDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualCustombrocker.Login.Should().BeEquivalentTo(custombrocker.Login);
            //actualCustombrocker.FirstName.Should().BeEquivalentTo(custombrocker.FirstName);
            //actualCustombrocker.LastName.Should().BeEquivalentTo(custombrocker.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об таможенном представителе
        /// </summary>
        /// <param name="id">Идентификатор таможенного представителя для обновления</param>
        [Test, Order(2)]
        public void UpdateCustombrockerTest([Random(0, CustombrockersCount - 1, 5)] short id)
        {
            // Arrange
            var custombrocker = _custombrockerService.GetCustombrocker(id);
            custombrocker.FIRSTNAME = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _custombrockerService.UpdateCustombrocker(custombrocker);
            var actualCustombrocker = _fakeCustombrockers.Custombrockers.Find(f => f.CUSTOMBROCKERID == id);

            // Assert
            actualCustombrocker.FIRSTNAME.Should().Be(custombrocker.FIRSTNAME);
        }

        /// <summary>
        /// Тест метода удаления таможенного представителя из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор таможенного представителя для удаления</param>
        [Test, Order(3)]
        public void DeleteCustombrockerTest([Random(0, CustombrockersCount - 1, 5)] short id)
        {
            // Arrange
            _custombrockerService.DeleteCustombrocker(id);

            // Act
            var actualCustombrocker = _fakeCustombrockers.Custombrockers.Find(custombrocker => custombrocker.CUSTOMBROCKERID == id);

            // Assert
            actualCustombrocker.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия таможенного представителя в хранилище данных.
        /// таможенный представитель существует
        /// </summary>
        /// <param name="id">Идентификатор таможенного представителя для поиска</param>
        [Test, Order(2)]
        public void CustombrockerExistsTest([Random(0, CustombrockersCount - 1, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeCustombrockers.Custombrockers.Find(dbf => dbf.CUSTOMBROCKERID == id).FIRSTNAME;

            // Act
            var actual = _custombrockerService
                .CustombrockerExists(f => f.FIRSTNAME == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия таможенного представителя в хранилище данных.
        /// таможенный представитель не существует
        /// </summary>
        /// <param name="login">Логин таможенного представителя для поиска</param>
        [Test, Order(2)]
        public void CustombrockerNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _custombrockerService
                .CustombrockerExists(f => f.FIRSTNAME == VIN);

            actual.Should().BeFalse();
        }
    }
}

