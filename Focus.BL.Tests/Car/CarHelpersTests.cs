﻿using FluentAssertions;
using Focus.BL.Helpers;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Focus.BL.Tests.Car
{
    /// <summary>
    /// Unit-тесты для хелпера <see cref="CarHelper"/>
    /// </summary>
    [TestFixture]
    public class CarHelpersTests
    {
        private const int CarsCount = 300;
        private FakeCar _fakeCars;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {
            _fakeCars = new FakeCar(CarsCount);
        }

        /// <summary>
        /// Тест метода получения экземпляров автомобилей
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Repeat(5), Order(1)]
        public void GetCarDataAscTest()
        {
            List<CAR> cars = new FakeCar(5).Cars;

            cars[0].CARID = 135;
            cars[1].CARID = 45;
            cars[2].CARID = 67;
            cars[3].CARID = 447;
            cars[4].CARID = 3;

            int index = 1;
            int count = 5;
            const int ZERO_INDEX = 0;
            StringBuilder stringBuilder = new StringBuilder();
            List<CAR> carsExpected = cars.OrderBy(c => c.CARID).ToList();
            stringBuilder.Append(carsExpected[ZERO_INDEX].CARID.ToString());
            while (index < count)
            {
                stringBuilder.Append("," + carsExpected[index++].CARID.ToString());
            }
            string expected = stringBuilder.ToString();


            // Act

            string actual = carsExpected.GetCarDataAsc();

            // Assert
            actual.Should().BeEquivalentTo(expected);
        }
    }
}

