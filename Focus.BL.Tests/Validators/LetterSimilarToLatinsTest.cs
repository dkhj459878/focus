﻿using FluentAssertions;
using Focus.BL.Validators;
using NUnit.Framework;

namespace Focus.BL.Tests.Validators
{
    /// <summary>
    /// Unit-тесты для валидации <see cref="ConvertSimilarLatinsTest"/>
    /// </summary>
    [TestFixture]
    public class ConvertSimilarLatinsTest
    {
        StringMaster _stringMaster;

        [OneTimeSetUp]
        public void SetUp()
        {
            _stringMaster = new StringMaster();
        }

        /// <summary>
        /// Проверка конвертации.
        /// </summary>
        [Test, Order(1)]
        public void ConvertSimilarLatinsTestAllRight()
        {
            // Arrange
            string strCirillica = "КЕНХОРАВСМТЗ";

            string strExpected = "KEHXOPABCMTЗ";

            // Act

            string result = _stringMaster.ConvertSimilarLatins(strCirillica);
            var simbols = result.ToCharArray();
            // Assert
            strExpected.Should().Equals(result);
        }

        [Test, Order(1)]
        public void GetLastFolderNameAllRight()
        {
            // Arrange
            string pathString = @"E:\Tests\SampleMySQlidentity\";
            string pathString1 = @"E:\Tests\";
            string pathString2 = @"E:\";
            string awaited = "SampleMySQlidentity";
            string awaited1 = "Tests";
            string awaited2 = "E:";
            // Assert
            string result =_stringMaster.GetLastFolderName(pathString);
            string result1 = _stringMaster.GetLastFolderName(pathString1);
            string result2 = _stringMaster.GetLastFolderName(pathString2);
            //Assert
            result.Should().Be(awaited);
            result1.Should().Be(awaited1);
            result2.Should().Be(awaited2);
        }

        [Test, Order(1)]
        public void GetLastFolderNameWringString()
        {
            // Arrange
            string pathString = "Tests";
            string awaited = string.Empty;
            // Assert
            string result = _stringMaster.GetLastFolderName(pathString);

            //Assert
            result.Should().Equals(awaited);
        }

    }
}