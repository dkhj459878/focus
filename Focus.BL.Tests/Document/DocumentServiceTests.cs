﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Document
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="DocumentService"/>
    /// </summary>
    [TestFixture]
    public class DocumentServiceTests
    {
        private const int DocumentsCount = 20;
        private IDocumentService _documentService;
        private FakeDocument _fakeDocuments;
        private Mock<IDbDocumentRepository> mockdocumentRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeDocuments = new FakeDocument(DocumentsCount);

            mockdocumentRepository = new Mock<IDbDocumentRepository>();
            mockdocumentRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<DOCUMENT, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<DOCUMENT, bool>> filter,
                    Func<IQueryable<DOCUMENT>, IOrderedQueryable<DOCUMENT>> orderBy,
                    string includeProperties) => _fakeDocuments.Documents.Where(filter.Compile()));
            mockdocumentRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<DOCUMENT, bool>> filter,
                    Func<IQueryable<DOCUMENT>, IOrderedQueryable<DOCUMENT>> orderBy,
                    string includeProperties) => _fakeDocuments.Documents);
            mockdocumentRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<DOCUMENT, bool>> filter,
                    Func<IQueryable<DOCUMENT>, IOrderedQueryable<DOCUMENT>> orderBy,
                    string includeProperties) => _fakeDocuments.Documents.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockdocumentRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeDocuments.Documents.Find(f => f.DOCUMENTID == id));
            mockdocumentRepository.Setup(mock => mock.Insert(It.IsAny<DOCUMENT>()))
                .Callback((DOCUMENT f) => _fakeDocuments.Documents.Add(f));
            mockdocumentRepository.Setup(mock => mock.Update(It.IsAny<DOCUMENT>()))
                .Callback((DOCUMENT f) => _fakeDocuments.Documents.Find(d => d.DOCUMENTID == f.DOCUMENTID).NUMBER_ = f.NUMBER_);
            mockdocumentRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeDocuments.Documents
                    .Remove(_fakeDocuments.Documents.Find(f => f.DOCUMENTID == id)));

            var mockdocumentUnitOfWork = new Mock<IUnitOfWork>();
            mockdocumentUnitOfWork.Setup(mock => mock.DocumentRepository)
                .Returns(() => mockdocumentRepository.Object); //todo - закоментил, не знаю в чем дело

            _documentService = new DocumentService(mockdocumentUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// выбранной документовицы пейджинга. Выбрана может быть любая документовица.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDocumentsPageTest([Random(1, 5, 1)]int pageNumber)
        {
            const int pagesize = 3;

            // Arrange
            var documents = _fakeDocuments.Documents;

            var documentsPageIndexes = new List<int>();

            var startDocumentIndexInPage = pagesize * (pageNumber - 1);

            var finishDocumentIndexInPage = startDocumentIndexInPage + pagesize - 1;

            for (var i = startDocumentIndexInPage; i <= finishDocumentIndexInPage; i++)
            {
                documentsPageIndexes.Add(i);
            }

            List<DOCUMENT> documentsPage;
            documentsPage = documentsPageIndexes.Select(documentsPageIndex => documents[documentsPageIndex]).ToList();


            // Act

            var actualDocumentsPage = _documentService.GetDocumentsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            documentsPage.Should().BeEquivalentTo(actualDocumentsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// выбранной документовицы пейджинга. Выбрана последняя документовица.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDocumentsLastPageTest()
        {
            const int pagesize = 19;
            const int pageNumber = 2;

            // Arrange
            var dbDocuments = _fakeDocuments.Documents;

            var documentsPageIndexes = new List<int>();

            var startDocumentIndexInPage = pagesize * (pageNumber - 1);

            var finishDocumentIndexInPage = startDocumentIndexInPage + DocumentsCount % pagesize - 1;

            for (var i = startDocumentIndexInPage; i <= finishDocumentIndexInPage; i++)
            {
                documentsPageIndexes.Add(i);
            }

            var dbDocumentsPage = documentsPageIndexes.Select(documentsPageIndex => dbDocuments[documentsPageIndex]).ToList();

            // Act
            var actualDocumentsPage = _documentService.GetDocumentsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbDocumentsPage.Should().BeEquivalentTo(actualDocumentsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// выбранной документовицы пейджинга. Номер документовицы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDocumentsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DOCUMENT> actualDocumentsPage = _documentService.GetDocumentsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<DOCUMENT> expectedPage = _documentService.GetDocumentsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualDocumentsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// выбранной документовицы пейджинга. Номер документовицы больше общего числа документовиц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDocumentsPageWithValueIfPageNumbersMoreThanTotalDocumentsPagesTest([Random(DocumentsCount, DocumentsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DOCUMENT> actualDocumentsPage = _documentService.GetDocumentsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDocumentsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров документов
        /// ввыбранной документовицы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedDocumentsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeDocuments.Documents.Clear();

            // Act
            ICollection<DOCUMENT> actualDocumentsPage = _documentService.GetDocumentsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDocumentsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров документов
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllDocumentsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeDocuments.Documents.Clear();

            // Act
            var actualDocuments = _documentService.GetAllDocuments(filter: null);

            // Assert
            actualDocuments.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров документов
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedDocumentsTest()
        {
            // Arrange
            var expectedDocuments = _fakeDocuments.Documents;

            // Act
            var actualDocuments = _documentService.GetAllDocuments().ToList();

            // Assert
            //foreach (var expectedDocument in expectedDocuments)
            //{
            //    expectedDocument.DOCUMENTID.Should().Be((actualDocuments.Find(c => c.DOCUMENTID == expectedDocument.DOCUMENTID).DOCUMENTID));
            //    expectedDocument.NUMBER.Should().Be((actualDocuments.Find(c => c.DOCUMENTID == expectedDocument.DOCUMENTID).NUMBER_));
            //}

            expectedDocuments.Should().BeEquivalentTo(actualDocuments,
                options => options.ExcludingMissingMembers());

            expectedDocuments.Count.Should().Be(actualDocuments.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра документы по заданному идентификатору.
        /// документ с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор документы</param>
        [Test, Order(1)]
        public void GetExistedDocumentTest()
        {
            // Arrange
            var documentId = _fakeDocuments.Documents.ToArray()[(new Random()).Next(DocumentsCount)].DOCUMENTID;


            var expectedDocument = _fakeDocuments.Documents.Find(f => f.DOCUMENTID == documentId);

            // Act
            var actualDocument = _documentService.GetDocument(documentId);

            // Assert


            actualDocument.Should().BeEquivalentTo(expectedDocument,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра документы по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор документы</param>
        [Test, Order(1)]
        public void GetNotExistedDocumentTest([Random(DocumentsCount, DocumentsCount + DocumentsCount, 5)]
                    short id)
        {
            var actualDocument = _documentService.GetDocument(id);

            actualDocument.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления документы в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddDocumentTest()
        {

            var documentId = _fakeDocuments.Documents.ToArray()[(new Random()).Next(DocumentsCount)].DOCUMENTID;
                        
            // Arrange
            var documentsFake = _fakeDocuments.Documents;

            var documentFake = documentsFake.Where(c => c.DOCUMENTID == documentId).FirstOrDefault();
            

            // Act

            var actualDocument = _documentService.GetDocument(documentId);

            // Assert

            actualDocument.Should().BeEquivalentTo(actualDocument,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода обновления данных о документ
        /// </summary>
        /// <param name="id">Идентификатор документы для обновления</param>
        [Test, Order(2)]
        public void UpdateDocumentTest()
        {
            // Arrange
            var documentId = _fakeDocuments.Documents.ToArray()[(new Random()).Next(DocumentsCount)].DOCUMENTID;

            var document = _documentService.GetDocument(documentId);
            document.NUMBER_ = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _documentService.UpdateDocument(document);
            var actualDocument = _fakeDocuments.Documents.Find(f => f.DOCUMENTID == documentId);

            // Assert
            actualDocument.NUMBER_.Should().Be(document.NUMBER_);
        }

        /// <summary>
        /// Тест метода удаления документы из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор документы для удаления</param>
        [Test, Order(3)]
        public void DeleteDocumentTest()
        {
            // Arrange

            var documentId = _fakeDocuments.Documents.ToArray()[(new Random()).Next(DocumentsCount)].DOCUMENTID;

            _documentService.DeleteDocument(documentId);

            // Act
            var actualDocument = _fakeDocuments.Documents.Find(document => document.DOCUMENTID == documentId);

            // Assert
            actualDocument.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия документы в хранилище данных.
        /// документ существует
        /// </summary>
        /// <param name="id">Идентификатор документы для поиска</param>
        [Test, Order(2)]
        public void DocumentExistsTest()
        {
            // Arrange

            var documentId = _fakeDocuments.Documents.ToArray()[(new Random()).Next(DocumentsCount)].DOCUMENTID;

            var loginToFind = _fakeDocuments.Documents.Find(dbf => dbf.DOCUMENTID == documentId).NUMBER_;

            // Act
            var actual = _documentService
                .DocumentExists(f => f.NUMBER_ == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия документы в хранилище данных.
        /// документ не существует
        /// </summary>
        /// <param name="login">Логин документы для поиска</param>
        [Test, Order(2)]
        public void DocumentNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _documentService
                .DocumentExists(f => f.NUMBER_ == VIN);

            actual.Should().BeFalse();
        }
    }
}

