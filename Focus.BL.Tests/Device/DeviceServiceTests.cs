﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Device
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="DeviceService"/>
    /// </summary>
    [TestFixture]
    public class DeviceServiceTests
    {
        private const int DevicesCount = 300;
        private IDeviceService _deviceService;
        private FakeDevice _fakeDevices;
        private Mock<IDbDeviceRepository> mockdeviceRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeDevices = new FakeDevice(DevicesCount);

            mockdeviceRepository = new Mock<IDbDeviceRepository>();
            mockdeviceRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<DEVICE, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<DEVICE, bool>> filter,
                    Func<IQueryable<DEVICE>, IOrderedQueryable<DEVICE>> orderBy,
                    string includeProperties) => _fakeDevices.Devices.Where(filter.Compile()));
            mockdeviceRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<DEVICE, bool>> filter,
                    Func<IQueryable<DEVICE>, IOrderedQueryable<DEVICE>> orderBy,
                    string includeProperties) => _fakeDevices.Devices);
            mockdeviceRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<DEVICE, bool>> filter,
                    Func<IQueryable<DEVICE>, IOrderedQueryable<DEVICE>> orderBy,
                    string includeProperties) => _fakeDevices.Devices.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockdeviceRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeDevices.Devices.Find(f => f.DEVICEID == id));
            mockdeviceRepository.Setup(mock => mock.Insert(It.IsAny<DEVICE>()))
                .Callback((DEVICE f) => _fakeDevices.Devices.Add(f));
            mockdeviceRepository.Setup(mock => mock.Update(It.IsAny<DEVICE>()))
                .Callback((DEVICE f) => _fakeDevices.Devices.Find(d => d.DEVICEID == f.DEVICEID).SERIALNUMBER = f.SERIALNUMBER);
            mockdeviceRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeDevices.Devices
                    .Remove(_fakeDevices.Devices.Find(f => f.DEVICEID == id)));

            var mockdeviceUnitOfWork = new Mock<IUnitOfWork>();
            mockdeviceUnitOfWork.Setup(mock => mock.DeviceRepository)
                .Returns(() => mockdeviceRepository.Object); //todo - закоментил, не знаю в чем дело

            _deviceService = new DeviceService(mockdeviceUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров ТСТК
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDevicesPageTest([Random(1, 14, 1)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            var devices = _fakeDevices.Devices;

            var devicesPageIndexes = new List<int>();

            var startDeviceIndexInPage = pagesize * (pageNumber - 1);

            var finishDeviceIndexInPage = startDeviceIndexInPage + pagesize - 1;

            for (var i = startDeviceIndexInPage; i <= finishDeviceIndexInPage; i++)
            {
                devicesPageIndexes.Add(i);
            }

            List<DEVICE> devicesPage;
            devicesPage = devicesPageIndexes.Select(devicesPageIndex => devices[devicesPageIndex]).ToList();


            // Act

            var actualDevicesPage = _deviceService.GetDevicesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            devicesPage.Should().BeEquivalentTo(actualDevicesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров ТСТК
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDevicesLastPageTest()
        {
            const int pagesize = 16;
            const int pageNumber = 19;

            // Arrange
            var dbDevices = _fakeDevices.Devices;

            var devicesPageIndexes = new List<int>();

            var startDeviceIndexInPage = pagesize * (pageNumber - 1);

            var finishDeviceIndexInPage = startDeviceIndexInPage + DevicesCount % pagesize - 1;

            for (var i = startDeviceIndexInPage; i <= finishDeviceIndexInPage; i++)
            {
                devicesPageIndexes.Add(i);
            }

            var dbDevicesPage = devicesPageIndexes.Select(devicesPageIndex => dbDevices[devicesPageIndex]).ToList();

            // Act
            var actualDevicesPage = _deviceService.GetDevicesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbDevicesPage.Should().BeEquivalentTo(actualDevicesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров ТСТК
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDevicesPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DEVICE> actualDevicesPage = _deviceService.GetDevicesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<DEVICE> expectedPage = _deviceService.GetDevicesPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualDevicesPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров ТСТК
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDevicesPageWithValueIfPageNumbersMoreThanTotalDevicesPagesTest([Random(DevicesCount, DevicesCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DEVICE> actualDevicesPage = _deviceService.GetDevicesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDevicesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров ТСТК
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedDevicesPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeDevices.Devices.Clear();

            // Act
            ICollection<DEVICE> actualDevicesPage = _deviceService.GetDevicesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDevicesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров ТСТК
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllDevicesFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeDevices.Devices.Clear();

            // Act
            var actualDevices = _deviceService.GetAllDevices(filter: null);

            // Assert
            actualDevices.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров ТСТК
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedDevicesTest()
        {
            // Arrange
            var expectedDevices = _fakeDevices.Devices;

            // Act
            var actualDevices = _deviceService.GetAllDevices().ToList();

            // Assert
            foreach (var expectedDevice in expectedDevices)
            {
                var i = expectedDevice.DEVICEID;

                expectedDevices[i].DEVICEID.Should().Be(actualDevices[i].DEVICEID);
                expectedDevices[i].SERIALNUMBER.Should().Be(actualDevices[i].SERIALNUMBER);
            }

            expectedDevices.Should().BeEquivalentTo(actualDevices,
                options => options.ExcludingMissingMembers());

            expectedDevices.Count.Should().Be(actualDevices.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра ТСТК по заданному идентификатору.
        /// ТСТК с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор ТСТК</param>
        [Test, Order(1)]
        public void GetExistedDeviceTest([Random(0, DevicesCount - 1, 5)] short id)
        {
            // Arrange
            var expectedDevice = _fakeDevices.Devices.Find(f => f.DEVICEID == id);

            // Act
            var actualDevice = _deviceService.GetDevice(id);

            // Assert


            actualDevice.Should().BeEquivalentTo(expectedDevice,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра ТСТК по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор ТСТК</param>
        [Test, Order(1)]
        public void GetNotExistedDeviceTest([Random(DevicesCount, DevicesCount + 300, 5)]
                    short id)
        {
            var actualDevice = _deviceService.GetDevice(id);

            actualDevice.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления ТСТК в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddDeviceTest()
        {

            short deviceId = 15;
            
            // Arrange
            var devicesFake = _fakeDevices.Devices;

            var deviceFake = devicesFake.Where(c => c.DEVICEID == deviceId).FirstOrDefault();
            

            // Act

            var actualDevice = _deviceService.GetDevice(deviceId);

            // Assert

            actualDevice.Should().BeEquivalentTo(actualDevice,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления ТСТК в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddDeviceIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var device = new FakeDevices(1, false, false, false, false, 5, 5).Devices[0];
            //var deviceManagementService = new DeviceManagementService(_unitOfWork);
            //device.Id = _fakeDevices.Devices.Last().Id + 1;

            //// Act
            //var actualId = _deviceService.AddDevice(device);

            //var actualDevice = _deviceService.GetDevice(actualId);

            //// Assert
            //actualDevice.DeviceDetail.EMailAddresses.Should().BeEquivalentTo(device.DeviceDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualDevice.Login.Should().BeEquivalentTo(device.Login);
            //actualDevice.FirstName.Should().BeEquivalentTo(device.FirstName);
            //actualDevice.LastName.Should().BeEquivalentTo(device.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об автомобиле
        /// </summary>
        /// <param name="id">Идентификатор ТСТК для обновления</param>
        [Test, Order(2)]
        public void UpdateDeviceTest([Random(0, DevicesCount - 1, 5)] short id)
        {
            // Arrange
            var device = _deviceService.GetDevice(id);
            device.SERIALNUMBER = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _deviceService.UpdateDevice(device);
            var actualDevice = _fakeDevices.Devices.Find(f => f.DEVICEID == id);

            // Assert
            actualDevice.SERIALNUMBER.Should().Be(device.SERIALNUMBER);
        }

        /// <summary>
        /// Тест метода удаления ТСТК из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор ТСТК для удаления</param>
        [Test, Order(3)]
        public void DeleteDeviceTest([Random(0, DevicesCount - 1, 5)] short id)
        {
            // Arrange
            _deviceService.DeleteDevice(id);

            // Act
            var actualDevice = _fakeDevices.Devices.Find(device => device.DEVICEID == id);

            // Assert
            actualDevice.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия ТСТК в хранилище данных.
        /// ТСТК существует
        /// </summary>
        /// <param name="id">Идентификатор ТСТК для поиска</param>
        [Test, Order(2)]
        public void DeviceExistsTest([Random(0, DevicesCount - 1, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeDevices.Devices.Find(dbf => dbf.DEVICEID == id).SERIALNUMBER;

            // Act
            var actual = _deviceService
                .DeviceExists(f => f.SERIALNUMBER == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия ТСТК в хранилище данных.
        /// ТСТК не существует
        /// </summary>
        /// <param name="login">Логин ТСТК для поиска</param>
        [Test, Order(2)]
        public void DeviceNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _deviceService
                .DeviceExists(f => f.SERIALNUMBER == VIN);

            actual.Should().BeFalse();
        }
    }
}

