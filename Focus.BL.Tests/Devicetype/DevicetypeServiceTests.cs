﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Devicetype
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="DevicetypeService"/>
    /// </summary>
    [TestFixture]
    public class DevicetypeServiceTests
    {
        private const int DevicetypesCount = 3;
        private IDevicetypeService _devicetypeService;
        private FakeDevicetype _fakeDevicetypes;
        private Mock<IDbDevicetypeRepository> mockdevicetypeRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeDevicetypes = new FakeDevicetype();

            mockdevicetypeRepository = new Mock<IDbDevicetypeRepository>();
            mockdevicetypeRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<DEVICETYPE, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<DEVICETYPE, bool>> filter,
                    Func<IQueryable<DEVICETYPE>, IOrderedQueryable<DEVICETYPE>> orderBy,
                    string includeProperties) => _fakeDevicetypes.Devicetypes.Where(filter.Compile()));
            mockdevicetypeRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<DEVICETYPE, bool>> filter,
                    Func<IQueryable<DEVICETYPE>, IOrderedQueryable<DEVICETYPE>> orderBy,
                    string includeProperties) => _fakeDevicetypes.Devicetypes);
            mockdevicetypeRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<DEVICETYPE, bool>> filter,
                    Func<IQueryable<DEVICETYPE>, IOrderedQueryable<DEVICETYPE>> orderBy,
                    string includeProperties) => _fakeDevicetypes.Devicetypes.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockdevicetypeRepository.Setup(mock => mock.GetById(It.IsAny<byte>()))
                .Returns((byte id) => _fakeDevicetypes.Devicetypes.Find(f => f.DEVICETYPEID == id));
            mockdevicetypeRepository.Setup(mock => mock.Insert(It.IsAny<DEVICETYPE>()))
                .Callback((DEVICETYPE f) => _fakeDevicetypes.Devicetypes.Add(f));
            mockdevicetypeRepository.Setup(mock => mock.Update(It.IsAny<DEVICETYPE>()))
                .Callback((DEVICETYPE f) => _fakeDevicetypes.Devicetypes.Find(d => d.DEVICETYPEID == f.DEVICETYPEID).NAME = f.NAME);
            mockdevicetypeRepository.Setup(mock => mock.Delete(It.IsAny<byte>()))
                .Callback((byte id) => _fakeDevicetypes.Devicetypes
                    .Remove(_fakeDevicetypes.Devicetypes.Find(f => f.DEVICETYPEID == id)));

            var mockdevicetypeUnitOfWork = new Mock<IUnitOfWork>();
            mockdevicetypeUnitOfWork.Setup(mock => mock.DevicetypeRepository)
                .Returns(() => mockdevicetypeRepository.Object); //todo - закоментил, не знаю в чем дело

            _devicetypeService = new DevicetypeService(mockdevicetypeUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDevicetypesPageTest([Random(1, 2, 1)]int pageNumber)
        {
            const int pagesize = 2;

            // Arrange
            var devicetypes = _fakeDevicetypes.Devicetypes;

            var devicetypesPageIndexes = new List<int>();

            var startDevicetypeIndexInPage = pagesize * (pageNumber - 1);

            var finishDevicetypeIndexInPage = startDevicetypeIndexInPage + pagesize - 1;

            for (var i = startDevicetypeIndexInPage; i <= finishDevicetypeIndexInPage; i++)
            {
                devicetypesPageIndexes.Add(i);
            }

            List<DEVICETYPE> devicetypesPage;
            devicetypesPage = devicetypesPageIndexes.Select(devicetypesPageIndex => devicetypes[devicetypesPageIndex]).ToList();


            // Act

            var actualDevicetypesPage = _devicetypeService.GetDevicetypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            devicetypesPage.Should().BeEquivalentTo(actualDevicetypesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedDevicetypesLastPageTest()
        {
            const int pagesize = 2;
            const int pageNumber = 2;

            // Arrange
            var dbDevicetypes = _fakeDevicetypes.Devicetypes;

            var devicetypesPageIndexes = new List<int>();

            var startDevicetypeIndexInPage = pagesize * (pageNumber - 1);

            var finishDevicetypeIndexInPage = startDevicetypeIndexInPage + DevicetypesCount % pagesize - 1;

            for (var i = startDevicetypeIndexInPage; i <= finishDevicetypeIndexInPage; i++)
            {
                devicetypesPageIndexes.Add(i);
            }

            var dbDevicetypesPage = devicetypesPageIndexes.Select(devicetypesPageIndex => dbDevicetypes[devicetypesPageIndex]).ToList();

            // Act
            var actualDevicetypesPage = _devicetypeService.GetDevicetypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbDevicetypesPage.Should().BeEquivalentTo(actualDevicetypesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDevicetypesPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DEVICETYPE> actualDevicetypesPage = _devicetypeService.GetDevicetypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<DEVICETYPE> expectedPage = _devicetypeService.GetDevicetypesPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualDevicetypesPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedDevicetypesPageWithValueIfPageNumbersMoreThanTotalDevicetypesPagesTest([Random(DevicetypesCount, DevicetypesCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<DEVICETYPE> actualDevicetypesPage = _devicetypeService.GetDevicetypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDevicetypesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedDevicetypesPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeDevicetypes.Devicetypes.Clear();

            // Act
            ICollection<DEVICETYPE> actualDevicetypesPage = _devicetypeService.GetDevicetypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualDevicetypesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров типов транспортных средств
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllDevicetypesFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeDevicetypes.Devicetypes.Clear();

            // Act
            var actualDevicetypes = _devicetypeService.GetAllDevicetypes(filter: null);

            // Assert
            actualDevicetypes.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров типов транспортных средств
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedDevicetypesTest()
        {
            // Arrange
            var expectedDevicetypes = _fakeDevicetypes.Devicetypes;

            // Act
            var actualDevicetypes = _devicetypeService.GetAllDevicetypes().ToList();

            // Assert
            
            expectedDevicetypes.Should().BeEquivalentTo(actualDevicetypes,
                options => options.ExcludingMissingMembers());

            expectedDevicetypes.Count.Should().Be(actualDevicetypes.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра типа транспортного средства по заданному идентификатору.
        /// тип транспортного средства с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства</param>
        [Test, Order(1)]
        public void GetExistedDevicetypeTest([Random(0, DevicetypesCount - 1, 5)] byte id)
        {
            // Arrange
            var expectedDevicetype = _fakeDevicetypes.Devicetypes.Find(f => f.DEVICETYPEID == id);

            // Act
            var actualDevicetype = _devicetypeService.GetDevicetype(id);

            // Assert


            actualDevicetype.Should().BeEquivalentTo(expectedDevicetype,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра типа транспортного средства по заданному идентификатору.
        /// тип транспортного средства с передаваемым идентификатором не существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства</param>
        [Test, Order(1)]
        public void GetNotExistedDevicetypeTest([Random(DevicetypesCount + 1, DevicetypesCount + 200, 5)]
                    byte id)
        {
            var actualDevicetype = _devicetypeService.GetDevicetype(id);

            actualDevicetype.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления типа транспортного средства в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddDevicetypeTest()
        {

            byte devicetypeId = 15;
            
            // Arrange
            var devicetypesFake = _fakeDevicetypes.Devicetypes;

            var devicetypeFake = devicetypesFake.Where(c => c.DEVICETYPEID == devicetypeId).FirstOrDefault();
            

            // Act

            var actualDevicetype = _devicetypeService.GetDevicetype(devicetypeId);

            // Assert

            actualDevicetype.Should().BeEquivalentTo(actualDevicetype,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления типа транспортного средства в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddDevicetypeIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var devicetype = new FakeDevicetype(1, false, false, false, false, 5, 5).Devicetypes[0];
            //var devicetypeManagementService = new DevicetypeManagementService(_unitOfWork);
            //devicetype.Id = _fakeDevicetypes.Devicetypes.Last().Id + 1;

            //// Act
            //var actualId = _devicetypeService.AddDevicetype(devicetype);

            //var actualDevicetype = _devicetypeService.GetDevicetype(actualId);

            //// Assert
            //actualDevicetype.DevicetypeDetail.EMailAddresses.Should().BeEquivalentTo(devicetype.DevicetypeDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualDevicetype.Login.Should().BeEquivalentTo(devicetype.Login);
            //actualDevicetype.FirstName.Should().BeEquivalentTo(devicetype.FirstName);
            //actualDevicetype.LastName.Should().BeEquivalentTo(devicetype.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об типе транспортного средства
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для обновления</param>
        [Test, Order(2)]
        public void UpdateDevicetypeTest([Random(1, DevicetypesCount, 5)] byte id)
        {
            // Arrange
            var devicetype = _devicetypeService.GetDevicetype(id);
            devicetype.NAME = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _devicetypeService.UpdateDevicetype(devicetype);
            var actualDevicetype = _fakeDevicetypes.Devicetypes.Find(f => f.DEVICETYPEID == id);

            // Assert
            actualDevicetype.NAME.Should().Be(devicetype.NAME);
        }

        /// <summary>
        /// Тест метода удаления типа транспортного средства из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для удаления</param>
        [Test, Order(3)]
        public void DeleteDevicetypeTest([Random(1, DevicetypesCount, 5)] byte id)
        {
            // Arrange
            _devicetypeService.DeleteDevicetype(id);

            // Act
            var actualDevicetype = _fakeDevicetypes.Devicetypes.Find(devicetype => devicetype.DEVICETYPEID == id);

            // Assert
            actualDevicetype.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия типа транспортного средства в хранилище данных.
        /// тип транспортного средства существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для поиска</param>
        [Test, Order(2)]
        public void DevicetypeExistsTest([Random(1, DevicetypesCount, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeDevicetypes.Devicetypes.Find(dbf => dbf.DEVICETYPEID == id).NAME;

            // Act
            var actual = _devicetypeService
                .DevicetypeExists(f => f.NAME == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия типа транспортного средства в хранилище данных.
        /// тип транспортного средства не существует
        /// </summary>
        /// <param name="login">Логин типа транспортного средства для поиска</param>
        [Test, Order(2)]
        public void DevicetypeNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _devicetypeService
                .DevicetypeExists(f => f.NAME == VIN);

            actual.Should().BeFalse();
        }
    }
}

