﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Focus.DAL.Common.DbModels;

namespace Focus.BL.Tests.Report
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="ReportService"/>
    /// </summary>
    [TestFixture]
    public class ReportServiceTests
    {
        private const int ReportsCount = 300;
        private IReportService _reportService;
        private FakeReport _fakeReports;
        private Mock<IDbReportRepository> mockreportRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeReports = new FakeReport(ReportsCount);

            mockreportRepository = new Mock<IDbReportRepository>();
            mockreportRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<REPORT, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<REPORT, bool>> filter,
                    Func<IQueryable<REPORT>, IOrderedQueryable<REPORT>> orderBy,
                    string includeProperties) => _fakeReports.Reports.Where(filter.Compile()));
            mockreportRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<REPORT, bool>> filter,
                    Func<IQueryable<REPORT>, IOrderedQueryable<REPORT>> orderBy,
                    string includeProperties) => _fakeReports.Reports);
            mockreportRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<REPORT, bool>> filter,
                    Func<IQueryable<REPORT>, IOrderedQueryable<REPORT>> orderBy,
                    string includeProperties) => _fakeReports.Reports.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockreportRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeReports.Reports.Find(f => f.REPORTID == id));
            mockreportRepository.Setup(mock => mock.Insert(It.IsAny<REPORT>()))
                .Callback((REPORT f) => _fakeReports.Reports.Add(f));
            mockreportRepository.Setup(mock => mock.Update(It.IsAny<REPORT>()))
                .Callback((REPORT f) => _fakeReports.Reports.Find(d => d.REPORTID == f.REPORTID).FULLNAME = f.FULLNAME);
            mockreportRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeReports.Reports
                    .Remove(_fakeReports.Reports.Find(f => f.REPORTID == id)));

            var mockreportUnitOfWork = new Mock<IUnitOfWork>();
            mockreportUnitOfWork.Setup(mock => mock.ReportRepository)
                .Returns(() => mockreportRepository.Object); //todo - закоментил, не знаю в чем дело

            _reportService = new ReportService(mockreportUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров протоколирования действий
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedReportsPageTest([Random(1, 14, 1)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            var reports = _fakeReports.Reports;

            var reportsPageIndexes = new List<int>();

            var startReportIndexInPage = pagesize * (pageNumber - 1);

            var finishReportIndexInPage = startReportIndexInPage + pagesize - 1;

            for (var i = startReportIndexInPage; i <= finishReportIndexInPage; i++)
            {
                reportsPageIndexes.Add(i);
            }

            List<REPORT> reportsPage;
            reportsPage = reportsPageIndexes.Select(reportsPageIndex => reports[reportsPageIndex]).ToList();


            // Act

            var actualReportsPage = _reportService.GetReportsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            reportsPage.Should().BeEquivalentTo(actualReportsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров протоколирования действий
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedReportsLastPageTest()
        {
            const int pagesize = 16;
            const int pageNumber = 19;

            // Arrange
            var dbReports = _fakeReports.Reports;

            var reportsPageIndexes = new List<int>();

            var startReportIndexInPage = pagesize * (pageNumber - 1);

            var finishReportIndexInPage = startReportIndexInPage + ReportsCount % pagesize - 1;

            for (var i = startReportIndexInPage; i <= finishReportIndexInPage; i++)
            {
                reportsPageIndexes.Add(i);
            }

            var dbReportsPage = reportsPageIndexes.Select(reportsPageIndex => dbReports[reportsPageIndex]).ToList();

            // Act
            var actualReportsPage = _reportService.GetReportsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbReportsPage.Should().BeEquivalentTo(actualReportsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров протоколирования действий
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedReportsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<REPORT> actualReportsPage = _reportService.GetReportsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<REPORT> expectedPage = _reportService.GetReportsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualReportsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров протоколирования действий
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedReportsPageWithValueIfPageNumbersMoreThanTotalReportsPagesTest([Random(ReportsCount, ReportsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<REPORT> actualReportsPage = _reportService.GetReportsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualReportsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров протоколирования действий
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedReportsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeReports.Reports.Clear();

            // Act
            ICollection<REPORT> actualReportsPage = _reportService.GetReportsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualReportsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров протоколирования действий
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllReportsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeReports.Reports.Clear();

            // Act
            var actualReports = _reportService.GetAllReports(filter: null);

            // Assert
            actualReports.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров протоколирования действий
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedReportsTest()
        {
            // Arrange
            var expectedReports = _fakeReports.Reports;

            // Act
            var actualReports = _reportService.GetAllReports().ToList();

            // Assert
            foreach (var expectedReport in expectedReports)
            {
                var i = expectedReport.REPORTID;

                expectedReports[i].NUMBER_.Should().Be(actualReports[i].NUMBER_);
                expectedReports[i].FULLNAME.Should().Be(actualReports[i].FULLNAME);
            }

            expectedReports.Should().BeEquivalentTo(actualReports,
                options => options.ExcludingMissingMembers());

            expectedReports.Count.Should().Be(actualReports.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра протоколирования действия по заданному идентификатору.
        /// протоколирование действия с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор протоколирования действия</param>
        [Test, Order(1)]
        public void GetExistedReportTest([Random(0, ReportsCount - 1, 5)] short id)
        {
            // Arrange
            var expectedReport = _fakeReports.Reports.Find(f => f.REPORTID == id);

            // Act
            var actualReport = _reportService.GetReport(id);

            // Assert


            actualReport.Should().BeEquivalentTo(expectedReport,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра протоколирования действия по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор протоколирования действия</param>
        [Test, Order(1)]
        public void GetNotExistedReportTest([Random(ReportsCount, ReportsCount + 300, 5)]
                    short id)
        {
            var actualReport = _reportService.GetReport(id);

            actualReport.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления протоколирования действия в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddReportTest()
        {

            short reportId = 15;
            
            // Arrange
            var reportsFake = _fakeReports.Reports;

            var reportFake = reportsFake.Where(c => c.REPORTID == reportId).FirstOrDefault();
            

            // Act

            var actualReport = _reportService.GetReport(reportId);

            // Assert

            actualReport.Should().BeEquivalentTo(actualReport,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления протоколирования действия в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddReportIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var report = new FakeReports(1, false, false, false, false, 5, 5).Reports[0];
            //var reportManagementService = new ReportManagementService(_unitOfWork);
            //report.Id = _fakeReports.Reports.Last().Id + 1;

            //// Act
            //var actualId = _reportService.AddReport(report);

            //var actualReport = _reportService.GetReport(actualId);

            //// Assert
            //actualReport.ReportDetail.EMailAddresses.Should().BeEquivalentTo(report.ReportDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualReport.Login.Should().BeEquivalentTo(report.Login);
            //actualReport.FirstName.Should().BeEquivalentTo(report.FirstName);
            //actualReport.LastName.Should().BeEquivalentTo(report.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об автомобиле
        /// </summary>
        /// <param name="id">Идентификатор протоколирования действия для обновления</param>
        [Test, Order(2)]
        public void UpdateReportTest([Random(0, ReportsCount - 1, 5)] short id)
        {
            // Arrange
            var report = _reportService.GetReport(id);
            report.FULLNAME = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _reportService.UpdateReport(report, "", "");
            var actualReport = _fakeReports.Reports.Find(f => f.REPORTID == id);

            // Assert
            actualReport.FULLNAME.Should().Be(report.FULLNAME);
        }

        /// <summary>
        /// Тест метода удаления протоколирования действия из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор протоколирования действия для удаления</param>
        [Test, Order(3)]
        public void DeleteReportTest([Random(0, ReportsCount - 1, 5)] short id)
        {
            // Arrange
            _reportService.DeleteReport(id, "", "");

            // Act
            var actualReport = _fakeReports.Reports.Find(report => report.REPORTID == id);

            // Assert
            actualReport.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия протоколирования действия в хранилище данных.
        /// протоколирование действия существует
        /// </summary>
        /// <param name="id">Идентификатор протоколирования действия для поиска</param>
        [Test, Order(2)]
        public void ReportExistsTest([Random(0, ReportsCount - 1, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeReports.Reports.Find(dbf => dbf.REPORTID == id).FULLNAME;

            // Act
            var actual = _reportService
                .ReportExists(f => f.FULLNAME == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия протоколирования действия в хранилище данных.
        /// протоколирование действия не существует
        /// </summary>
        /// <param name="login">Логин протоколирования действия для поиска</param>
        [Test, Order(2)]
        public void ReportNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _reportService
                .ReportExists(f => f.FULLNAME == VIN);

            actual.Should().BeFalse();
        }
    }
}

