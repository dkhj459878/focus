﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Container
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="ContainerService"/>
    /// </summary>
    [TestFixture]
    public class ContainerServiceTests
    {
        private const int ContainersCount = 300;
        private IContainerService _containerService;
        private FakeContainer _fakeContainers;
        private Mock<IDbContainerRepository> mockcontainerRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeContainers = new FakeContainer(ContainersCount);

            mockcontainerRepository = new Mock<IDbContainerRepository>();
            mockcontainerRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<CONTAINER, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<CONTAINER, bool>> filter,
                    Func<IQueryable<CONTAINER>, IOrderedQueryable<CONTAINER>> orderBy,
                    string includeProperties) => _fakeContainers.Containers.Where(filter.Compile()));
            mockcontainerRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<CONTAINER, bool>> filter,
                    Func<IQueryable<CONTAINER>, IOrderedQueryable<CONTAINER>> orderBy,
                    string includeProperties) => _fakeContainers.Containers);
            mockcontainerRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<CONTAINER, bool>> filter,
                    Func<IQueryable<CONTAINER>, IOrderedQueryable<CONTAINER>> orderBy,
                    string includeProperties) => _fakeContainers.Containers.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockcontainerRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeContainers.Containers.Find(f => f.CONTAINERID == id));
            mockcontainerRepository.Setup(mock => mock.Insert(It.IsAny<CONTAINER>()))
                .Callback((CONTAINER f) => _fakeContainers.Containers.Add(f));
            mockcontainerRepository.Setup(mock => mock.Update(It.IsAny<CONTAINER>()))
                .Callback((CONTAINER f) => _fakeContainers.Containers.Find(d => d.CONTAINERID == f.CONTAINERID).REGNUMBER = f.REGNUMBER);
            mockcontainerRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeContainers.Containers
                    .Remove(_fakeContainers.Containers.Find(f => f.CONTAINERID == id)));

            var mockcontainerUnitOfWork = new Mock<IUnitOfWork>();
            mockcontainerUnitOfWork.Setup(mock => mock.ContainerRepository)
                .Returns(() => mockcontainerRepository.Object); //todo - закоментил, не знаю в чем дело

            _containerService = new ContainerService(mockcontainerUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров контейнеров
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedContainersPageTest([Random(1, 14, 1)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            var containers = _fakeContainers.Containers;

            var containersPageIndexes = new List<int>();

            var startContainerIndexInPage = pagesize * (pageNumber - 1);

            var finishContainerIndexInPage = startContainerIndexInPage + pagesize - 1;

            for (var i = startContainerIndexInPage; i <= finishContainerIndexInPage; i++)
            {
                containersPageIndexes.Add(i);
            }

            List<CONTAINER> containersPage;
            containersPage = containersPageIndexes.Select(containersPageIndex => containers[containersPageIndex]).ToList();


            // Act

            var actualContainersPage = _containerService.GetContainersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            containersPage.Should().BeEquivalentTo(actualContainersPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров контейнеров
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedContainersLastPageTest()
        {
            const int pagesize = 16;
            const int pageNumber = 19;

            // Arrange
            var dbContainers = _fakeContainers.Containers;

            var containersPageIndexes = new List<int>();

            var startContainerIndexInPage = pagesize * (pageNumber - 1);

            var finishContainerIndexInPage = startContainerIndexInPage + ContainersCount % pagesize - 1;

            for (var i = startContainerIndexInPage; i <= finishContainerIndexInPage; i++)
            {
                containersPageIndexes.Add(i);
            }

            var dbContainersPage = containersPageIndexes.Select(containersPageIndex => dbContainers[containersPageIndex]).ToList();

            // Act
            var actualContainersPage = _containerService.GetContainersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbContainersPage.Should().BeEquivalentTo(actualContainersPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров контейнеров
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedContainersPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<CONTAINER> actualContainersPage = _containerService.GetContainersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<CONTAINER> expectedPage = _containerService.GetContainersPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualContainersPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров контейнеров
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedContainersPageWithValueIfPageNumbersMoreThanTotalContainersPagesTest([Random(ContainersCount, ContainersCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<CONTAINER> actualContainersPage = _containerService.GetContainersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualContainersPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров контейнеров
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedContainersPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeContainers.Containers.Clear();

            // Act
            ICollection<CONTAINER> actualContainersPage = _containerService.GetContainersPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualContainersPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров контейнеров
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllContainersFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeContainers.Containers.Clear();

            // Act
            var actualContainers = _containerService.GetAllContainers(filter: null);

            // Assert
            actualContainers.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров контейнеров
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedContainersTest()
        {
            // Arrange
            var expectedContainers = _fakeContainers.Containers;

            // Act
            var actualContainers = _containerService.GetAllContainers().ToList();

            // Assert
            foreach (var expectedContainer in expectedContainers)
            {
                var i = expectedContainer.CONTAINERID;

                expectedContainers[i].COUNTRYCODE.Should().Be(actualContainers[i].COUNTRYCODE);
                expectedContainers[i].REGNUMBER.Should().Be(actualContainers[i].REGNUMBER);
            }

            expectedContainers.Should().BeEquivalentTo(actualContainers,
                options => options.ExcludingMissingMembers());

            expectedContainers.Count.Should().Be(actualContainers.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра контейнера по заданному идентификатору.
        /// контейнер с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор контейнера</param>
        [Test, Order(1)]
        public void GetExistedContainerTest([Random(0, ContainersCount - 1, 5)] short id)
        {
            // Arrange
            var expectedContainer = _fakeContainers.Containers.Find(f => f.CONTAINERID == id);

            // Act
            var actualContainer = _containerService.GetContainer(id);

            // Assert


            actualContainer.Should().BeEquivalentTo(expectedContainer,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра контейнера по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор контейнера</param>
        [Test, Order(1)]
        public void GetNotExistedContainerTest([Random(ContainersCount, ContainersCount + 300, 5)]
                    short id)
        {
            var actualContainer = _containerService.GetContainer(id);

            actualContainer.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления контейнера в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddContainerTest()
        {

            short containerId = 15;
            
            // Arrange
            var containersFake = _fakeContainers.Containers;

            var containerFake = containersFake.Where(c => c.CONTAINERID == containerId).FirstOrDefault();
            

            // Act

            var actualContainer = _containerService.GetContainer(containerId);

            // Assert

            actualContainer.Should().BeEquivalentTo(actualContainer,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления контейнера в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddContainerIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var container = new FakeContainers(1, false, false, false, false, 5, 5).Containers[0];
            //var containerManagementService = new ContainerManagementService(_unitOfWork);
            //container.Id = _fakeContainers.Containers.Last().Id + 1;

            //// Act
            //var actualId = _containerService.AddContainer(container);

            //var actualContainer = _containerService.GetContainer(actualId);

            //// Assert
            //actualContainer.ContainerDetail.EMailAddresses.Should().BeEquivalentTo(container.ContainerDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualContainer.Login.Should().BeEquivalentTo(container.Login);
            //actualContainer.FirstName.Should().BeEquivalentTo(container.FirstName);
            //actualContainer.LastName.Should().BeEquivalentTo(container.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об автомобиле
        /// </summary>
        /// <param name="id">Идентификатор контейнера для обновления</param>
        [Test, Order(2)]
        public void UpdateContainerTest([Random(0, ContainersCount - 1, 5)] short id)
        {
            // Arrange
            var container = _containerService.GetContainer(id);
            container.REGNUMBER = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _containerService.UpdateContainer(container);
            var actualContainer = _fakeContainers.Containers.Find(f => f.CONTAINERID == id);

            // Assert
            actualContainer.REGNUMBER.Should().Be(container.REGNUMBER);
        }

        /// <summary>
        /// Тест метода удаления контейнера из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор контейнера для удаления</param>
        [Test, Order(3)]
        public void DeleteContainerTest([Random(0, ContainersCount - 1, 5)] short id)
        {
            // Arrange
            _containerService.DeleteContainer(id);

            // Act
            var actualContainer = _fakeContainers.Containers.Find(container => container.CONTAINERID == id);

            // Assert
            actualContainer.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия контейнера в хранилище данных.
        /// контейнер существует
        /// </summary>
        /// <param name="id">Идентификатор контейнера для поиска</param>
        [Test, Order(2)]
        public void ContainerExistsTest([Random(0, ContainersCount - 1, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeContainers.Containers.Find(dbf => dbf.CONTAINERID == id).REGNUMBER;

            // Act
            var actual = _containerService
                .ContainerExists(f => f.REGNUMBER == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия контейнера в хранилище данных.
        /// контейнер не существует
        /// </summary>
        /// <param name="login">Логин контейнера для поиска</param>
        [Test, Order(2)]
        public void ContainerNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _containerService
                .ContainerExists(f => f.REGNUMBER == VIN);

            actual.Should().BeFalse();
        }
    }
}

