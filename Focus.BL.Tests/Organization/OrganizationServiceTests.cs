﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Organization
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="OrganizationService"/>
    /// </summary>
    [TestFixture]
    public class OrganizationServiceTests
    {
        private const int OrganizationsCount = 300;
        private IOrganizationService _organizationService;
        private FakeOrganization _fakeOrganizations;
        private Mock<IDbOrganizationRepository> mockorganizationRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeOrganizations = new FakeOrganization(OrganizationsCount);

            mockorganizationRepository = new Mock<IDbOrganizationRepository>();
            mockorganizationRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<ORGANIZATION, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<ORGANIZATION, bool>> filter,
                    Func<IQueryable<ORGANIZATION>, IOrderedQueryable<ORGANIZATION>> orderBy,
                    string includeProperties) => _fakeOrganizations.Organizations.Where(filter.Compile()));
            mockorganizationRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<ORGANIZATION, bool>> filter,
                    Func<IQueryable<ORGANIZATION>, IOrderedQueryable<ORGANIZATION>> orderBy,
                    string includeProperties) => _fakeOrganizations.Organizations);
            mockorganizationRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<ORGANIZATION, bool>> filter,
                    Func<IQueryable<ORGANIZATION>, IOrderedQueryable<ORGANIZATION>> orderBy,
                    string includeProperties) => _fakeOrganizations.Organizations.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockorganizationRepository.Setup(mock => mock.GetById(It.IsAny<short>()))
                .Returns((short id) => _fakeOrganizations.Organizations.Find(f => f.ORGANIZATIONID == id));
            mockorganizationRepository.Setup(mock => mock.Insert(It.IsAny<ORGANIZATION>()))
                .Callback((ORGANIZATION f) => _fakeOrganizations.Organizations.Add(f));
            mockorganizationRepository.Setup(mock => mock.Update(It.IsAny<ORGANIZATION>()))
                .Callback((ORGANIZATION f) => _fakeOrganizations.Organizations.Find(d => d.ORGANIZATIONID == f.ORGANIZATIONID).UNP = f.UNP);
            mockorganizationRepository.Setup(mock => mock.Delete(It.IsAny<short>()))
                .Callback((short id) => _fakeOrganizations.Organizations
                    .Remove(_fakeOrganizations.Organizations.Find(f => f.ORGANIZATIONID == id)));

            var mockorganizationUnitOfWork = new Mock<IUnitOfWork>();
            mockorganizationUnitOfWork.Setup(mock => mock.OrganizationRepository)
                .Returns(() => mockorganizationRepository.Object); //todo - закоментил, не знаю в чем дело

            _organizationService = new OrganizationService(mockorganizationUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров организаций
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedOrganizationsPageTest([Random(1, 14, 1)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            var organizations = _fakeOrganizations.Organizations;

            var organizationsPageIndexes = new List<int>();

            var startOrganizationIndexInPage = pagesize * (pageNumber - 1);

            var finishOrganizationIndexInPage = startOrganizationIndexInPage + pagesize - 1;

            for (var i = startOrganizationIndexInPage; i <= finishOrganizationIndexInPage; i++)
            {
                organizationsPageIndexes.Add(i);
            }

            List<ORGANIZATION> organizationsPage;
            organizationsPage = organizationsPageIndexes.Select(organizationsPageIndex => organizations[organizationsPageIndex]).ToList();


            // Act

            var actualOrganizationsPage = _organizationService.GetOrganizationsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            organizationsPage.Should().BeEquivalentTo(actualOrganizationsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров организаций
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedOrganizationsLastPageTest()
        {
            const int pagesize = 16;
            const int pageNumber = 19;

            // Arrange
            var dbOrganizations = _fakeOrganizations.Organizations;

            var organizationsPageIndexes = new List<int>();

            var startOrganizationIndexInPage = pagesize * (pageNumber - 1);

            var finishOrganizationIndexInPage = startOrganizationIndexInPage + OrganizationsCount % pagesize - 1;

            for (var i = startOrganizationIndexInPage; i <= finishOrganizationIndexInPage; i++)
            {
                organizationsPageIndexes.Add(i);
            }

            var dbOrganizationsPage = organizationsPageIndexes.Select(organizationsPageIndex => dbOrganizations[organizationsPageIndex]).ToList();

            // Act
            var actualOrganizationsPage = _organizationService.GetOrganizationsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbOrganizationsPage.Should().BeEquivalentTo(actualOrganizationsPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров организаций
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedOrganizationsPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<ORGANIZATION> actualOrganizationsPage = _organizationService.GetOrganizationsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<ORGANIZATION> expectedPage = _organizationService.GetOrganizationsPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualOrganizationsPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров организаций
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedOrganizationsPageWithValueIfPageNumbersMoreThanTotalOrganizationsPagesTest([Random(OrganizationsCount, OrganizationsCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<ORGANIZATION> actualOrganizationsPage = _organizationService.GetOrganizationsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualOrganizationsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров организаций
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedOrganizationsPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeOrganizations.Organizations.Clear();

            // Act
            ICollection<ORGANIZATION> actualOrganizationsPage = _organizationService.GetOrganizationsPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualOrganizationsPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров организаций
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllOrganizationsFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeOrganizations.Organizations.Clear();

            // Act
            var actualOrganizations = _organizationService.GetAllOrganizations(filter: null);

            // Assert
            actualOrganizations.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров организаций
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedOrganizationsTest()
        {
            // Arrange
            var expectedOrganizations = _fakeOrganizations.Organizations;

            // Act
            var actualOrganizations = _organizationService.GetAllOrganizations().ToList();

            // Assert
            foreach (var expectedOrganization in expectedOrganizations)
            {
                var i = expectedOrganization.ORGANIZATIONID;

                expectedOrganizations[i].PHONE.Should().Be(actualOrganizations[i].PHONE);
                expectedOrganizations[i].UNP.Should().Be(actualOrganizations[i].UNP);
            }

            expectedOrganizations.Should().BeEquivalentTo(actualOrganizations,
                options => options.ExcludingMissingMembers());

            expectedOrganizations.Count.Should().Be(actualOrganizations.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра организации по заданному идентификатору.
        /// организация с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор организации</param>
        [Test, Order(1)]
        public void GetExistedOrganizationTest([Random(0, OrganizationsCount - 1, 5)] short id)
        {
            // Arrange
            var expectedOrganization = _fakeOrganizations.Organizations.Find(f => f.ORGANIZATIONID == id);

            // Act
            var actualOrganization = _organizationService.GetOrganization(id);

            // Assert


            actualOrganization.Should().BeEquivalentTo(expectedOrganization,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра организации по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор организации</param>
        [Test, Order(1)]
        public void GetNotExistedOrganizationTest([Random(OrganizationsCount, OrganizationsCount + 300, 5)]
                    short id)
        {
            var actualOrganization = _organizationService.GetOrganization(id);

            actualOrganization.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления организации в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddOrganizationTest()
        {

            var organizationId = (short)15;
           
            // Arrange
            var organizationsFake = _fakeOrganizations.Organizations;

            var organizationFake = organizationsFake.Where(c => c.ORGANIZATIONID == organizationId).FirstOrDefault();
            

            // Act

            var actualOrganization = _organizationService.GetOrganization(organizationId);

            // Assert

            actualOrganization.Should().BeEquivalentTo(actualOrganization,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления организации в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddOrganizationIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var organization = new FakeOrganizations(1, false, false, false, false, 5, 5).Organizations[0];
            //var organizationManagementService = new OrganizationManagementService(_unitOfWork);
            //organization.Id = _fakeOrganizations.Organizations.Last().Id + 1;

            //// Act
            //var actualId = _organizationService.AddOrganization(organization);

            //var actualOrganization = _organizationService.GetOrganization(actualId);

            //// Assert
            //actualOrganization.OrganizationDetail.EMailAddresses.Should().BeEquivalentTo(organization.OrganizationDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualOrganization.Login.Should().BeEquivalentTo(organization.Login);
            //actualOrganization.FirstName.Should().BeEquivalentTo(organization.FirstName);
            //actualOrganization.LastName.Should().BeEquivalentTo(organization.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об автомобиле
        /// </summary>
        /// <param name="id">Идентификатор организации для обновления</param>
        [Test, Order(2)]
        public void UpdateOrganizationTest([Random(0, OrganizationsCount - 1, 5)] short id)
        {
            // Arrange
            var organization = _organizationService.GetOrganization(id);
            organization.UNP = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _organizationService.UpdateOrganization(organization);
            var actualOrganization = _fakeOrganizations.Organizations.Find(f => f.ORGANIZATIONID == id);

            // Assert
            actualOrganization.UNP.Should().Be(organization.UNP);
        }

        /// <summary>
        /// Тест метода удаления организации из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор организации для удаления</param>
        [Test, Order(3)]
        public void DeleteOrganizationTest([Random(0, OrganizationsCount - 1, 5)] short id)
        {
            // Arrange
            _organizationService.DeleteOrganization(id);

            // Act
            var actualOrganization = _fakeOrganizations.Organizations.Find(organization => organization.ORGANIZATIONID == id);

            // Assert
            actualOrganization.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия организации в хранилище данных.
        /// организация существует
        /// </summary>
        /// <param name="id">Идентификатор организации для поиска</param>
        [Test, Order(2)]
        public void OrganizationExistsTest([Random(0, OrganizationsCount - 1, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeOrganizations.Organizations.Find(dbf => dbf.ORGANIZATIONID == id).UNP;

            // Act
            var actual = _organizationService
                .OrganizationExists(f => f.UNP == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия организации в хранилище данных.
        /// организация не существует
        /// </summary>
        /// <param name="login">Логин организации для поиска</param>
        [Test, Order(2)]
        public void OrganizationNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _organizationService
                .OrganizationExists(f => f.UNP == VIN);

            actual.Should().BeFalse();
        }
    }
}

