﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Vehicletype
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="VehicletypeService"/>
    /// </summary>
    [TestFixture]
    public class VehicletypeServiceTests
    {
        private const int VehicletypesCount = 2;
        private IVehicletypeService _vehicletypeService;
        private FakeVehicletype _fakeVehicletypes;
        private Mock<IDbVehicletypeRepository> mockvehicletypeRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeVehicletypes = new FakeVehicletype();

            mockvehicletypeRepository = new Mock<IDbVehicletypeRepository>();
            mockvehicletypeRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<VEHICLETYPE, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<VEHICLETYPE, bool>> filter,
                    Func<IQueryable<VEHICLETYPE>, IOrderedQueryable<VEHICLETYPE>> orderBy,
                    string includeProperties) => _fakeVehicletypes.Vehicletypes.Where(filter.Compile()));
            mockvehicletypeRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<VEHICLETYPE, bool>> filter,
                    Func<IQueryable<VEHICLETYPE>, IOrderedQueryable<VEHICLETYPE>> orderBy,
                    string includeProperties) => _fakeVehicletypes.Vehicletypes);
            mockvehicletypeRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<VEHICLETYPE, bool>> filter,
                    Func<IQueryable<VEHICLETYPE>, IOrderedQueryable<VEHICLETYPE>> orderBy,
                    string includeProperties) => _fakeVehicletypes.Vehicletypes.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockvehicletypeRepository.Setup(mock => mock.GetById(It.IsAny<byte>()))
                .Returns((byte id) => _fakeVehicletypes.Vehicletypes.Find(f => f.VEHICLETYPEID == id));
            mockvehicletypeRepository.Setup(mock => mock.Insert(It.IsAny<VEHICLETYPE>()))
                .Callback((VEHICLETYPE f) => _fakeVehicletypes.Vehicletypes.Add(f));
            mockvehicletypeRepository.Setup(mock => mock.Update(It.IsAny<VEHICLETYPE>()))
                .Callback((VEHICLETYPE f) => _fakeVehicletypes.Vehicletypes.Find(d => d.VEHICLETYPEID == f.VEHICLETYPEID).NAME = f.NAME);
            mockvehicletypeRepository.Setup(mock => mock.Delete(It.IsAny<byte>()))
                .Callback((byte id) => _fakeVehicletypes.Vehicletypes
                    .Remove(_fakeVehicletypes.Vehicletypes.Find(f => f.VEHICLETYPEID == id)));

            var mockvehicletypeUnitOfWork = new Mock<IUnitOfWork>();
            mockvehicletypeUnitOfWork.Setup(mock => mock.VehicletypeRepository)
                .Returns(() => mockvehicletypeRepository.Object); //todo - закоментил, не знаю в чем дело

            _vehicletypeService = new VehicletypeService(mockvehicletypeUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedVehicletypesPageTest([Random(1, 2, 1)]int pageNumber)
        {
            const int pagesize = 2;

            // Arrange
            var vehicletypes = _fakeVehicletypes.Vehicletypes;

            var vehicletypesPageIndexes = new List<int>();

            var startVehicletypeIndexInPage = pagesize * (pageNumber - 1);

            var finishVehicletypeIndexInPage = startVehicletypeIndexInPage + pagesize - 1;

            for (var i = startVehicletypeIndexInPage; i <= finishVehicletypeIndexInPage; i++)
            {
                vehicletypesPageIndexes.Add(i);
            }

            List<VEHICLETYPE> vehicletypesPage;
            vehicletypesPage = vehicletypesPageIndexes.Select(vehicletypesPageIndex => vehicletypes[vehicletypesPageIndex]).ToList();


            // Act

            var actualVehicletypesPage = _vehicletypeService.GetVehicletypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            vehicletypesPage.Should().BeEquivalentTo(actualVehicletypesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedVehicletypesLastPageTest()
        {
            const int pagesize = 2;
            const int pageNumber = 2;

            // Arrange
            var dbVehicletypes = _fakeVehicletypes.Vehicletypes;

            var vehicletypesPageIndexes = new List<int>();

            var startVehicletypeIndexInPage = pagesize * (pageNumber - 1);

            var finishVehicletypeIndexInPage = startVehicletypeIndexInPage + VehicletypesCount % pagesize - 1;

            for (var i = startVehicletypeIndexInPage; i <= finishVehicletypeIndexInPage; i++)
            {
                vehicletypesPageIndexes.Add(i);
            }

            var dbVehicletypesPage = vehicletypesPageIndexes.Select(vehicletypesPageIndex => dbVehicletypes[vehicletypesPageIndex]).ToList();

            // Act
            var actualVehicletypesPage = _vehicletypeService.GetVehicletypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbVehicletypesPage.Should().BeEquivalentTo(actualVehicletypesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedVehicletypesPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<VEHICLETYPE> actualVehicletypesPage = _vehicletypeService.GetVehicletypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<VEHICLETYPE> expectedPage = _vehicletypeService.GetVehicletypesPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualVehicletypesPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedVehicletypesPageWithValueIfPageNumbersMoreThanTotalVehicletypesPagesTest([Random(VehicletypesCount, VehicletypesCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<VEHICLETYPE> actualVehicletypesPage = _vehicletypeService.GetVehicletypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualVehicletypesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров типов транспортных средств
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedVehicletypesPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeVehicletypes.Vehicletypes.Clear();

            // Act
            ICollection<VEHICLETYPE> actualVehicletypesPage = _vehicletypeService.GetVehicletypesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualVehicletypesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров типов транспортных средств
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllVehicletypesFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeVehicletypes.Vehicletypes.Clear();

            // Act
            var actualVehicletypes = _vehicletypeService.GetAllVehicletypes(filter: null);

            // Assert
            actualVehicletypes.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров типов транспортных средств
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedVehicletypesTest()
        {
            // Arrange
            var expectedVehicletypes = _fakeVehicletypes.Vehicletypes;

            // Act
            var actualVehicletypes = _vehicletypeService.GetAllVehicletypes().ToList();

            // Assert
            
            expectedVehicletypes.Should().BeEquivalentTo(actualVehicletypes,
                options => options.ExcludingMissingMembers());

            expectedVehicletypes.Count.Should().Be(actualVehicletypes.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра типа транспортного средства по заданному идентификатору.
        /// тип транспортного средства с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства</param>
        [Test, Order(1)]
        public void GetExistedVehicletypeTest([Random(0, VehicletypesCount - 1, 5)] byte id)
        {
            // Arrange
            var expectedVehicletype = _fakeVehicletypes.Vehicletypes.Find(f => f.VEHICLETYPEID == id);

            // Act
            var actualVehicletype = _vehicletypeService.GetVehicletype(id);

            // Assert


            actualVehicletype.Should().BeEquivalentTo(expectedVehicletype,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра типа транспортного средства по заданному идентификатору.
        /// тип транспортного средства с передаваемым идентификатором не существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства</param>
        [Test, Order(1)]
        public void GetNotExistedVehicletypeTest([Random(VehicletypesCount + 1, VehicletypesCount + 300, 5)]
                    byte id)
        {
            var actualVehicletype = _vehicletypeService.GetVehicletype(id);

            actualVehicletype.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления типа транспортного средства в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddVehicletypeTest()
        {

            byte vehicletypeId = 15;
            
            // Arrange
            var vehicletypesFake = _fakeVehicletypes.Vehicletypes;

            var vehicletypeFake = vehicletypesFake.Where(c => c.VEHICLETYPEID == vehicletypeId).FirstOrDefault();
            

            // Act

            var actualVehicletype = _vehicletypeService.GetVehicletype(vehicletypeId);

            // Assert

            actualVehicletype.Should().BeEquivalentTo(actualVehicletype,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления типа транспортного средства в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddVehicletypeIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var vehicletype = new FakeVehicletype(1, false, false, false, false, 5, 5).Vehicletypes[0];
            //var vehicletypeManagementService = new VehicletypeManagementService(_unitOfWork);
            //vehicletype.Id = _fakeVehicletypes.Vehicletypes.Last().Id + 1;

            //// Act
            //var actualId = _vehicletypeService.AddVehicletype(vehicletype);

            //var actualVehicletype = _vehicletypeService.GetVehicletype(actualId);

            //// Assert
            //actualVehicletype.VehicletypeDetail.EMailAddresses.Should().BeEquivalentTo(vehicletype.VehicletypeDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualVehicletype.Login.Should().BeEquivalentTo(vehicletype.Login);
            //actualVehicletype.FirstName.Should().BeEquivalentTo(vehicletype.FirstName);
            //actualVehicletype.LastName.Should().BeEquivalentTo(vehicletype.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных об типе транспортного средства
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для обновления</param>
        [Test, Order(2)]
        public void UpdateVehicletypeTest([Random(1, VehicletypesCount, 5)] byte id)
        {
            // Arrange
            var vehicletype = _vehicletypeService.GetVehicletype(id);
            vehicletype.NAME = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _vehicletypeService.UpdateVehicletype(vehicletype);
            var actualVehicletype = _fakeVehicletypes.Vehicletypes.Find(f => f.VEHICLETYPEID == id);

            // Assert
            actualVehicletype.NAME.Should().Be(vehicletype.NAME);
        }

        /// <summary>
        /// Тест метода удаления типа транспортного средства из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для удаления</param>
        [Test, Order(3)]
        public void DeleteVehicletypeTest([Random(1, VehicletypesCount, 5)] byte id)
        {
            // Arrange
            _vehicletypeService.DeleteVehicletype(id);

            // Act
            var actualVehicletype = _fakeVehicletypes.Vehicletypes.Find(vehicletype => vehicletype.VEHICLETYPEID == id);

            // Assert
            actualVehicletype.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия типа транспортного средства в хранилище данных.
        /// тип транспортного средства существует
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства для поиска</param>
        [Test, Order(2)]
        public void VehicletypeExistsTest([Random(1, VehicletypesCount, 5)] short id)
        {
            // Arrange
            var loginToFind = _fakeVehicletypes.Vehicletypes.Find(dbf => dbf.VEHICLETYPEID == id).NAME;

            // Act
            var actual = _vehicletypeService
                .VehicletypeExists(f => f.NAME == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия типа транспортного средства в хранилище данных.
        /// тип транспортного средства не существует
        /// </summary>
        /// <param name="login">Логин типа транспортного средства для поиска</param>
        [Test, Order(2)]
        public void VehicletypeNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _vehicletypeService
                .VehicletypeExists(f => f.NAME == VIN);

            actual.Should().BeFalse();
        }
    }
}

