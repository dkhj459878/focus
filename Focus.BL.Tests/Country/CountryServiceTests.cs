﻿using FluentAssertions;
using Focus.BL.Common.Services;
using Focus.BL.Services;
using Focus.BL.Tests.FakeData;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Tests.Country
{
    /// <summary>
    /// Unit-тесты для сервиса <see cref="CountryService"/>
    /// </summary>
    [TestFixture]
    public class CountryServiceTests
    {
        private const int CountriesCount = 20;
        private ICountryService _countryService;
        private FakeCountry _fakeCountries;
        private Mock<IDbCountryRepository> mockcountryRepository;

        /// <summary>
        /// Осуществляет настройки
        /// </summary>
        [OneTimeSetUp]
        public void SetUp()
        {


            _fakeCountries = new FakeCountry(CountriesCount);

            mockcountryRepository = new Mock<IDbCountryRepository>();
            mockcountryRepository
                .Setup(mock => mock.Get(It.IsAny<Expression<Func<COUNTRY, bool>>>(), null, It.IsAny<string>()))
                .Returns((Expression<Func<COUNTRY, bool>> filter,
                    Func<IQueryable<COUNTRY>, IOrderedQueryable<COUNTRY>> orderBy,
                    string includeProperties) => _fakeCountries.Countries.Where(filter.Compile()));
            mockcountryRepository
                .Setup(mock => mock.Get(null, null, It.IsAny<string>()))
                .Returns((Expression<Func<COUNTRY, bool>> filter,
                    Func<IQueryable<COUNTRY>, IOrderedQueryable<COUNTRY>> orderBy,
                    string includeProperties) => _fakeCountries.Countries);
            mockcountryRepository
                .Setup(mock => mock.GetPage(It.IsAny<int>(), It.IsAny<int>(), null, null, ""))
                .Returns((int pageSize,
                    int pageNumber,
                    Expression<Func<COUNTRY, bool>> filter,
                    Func<IQueryable<COUNTRY>, IOrderedQueryable<COUNTRY>> orderBy,
                    string includeProperties) => _fakeCountries.Countries.Skip((pageNumber - 1) * pageSize).Take(pageSize));
            mockcountryRepository.Setup(mock => mock.GetById(It.IsAny<string>()))
                .Returns((string id) => _fakeCountries.Countries.Find(f => f.ABBR2 == id));
            mockcountryRepository.Setup(mock => mock.Insert(It.IsAny<COUNTRY>()))
                .Callback((COUNTRY f) => _fakeCountries.Countries.Add(f));
            mockcountryRepository.Setup(mock => mock.Update(It.IsAny<COUNTRY>()))
                .Callback((COUNTRY f) => _fakeCountries.Countries.Find(d => d.ABBR2 == f.ABBR2).NAME = f.NAME);
            mockcountryRepository.Setup(mock => mock.Delete(It.IsAny<string>()))
                .Callback((string id) => _fakeCountries.Countries
                    .Remove(_fakeCountries.Countries.Find(f => f.ABBR2 == id)));

            var mockcountryUnitOfWork = new Mock<IUnitOfWork>();
            mockcountryUnitOfWork.Setup(mock => mock.CountryRepository)
                .Returns(() => mockcountryRepository.Object); //todo - закоментил, не знаю в чем дело

            _countryService = new CountryService(mockcountryUnitOfWork.Object);
        }

        /// <summary>
        /// Тест метода получения экземпляров стран
        /// выбранной страницы пейджинга. Выбрана может быть любая страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedCountriesPageTest([Random(1, 5, 1)]int pageNumber)
        {
            const int pagesize = 3;

            // Arrange
            var countrys = _fakeCountries.Countries;

            var countrysPageIndexes = new List<int>();

            var startCountryIndexInPage = pagesize * (pageNumber - 1);

            var finishCountryIndexInPage = startCountryIndexInPage + pagesize - 1;

            for (var i = startCountryIndexInPage; i <= finishCountryIndexInPage; i++)
            {
                countrysPageIndexes.Add(i);
            }

            List<COUNTRY> countrysPage;
            countrysPage = countrysPageIndexes.Select(countrysPageIndex => countrys[countrysPageIndex]).ToList();


            // Act

            var actualCountriesPage = _countryService.GetCountriesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            countrysPage.Should().BeEquivalentTo(actualCountriesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров стран
        /// выбранной страницы пейджинга. Выбрана последняя страница.
        /// </summary>
        [Test, Order(1)]
        public void GetExistedCountriesLastPageTest()
        {
            const int pagesize = 19;
            const int pageNumber = 2;

            // Arrange
            var dbCountries = _fakeCountries.Countries;

            var countrysPageIndexes = new List<int>();

            var startCountryIndexInPage = pagesize * (pageNumber - 1);

            var finishCountryIndexInPage = startCountryIndexInPage + CountriesCount % pagesize - 1;

            for (var i = startCountryIndexInPage; i <= finishCountryIndexInPage; i++)
            {
                countrysPageIndexes.Add(i);
            }

            var dbCountriesPage = countrysPageIndexes.Select(countrysPageIndex => dbCountries[countrysPageIndex]).ToList();

            // Act
            var actualCountriesPage = _countryService.GetCountriesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            dbCountriesPage.Should().BeEquivalentTo(actualCountriesPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров стран
        /// выбранной страницы пейджинга. Номер страницы отрицательный.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedCountriesPageWithNegativePageNumbersTest([Random(-100500, -1, 5)] int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<COUNTRY> actualCountriesPage = _countryService.GetCountriesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            ICollection<COUNTRY> expectedPage = _countryService.GetCountriesPage(pageSize: pagesize, pageNumber: 1).ToList();

            // Assert
            actualCountriesPage.Should().BeEquivalentTo(expectedPage,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляров стран
        /// выбранной страницы пейджинга. Номер страницы больше общего числа страниц.
        /// </summary>
        [Test, Order(1)]
        public void GetNotExistedCountriesPageWithValueIfPageNumbersMoreThanTotalCountriesPagesTest([Random(CountriesCount, CountriesCount + 100500, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Act
            ICollection<COUNTRY> actualCountriesPage = _countryService.GetCountriesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualCountriesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения экземпляров стран
        /// ввыбранной страницы пейджинга из пустого репозитария.
        /// </summary>
        [Test, Order(5)]
        public void GetNotExistedCountriesPageFromEmptyRepositoryTest([Random(1, 18, 5)]int pageNumber)
        {
            const int pagesize = 16;

            // Arrange
            _fakeCountries.Countries.Clear();

            // Act
            ICollection<COUNTRY> actualCountriesPage = _countryService.GetCountriesPage(pageSize: pagesize, pageNumber: pageNumber).ToList();

            // Assert
            actualCountriesPage.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров стран
        /// в пустом репозитарии.
        /// </summary>
        [Test, Order(5)]
        public void GetAllCountriesFromEmptyRepositoryTest()
        {
            // Arrange
            _fakeCountries.Countries.Clear();

            // Act
            var actualCountries = _countryService.GetAllCountries(filter: null);

            // Assert
            actualCountries.Should().BeEmpty();
        }

        /// <summary>
        /// Тест метода получения всех экземпляров стран
        /// в не пустом репозитарии.
        /// </summary>
        [Test, Order(1)]
        public void GetAllExistedCountriesTest()
        {
            // Arrange
            var expectedCountries = _fakeCountries.Countries;

            // Act
            var actualCountries = _countryService.GetAllCountries().ToList();

            // Assert
            //foreach (var expectedCountry in expectedCountries)
            //{
            //    expectedCountry.ABBR2.Should().Be((actualCountries.Find(c => c.ABBR2 == expectedCountry.ABBR2).ABBR2));
            //    expectedCountry.NAME.Should().Be((actualCountries.Find(c => c.ABBR2 == expectedCountry.ABBR2).NAME));
            //}

            expectedCountries.Should().BeEquivalentTo(actualCountries,
                options => options.ExcludingMissingMembers());

            expectedCountries.Count.Should().Be(actualCountries.Count);
        }

        /// <summary>
        /// Тест метода получения экземпляра страны по заданному идентификатору.
        /// страна с передаваемым идентификатором существует
        /// </summary>
        /// <param name="id">Идентификатор страны</param>
        [Test, Order(1)]
        public void GetExistedCountryTest()
        {
            // Arrange
            var countryId = _fakeCountries.Countries.ToArray()[(new Random()).Next(CountriesCount)].ABBR2;


            var expectedCountry = _fakeCountries.Countries.Find(f => f.ABBR2 == countryId);

            // Act
            var actualCountry = _countryService.GetCountry(countryId);

            // Assert


            actualCountry.Should().BeEquivalentTo(expectedCountry,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода получения экземпляра страны по заданному идентификатору.
        /// Контейнер с передаваемым идентификатором не существует.
        /// </summary>
        /// <param name="id">Идентификатор страны</param>
        [Test, Order(1)]
        public void GetNotExistedCountryTest([Random(CountriesCount, CountriesCount + CountriesCount, 5)]
                    short id)
        {
            var actualCountry = _countryService.GetCountry(id.ToString().Substring(2));

            actualCountry.Should().BeNull();
        }

        /// <summary>
        /// Тест метода добавления страны в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddCountryTest()
        {

            var countryId = _fakeCountries.Countries.ToArray()[(new Random()).Next(CountriesCount)].ABBR2;
                        
            // Arrange
            var countrysFake = _fakeCountries.Countries;

            var countryFake = countrysFake.Where(c => c.ABBR2 == countryId).FirstOrDefault();
            

            // Act

            var actualCountry = _countryService.GetCountry(countryId);

            // Assert

            actualCountry.Should().BeEquivalentTo(actualCountry,
                options => options.ExcludingMissingMembers());
        }

        /// <summary>
        /// Тест метода добавления страны в хранилище данных
        /// </summary>
        [Test, Repeat(5), Order(2)]
        public void AddCountryIntegratedTest()
        {
            //IUnitOfWork _unitOfWork;

            //// Arrange
            //var country = new FakeCountries(1, false, false, false, false, 5, 5).Countries[0];
            //var countryManagementService = new CountryManagementService(_unitOfWork);
            //country.Id = _fakeCountries.Countries.Last().Id + 1;

            //// Act
            //var actualId = _countryService.AddCountry(country);

            //var actualCountry = _countryService.GetCountry(actualId);

            //// Assert
            //actualCountry.CountryDetail.EMailAddresses.Should().BeEquivalentTo(country.CountryDetail.EMailAddresses,
            //    options => options.ExcludingMissingMembers());
            //actualCountry.Login.Should().BeEquivalentTo(country.Login);
            //actualCountry.FirstName.Should().BeEquivalentTo(country.FirstName);
            //actualCountry.LastName.Should().BeEquivalentTo(country.LastName);
        }

        /// <summary>
        /// Тест метода обновления данных о странае
        /// </summary>
        /// <param name="id">Идентификатор страны для обновления</param>
        [Test, Order(2)]
        public void UpdateCountryTest()
        {
            // Arrange
            var countryId = _fakeCountries.Countries.ToArray()[(new Random()).Next(CountriesCount)].ABBR2;

            var country = _countryService.GetCountry(countryId);
            country.NAME = new Bogus.Faker().Random.AlphaNumeric(17);

            // Act
            _countryService.UpdateCountry(country);
            var actualCountry = _fakeCountries.Countries.Find(f => f.ABBR2 == countryId);

            // Assert
            actualCountry.NAME.Should().Be(country.NAME);
        }

        /// <summary>
        /// Тест метода удаления страны из хранилища данных
        /// </summary>
        /// <param name="id">Идентификатор страны для удаления</param>
        [Test, Order(3)]
        public void DeleteCountryTest()
        {
            // Arrange

            var countryId = _fakeCountries.Countries.ToArray()[(new Random()).Next(CountriesCount)].ABBR2;

            _countryService.DeleteCountry(countryId);

            // Act
            var actualCountry = _fakeCountries.Countries.Find(country => country.ABBR2 == countryId);

            // Assert
            actualCountry.Should().BeNull();
        }

        /// <summary>
        /// Тест метода проверки наличия страны в хранилище данных.
        /// страна существует
        /// </summary>
        /// <param name="id">Идентификатор страны для поиска</param>
        [Test, Order(2)]
        public void CountryExistsTest()
        {
            // Arrange

            var countryId = _fakeCountries.Countries.ToArray()[(new Random()).Next(CountriesCount)].ABBR2;

            var loginToFind = _fakeCountries.Countries.Find(dbf => dbf.ABBR2 == countryId).NAME;

            // Act
            var actual = _countryService
                .CountryExists(f => f.NAME == loginToFind);

            // Assert
            actual.Should().BeTrue();
        }

        /// <summary>
        /// Тест метода проверки наличия страны в хранилище данных.
        /// страна не существует
        /// </summary>
        /// <param name="login">Логин страны для поиска</param>
        [Test, Order(2)]
        public void CountryNotExistsTest([Values("shameonyou", "qwer", "", "befastandclearver", "strangelogin")]
                    string VIN)
        {
            var actual = _countryService
                .CountryExists(f => f.NAME == VIN);

            actual.Should().BeFalse();
        }
    }
}

