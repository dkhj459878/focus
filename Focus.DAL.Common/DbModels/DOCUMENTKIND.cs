//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.DAL.Common.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class DOCUMENTKIND
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DOCUMENTKIND()
        {
            this.DOCUMENT = new HashSet<DOCUMENT>();
        }
    
        public byte DOCUMENTKINDID { get; set; }
        public string NAME { get; set; }
        public System.DateTime LAST_UPDATE { get; set; }
        public string USERID { get; set; }
        public string SID { get; set; }
        public string USERINFO { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DOCUMENT> DOCUMENT { get; set; }
    }
}
