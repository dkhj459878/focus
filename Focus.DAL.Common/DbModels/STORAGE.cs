//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Focus.DAL.Common.DbModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class STORAGE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public STORAGE()
        {
            this.POST = new HashSet<POST>();
        }
    
        public byte ID { get; set; }
        public string NAME { get; set; }
        public string ADDRESS { get; set; }
        public string NUMBER_ { get; set; }
        public System.DateTime LAST_UPDATE { get; set; }
        public string USERID { get; set; }
        public string SID { get; set; }
        public string USERINFO { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<POST> POST { get; set; }
    }
}
