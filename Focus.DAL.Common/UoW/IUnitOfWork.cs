﻿using System;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Common.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// Представляет репозиторий для автомобилей, как товара.
        /// </summary>
        IDbCarRepository CarRepository { get; }

        /// <summary>
        /// Представляет репозиторий для контейнеров.
        /// </summary>
        IDbContainerRepository ContainerRepository { get; }

        /// <summary>
        /// Представляет репозиторий для автомобилей, как товара.
        /// </summary>
        IDbCountryRepository CountryRepository { get; }

        /// <summary>
        /// Представляет репозиторий для автомобилей, как товара.
        /// </summary>
        IDbCustombrockerRepository CustombrockerRepository { get; }

        /// <summary>
        /// Представляет репозиторий для автомобилей, как товара.
        /// </summary>
        IDbDeviceRepository DeviceRepository { get; }

        /// <summary>
        /// Представляет репозиторий для автомобилей, как товара.
        /// </summary>
        IDbDevicetypeRepository DevicetypeRepository { get; }

        /// <summary>
        /// Представляет репозиторий для документов.
        /// </summary>
        IDbDocRepository DocRepository { get; }

        /// <summary>
        /// Представляет репозиторий для типов документов, содержащих сведения о полномочиях таможенных представителей.
        /// </summary>
        IDbDocumentkindRepository DocumentkindRepository { get; }

        /// <summary>
        /// Представляет репозиторий для документов, содержащих сведения о полномочиях таможенных представителей.
        /// </summary>
        IDbDocumentRepository DocumentRepository { get; }

        /// <summary>
        /// Представляет репозиторий для должностных лиц таможни.
        /// </summary>
        IDbInspectorRepository InspectorRepository { get; }

        /// <summary>
        /// Представляет репозиторий для организаций.
        /// </summary>
        IDbOrganizationRepository OrganizationRepository { get; }

        /// <summary>
        /// Представляет репозиторий для разрешений.
        /// </summary>
        IDbPermissionRepository PermissionRepository { get; }

        /// <summary>
        /// Представляет репозиторий для пунктов таможенного оформления.
        /// </summary>
        IDbPostRepository PostRepository { get; }

        /// <summary>
        /// Представляет репозиторий для отчетов (протоколирования действий пользователей).
        /// </summary>
        IDbReportRepository ReportRepository { get; }

        /// <summary>
        /// Представляет репозиторий для отчетов (протоколирования действий пользователей).
        /// </summary>
        IDbUserRepository UserRepository { get; }

        /// <summary>
        /// Представляет репозиторий для складов временного хранения.
        /// .
        /// </summary>
        IDbStorageRepository StorageRepository { get; }

        /// <summary>
        /// Представляет репозиторий для видов транспортных средств.
        /// </summary>
        IDbVehiclekindRepository VehiclekindRepository { get; }

        /// <summary>
        /// Представляет репозиторий для транспортных средств.
        /// </summary>
        IDbVehicleRepository VehicleRepository { get; }

        /// <summary>
        /// Представляет репозиторий для типов транспортных средств.
        /// </summary>
        IDbVehicletypeRepository VehicletypeRepository { get; }

        /// <summary>
        /// Представляет репозиторий для записей аудита.
        /// </summary>
        IDbAudit_trailRepository Audit_trailRepository { get; }

        /// <summary>
        /// Представляет репозиторий для доп. функций программного обеспечения.
        /// </summary>
        IDbFeatureRepository FeatureRepository { get; }

        /// <summary>
        /// Представляет репозиторий для поддержки версионности.
        /// </summary>
        IDbVersionRepository VersionRepository { get; }

        /// <summary>
        /// Представляет репозиторий для поддержки версионности.
        /// </summary>
        IDbRoleRepository RoleRepository { get; }

        /// <summary>
        /// Представляет репозиторий для поддержки версионности.
        /// </summary>
        IDbUserRoleRepository UserRoleRepository { get; }

        /// <summary>
        /// Сохраняет изменения в хранилище данных
        /// </summary>
        /// <returns> int </returns>
        int SaveChanges();

        /// <summary>
        /// Сохраняет изменения в хранилище данных
        /// </summary>
        /// <returns> int </returns>
        int SaveChanges(string userId, string userInfor);
    }
}
