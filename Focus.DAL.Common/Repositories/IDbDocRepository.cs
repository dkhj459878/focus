﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для документов.
    /// </summary>
    public interface IDbDocRepository : IBaseRepository<DOC>
    {
    }
}  