﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий версионности.
    /// </summary>
    public interface IDbVersionRepository : IBaseRepository<VERSION>
    {
    }
}