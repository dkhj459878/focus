﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для отчетов (протоколирования действий пользователей).
    /// </summary>
    public interface IDbReportRepository : IBaseRepository<REPORT>
    {
    }
}