﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для складов временного хранения.
    /// .
    /// </summary>
    public interface IDbStorageRepository : IBaseRepository<STORAGE>
    {
    }
}