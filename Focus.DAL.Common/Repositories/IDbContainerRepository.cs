﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для контейнеров.
    /// </summary>
    public interface IDbContainerRepository : IBaseRepository<CONTAINER>
    {
    }
}