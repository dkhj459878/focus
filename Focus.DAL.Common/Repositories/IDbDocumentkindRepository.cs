﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для типов документов, содержащих сведения о полномочиях таможенных представителей.
    /// </summary>
    public interface IDbDocumentkindRepository : IBaseRepository<DOCUMENTKIND>
    {
    }
}