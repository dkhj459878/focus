﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для разрешений.
    /// </summary>
    public interface IDbPermissionRepository : IBaseRepository<PERMISSION>
    {
    }
}