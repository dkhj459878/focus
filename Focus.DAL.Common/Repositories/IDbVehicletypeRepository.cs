﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для типов транспортных средств.
    /// </summary>
    public interface IDbVehicletypeRepository : IBaseRepository<VEHICLETYPE>
    {
    }
}