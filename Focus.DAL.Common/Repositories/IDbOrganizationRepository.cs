﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для организаций.
    /// </summary>
    public interface IDbOrganizationRepository : IBaseRepository<ORGANIZATION>
    {
    }
}