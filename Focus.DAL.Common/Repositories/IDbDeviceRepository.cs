﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для технических средств таможенного контроля.
    /// </summary>
    public interface IDbDeviceRepository : IBaseRepository<DEVICE>
    {
    }
}