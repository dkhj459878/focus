﻿using Focus.DAL.Common.DbModels;

namespace Focus.DAL.Common.Repositories
{
    /// <summary>
    /// Представляет репозиторий для уполномоченных представителей (декларантов).
    /// </summary>
    public interface IDbCustombrockerRepository : IBaseRepository<CUSTOMBROCKER>
    {
    }
}