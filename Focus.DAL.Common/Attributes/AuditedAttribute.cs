﻿using System;

namespace Focus.DAL.Common.DbModels
{
    public class AuditedAttribute: Attribute
    {
        // Mark properties which is not being audited.
        public string SkipPropertes { get; set; }

        public AuditedAttribute()
        {

        }

        public AuditedAttribute( string skipPreoperties)
        {
            SkipPropertes = skipPreoperties;
        }
    }
}
