﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using Focus.DAL.Common.DbModels;

namespace Focus.BL.Mappers
{
    public class MapperTemplate // Это как бы мапер 
    {
        private DOC _doc;

        private Entities db = new Entities();

        public Dictionary<string, string> Exchanger { get; }

        public MapperTemplate(DOC doc) // тут можно и id
        {
            const string doubleSpace = "  ";
            //а потом замапить?
            _doc = doc;
            this.Exchanger = new Dictionary<string, string>()
            {
                // ДОСМОТР/ОСМОТР
                { "{листов досмотр}", (2 + Numeric(_doc.PHOTOCOPIES ?? "") * Numeric(_doc.PHOTOPAGES ?? "") + Numeric(_doc.ADDITIONALPAGES1 ?? "") + Numeric(_doc.ADDITIONALPAGES2 ?? "") + Numeric(_doc.ADDITIONALPAGES3 ?? "") + Numeric(_doc.ADDITIONALPAGES4 ?? "") + Numeric(_doc.ADDITIONALPAGES5 ?? "")).ToString() },
                { "{дата число}",   _doc.PERIODSTARTDATE == null ? DateTime.Now.ToString("d") : _doc.PERIODSTARTDATE.Value.ToString("dd")},
                { "{месяц}", _doc.PERIODSTARTDATE == null ?  DateTime.Now.ToString("MMMM").ToLower().Replace("ь", "я").Replace("й", "я").Replace("март", "марта").Replace("август", "августа") : _doc.PERIODSTARTDATE.Value.ToString("MMMM").ToLower().Replace("ь", "я").Replace("й", "я").Replace("март", "марта").Replace("август", "августа")},
                { "{год}", _doc.PERIODSTARTDATE == null ?  DateTime.Now.Year.ToString() : _doc.PERIODSTARTDATE.Value.Year.ToString()},
                { "{время начала}", _doc.PERIODSTARTTIME == null ? DateTime.Now.ToString("HH:mm") : _doc.PERIODSTARTTIME.Value.ToString("HH\\:mm")},
                { "{дата начала}", _doc.PERIODSTARTDATE == null ? DateTime.Now.ToShortDateString() : _doc.PERIODSTARTDATE.Value.ToShortDateString()},
                { "{инспектор краткий}", _doc.INSPECTORSHORTNAME ?? "" },
                { "{декларант}", GetCustomBrockerInfo() },
                { "{номер АТД}", _doc.NUM ?? "" },
                // Проведен таможенный досмотр.
                { "{д}", ((doc.CUSTOMS).ToString().Contains("1") && (_doc.CUSTOMS.ToString().Length == 1)) ? "■" : doubleSpace },
                
                // Проведен таможенный осмотр.
                { "{о}", (doc.CUSTOMS.ToString().Contains("1") && (_doc.CUSTOMS.ToString().Length == 1)) ? doubleSpace : "■" },
                { "{страна}", _doc.GOODSARRIVALCOUNTRY ?? "" },
                { "{наименование организации}", _doc.CONSIGNEENAME ?? "" },
                { "{УНП}", string.IsNullOrEmpty(_doc.CONSIGNEETIN) ? "" : "УНП " + _doc.CONSIGNEETIN},
                { "{адрес организации}", _doc.CONSIGNEEADDRESS ?? "" },
                { "{документы декларации}", GetDocuments() },
                { "{СВХ}", _doc.CUSTOMSPLACE ?? "" },
                { "{ТСТК часть1}", GetDevices().firstPart },
                { "{ТСТК часть2}", GetDevices().secondPart },
                { "{товар предъявлен}", string.IsNullOrEmpty(GetNameOfGood()) ? doubleSpace : "■"},
                { "{товар}", GetNameOfGood()},
                { "{мест}", _doc.PLACESNUMBER == null ? "" :  Numeric(_doc.PLACESNUMBER).ToString()},
                { "{вес}", _doc.GOODSTOTALBRUTTO == null ? "" : Convert.ToDouble(_doc.GOODSTOTALBRUTTO).ToString("0.#####")}, 
                // Есть транспорт или нет.
                { "{транспорт предъявлен}", _doc.VEHICLE.Count > 0 ? "■"  : doubleSpace },
                { "{рег номер ТС}", GetVehicles()},
                { "{факт нетто}",  _doc.WEIGHINGACTUALNETTO == null ? "" : Convert.ToDouble(_doc.WEIGHINGACTUALNETTO).ToString("0.#####")},
                { "{факт брутто}", _doc.WEIGHINGACTUALBRUTTO == null ? "" : Convert.ToDouble(_doc.WEIGHINGACTUALBRUTTO).ToString("0.#####")},
                { "{фактического взвешивания}", (doc.ISACTUAL.ToString().Contains("1") && (_doc.ISACTUAL.ToString().Length == 1)) ? "■" : doubleSpace },
                { "{определением среднего}", (doc.ISAVERAGE.ToString().Contains("1") && (_doc.ISAVERAGE.ToString().Length == 1)) ? "■" : doubleSpace },
                { "{ср.мест}", (_doc.ISAVERAGE == null) ? string.Empty : (_doc.ISAVERAGE == 1 ? "3" : string.Empty) },
                { "{ср.вес1}", (_doc.ISAVERAGE == null) ? string.Empty : (_doc.ISAVERAGE == 1 ? ((_doc.ISAVERAGEFIRST == null) ? string.Empty : Convert.ToDouble(_doc.ISAVERAGEFIRST).ToString("0.#####")) : string.Empty) },
                { "{ср.вес2}", (_doc.ISAVERAGE == null) ? string.Empty : (_doc.ISAVERAGE == 1 ? ((_doc.ISAVERAGESECOND == null) ? string.Empty : Convert.ToDouble(_doc.ISAVERAGESECOND).ToString("0.#####")) : string.Empty) },
                { "{ср.вес3}", (_doc.ISAVERAGE == null) ? string.Empty : (_doc.ISAVERAGE == 1 ? ((_doc.ISAVERAGETHIRD == null) ? string.Empty : Convert.ToDouble(_doc.ISAVERAGETHIRD).ToString("0.#####")) : string.Empty) },
                { "{расчетным методом}", (doc.ISCALCULATION.ToString().Contains("1") && (_doc.ISCALCULATION.ToString().Length == 1)) ? "■" : doubleSpace },
                { "{расчетный метод описание}", string.IsNullOrEmpty(_doc.ISCALCULATIONDESCRIPTION) ? string.Empty : _doc.ISCALCULATIONDESCRIPTION},
                { "{иным методом}", (_doc.ISOTHER == null) ? "  " : ((doc.ISOTHER.ToString().Contains("1") && (_doc.CUSTOMS.ToString().Length == 1)) ? "■" : doubleSpace )},
                { "{иным методом описание}", (string.IsNullOrEmpty(_doc.ISOTHERDESCRIPTION)) ? string.Empty : _doc.ISOTHERDESCRIPTION},
                { "{ТС не своим ходом}", CarCount() == 0 ? doubleSpace : (_doc.ONITSOWNWHEELS == null) ? "  " : ((_doc.ONITSOWNWHEELS.ToString().Contains("0") && (_doc.ONITSOWNWHEELS.ToString().Length == 1)) ? "■" : doubleSpace )},
                { "{ТС своим ходом}", CarCount() == 0 ? doubleSpace : (_doc.ONITSOWNWHEELS == null) ? "  " : ((_doc.ONITSOWNWHEELS.ToString().Contains("0") && (_doc.ONITSOWNWHEELS.ToString().Length == 1)) ? doubleSpace : "■") },
                { "{ТС товар, № ТС}", GetVehiclesWithoutContainers()},
                { "{ТС товар, № контейнеров}", GetContainersAsVehicles()},
                { "{средства идентификации}",  _doc.SEALS == null ? "■" : String.IsNullOrEmpty(_doc.SEALS.Trim()) ? "■" : doubleSpace },
                { "{не нарушены}", (_doc.SEALS == null) ? "  " : ((_doc.SEALSVIOLATED.ToString().Contains("1") && (_doc.CUSTOMS.ToString().Length == 1)) ? doubleSpace : "■" )},
                { "{нарушены}", (_doc.SEALS == null) ? "  " : ((_doc.SEALSVIOLATED.ToString().Contains("1") && (_doc.CUSTOMS.ToString().Length == 1)) ? "■" : doubleSpace )},
                { "{идентификация на ТС}",  Seals() },
                { "{приложение досмотр часть1}", GetAttachmentsPartOne()},
                { "{приложение досмотр часть2}", GetAttachmentsPartTwo()},
                { "{результат}", GetResult()},
                // Изъятий не производилось.
                { "{из}", string.IsNullOrEmpty(_doc.SAMPLINGNUM) ? "■" : doubleSpace },
                // Грузовые места упакованы в нашем присутствии.
                { "{уп}", (_doc.PACKEDATOURPRESENCE == null) ? "  " : ((_doc.PACKEDATOURPRESENCE.ToString().Contains("1") && (_doc.CUSTOMS.ToString().Length == 1)) ? "■" : "  " ) },
                { "{отбор дата}", (!string.IsNullOrEmpty(_doc.SAMPLINGNUM)) ? ((_doc.PEROIDFINISHDATE == null) ? (string.IsNullOrEmpty(_doc.SAMPLINGNUM) ? DateTime.Now.ToShortDateString() : string.Empty) : _doc.PEROIDFINISHDATE.Value.ToShortDateString()) : string.Empty},
                { "{отбор№}", _doc.SAMPLINGNUM ?? ""},
                // Обнаружены признаки правонарушений.
                { "{нарушения}", (_doc.SIGNSSOFVIOLATIONS.ToString().Contains("1") && (_doc.CUSTOMS.ToString().Length == 1)) ? _doc.VIOLATIONSDESCRIPTION ?? "" : "нет" },
                // Наложены средства идентификации.
                { "{идентификация}", _doc.IDENTIFICATIONTOOLS ?? ""},
                // в количестве.
                { "{идентификация шт}", _doc.IDENTIFICATIONTOOLSNUMBER == null ? "" : _doc.IDENTIFICATIONTOOLSNUMBER.ToString()},
                { "{вопросы}", _doc.QUESTIONS ?? ""},
                { "{декларант краткий}", GetCustomBrocker() },
                { "{время завершения}",  _doc.PERIODFINISHTIME == null ? DateTime.Now.ToShortTimeString() : _doc.PERIODFINISHTIME.Value.ToString("HH\\:mm")},
                { "{дата завершения}", _doc.PEROIDFINISHDATE == null ? DateTime.Now.ToShortDateString() : (_doc.PEROIDFINISHDATE.Value.ToShortDateString())},
                { "{дата и время получения}", ((_doc.TRANSFERRINGDATE == null) ? DateTime.Now.ToShortDateString() : _doc.TRANSFERRINGDATE.Value.ToShortDateString()) + " в " +  ((_doc.TRANSFERRINGTIME == null) ? DateTime.Now.ToShortTimeString() : _doc.TRANSFERRINGTIME.Value.ToString("HH\\:mm")) },
                // ОТБОР.
                { "{листов отбор}", "2"},
                { "{дата отбора}", _doc.PEROIDFINISHDATE.Value == null ? DateTime.Now.ToString("d") : _doc.PEROIDFINISHDATE.Value.Day.ToString("d")},
                { "{месяц отбора}",  _doc.PEROIDFINISHDATE.Value == null ? DateTime.Now.ToString("MMMM").ToLower().Replace("ь", "я").Replace("й", "я").Replace("март", "марта").Replace("август", "августа") : _doc.PEROIDFINISHDATE.Value.ToString("MMMM").ToLower().Replace("ь", "я").Replace("й", "я").Replace("март", "марта").Replace("август", "августа")},
                { "{год отбора}", _doc.PEROIDFINISHDATE.Value == null ? DateTime.Now.Year.ToString() : _doc.PEROIDFINISHDATE.Value.Year.ToString()},
                { "{должность инспектора досмотра}", ((_doc.INSPECTORTITLE ?? "") + (string.IsNullOrEmpty(_doc.INSPECTORDEVISION) ? "" : " " + _doc.INSPECTORDEVISION)).Trim()  },
                { "{ФИО инспектора отбора}", _doc.INSPECTORSHORTNAME  ?? ""},
                { "{товар для экспертизы}", _doc.GOODSDESCRIPTION  ?? ""},
                { "{погода}", _doc.WHEATHER  ?? "" },
                { "{освещение}", _doc.LIGHTING  ?? "" },
                { "{СВХ адрес}", _doc.CUSTOMSPLACE  ?? "" },
                { "{ТСТК отбор}", GetDevices().firstPart + (string.IsNullOrEmpty(GetDevices().secondPart) ? string.Empty : ", " + GetDevices().secondPart) },
                { "{$проб и (или) образцов$}",  string.IsNullOrEmpty(_doc.KINDOFSAMPLE) ? "" : (_doc.KINDOFSAMPLE.Contains("проба") ? string.Empty : "{$проб и (или) образцов$}")},
                { "{@проб и (или) образцов@}", string.IsNullOrEmpty(_doc.KINDOFSAMPLE) ? "" : (_doc.KINDOFSAMPLE.Contains("проба") ? "{@проб и (или) образцов@}" : string.Empty) },
                { "{$", string.Empty},
                { "$}", string.Empty},
                { "{@", string.Empty},
                { "@}", string.Empty},
                { "{проба/образец}", _doc.KINDOFSAMPLE ?? "" },
                { "{вес пробы}", _doc.SAMPLEWEIGHT == null ? "" : Convert.ToDouble(_doc.SAMPLEWEIGHT).ToString("0.#####")},
                { "{стоимость пробы}", _doc.SAMPLECOST == null ? "" : Convert.ToDouble(_doc.SAMPLECOST).ToString("0.##")},
                { "{кол-во проб}", _doc.SAMPLESNUM ?? string.Empty },
                { "{валюты стоимости пробы}", _doc.CURRENCYNAME ?? ""},
                { "{опасные}", _doc.ISDANGER ?? ""},
                { "{скоропортящиеся}", _doc.ISPERISHABLEPRODUCT ?? ""},
                { "{упаковка образца}", _doc.SAMPLINGPACKAGE ?? ""},
                { "{пломба отбора}", _doc.SEALNUM ?? ""},
                // РЕШЕНИЕ.
                { "{номер решения}", _doc.DECISIONNUM ?? ""},
                { "{дата решения}", _doc.PEROIDFINISHDATE == null ? DateTime.Now.ToShortDateString() : _doc.PEROIDFINISHDATE.Value.ToShortDateString()},
                { "{ДТ}", GetDeclarationOnGoodsNumber()},
                { "{номер профиля}", _doc.PROFILENUMBER ?? ""},
                { "{решение начальник}", (string.IsNullOrEmpty(_doc.CHIEFTITLE) ? string.Empty : _doc.CHIEFTITLE.Trim()) + (string.IsNullOrEmpty(_doc.INSPECTORDEVISION) ? string.Empty : " " + _doc.INSPECTORDEVISION.Trim()) },
                { "{телефон начальника}", _doc.CHIEFSHORTNAME ?? "" + (((!string.IsNullOrEmpty(_doc.CHIEFSHORTNAME)) && (!string.IsNullOrEmpty(_doc.CHIEFPHONENUMBER))) ? ", " : string.Empty) +  "тел. " + _doc.CHIEFPHONENUMBER ?? "" },
                { "{вид таможенной экспертизы}", _doc.EXPERTINSINKIND ?? ""}
            };
        }

        private string GetPassportData()
        {
            return ", паспорт №" + _doc.PASSPORTNUMBER + ", выдан " + _doc.PASSPORTAUTHORITYBODY + " " + _doc.PASSPORTISSUEDDATE;
        }
        
        // Формирование строки второй части приложения.
        private string GetAttachmentsPartTwo()
        {
            var photoNumber = Numeric(_doc.PHOTOPAGES);

            if (photoNumber == 0)
            {
                if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME3))
                {
                    string attachment3 = string.Empty;
                    string attachment4 = string.Empty;
                    string attachment5 = string.Empty;
                    attachment3 = _doc.ADDITIONALNAME3 + " на " +
                    _doc.ADDITIONALPAGES3 + " л. в " +
                    _doc.ADDITIONALCOPIES3 + " экз.";

                    if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME4))
                    {
                        attachment4 = " , " + _doc.ADDITIONALNAME4 + " на " +
                        _doc.ADDITIONALPAGES4 + " л. в " +
                        _doc.ADDITIONALCOPIES4 + " экз.";
                        if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME5))
                        {
                            attachment5 = ", " + _doc.ADDITIONALNAME5 + " на " +
                                _doc.ADDITIONALPAGES5 + " л. в " +
                                _doc.ADDITIONALCOPIES5 + " экз.";
                        }
                    }
                    return attachment3 + attachment4 + attachment5;
                }
            }
            else
            {
                var photoAttachment = "Фотография(и) на " + _doc.PHOTOPAGES + " л. в " + _doc.PHOTOCOPIES + " экз.";

                if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME3))
                {
                    string attachment2 = string.Empty;
                    string attachment3 = string.Empty;
                    string attachment4 = string.Empty;
                    string attachment5 = string.Empty;
                    attachment2 = _doc.ADDITIONALNAME2 + " на " +
                        _doc.ADDITIONALPAGES2 + " л. в " +
                        _doc.ADDITIONALCOPIES2 + " экз.";

                    if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME3))
                    {
                        attachment3 = ", " + _doc.ADDITIONALNAME3 + " на " +
                            _doc.ADDITIONALPAGES3 + " л. в " +
                            _doc.ADDITIONALCOPIES3 + " экз.";
                        if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME4))
                        {
                            attachment4 = ", " + _doc.ADDITIONALNAME4 + " на " +
                                _doc.ADDITIONALPAGES4 + " л. в " +
                                _doc.ADDITIONALCOPIES4 + " экз.";
                            if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME5))
                            {
                                attachment5 = ", " + _doc.ADDITIONALNAME5 + " на " +
                                    _doc.ADDITIONALPAGES5 + " л. в " +
                                    _doc.ADDITIONALCOPIES5 + " экз.";
                            }
                        }
                    }
                    return attachment2 + attachment3 + attachment4 + attachment5;
                }
            }
            return string.Empty;
        }

        private string GetNameOfGood()
        {
            string nameOfGood = _doc.GOODSDESCRIPTION ?? string.Empty;

            if (!string.IsNullOrEmpty(nameOfGood))
            {
                return $@"«{nameOfGood}»";
            }
            return string.Empty;
        }

        private string GetResult()
        {
            return _doc.CUSTOMSRESULT ?? string.Empty;
        }

        // Формирование строки первой части приложения.
        private string GetAttachmentsPartOne()
        {
            var photoNumber = Numeric(_doc.PHOTOPAGES);

            if (photoNumber == 0)
            {
                if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME1))
                {
                    string attachment1 = string.Empty;
                    string attachment2 = string.Empty;
                   attachment1 = _doc.ADDITIONALNAME1 + " на " +
                   _doc.ADDITIONALPAGES1 + " л. в " +
                   _doc.ADDITIONALCOPIES1 + " экз.";

                    if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME2))
                    {
                            attachment2 = " , " + _doc.ADDITIONALNAME2 + " на " +
                            _doc.ADDITIONALPAGES2 + " л. в " +
                            _doc.ADDITIONALCOPIES2 + " экз.";
                    }
                    return attachment1 + attachment2;
                }
            }
            else
            {
                var photoAttachment = "Фотография(и) на " + _doc.PHOTOPAGES + " л. в " + _doc.PHOTOCOPIES + " экз.";

                if (!string.IsNullOrEmpty(_doc.ADDITIONALNAME1))
                {
                    string attachment1 = string.Empty;
                    attachment1 = ", " + _doc.ADDITIONALNAME1 + " на " +
                    _doc.ADDITIONALPAGES1 + " л. в " +
                    _doc.ADDITIONALCOPIES1 + " экз.";

                    return photoAttachment + attachment1;
                }

                return photoAttachment;
            }
            return "нет";
        }

        // Возвращает строку со сведениями о контейнерах.
        private string GetContainersAsVehicles()
        {
            var resultContainers = string.Empty;

            if ((_doc.CAR.Count > 0) && (_doc.ONITSOWNWHEELS.ToString().Contains("0") && (_doc.CUSTOMS.ToString().Length == 1)))
            {
                foreach (var containerLocal in _doc.CONTAINER)
                {
                    resultContainers += ", " + GetNumberContainer(containerLocal);
                }
                return string.IsNullOrEmpty(resultContainers) ? string.Empty : resultContainers.Trim().Substring(2);
            }

            return resultContainers;
        }

        // Возвращает строку со сведениями о транспортных средствах.
        private string GetVehiclesWithoutContainers()
        {
            var resultVehicles = string.Empty;
           
            if ((_doc.CAR.Count > 0) && (_doc.ONITSOWNWHEELS.ToString().Contains("0") && (_doc.CUSTOMS.ToString().Length == 1)))
            {
                foreach (var vehicleLocal in _doc.VEHICLE)
                {
                    resultVehicles += ", " + GetNumberVehicle(vehicleLocal);
                }
                return string.IsNullOrEmpty(resultVehicles) ? string.Empty : resultVehicles.Trim().Substring(2);
            }
            return resultVehicles;

        }

        private string Seals()
        {
            if (_doc.SEALS != null)
            {
                var seals = _doc.SEALS;
                if (!string.IsNullOrEmpty(seals.Trim()))
                {
                    var sealArray = seals.Split(new[] { ",", ":", ";", "//", @"\" }, StringSplitOptions.RemoveEmptyEntries);
                    int sealCount = sealArray.Length;
                    string sealCountString = sealCount == 0 ? string.Empty : sealCount.ToString() + string.Empty + "ед.";
                    seals += string.Empty + ", " +  sealCountString;
                    return seals;
                }
            }
           
            return string.Empty;
        }

        // Получение сведений о номерах транспортных средств
        // одной строкой (с контейнерами).
        private string GetVehicles()
        {
            var resultVehicles = string.Empty;

            var resultContainers = string.Empty;

            if (_doc.VEHICLE.Count > 0)
            {
                foreach (var vehicleLocal in _doc.VEHICLE)
                {
                    resultVehicles += ", " + GetNumberVehicle(vehicleLocal);
                }
            }
            if (_doc.CONTAINER.Count > 0)
            {
                foreach (var containerLocal in _doc.CONTAINER)
                {
                    resultContainers += ", " + GetNumberContainer(containerLocal);
                }
                resultContainers = string.IsNullOrEmpty(resultContainers) ? string.Empty : resultContainers.Trim().Substring(2);
                resultContainers = string.IsNullOrEmpty(resultVehicles) ? ("Контейнеры " + resultContainers) : (", контейнеры " + resultContainers);
            }

            return string.IsNullOrEmpty(resultVehicles) ? string.Empty : resultVehicles.Trim().Substring(2) + resultContainers;
        }
        
        // Возвращает номер транспортного средства.
        private string GetNumberVehicle(VEHICLE vehicle)
        {
            if (vehicle != null)
            {
                return vehicle.REGNUMBER;
            }
            return string.Empty;
        }

        // Возвращает номер контейнера.
        private string GetNumberContainer(CONTAINER container)
        {
            if (container != null)
            {
                return container.REGNUMBER;
            }
            return string.Empty;
        }

        // Получение числа из строки.
        private int Numeric(string str)
        {
            var result = 0;
            if (String.IsNullOrEmpty(str))
            {
                return 0;
            }
            else if (Int32.TryParse(str, out result))
            {
                return result;
            }
            else
            {
                return 0;
            }
        }

        // Получение номера декларации строкой в префиксом "ДТ № ".
        private string GetDeclarationOnGoodsNumber()
        {
            return String.IsNullOrEmpty(_doc.DOCDECLARATIONONGOODS) ? "" : _doc.DOCDECLARATIONONGOODS;
        }

        private string GetDeclarationOnGoodsNumberString()
        {
            return String.IsNullOrEmpty(_doc.DOCDECLARATIONONGOODS) ? "" : "ДТ № " + _doc.DOCDECLARATIONONGOODS;
        }

        // Агригирование информации о ТСД.
        private string GetDocuments()
        {
            var notification  = String.IsNullOrEmpty(_doc.DOCNOTIFICATIONPLACEMENT) ? "" : ", УВР № " + _doc.DOCNOTIFICATIONPLACEMENT;
            var customDeclarationOnGoods = String.IsNullOrEmpty(_doc.DOCDECLARATIONONGOODS) ? "" : ", ДТ № " + _doc.DOCDECLARATIONONGOODS;
            var transitDeclaration = String.IsNullOrEmpty(_doc.DOCTRANSITDECLARATION) ? "" : ", ТД № " + _doc.DOCTRANSITDECLARATION;
            var cmr = (String.IsNullOrEmpty(_doc.DOCCMRNUM) && String.IsNullOrEmpty(_doc.DOCCMRDATE)) ? "" : ", CMR " +
                (String.IsNullOrEmpty(_doc.DOCCMRNUM) ? "" : "№ " + _doc.DOCCMRNUM) + 
                (String.IsNullOrEmpty(_doc.DOCCMRDATE) ? "" : " от " + _doc.DOCCMRDATE);
            var invoce = (String.IsNullOrEmpty(_doc.DOCINVOCENUM) && String.IsNullOrEmpty(_doc.DOCINVOICEDATE)) ? "" : ", Invoce " +
                (String.IsNullOrEmpty(_doc.DOCINVOCENUM) ? "" : "№ " + _doc.DOCINVOCENUM) +
                (String.IsNullOrEmpty(_doc.DOCINVOICEDATE) ? "" : " от " + _doc.DOCINVOICEDATE);
            var carnetTIR = (String.IsNullOrEmpty(_doc.DOCCARNERNUM) && String.IsNullOrEmpty(_doc.DOCCARNETDATE)) ? "" : ", Carnet TIR " +
               (String.IsNullOrEmpty(_doc.DOCCARNERNUM) ? "" : "№ " + _doc.DOCCARNERNUM) +
               (String.IsNullOrEmpty(_doc.DOCCARNETDATE) ? "" : " от " + _doc.DOCCARNETDATE);

            var result = (customDeclarationOnGoods + transitDeclaration + notification + cmr + invoce + carnetTIR).Trim();
           
            return String.IsNullOrEmpty(result) ? String.Empty : (result.Substring(0,1).Contains(",") ? result.Substring(1).Trim() : result) ;
        }
        // _doc - доменная модель. Мне надо вытащить Devices. И работать с ними. Но если ставить точку на DEvice
        // то ничего не происходит. Это коллекция
        public (String firstPart, String secondPart) GetDevices()
        {
            switch (_doc.DEVICE.Count)
            {
                case 0:
                    return (firstPart: "нет", secondPart: String.Empty);
                case 1:
                    var deviceMy = new List<DEVICE>(_doc.DEVICE).ToArray()[0];
                    return (firstPart: GetDevice(deviceMy), secondPart: String.Empty);
                default:
                    var deviceArray = new List<DEVICE>(_doc.DEVICE).ToArray();
                    var deviceFirst = GetDevice(deviceArray[0]);
                    var deviceSecond = deviceArray[1];

                    string secondPartDevice = GetDevice(deviceSecond);
                    if (deviceArray.Length > 2)
                    {
                        for (int i = 1; i < deviceArray.Length; i++)
                        {
                            secondPartDevice += ", " + GetDevice(deviceArray[i]);
                        }
                    }
                    return ( deviceFirst, secondPartDevice);
            }
        }

        // Получение информации о ТСТК.
        public string GetDevice(DEVICE device)
        {
            if (device == null)
            {
                return String.Empty;
            }

            var deviceKind = db.DEVICETYPE.Find(device.DEVICETYPE.DEVICETYPEID).NAME;
            var serialNumber = String.IsNullOrEmpty(device.SERIALNUMBER) ? String.Empty : ", s\n " + device.SERIALNUMBER;
            var mark = String.IsNullOrEmpty(device.MARK) ? String.Empty : ", " + device.MARK;
            var model = String.IsNullOrEmpty(device.MODEL) ? String.Empty : ", " + device.MODEL;
            var name = String.IsNullOrEmpty(device.NAME) ? String.Empty : ", " + device.NAME;
            
            var callibration = device.CALLIBRATION == null ? String.Empty : ", дата поверки: " + device.CALLIBRATION.Value.ToShortDateString();
            
            var result = (deviceKind + serialNumber + mark + model + name + callibration).Trim();

            if (String.IsNullOrEmpty(result))
            {
                return String.Empty;
            }

            return result.Substring(0, 1).Contains(",") ? result.Substring(1) : result;
        }

        private string GetCustomBrockerInfo()
        {
            const string dotChar = ".";

            const string spaceAndCommaChars = ", ";

            const string spaceChar = " ";

            const string passportString = "паспорт";

            const string bodyIssuedString = "орган выдачи:";

            const string dateIssuedString = "дата выдачи:";

            const string numberString = "№";

            string result = string.Empty;

            if (string.IsNullOrEmpty(_doc.LASTNAME))
            {
                return string.Empty;
            }

            var lastName = _doc.LASTNAME;

            var firstName = _doc.FIRSTNAME ?? "";

            var middleName = _doc.FATHERSNAME ?? "";

            var workPlace = _doc.PLACEOFWORK ?? "";

            var title = _doc.POST ?? "";

            var passportNumber = _doc.PASSPORTNUMBER ?? "";

            var passportIssuedDate = _doc.PASSPORTISSUEDDATE ?? "";

            var passportAuthorityBody = _doc.PASSPORTAUTHORITYBODY ?? "";

            string passport = string.Empty;

            string firstAndMiddleShortName = string.Empty;

            string address = _doc.ADDRESS ?? "";

            if (firstName != "")
            {
                if (middleName == string.Empty)
                {
                    firstAndMiddleShortName = firstName.Substring(0, 1) + dotChar;
                }
                else
                {
                    firstAndMiddleShortName = firstName.Substring(0, 1) + dotChar + middleName.Substring(0, 1) + dotChar;
                }
            }

            if (address != "")
            {
                address = ", адрес проживания: " + address;
            }

            if (passportNumber != "")
            {
                if (passportAuthorityBody !="")
                {
                    if (passportIssuedDate == "")
                    {
                        passport = spaceAndCommaChars + bodyIssuedString + spaceChar + passportAuthorityBody;
                    }
                    if (passportIssuedDate != "")
                    {
                        passport = spaceAndCommaChars + bodyIssuedString + spaceChar + passportAuthorityBody + spaceAndCommaChars + dateIssuedString + spaceChar + passportIssuedDate;
                    }
                }
                if (passportAuthorityBody == "")
                {
                    if (passportIssuedDate != "")
                    {
                        passport = spaceAndCommaChars + dateIssuedString + spaceChar + passportIssuedDate;
                    }
                }
                passport = spaceAndCommaChars + passportString + spaceChar + numberString + spaceChar + passportNumber + passport;
            }

            if (workPlace != string.Empty) workPlace = spaceAndCommaChars + workPlace;

            if (title != string.Empty) title = spaceAndCommaChars + title;

            return lastName + spaceChar + firstAndMiddleShortName + workPlace + title + passport + address;
        }

        private string GetCustomBrocker()
        {
            const string dotChar = ".";

            const string spaceChar = " ";

            string result = string.Empty;

            if (string.IsNullOrEmpty(_doc.LASTNAME))
            {
                return string.Empty;
            }

            var lastName = _doc.LASTNAME;

            var firstName = _doc.FIRSTNAME ?? "";

            var middleName = _doc.FATHERSNAME ?? "";

            var workPlace = _doc.PLACEOFWORK ?? "";

            var title = _doc.POST ?? "";

            string firstAndMiddleShortName = string.Empty;

            if (firstName != "")
            {
                if (middleName == string.Empty)
                {
                    firstAndMiddleShortName = firstName.Substring(0, 1) + dotChar;
                }
                else
                {
                    firstAndMiddleShortName = firstName.Substring(0, 1) + dotChar + middleName.Substring(0, 1) + dotChar;
                }
            }

            return lastName + spaceChar + firstAndMiddleShortName;
        }

        private int CarCount()
        {
            if (_doc.CAR == null)
            {
                return 0;
            }

            return _doc.CAR.Count;
        }
    }
}