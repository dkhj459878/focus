﻿using System;

namespace Focus.BL.Exceptions
{
    internal class AuditTrailsNotConsistentException : Exception
    {
        public AuditTrailsNotConsistentException()
        {

        }

        public AuditTrailsNotConsistentException(string message)
            : base(message)
        {

        }

        public AuditTrailsNotConsistentException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
