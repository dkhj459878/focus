﻿using System;

namespace Focus.BL.Exceptions
{
    internal class EntityIdentityIsNotSetException : Exception
    {
        public EntityIdentityIsNotSetException()
        {

        }

        public EntityIdentityIsNotSetException(string message)
            : base(message)
        {

        }

        public EntityIdentityIsNotSetException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }
}
