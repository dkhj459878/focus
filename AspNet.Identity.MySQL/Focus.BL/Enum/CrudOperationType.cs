﻿namespace Focus.BL.Enum
{
    public enum CrudOperation
    {
        Added,
        Modified,
        Deleted
    }
}