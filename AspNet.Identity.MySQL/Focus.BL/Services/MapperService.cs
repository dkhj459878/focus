﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Services
{
    public sealed class MapperService : IMapperService
    {
        public Dictionary<Type, IEnumerable<Type>> GetMap { get;} = new Dictionary<Type, IEnumerable<Type>>();

        public Dictionary<Type, IEnumerable<Type>> GetCollection { get; } = new Dictionary<Type, IEnumerable<Type>>();

        public MapperService()
        {
            GetMap.Add(typeof(CAR), new List<Type> () { typeof(COUNTRY) , typeof(VEHICLEKIND) });
            GetMap.Add(typeof(CONTAINER) , new List<Type> () { typeof(COUNTRY) });
            GetMap.Add(typeof(DEVICE) , new List<Type> () { typeof(DEVICETYPE) });
            GetMap.Add(typeof(VEHICLE) , new List<Type>() { typeof(COUNTRY) , typeof(VEHICLEKIND) });
            GetMap.Add(typeof(VEHICLEKIND) , new List<Type> () { typeof(VEHICLETYPE) });
            GetMap.Add(typeof(CUSTOMBROCKER) , new List<Type> () { typeof(COUNTRY) });
            GetMap.Add(typeof(INSPECTOR) , new List<Type> () { typeof(POST) });
            GetMap.Add(typeof(DOCUMENT) , new List<Type> () { typeof(ORGANIZATION) , typeof(PERMISSION) , typeof(DOCUMENTKIND) });
            GetMap.Add(typeof(PERMISSION), new List<Type> () { typeof(CUSTOMBROCKER) });
            GetMap.Add(typeof(ORGANIZATION) , new List<Type> () { typeof(COUNTRY) });
            GetMap.Add(typeof(DOC) , new List<Type> ());

            GetCollection.Add(typeof(DOC), new List<Type>() { typeof(CAR) , typeof(CONTAINER) , typeof(DEVICE) , typeof(VEHICLE) });
            GetCollection.Add(typeof(DOCUMENT), new List<Type>() { typeof(ORGANIZATION) , typeof(PERMISSION), typeof(DOCUMENTKIND) });
            GetCollection.Add(typeof(PERMISSION), new List<Type>() { typeof(CUSTOMBROCKER) });
            GetCollection.Add(typeof(INSPECTOR), new List<Type>() { typeof(POST) });
            GetCollection.Add(typeof(POST), new List<Type>() { typeof(STORAGE) });
        }
    }
}
