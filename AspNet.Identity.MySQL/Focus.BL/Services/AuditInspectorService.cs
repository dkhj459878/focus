﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditInspectorService : IAuditInspectorService 
    {
        #region Startup.
        private readonly IAudit_trailService _audit_TrailService;
        private readonly IRetrievingEntityOptimaService _retrievingEntityOptimaService;
        private readonly IPostService _postService;

        public AuditInspectorService(
            IRetrievingEntityOptimaService retrievingEntityOptimaService, IAudit_trailService audit_TrailService, IPostService postService
            )
        {
            _audit_TrailService = audit_TrailService;
            _retrievingEntityOptimaService = retrievingEntityOptimaService;
            _postService = postService;
        }
        #endregion

        public List<INSPECTOR> GetInspectorVersions(short id)
        {
            string entityTypeName = typeof(INSPECTOR).Name;

            // Retrieving from database audit records.
            List<AUDIT_TRAIL> audit_Trails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == entityTypeName && audit.ENTITYID == id).ToList();

            if (audit_Trails == null || audit_Trails.Count < 1)
            {
                return new List<INSPECTOR>();
            }

            List<INSPECTOR> inspectors = new List<INSPECTOR>(_retrievingEntityOptimaService.RetrieveEntityVersions<INSPECTOR>(audit_Trails).ToList());

            // Seed collection elements.

            return inspectors;
        }
    }
}
