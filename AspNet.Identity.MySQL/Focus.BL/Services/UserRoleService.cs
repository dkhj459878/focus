﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы со странами
    /// в хранилище данных.
    /// </summary>
    public class UserRoleService : BaseService, IUserRoleService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="UserRoleManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="userRolemanager"> manager </param>
        public UserRoleService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает страны
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров стран для пейджинга.</returns>
        public IEnumerable<USERROLES> GetAllUserRoles(
            Expression<Func<USERROLES, bool>> filter = null,
            Func<IQueryable<USERROLES>, IOrderedQueryable<USERROLES>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.UserRoleRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о странах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о странах.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр страны.</returns>
        public IEnumerable<USERROLES> GetUserRolesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<USERROLES, bool>> filter = null,
            Func<IQueryable<USERROLES>, IOrderedQueryable<USERROLES>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.UserRoleRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns>Экземпляр страны.</returns>
        public USERROLES GetUserRole(string id)
        {
            return _unitOfWork.UserRoleRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданную страну в хранилище данных
        /// и возвращает идентификатор добавленной страны.
        /// </summary>
        /// <param name="userRole">Экземпляр страны для добавления.</param>
        /// <returns>Идентификатор страны.</returns>
        public string AddUserRole(USERROLES userRole,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.UserRoleRepository.Insert(userRole);
            _unitOfWork.SaveChanges(userId, userInfo);
            return userRole.ROLEID;
        }

        /// <summary>
        /// Обновляет информацию заданной страны в хранилище данных.
        /// </summary>
        /// <param name="userRole">Экземпляр страны для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateUserRole(USERROLES userRole,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.UserRoleRepository.Update(userRole);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns> Task </returns>
        public void DeleteUserRole(string id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.UserRoleRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие страны в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска страны.</param>
        /// <returns>Возвращает <see langword="true"/>, если страна существует в хранилище данных.</returns>
        public bool UserRoleExists(Expression<Func<USERROLES, bool>> filter)
        {
            return _unitOfWork.UserRoleRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}