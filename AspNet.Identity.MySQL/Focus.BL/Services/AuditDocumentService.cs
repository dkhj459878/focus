﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocumentService : IAuditDocumentService
    {
        #region Startup.
        private readonly IAudit_trailService _audit_TrailService;
        private readonly IRetrievingEntityOptimaService _retrievingEntityOptimaService;
        private readonly IPermissionService _permissionService;
        private readonly IDocumentkindService _documentkindService;
        private readonly IOrganizationService _organizationService;
        private readonly IDocumentService _documentService;

        public AuditDocumentService(
            IRetrievingEntityOptimaService retrievingEntityOptimaService, IAudit_trailService audit_TrailService, IPermissionService permissionService, IDocumentkindService documentkindService,
            IOrganizationService organizationService,
            IDocumentService documentService
            )
        {
            _audit_TrailService = audit_TrailService;
            _retrievingEntityOptimaService = retrievingEntityOptimaService;
            _permissionService = permissionService;
            _documentkindService = documentkindService;
            _organizationService = organizationService;
            _documentService = documentService;
        }
        #endregion

        public List<DOCUMENT> GetDocumentVersions(short id)
        {
            string entityTypeName = typeof(DOCUMENT).Name;

            // Retrieving from database audit records.
            List<AUDIT_TRAIL> audit_Trails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == entityTypeName && audit.ENTITYID == id).ToList();

            if (audit_Trails == null || audit_Trails.Count < 1)
            {
                return new List<DOCUMENT>();
            }



            List<DOCUMENT> documents = _retrievingEntityOptimaService.RetrieveEntityVersions<DOCUMENT>(audit_Trails).ToList();


            List<DOCUMENT> documentLocal = new List<DOCUMENT>();

            DOCUMENT documentFromDb = _documentService.GetDocument(id);
            if (documentFromDb == null)
            {
                return new List<DOCUMENT>();
            }

            List<PERMISSION> permissions = documentFromDb.PERMISSION.ToList();

            if (permissions == null || permissions.Count == 0) // Такого быть не должно, иначе нет смысла в документе.
            {
                return new List<DOCUMENT>();
            }

            // Seed collection elements.
            foreach (DOCUMENT document in documents)
            {
                List<ORGANIZATION> organizations = documentFromDb.ORGANIZATION.ToList();

                if (organizations == null || organizations.Count == 0)
                {
                    return new List<DOCUMENT>();
                }
                document.ORGANIZATION = null;
                document.ORGANIZATION = organizations;
                
                document.PERMISSION = null;
                document.PERMISSION = permissions;

                DOCUMENTKIND documentKind = _documentkindService.GetDocumentkind(document.DOCUMENTKINDID);
                if (documentKind == null) // Не должно так быть.
                {
                    return new List<DOCUMENT>();
                }

                document.DOCUMENTKIND = null;
                document.DOCUMENTKIND = documentKind; // Не должен быть null.

            documentLocal.Add(document);
            }

            return documentLocal;
        }
    }
}
