﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    internal class RequestService : IRequestService
    {
        private readonly IMapperService _mapperService;

        public RequestService(IMapperService mapperService)
        {
            _mapperService = mapperService;
        }

        public Dictionary<Type, IEnumerable<short?>> GetIdentitiesNextGeneration(Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroupsSorted)
        {
            if (audit_TrailsGroupsSorted == null || audit_TrailsGroupsSorted.Count == 0)
            {
                return new Dictionary<Type, IEnumerable<short?>>();
            }
            Dictionary<Type, IEnumerable<short?>> result = new Dictionary<Type, IEnumerable<short?>>();
            List<AUDIT_TRAIL> allAuditTrailRecords = audit_TrailsGroupsSorted.Values.SelectMany(p => p).ToList();
            IEnumerable<Type> maps = GetActualMaps(audit_TrailsGroupsSorted);
            result = AddCollections(result, allAuditTrailRecords, maps, (t) => { return $"{t.Name}ID"; });
            IEnumerable<Type> collections = GetActualCollections(audit_TrailsGroupsSorted);
            result = AddCollections(result, allAuditTrailRecords, collections, (t) => { return $"{t.Name}"; });

            return result;
        }

        private Dictionary<Type, IEnumerable<short?>> AddCollections(Dictionary<Type, IEnumerable<short?>> initialDictionary, List<AUDIT_TRAIL> allAuditTrailRecords, IEnumerable<Type> typesDiscrepencies, Func<Type, string> fieldCreator)
        {
            if (typesDiscrepencies.Count() > 0)
            {
                Dictionary<Type, IEnumerable<short?>> result = new Dictionary<Type, IEnumerable<short?>>();
                foreach (KeyValuePair<Type, IEnumerable<short?>> initial in initialDictionary)
                {
                    result.Add(initial.Key, initial.Value);
                }
                foreach (Type discrepency in typesDiscrepencies)
                {
                    (Type Type, IEnumerable<short?> IdentityCollection) typeIdentities = GetTypeIdentities(discrepency, allAuditTrailRecords.ToList(), fieldCreator(discrepency));
                    if (typeIdentities.Type != null)
                    {
                        result.Add(typeIdentities.Type, typeIdentities.IdentityCollection);
                    }
                }
                return result;
            }
            else
            {
                return new Dictionary<Type, IEnumerable<short?>>();
            }
        }

        private (Type, IEnumerable<short?>) GetTypeIdentities(Type type, List<AUDIT_TRAIL> allAuditTrailRecords, string fieldName)
        {
            List<AUDIT_TRAIL> recordsWithIdentities = allAuditTrailRecords.Where(p => p.FIELD == fieldName).ToList();
            if (recordsWithIdentities.Count == 0)
            {
                return (null, null); 
            }
            List<string> stringsWithIdentities = recordsWithIdentities.Select(p => p.VALUENEW).ToList();

            IEnumerable<IEnumerable<short?>> identitiesGroups = stringsWithIdentities.Select(p => p.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(s => (short?)short.Parse(s)));

            IEnumerable<short?> identitiesUnuqueSorted = identitiesGroups.SelectMany(p => p).Distinct().OrderBy(p => p);

            return (type, identitiesUnuqueSorted);

        }
        
        private string GetTypeName(Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroupsSorted)
        {
            return audit_TrailsGroupsSorted.FirstOrDefault().Value.FirstOrDefault().ENTITY;
        }

        private IEnumerable<Type> GetActualCollections(Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroupsSorted)
        {
            string typeName = GetTypeName(audit_TrailsGroupsSorted);

            IEnumerable<Type> actualTypeList = new List<Type>();

            foreach (KeyValuePair<Type, IEnumerable<Type>> map in _mapperService.GetCollection)
            {
                if (map.Key.Name == typeName)
                {
                    return map.Value;
                }
            }

            return actualTypeList;
        }

        private IEnumerable<Type> GetActualMaps(Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroupsSorted)
        {
            string typeName = GetTypeName(audit_TrailsGroupsSorted);

            IEnumerable<Type> actualTypeList = new List<Type>();
                
            foreach (KeyValuePair<Type, IEnumerable<Type>> map in _mapperService.GetMap)
            {
                if (map.Key.Name == typeName)
                {
                    return map.Value;
                }
            }

            return actualTypeList;
        }
    }
}   
