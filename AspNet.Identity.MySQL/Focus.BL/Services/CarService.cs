﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с автомобилями
    /// в хранилище данных.
    /// </summary>
    public class CarService : BaseService, ICarService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="CarManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="carmanager"> manager </param>
        public CarService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все автомобили
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров автомобилей для пейджинга.</returns>
        public IEnumerable<CAR> GetAllCars(
            Expression<Func<CAR, bool>> filter = null,
            Func<IQueryable<CAR>, IOrderedQueryable<CAR>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.CarRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями об автомобилях.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр автомобиля.</returns>
        public IEnumerable<CAR> GetCarsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<CAR, bool>> filter = null,
            Func<IQueryable<CAR>, IOrderedQueryable<CAR>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.CarRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор автомобиля.</param>
        /// <returns>Экземпляр автомобиля.</returns>
        public CAR GetCar(short id)
        {
            return _unitOfWork.CarRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный автомобиль в хранилище данных
        /// и возвращает идентификатор добавленного автомобиля.
        /// </summary>
        /// <param name="car">Экземпляр автомобиля для добавления.</param>
        /// <returns>Идентификатор автомобиля.</returns>
        public short AddCar(CAR car,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CarRepository.Insert(car);
            _unitOfWork.SaveChanges(userId, userInfo);
            return car.CARID;
        }

        /// <summary>
        /// Обновляет информацию заданного автомобиля в хранилище данных.
        /// </summary>
        /// <param name="car">Экземпляр автомобиля для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateCar(CAR car,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CarRepository.UpdateSimpleProperties(car, car.CARID);
            _unitOfWork.SaveChanges();
        }

        /// <summary>
        /// Удаляет автомобиль с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор автомобиля.</param>
        /// <returns> Task </returns>
        public void DeleteCar(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CarRepository.Delete(id);
            _unitOfWork.SaveChanges();
        }

        /// <summary>
        /// Проверяет наличие автомобиля в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска автомобиля.</param>
        /// <returns>Возвращает <see langword="true"/>, если автомобиль существует в хранилище данных.</returns>
        public bool CarExists(Expression<Func<CAR, bool>> filter)
        {
            return _unitOfWork.CarRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}