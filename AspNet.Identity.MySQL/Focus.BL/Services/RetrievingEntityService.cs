﻿using Focus.BL.Common.Services;
using Focus.BL.Enum;
using Focus.BL.Exceptions;
using Focus.BL.Extensions;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Focus.BL.Services
{
    public class RetrievingEntityService : IRetrievingEntityService
    {
        private readonly IMapperService _mapperService;

        public RetrievingEntityService()
        {
            _mapperService = new MapperService();
        }

        public IEnumerable<TEntity> RetrieveEntityVersions<TEntity>(IEnumerable<AUDIT_TRAIL> auditTrails) where TEntity : class
        {
            // Check either audit trails information has any content.
            if (auditTrails == null || auditTrails.Count() < 1)
            {
                return new List<TEntity>();
            }
            // Validate audit trails input information.
            ValidateAuditTrails(auditTrails);

            // Group audit trails by the time stamp information.
            Dictionary<DateTime?, List<AUDIT_TRAIL>> auditTrailGroupByTimeStamp = GroupByDateTime(auditTrails.ToList());

            // Create initial entity defined as created.
            TEntity entityCreated = RetrieveCreatedEntity<TEntity>(auditTrailGroupByTimeStamp.Values.FirstOrDefault());

            // Creating of the returning entity's versions collection.
            // Add recently created entity to the returning entity's versions collection.
            List<TEntity> entities = new List<TEntity>
            {
                entityCreated
            };

            // Create entity's instance defined as the 'entityPrevious'. It is needed for the creation modified version 
            // of the previous entity version as soon as in the audit trails information contains only changes for the
            // modified version of the entity but not all entity information for modification version.
            TEntity entityPrevious = ShadowClone(entityCreated);

            // Transform dictionary of the audit trails group grouped by time stamp
            // to the collection of the audit trails information.
            List<List<AUDIT_TRAIL>> auditTrailsLocal = new List<List<AUDIT_TRAIL>>();
            auditTrailsLocal.AddRange(auditTrailGroupByTimeStamp.Values.ToList());

            // Create modified and deleted versions of the entity instances.
            for (int i = 1; i < auditTrailsLocal.Count; i++)
            {
                if (IsModified(auditTrailsLocal[i]))
                {
                    TEntity entityNext = ApplyChangesToEntity(entityPrevious, auditTrailsLocal[i]);
                    entities.Add(entityNext);
                    entityPrevious = entityNext;
                }
                else if (IsDeleted(auditTrailsLocal[i]))
                {
                    TEntity entityClosed = RetrieveDeletedEntity<TEntity>(auditTrailsLocal[i]);
                    entities.Add(entityClosed);
                    continue;
                }
            }

            // Return resulting entity vesrions.
            return entities;
        }

        /// <summary>
        /// Transforms audit trail information in the created entity instance.
        /// </summary>
        /// <typeparam name="TEntity">Transforming entity type.</typeparam>
        /// <param name="auditTrails">Audit trails correspond created entity information.</param>
        /// <returns>Instance of the created entity.</returns>
        public TEntity RetrieveCreatedEntity<TEntity>(IEnumerable<AUDIT_TRAIL> auditTrails) where TEntity : class
        {
            Type entityType = GetType(auditTrails);
            TEntity createdEntity = Activator.CreateInstance(entityType) as TEntity;
            createdEntity.SetValue("LAST_UPDATE", auditTrails.FirstOrDefault().DATE_);
            string keyFieldName = new List<string>() { "FEATURE", "INSPECTOR", "POST", "STORAGE", "VERSION" }.Contains(entityType.Name) ? "ID" : entityType.Name + "ID";
            createdEntity.SetValue(keyFieldName, auditTrails.FirstOrDefault().ENTITYID);
            createdEntity.SetValue("USERINFO", auditTrails.FirstOrDefault().USERINFO);
            createdEntity.SetValue("SID", auditTrails.FirstOrDefault().SID);
            List<PropertyInfo> simplePropertyInfos = entityType.GetProperties().ToList();
            List<string> propertyNames = simplePropertyInfos.Select(p => p.Name).ToList();
            List<string> nestedClassNames = GetNestedClassTypeNames(createdEntity).ToList();
            List<string> collectionNames = GetCollectionTypeNames(createdEntity).ToList();

            foreach (AUDIT_TRAIL audit_Trail in auditTrails)
            {
                // Continue if information in audit trails does not correspond to the filling entity.
                if (!propertyNames.Contains(audit_Trail.FIELD))
                {
                    continue;
                }

                // Seed country.
                string[] countriesKeyPossibleNames = { "COUNTRYCODE", "COUNTRY_ID" };
                if (countriesKeyPossibleNames.Contains(audit_Trail.FIELD) && _mapperService.GetMap[entityType].Contains(typeof(COUNTRY)))
                {
                    string nestedClassId = audit_Trail.VALUENEW;
                    object nestedClass = CreateCountryInstance(nestedClassId);
                    createdEntity.SetValue(typeof(COUNTRY).Name, Convert.ChangeType(nestedClass, typeof(COUNTRY)));
                }
                else
                {
                    // There is no logic for the branch.
                }

                // Seed nested classes values.
                string possibleForeignKeyName = audit_Trail.FIELD.Replace("ID", string.Empty);
                bool isKey = possibleForeignKeyName == entityType.Name;
                if (GetType(possibleForeignKeyName) != null && !isKey)
                {
                    List<Type> childTypes = _mapperService.GetMap[entityType].ToList();
                    if (nestedClassNames.Contains(possibleForeignKeyName) && childTypes.Select(p => p.Name).Any(p => p == possibleForeignKeyName))
                    {
                        string nestedClassName = audit_Trail.FIELD.Replace("ID", string.Empty);
                        string keyPropertyName = audit_Trail.FIELD + "ID";
                        short nestedClassId = short.Parse(audit_Trail.VALUENEW);
                        object nestedClass = CreateInstance(nestedClassName, nestedClassId);

                        createdEntity.SetValue(nestedClassName, Convert.ChangeType(nestedClass, GetType(nestedClassName)));
                        //continue;
                    }
                    else
                    {
                        // There is no logic for the branch.
                    }
                }

                // Seed collection values.
                if (collectionNames.Contains(audit_Trail.FIELD) &&
                    _mapperService.GetCollection.ContainsKey(entityType) &&
                    _mapperService.GetCollection[entityType].Select(p => p.Name).Any(p => p == audit_Trail.FIELD))
                {
                    string genericTypeName = audit_Trail.FIELD;
                    string entityCollectionPropertyName = audit_Trail.FIELD;
                    // Create collection.
                    object genericCollection = CreateCollectionInstance(genericTypeName);

                    short[] indexCollection = audit_Trail.VALUENEW.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => short.Parse(p)).OrderBy(p => p).ToArray();
                    
                    // Add objects to the collection which were created by indexes contains in the audit trails information.
                    foreach (short index in indexCollection)
                    {
                        object collectionItem = CreateInstance(genericTypeName, index);
                        object[] parametersArray = new object[] { collectionItem };
                        MethodInfo methodInfo = genericCollection.GetType().GetMethod("Add");
                        methodInfo.Invoke(genericCollection, parametersArray);
                    }

                    // Set collection to the deleted entity.
                    createdEntity.SetValue(entityCollectionPropertyName, genericCollection);
                    continue;
                }
                else
                {
                    // There is no logic for the branch.
                }

                createdEntity.SetValue(audit_Trail.FIELD, audit_Trail.VALUENEW);
            }
            return createdEntity;
        }

        /// <summary>
        /// Create generic object ICollection type.
        /// </summary>
        /// <param name="genericTypeName">Generic type for the collection.</param>
        /// <returns>Object that represents generic collection type.</returns>
        public ICollection<TEntity> CreateCollectionInstance<TEntity>() where TEntity : class
        {
            Type genericType = typeof(TEntity);
            Type collectionType = typeof(List<>);
            Type contructedCOllectionType = collectionType.MakeGenericType(genericType);
            return (ICollection<TEntity>)Activator.CreateInstance(contructedCOllectionType);
        }

        /// <summary>
        /// Create object ICollection generic typed.
        /// </summary>
        /// <param name="genericTypeName">Generic type of the collection.</param>
        /// <returns>Object that represents generic collection type.</returns>
        public object CreateCollectionInstance(string collectionGenericTypeName)
        {
            Type genericType = GetType(collectionGenericTypeName);
            Type collectionType = typeof(List<>);
            Type contructedCOllectionType = collectionType.MakeGenericType(genericType);
            return Activator.CreateInstance(contructedCOllectionType);
        }

        /// <summary>
        /// Create instance of object of defined type with defined key value
        /// without any data property among key.
        /// </summary>
        /// <param name="instanceType">Type of being created instance.</param>
        /// <param name="key">Key value of being created  instance</param>
        /// <returns>Object instaqnce of the defined class type and key value.</returns>
        public object CreateInstance(string instanceType, short key)
        {
            object instance = Activator.CreateInstance(GetType(instanceType));
            string keyPropertyName = new List<string>() { "FEATURE", "INSPECTOR", "POST", "STORAGE", "VERSION" }.Contains(instanceType) ? "ID" : instanceType + "ID";
            short instanceId = key;
            if (instance.GetType().GetProperties().ToList().Select(p => p.Name).Contains(keyPropertyName))
            {
                instance.SetValue(keyPropertyName, instanceId);
            }
            return instance;
        }

        public object CreateCountryInstance(string id)
        {
            object instance = Activator.CreateInstance(typeof(COUNTRY));
            instance.SetValue("ABBR2", id);
            return instance;
        }

        /// <summary>
        /// Create instance of the COUNTRY type object with defined key value
        /// without any data property among key.
        /// </summary>
        /// <param name="key">Key value of being created COUNTRY type object instance</param>
        /// <returns>COUNTRY type object instance.</returns>
        public COUNTRY CreateCountry(string countryCode)
        {
            COUNTRY instance = Activator.CreateInstance(typeof(COUNTRY)) as COUNTRY;
            string keyPropertyName = "ABBR2";
            string instanceId = countryCode;
            instance.SetValue(keyPropertyName, instanceId);
            return instance;
        }

        /// <summary>
        /// Return nested class types collection of the some entity.
        /// </summary>
        /// <typeparam name="TEntity">Examined entity type.</typeparam>
        /// <param name="entity">Instance of the entity.</param>
        /// <returns>Properties' types of the nested classes of the some entity.</returns>
        public IEnumerable<string> GetNestedClassTypeNames<TEntity>(TEntity entity) where TEntity : class
        {
            Type entityType = entity.GetType();
            List<PropertyInfo> propertyInfos = entityType.GetMyProperties(
                p => p.PropertyType != typeof(string) && 
                p.PropertyType.IsClass && 
                !p.PropertyType.IsGenericType).
                ToList();
            List<string> nestedClassPropertyNames = new List<string>();
            nestedClassPropertyNames = propertyInfos.Select(p => p.Name).Except(GetCollectionTypeNames(entity)).ToList();
            return nestedClassPropertyNames;
        }

        /// <summary>
        /// Return nested collection types collection of the someentity.
        /// </summary>
        /// <typeparam name="TEntity">Examined entity type.</typeparam>
        /// <param name="entity">Instance of the entity.</param>
        /// <returns>Properties' types of the collection classes of the some entity.</returns>
        public IEnumerable<string> GetCollectionTypeNames<TEntity>(TEntity entity) where TEntity : class
        {
            Type entityType = entity.GetType();
            List<PropertyInfo> propertyInfos = entityType.GetMyProperties(p => p.PropertyType != typeof(string)).ToList();
            List<string> collectionPropertyNames = new List<string>();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                bool isCollection = propertyInfo.IsCollection();
                if (isCollection)
                {
                    collectionPropertyNames.Add(propertyInfo.Name);
                }
            }
            return collectionPropertyNames;
        }

        /// <summary>
        /// Transforms audit trail information in the deleted entity instance.
        /// </summary>
        /// <typeparam name="TEntity">Transforming entity type.</typeparam>
        /// <param name="auditTrails">Audit trails correspond deleted entity information.</param>
        /// <returns>Instance of the deleted entity.</returns>
        public TEntity RetrieveDeletedEntity<TEntity>(List<AUDIT_TRAIL> auditTrails) where TEntity : class
        {
            Type entityType = typeof(TEntity);
            TEntity deletedEntity = Activator.CreateInstance(entityType) as TEntity;
            List<PropertyInfo> propertyInfos = entityType.GetMyProperties(p => !p.PropertyType.IsClass || p.PropertyType == typeof(string)).ToList();
            List<string> propertyNames = propertyInfos.Select(p => p.Name).ToList();
            deletedEntity.SetValue("LAST_UPDATE", auditTrails.FirstOrDefault().DATE_.Value);
            List<string> nestedClassNames = GetNestedClassTypeNames(deletedEntity).ToList();
            List<string> collectionNames = GetCollectionTypeNames(deletedEntity).ToList();

            foreach (AUDIT_TRAIL audit_Trail in auditTrails)
            {
                // Continue if information in audit trails does not correspond to the filling entity.
                if (!propertyNames.Contains(audit_Trail.FIELD))
                {
                    continue;
                }

                // Seed country.
                string[] countriesKeyPossibleNames = { "COUNTRYCODE", "COUNTRY_ID" };
                if (countriesKeyPossibleNames.Contains(audit_Trail.FIELD) && _mapperService.GetMap[entityType].Contains(typeof(COUNTRY)))
                {
                    string nestedClassId = audit_Trail.VALUENEW;
                    object nestedClass = CreateCountryInstance(nestedClassId);

                    deletedEntity.SetValue(typeof(COUNTRY).Name, Convert.ChangeType(nestedClass, typeof(COUNTRY)));
                    //continue;
                }
                else
                {
                    // There is no logic for the branch.
                }

                // Seed nested classes values.
                string possibleForeignKeyName = audit_Trail.FIELD.Replace("ID", string.Empty);
                if (GetType(possibleForeignKeyName) != null)
                {
                    if (nestedClassNames.Contains(possibleForeignKeyName) && _mapperService.GetMap[entityType].Select(p => p.Name).Any(p => p == possibleForeignKeyName))
                    {
                        string nestedClassName = audit_Trail.FIELD.Replace("ID", string.Empty);
                        string keyPropertyName = audit_Trail.FIELD + "ID";
                        short nestedClassId = short.Parse(audit_Trail.VALUEOLD);
                        object nestedClass = CreateInstance(nestedClassName, nestedClassId);

                        deletedEntity.SetValue(nestedClassName, Convert.ChangeType(nestedClass, GetType(nestedClassName)));
                       // continue;
                    }
                    else
                    {
                        // There is no logic for the branch.
                    }
                }

                // Seed collection values.
                if (collectionNames.Contains(audit_Trail.FIELD) &&
                    _mapperService.GetCollection.ContainsKey(entityType) &&
                    _mapperService.GetCollection[entityType].Select(p => p.Name).Any(p => p == audit_Trail.FIELD))
                {
                    string genericTypeName = audit_Trail.FIELD;
                    string entityCollectionPropertyName = audit_Trail.FIELD;
                    // Create collection.
                    object genericCollection = CreateCollectionInstance(genericTypeName);

                    short[] indexCollection = audit_Trail.VALUEOLD.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => short.Parse(p)).OrderBy(p => p).ToArray();

                    // Add objects to the collection which were created by indexes contains in the audit trails information.
                    foreach (short index in indexCollection)
                    {
                        object collectionItem = CreateInstance(genericTypeName, index);
                        object[] parametersArray = new object[] { collectionItem };
                        MethodInfo methodInfo = genericCollection.GetType().GetMethod("Add");
                        methodInfo.Invoke(genericCollection, parametersArray);
                    }

                    // Set collection to the created entity.
                    deletedEntity.SetValue(entityCollectionPropertyName, genericCollection);
                    continue;
                }
                else
                {
                    // There is no logic for the branch.
                }
                if (!propertyNames.Contains(audit_Trail.FIELD))
                {
                    continue;
                }

                deletedEntity.SetValue(audit_Trail.FIELD, audit_Trail.VALUEOLD);
            }
            return deletedEntity;
        }

        /// <summary>
        /// Creates clone of the previous version if the entity instance and
        /// applies changes to it that are contained in the audit trails information
        /// passed to method like a parameter.
        /// </summary>
        /// <typeparam name="TEntity">Type of the new version of the entity that are being created.</typeparam>
        /// <param name="entityPrevious">Previous entity instance to which changes are being applied</param>
        /// <param name="changes">Audit trails contain information about changes for the previous version of the entity.</param>
        /// <returns>Modified entity instance.</returns>
        public TEntity ApplyChangesToEntity<TEntity>(TEntity entityPrevious, List<AUDIT_TRAIL> changes) where TEntity : class
        {
            TEntity entityNext = ShadowClone(entityPrevious);
            // Set time stamp.
            entityNext.SetValue("LAST_UPDATE", changes.FirstOrDefault().DATE_);
            Type entityType = entityNext.GetType();
            List<PropertyInfo> propertyInfos = entityType.GetMyProperties(p => !p.PropertyType.IsClass || p.PropertyType == typeof(string)).ToList();
            List<string> propertyNames = propertyInfos.Select(p => p.Name).ToList();
            List<string> nestedClassNames = GetNestedClassTypeNames(entityNext).ToList();
            List<string> collectionNames = GetCollectionTypeNames(entityNext).ToList();
            foreach (AUDIT_TRAIL change in changes)
            {
                if (!propertyNames.ToList().Contains(change.FIELD))
                {
                    continue;
                }
                else
                {
                    
                }

                // Seed country.
                string[] countriesKeyPossibleNames = { "COUNTRYCODE", "COUNTRY_ID" };
                if (countriesKeyPossibleNames.Contains(change.FIELD) && _mapperService.GetMap[entityType].Contains(typeof(COUNTRY)))
                {
                    string nestedClassId = change.VALUENEW;
                    object nestedClass = CreateCountryInstance(nestedClassId);

                    entityNext.SetValue(typeof(COUNTRY).Name, Convert.ChangeType(nestedClass, typeof(COUNTRY)));
                }
                else
                {
                    // There is no logic for the branch.
                }

                // Modified nested classes values.
                string possibleForeignKeyName = change.FIELD.Replace("ID", string.Empty);
                if (GetType(possibleForeignKeyName) != null)
                {
                    if (nestedClassNames.Contains(possibleForeignKeyName) && _mapperService.GetMap[entityType].Select(p => p.Name).Any(p => p == possibleForeignKeyName))
                    {
                        string nestedClassName = change.FIELD.Replace("ID", string.Empty);
                        string keyPropertyName = change.FIELD + "ID";
                        short nestedClassId = short.Parse(change.VALUENEW);
                        object nestedClass = CreateInstance(nestedClassName, nestedClassId);

                        entityNext.SetValue(nestedClassName, Convert.ChangeType(nestedClass, GetType(nestedClassName)));
                        //continue;
                    }
                    else
                    {
                        // There is no logic for the branch.
                    }
                }

                // MOdified collection values.
                if (collectionNames.Contains(change.FIELD) &&
                    _mapperService.GetCollection.ContainsKey(entityType) &&
                    _mapperService.GetCollection[entityType].Select(p => p.Name).Any(p => p == change.FIELD))
                {
                    string genericTypeName = change.FIELD;
                    string entityCollectionPropertyName = change.FIELD;
                    // Create collection.
                    object genericCollection = CreateCollectionInstance(genericTypeName);

                    short[] indexCollection = change.VALUENEW.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries).Select(p => short.Parse(p)).OrderBy(p => p).ToArray();

                    // Add objects to the collection which were created by indexes contains in the audit trails information.
                    foreach (short index in indexCollection)
                    {
                        object collectionItem = CreateInstance(genericTypeName, index);
                        object[] parametersArray = new object[] { collectionItem };
                        MethodInfo methodInfo = genericCollection.GetType().GetMethod("Add");
                        methodInfo.Invoke(genericCollection, parametersArray);
                    }

                    // Set collection to the created entity.
                    entityNext.SetValue(entityCollectionPropertyName, genericCollection);
                    continue;
                }
                else
                {
                    // There is no logic for the branch.
                }

                string propertyName = change.FIELD;
                entityNext.SetValue(propertyName, change.VALUENEW);
            }
            return entityNext;
        }

        /// <summary>
        /// Clones entity without nested classes and collections.
        /// </summary>
        /// <typeparam name="TEntity">Being cloned entity type.</typeparam>
        /// <param name="entity">Being cloned entity.</param>
        /// <returns>Clone of the entity without nested classes and collections.</returns>
        private TEntity ShadowClone<TEntity>(TEntity entity) where TEntity : class
        {
            if (entity == null)
            {
                return null;
            }
            TEntity clonedEntity = Activator.CreateInstance(entity.GetType()) as TEntity;
            // Copy shadow of fields values.
            Type entityType = entity.GetType();
            List<PropertyInfo> propertyInfos = entityType.GetProperties().ToList();
            foreach (PropertyInfo propertyInfo in propertyInfos)
            {
                string propertyName = propertyInfo.Name;
                clonedEntity.SetValue(propertyName, entity.GetValue(propertyName));
            }
            return clonedEntity;
        }

        /// <summary>
        /// Returns type of the entity that presented in the audit trail information.
        /// </summary>
        /// <param name="auditTrailsGroupWitDateTimeStamp">Element of the dictionary audit trail information that are grouped by time stamp.</param>
        /// <returns>Type of the entity that presented in the audit trail information.</returns>
        private Type GetType(KeyValuePair<DateTime?, List<AUDIT_TRAIL>> auditTrailsGroupWitDateTimeStamp)
        {
            if (auditTrailsGroupWitDateTimeStamp.Value == null || auditTrailsGroupWitDateTimeStamp.Value.Count < 1)
            {
                return null;
            }

            return GetType(auditTrailsGroupWitDateTimeStamp.Value.FirstOrDefault().ENTITY);
        }

        /// <summary>
        /// Returns type of the entity that presented in the audit trail information.
        /// </summary>
        /// <param name="auditTrailsGroupWitDateTimeStamp">Element of the dictionary audit trail information that are grouped by time stamp.</param>
        /// <returns>Type of the entity that presented in the audit trail information.</returns>
        private Type GetType(IEnumerable<AUDIT_TRAIL> auditTrails)
        {
            if (auditTrails == null || auditTrails.Count() < 1)
            {
                return null;
            }

            return GetType(auditTrails.FirstOrDefault().ENTITY);
        }

        /// <summary>
        /// Returns type which correspond with type name. Types are the entity types that are used in the project. >
        /// </summary>
        /// <param name="typeName">Type name.</param>
        /// <returns>Type that correspond type name.</returns
        private Type GetType(string typeName)
        {
            Dictionary<string, Type> typeFromName = new Dictionary<string, Type>();
            typeFromName.Add(typeof(DOC).Name, typeof(DOC));
            typeFromName.Add(typeof(CAR).Name, typeof(CAR));
            typeFromName.Add(typeof(CONTAINER).Name, typeof(CONTAINER));
            typeFromName.Add(typeof(COUNTRY).Name, typeof(COUNTRY));
            typeFromName.Add(typeof(CUSTOMBROCKER).Name, typeof(CUSTOMBROCKER));
            typeFromName.Add(typeof(DEVICE).Name, typeof(DEVICE));
            typeFromName.Add(typeof(DEVICETYPE).Name, typeof(DEVICETYPE));
            typeFromName.Add(typeof(DOCUMENT).Name, typeof(DOCUMENT));
            typeFromName.Add(typeof(DOCUMENTKIND).Name, typeof(DOCUMENTKIND));
            typeFromName.Add(typeof(FEATURE).Name, typeof(FEATURE));
            typeFromName.Add(typeof(VERSION).Name, typeof(VERSION));
            typeFromName.Add(typeof(INSPECTOR).Name, typeof(INSPECTOR));
            typeFromName.Add(typeof(ORGANIZATION).Name, typeof(ORGANIZATION));
            typeFromName.Add(typeof(PERMISSION).Name, typeof(PERMISSION));
            typeFromName.Add(typeof(POST).Name, typeof(POST));
            typeFromName.Add(typeof(REPORT).Name, typeof(REPORT));
            typeFromName.Add(typeof(STORAGE).Name, typeof(STORAGE));
            typeFromName.Add(typeof(VEHICLE).Name, typeof(VEHICLE));
            typeFromName.Add(typeof(VEHICLEKIND).Name, typeof(VEHICLEKIND));
            typeFromName.Add(typeof(VEHICLETYPE).Name, typeof(VEHICLETYPE));
            typeFromName.Add("INSPECTOR1", typeof(INSPECTOR));
            return typeFromName.ContainsKey(typeName) ? typeFromName[typeName] : null;
        }

        /// <summary>
        /// Checks whether audit group information for created version of entity or not.
        /// </summary>
        /// <param name="auditTrails">Exercising audit trail information.</param>
        /// <returns>True if audit group information for created version of the entity, false  - in the other case.</returns>
        private bool IsCreated(List<AUDIT_TRAIL> auditTrails)
        {
            if (auditTrails == null || auditTrails.Count < 1)
            {
                return false;
            }
            else
            {
                return auditTrails.FirstOrDefault().TYPE == (short)CrudOperation.Added ? true : false;
            }
        }

        /// <summary>
        /// Checks whether audit group information for the modifing version of entity or not.
        /// </summary>
        /// <param name="auditTrails">Exercising audit trail information.</param>
        /// <returns>True if audit group information for modifing version of the entity, false  - in the other case.</returns>
        private bool IsModified(List<AUDIT_TRAIL> auditTrails)
        {
            if (auditTrails == null || auditTrails.Count < 1)
            {
                return false;
            }
            else
            {
                return auditTrails.FirstOrDefault().TYPE == (short)CrudOperation.Modified ? true : false;
            }
        }

        /// <summary>
        /// Checks whether audit group information for deleting version of entity or not.
        /// </summary>
        /// <param name="auditTrails">Exercising audit trail information.</param>
        /// <returns>True if audit group information for deleting version of the entity, false  - in the other case.</returns>
        private bool IsDeleted(List<AUDIT_TRAIL> auditTrails)
        {
            if (auditTrails == null || auditTrails.Count < 1)
            {
                return false;
            }
            else
            {
                return auditTrails.FirstOrDefault().TYPE == (short)CrudOperation.Deleted ? true : false;
            }
        }

        /// <summary>
        /// Validates input data = audit trail information. If input data is not consistent
        /// exception will be erased.
        /// </summary>
        /// <param name="auditTrails">Audit trail information.</param>
        private void ValidateAuditTrails(IEnumerable<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroupById = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            if (audit_TrailsGroupById.Keys.Count > 1)
            {
                const string ERROR_MESSAGE_MORE_THAN_ONE_ID = "Exception have been erased during 'RetrievingEntityService()' working class while validating. Audition information has more than one id information about. ";
                throw new AuditTrailsNotConsistentException(ERROR_MESSAGE_MORE_THAN_ONE_ID);
            }
            Dictionary<string, List<AUDIT_TRAIL>> audit_TrailsGroupByEntity = auditTrails.GroupBy(p => p.ENTITY).ToDictionary(g => g.Key, g => g.ToList());
            if (audit_TrailsGroupByEntity.Keys.Count > 1)
            {
                const string ERROR_MESSAGE_MORE_THAN_ONE_ENTITY = "Exception have been erased during 'RetrievingEntityService()' working class while validating. Audition information has more than one entity information about. ";
                throw new AuditTrailsNotConsistentException(ERROR_MESSAGE_MORE_THAN_ONE_ENTITY);
            }
        }

        /// <summary>
        /// Converts audit records to audit records groups grouped by the time stamp of the 
        /// CRUD operations under the entity.
        /// </summary>
        /// <param name="audit_Trails">Audit records for certain entity.</param>
        /// <returns>Audit records groups grouped by the time stamp of the 
        /// CRUD operations under the entity.</returns>
        private Dictionary<DateTime?, List<AUDIT_TRAIL>> GroupByDateTime(List<AUDIT_TRAIL> audit_Trails)
        {
            // Group audit records in groups where key is a DateTime value and ordering groups by DateTime of the creation.
            Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroup = audit_Trails.GroupBy(p => p.DATE_).ToDictionary(g => g.Key, g => g.ToList());
            SortedDictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsGroupSorted = new SortedDictionary<DateTime?, List<AUDIT_TRAIL>>(audit_TrailsGroup);

            // Pump audit records group get above in new collection.
            Dictionary<DateTime?, List<AUDIT_TRAIL>> audit_TrailsResult = new Dictionary<DateTime?, List<AUDIT_TRAIL>>();
            foreach (KeyValuePair<DateTime?, List<AUDIT_TRAIL>> audit_Trail in audit_TrailsGroupSorted)
            {
                audit_TrailsResult.Add(audit_Trail.Key, audit_Trail.Value);
            }
            return audit_TrailsResult;
        }
    }
}
