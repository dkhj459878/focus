﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditCustombrockerService : IAuditCustombrockerService
    {
        #region Startup.
        private readonly IAudit_trailService _audit_TrailService;
        private readonly IRetrievingEntityOptimaService _retrievingEntityOptimaService;
        private readonly ICountryService _countryService;

        public AuditCustombrockerService(
            IRetrievingEntityOptimaService retrievingEntityOptimaService, IAudit_trailService audit_TrailService, ICountryService countryService
            )
        {
            _audit_TrailService = audit_TrailService;
            _retrievingEntityOptimaService = retrievingEntityOptimaService;
            _countryService = countryService;
        }
        #endregion

        public List<CUSTOMBROCKER> GetCustombrockerVersions(short id)
        {
            string entityTypeName = typeof(CUSTOMBROCKER).Name;

            // Retrieving from database audit records.
            List<AUDIT_TRAIL> audit_Trails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == entityTypeName && audit.ENTITYID == id).ToList();

            if (audit_Trails == null || audit_Trails.Count < 1)
            {
                return new List<CUSTOMBROCKER>();
            }

            List<CUSTOMBROCKER> custombrockers = new List<CUSTOMBROCKER>(_retrievingEntityOptimaService.RetrieveEntityVersions<CUSTOMBROCKER>(audit_Trails).ToList());

            // Seed collection elements.
            foreach (CUSTOMBROCKER custombrocker in custombrockers)
            {
                if (custombrocker.COUNTRY != null)
                {
                    COUNTRY country = _countryService.GetCountry(custombrocker.COUNTRY.ABBR2);
                    if (country != null)
                    {
                        custombrocker.COUNTRY = null;
                        custombrocker.COUNTRY = country;
                    }
                }
            }
            return custombrockers;
        }
    }
}
