﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с протоколированными событиями
    /// в хранилище данных.
    /// </summary>
    public class ReportService : BaseService, IReportService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="ReportManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="reportmanager"> manager </param>
        public ReportService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все протоколированные события
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров протоколированных событий для пейджинга.</returns>
        public IEnumerable<REPORT> GetAllReports(
            Expression<Func<REPORT, bool>> filter = null,
            Func<IQueryable<REPORT>, IOrderedQueryable<REPORT>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.ReportRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает протоколированное событие с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о протоколированных событиях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о протоколированных событиях.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр протоколированного события.</returns>
        public IEnumerable<REPORT> GetReportsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<REPORT, bool>> filter = null,
            Func<IQueryable<REPORT>, IOrderedQueryable<REPORT>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.ReportRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает протоколированные события с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор протоколированные события.</param>
        /// <returns>Экземпляр протоколированные события.</returns>
        public REPORT GetReport(short id)
        {
            return _unitOfWork.ReportRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный протоколированные события в хранилище данных
        /// и возвращает идентификатор добавленного протоколированные события.
        /// </summary>
        /// <param name="report">Экземпляр протоколированного события для добавления.</param>
        /// <returns>Идентификатор протоколированного события.</returns>
        public short AddReport(REPORT report,
             string userId,
             string userInfo)
        {
            _unitOfWork.ReportRepository.Insert(report);
            _unitOfWork.SaveChanges(userId, userInfo);
            return report.REPORTID;
        }

        /// <summary>
        /// Обновляет информацию заданного протоколированного события в хранилище данных.
        /// </summary>
        /// <param name="report">Экземпляр протоколированного события для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateReport(REPORT report,
             string userId,
             string userInfo)
        {
            _unitOfWork.ReportRepository.UpdateSimpleProperties(report, report.REPORTID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет протоколированные события с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор протоколированного события.</param>
        /// <returns> Task </returns>
        public void DeleteReport(short id,
             string userId,
             string userInfo)
        {
            _unitOfWork.ReportRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие протоколированные события в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска протоколированного события.</param>
        /// <returns>Возвращает <see langword="true"/>, если протоколированные события существует в хранилище данных.</returns>
        public bool ReportExists(Expression<Func<REPORT, bool>> filter)
        {
            return _unitOfWork.ReportRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}