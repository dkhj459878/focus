﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с документами
    /// в хранилище данных.
    /// </summary>
    public class DocumentService : BaseService, IDocumentService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="DocumentManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="documentmanager"> manager </param>
        public DocumentService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все документы
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров документов для пейджинга.</returns>
        public IEnumerable<DOCUMENT> GetAllDocuments(
            Expression<Func<DOCUMENT, bool>> filter = null,
            Func<IQueryable<DOCUMENT>, IOrderedQueryable<DOCUMENT>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DocumentRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает документ с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о документах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документах.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр документа.</returns>
        public IEnumerable<DOCUMENT> GetDocumentsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DOCUMENT, bool>> filter = null,
            Func<IQueryable<DOCUMENT>, IOrderedQueryable<DOCUMENT>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DocumentRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает документ с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        public DOCUMENT GetDocument(short id)
        {
            return _unitOfWork.DocumentRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный документ в хранилище данных
        /// и возвращает идентификатор добавленного документа.
        /// </summary>
        /// <param name="document">Экземпляр документа для добавления.</param>
        /// <returns>Идентификатор документа.</returns>
        public short AddDocument(DOCUMENT document,
            string userId = "",
            string userInfo = "")
        {            
            var permissions = new List<PERMISSION>();
            foreach (var permission in document.PERMISSION)
            {
                permissions.Add(_unitOfWork.PermissionRepository.GetById(permission.PERMISSIONID));
            }



            var organizations = new List<ORGANIZATION>();
            foreach (var organization in document.ORGANIZATION)
            {
                organizations.Add(_unitOfWork.OrganizationRepository.GetById(organization.ORGANIZATIONID));
            }



            document.PERMISSION = null;
            document.ORGANIZATION = null;

            _unitOfWork.DocumentRepository.Insert(document);

            try
            {
                _unitOfWork.SaveChanges(userId, userInfo);

                document.PERMISSION = permissions;
                document.ORGANIZATION = organizations;

                // Your code...
                // Could also be before try if you know the exception occurs in SaveChanges

                _unitOfWork.SaveChanges(userId, userInfo);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            return document.DOCUMENTID;
        }

        /// <summary>
        /// Обновляет информацию заданного документа в хранилище данных.
        /// </summary>
        /// <param name="document">Экземпляр документа для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateDocument(DOCUMENT document,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DocumentRepository.UpdateSimpleProperties(document, document.DOCUMENTID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет документ с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns> Task </returns>
        public void DeleteDocument(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DocumentRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие документа в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска документа.</param>
        /// <returns>Возвращает <see langword="true"/>, если документ существует в хранилище данных.</returns>
        public bool DocumentExists(Expression<Func<DOCUMENT, bool>> filter)
        {
            return _unitOfWork.DocumentRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}