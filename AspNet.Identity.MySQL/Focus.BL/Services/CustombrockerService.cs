﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с представителями
    /// в хранилище данных.
    /// </summary>
    public class CustombrockerService : BaseService, ICustombrockerService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="CustombrockerManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="custombrockermanager"> manager </param>
        public CustombrockerService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает всех представителей
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров представителей для пейджинга.</returns>
        public IEnumerable<CUSTOMBROCKER> GetAllCustombrockers(
            Expression<Func<CUSTOMBROCKER, bool>> filter = null,
            Func<IQueryable<CUSTOMBROCKER>, IOrderedQueryable<CUSTOMBROCKER>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.CustombrockerRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает представителя с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о представителях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о представителях.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр представителя.</returns>
        public IEnumerable<CUSTOMBROCKER> GetCustombrockersPage(
            int pageSize,
            int pageNumber,
            Expression<Func<CUSTOMBROCKER, bool>> filter = null,
            Func<IQueryable<CUSTOMBROCKER>, IOrderedQueryable<CUSTOMBROCKER>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.CustombrockerRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает представителя с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор представителя.</param>
        /// <returns>Экземпляр ТСТК.</returns>
        public CUSTOMBROCKER GetCustombrocker(short id)
        {
            return _unitOfWork.CustombrockerRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный представителя в хранилище данных
        /// и возвращает идентификатор добавленного представителя.
        /// </summary>
        /// <param name="custombrocker">Экземпляр представителя добавления.</param>
        /// <returns>Идентификатор представителя.</returns>
        public short AddCustombrocker(CUSTOMBROCKER custombrocker,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CustombrockerRepository.Insert(custombrocker);
            _unitOfWork.SaveChanges(userId, userInfo);
            return custombrocker.CUSTOMBROCKERID;
        }

        /// <summary>
        /// Обновляет информацию заданного представителя в хранилище данных.
        /// </summary>
        /// <param name="custombrocker">Экземпляр представителя для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateCustombrocker(CUSTOMBROCKER custombrocker,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CustombrockerRepository.UpdateSimpleProperties(custombrocker, custombrocker.CUSTOMBROCKERID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет представителя с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор представителя.</param>
        /// <returns> Task </returns>
        public void DeleteCustombrocker(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CustombrockerRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие представителя в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска представителя.</param>
        /// <returns>Возвращает <see langword="true"/>, если представитель существует в хранилище данных.</returns>
        public bool CustombrockerExists(Expression<Func<CUSTOMBROCKER, bool>> filter)
        {
            return _unitOfWork.CustombrockerRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}