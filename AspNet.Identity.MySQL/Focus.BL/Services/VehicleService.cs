﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с транспортными средствами
    /// в хранилище данных.
    /// </summary>
    public class VehicleService : BaseService, IVehicleService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="VehicleManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="vehiclemanager"> manager </param>
        public VehicleService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все транспортные средства
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров транспортных средств для пейджинга.</returns>
        public IEnumerable<VEHICLE> GetAllVehicles(
            Expression<Func<VEHICLE, bool>> filter = null,
            Func<IQueryable<VEHICLE>, IOrderedQueryable<VEHICLE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.VehicleRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает ТСТК с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о транспортных средствах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о транспортных средствах.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр транспортного средства.</returns>
        public IEnumerable<VEHICLE> GetVehiclesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<VEHICLE, bool>> filter = null,
            Func<IQueryable<VEHICLE>, IOrderedQueryable<VEHICLE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.VehicleRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает транспортное средство с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор транспортного средства.</param>
        /// <returns>Экземпляр ТСТК.</returns>
        public VEHICLE GetVehicle(short id)
        {
            return _unitOfWork.VehicleRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданное транспортное средство в хранилище данных
        /// и возвращает идентификатор добавленного ТСТК.
        /// </summary>
        /// <param name="vehicle">Экземпляр транспортного средства для добавления.</param>
        /// <returns>Идентификатор транспортного средства.</returns>
        public short AddVehicle(VEHICLE vehicle,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehicleRepository.Insert(vehicle);
            _unitOfWork.SaveChanges(userId, userInfo);
            return vehicle.VEHICLEID;
        }

        /// <summary>
        /// Обновляет информацию заданного транспортного средства в хранилище данных.
        /// </summary>
        /// <param name="vehicle">Экземпляр транспортного средства для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateVehicle(VEHICLE vehicle,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehicleRepository.UpdateSimpleProperties(vehicle, vehicle.VEHICLEID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет транспортное средство с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор транспортного средства.</param>
        /// <returns> Task </returns>
        public void DeleteVehicle(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.VehicleRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие транспортного средства в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска ТСТК.</param>
        /// <returns>Возвращает <see langword="true"/>, если транспортное средство существует в хранилище данных.</returns>
        public bool VehicleExists(Expression<Func<VEHICLE, bool>> filter)
        {
            return _unitOfWork.VehicleRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}