﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с разрешениями
    /// в хранилище данных.
    /// </summary>
    public class PermissionService : BaseService, IPermissionService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="PermissionManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="permissionmanager"> manager </param>
        public PermissionService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все разрешениями
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров разрешений для пейджинга.</returns>
        public IEnumerable<PERMISSION> GetAllPermissions(
            Expression<Func<PERMISSION, bool>> filter = null,
            Func<IQueryable<PERMISSION>, IOrderedQueryable<PERMISSION>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.PermissionRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает разрешения с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о разрешениях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о разрешении.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр разрешения.</returns>
        public IEnumerable<PERMISSION> GetPermissionsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<PERMISSION, bool>> filter = null,
            Func<IQueryable<PERMISSION>, IOrderedQueryable<PERMISSION>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.PermissionRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает разрешения с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор разрешения.</param>
        /// <returns>Экземпляр разрешение.</returns>
        public PERMISSION GetPermission(short id)
        {
            return _unitOfWork.PermissionRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный разрешение в хранилище данных
        /// и возвращает идентификатор добавленного разрешения.
        /// </summary>
        /// <param name="permission">Экземпляр разрешения для добавления.</param>
        /// <returns>Идентификатор разрешения.</returns>
        public short AddPermission(PERMISSION permission,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.PermissionRepository.Insert(permission);
            _unitOfWork.SaveChanges(userId, userInfo);
            return permission.PERMISSIONID;
        }

        /// <summary>
        /// Обновляет информацию заданного разрешения в хранилище данных.
        /// </summary>
        /// <param name="permission">Экземпляр разрешения для обновления.</param>
        /// <returns> Task </returns>
        public void UpdatePermission(PERMISSION permission,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.PermissionRepository.UpdateSimpleProperties(permission, permission.PERMISSIONID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет разрешение с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор разрешения.</param>
        /// <returns> Task </returns>
        public void DeletePermission(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.PermissionRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие разрешения в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска разрешения.</param>
        /// <returns>Возвращает <see langword="true"/>, если разрешение существует в хранилище данных.</returns>
        public bool PermissionExists(Expression<Func<PERMISSION, bool>> filter)
        {
            return _unitOfWork.PermissionRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}