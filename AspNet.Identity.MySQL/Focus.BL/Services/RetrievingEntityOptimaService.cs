﻿using Focus.BL.Common.Services;
using Focus.BL.Exceptions;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public class RetrievingEntityOptimaService : IRetrievingEntityOptimaService
    {
        private readonly IMapperService _mapperService;
        private readonly IRetrievingEntityService _retrievingEntityService;

        public RetrievingEntityOptimaService(IRetrievingEntityService retrievingEntityService)
        {
            _mapperService = new MapperService();
            _retrievingEntityService = retrievingEntityService;
        }

        public IEnumerable<TEntity> RetrieveEntityVersions<TEntity>(IEnumerable<AUDIT_TRAIL> auditTrails) where TEntity : class
        {
            // Check either audit trails information has any content.
            if (auditTrails == null || auditTrails.Count() < 1)
            {
                return new List<TEntity>();
            }
            // Validate audit trails input information.
            ValidateAuditTrails(auditTrails);

            if (!HasCreationInfo(auditTrails))
            {
                return new List<TEntity>();
            }
            else
            {
                return _retrievingEntityService.RetrieveEntityVersions<TEntity>(auditTrails);
            }
        }

        private bool HasCreationInfo(IEnumerable<AUDIT_TRAIL> auditTrails)
        {
            const short CREATION_VERSION_TYPE = 0; 
            return auditTrails.ToList().Any(a => a.TYPE == CREATION_VERSION_TYPE);
        }       
        
        /// <summary>
                 /// Validates input data = audit trail information. If input data is not consistent
                 /// exception will be erased.
                 /// </summary>
                 /// <param name="auditTrails">Audit trail information.</param>
        private void ValidateAuditTrails(IEnumerable<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroupById = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            if (audit_TrailsGroupById.Keys.Count > 1)
            {
                const string ERROR_MESSAGE_MORE_THAN_ONE_ID = "Exception have been erased during 'RetrievingEntityService()' working class while validating. Audition information has more than one id information about. ";
                throw new AuditTrailsNotConsistentException(ERROR_MESSAGE_MORE_THAN_ONE_ID);
            }
            Dictionary<string, List<AUDIT_TRAIL>> audit_TrailsGroupByEntity = auditTrails.GroupBy(p => p.ENTITY).ToDictionary(g => g.Key, g => g.ToList());
            if (audit_TrailsGroupByEntity.Keys.Count > 1)
            {
                const string ERROR_MESSAGE_MORE_THAN_ONE_ENTITY = "Exception have been erased during 'RetrievingEntityService()' working class while validating. Audition information has more than one entity information about. ";
                throw new AuditTrailsNotConsistentException(ERROR_MESSAGE_MORE_THAN_ONE_ENTITY);
            }
        }
    }
}
