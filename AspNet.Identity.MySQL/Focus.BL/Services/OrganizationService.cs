﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с организациями
    /// в хранилище данных.
    /// </summary>
    public class OrganizationService : BaseService, IOrganizationService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="OrganizationManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="organizationmanager"> manager </param>
        public OrganizationService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все организации
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров организаций для пейджинга.</returns>
        public IEnumerable<ORGANIZATION> GetAllOrganizations(
            Expression<Func<ORGANIZATION, bool>> filter = null,
            Func<IQueryable<ORGANIZATION>, IOrderedQueryable<ORGANIZATION>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.OrganizationRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает организацию с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об организациях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями об организации.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр организации.</returns>
        public IEnumerable<ORGANIZATION> GetOrganizationsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<ORGANIZATION, bool>> filter = null,
            Func<IQueryable<ORGANIZATION>, IOrderedQueryable<ORGANIZATION>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.OrganizationRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает организацию с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор организации.</param>
        /// <returns>Экземпляр организации.</returns>
        public ORGANIZATION GetOrganization(short id)
        {
            return _unitOfWork.OrganizationRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданную организацию в хранилище данных
        /// и возвращает идентификатор добавленной организации.
        /// </summary>
        /// <param name="organization">Экземпляр организации для добавления.</param>
        /// <returns>Идентификатор организации.</returns>
        public short AddOrganization(ORGANIZATION organization,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.OrganizationRepository.Insert(organization);
            _unitOfWork.SaveChanges(userId, userInfo);
            return organization.ORGANIZATIONID;
        }

        /// <summary>
        /// Обновляет информацию заданной организации в хранилище данных.
        /// </summary>
        /// <param name="organization">Экземпляр организации для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateOrganization(ORGANIZATION organization,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.OrganizationRepository.UpdateSimpleProperties(organization, organization.ORGANIZATIONID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет организацию с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор организации.</param>
        /// <returns> Task </returns>
        public void DeleteOrganization(short id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.OrganizationRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие организации в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска организации.</param>
        /// <returns>Возвращает <see langword="true"/>, если организация существует в хранилище данных.</returns>
        public bool OrganizationExists(Expression<Func<ORGANIZATION, bool>> filter)
        {
            return _unitOfWork.OrganizationRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}