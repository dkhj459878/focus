﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private const string FIELD_DEVICE_ENTITY_NAME = "DEVICE";
        private Dictionary<short?, IEnumerable<DEVICE>> GetDevices(IEnumerable<short?> deviceIdentities)
        {
            // Беру все по истории.
            List<AUDIT_TRAIL> auditTrails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == FIELD_DEVICE_ENTITY_NAME && !deviceIdentities.ToList().Contains(audit.ENTITYID)).ToList();
            // Смотрю то, чего нет в истории.
            IEnumerable<short?> devicesIdsNotInAudit = deviceIdentities.ToList().Intersect(auditTrails.Select(c => c.ENTITYID).Distinct());
            // Беру их bз базы.
            Dictionary<short?, IEnumerable<DEVICE>> devicesInDb = GetDevicesFromDatabase(devicesIdsNotInAudit);
            Dictionary<short?, IEnumerable<DEVICE>> devicesInAudit = GetDevicesFromAudit(auditTrails);
            Dictionary<short?, IEnumerable<DEVICE>> devices = new Dictionary<short?, IEnumerable<DEVICE>>(devicesInDb);
            // Конкотеначу их.
            foreach (KeyValuePair<short?, IEnumerable<DEVICE>> device in devicesInDb)
            {
                devices.Add(device.Key, device.Value);
            };
            return devices;
        }

        private Dictionary<short?, IEnumerable<DEVICE>> GetDevicesFromAudit(List<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, IEnumerable<DEVICE>> devices = new Dictionary<short?, IEnumerable<DEVICE>>();
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroup = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (short? deviceId in audit_TrailsGroup.Keys)
            {
                devices.Add(deviceId, _retrievingEntityService.RetrieveEntityVersions<DEVICE>(auditTrails));
            }
            return devices;
        }

        private Dictionary<short?, IEnumerable<DEVICE>> GetDevicesFromDatabase(IEnumerable<short?> devicesIdEpsentInAuditTrails)
        {
            IEnumerable<DEVICE> devices = _deviceService.GetAllDevices(filter: device => devicesIdEpsentInAuditTrails.ToList().Contains(device.DEVICEID));
            if (devices == null || devices.Count() < 1)
            {
                return new Dictionary<short?, IEnumerable<DEVICE>>();
            }
            Dictionary<short?, IEnumerable<DEVICE>> devicesDictionary = new Dictionary<short?, IEnumerable<DEVICE>>();
            Dictionary<short, List<DEVICE>> devicesGroup = devices.GroupBy(p => p.DEVICEID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (KeyValuePair<short, List<DEVICE>> device in devicesGroup)
            {
                devicesDictionary.Add(device.Key, device.Value);
            }
            return devicesDictionary;
        }
    }
}
