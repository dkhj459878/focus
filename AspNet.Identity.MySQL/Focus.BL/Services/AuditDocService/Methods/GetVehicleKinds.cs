﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        const string FIELD_VEHICLEKIND_ENTITY_NAME = "VEHICLEKIND";
        private Dictionary<short?, IEnumerable<VEHICLEKIND>> GetVehicleKinds(IEnumerable<short?> vehiclekindIdentities)
        {
            // Беру все по истории.
            List<AUDIT_TRAIL> auditTrails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == FIELD_VEHICLEKIND_ENTITY_NAME && !vehiclekindIdentities.ToList().Contains(audit.ENTITYID)).ToList();
            // Смотрю то, чего нет в истории.
            IEnumerable<short?> vehiclekindsIdsNotInAudit = vehiclekindIdentities.ToList().Intersect(auditTrails.Select(c => c.ENTITYID).Distinct());
            // Беру их bз базы.
            Dictionary<short?, IEnumerable<VEHICLEKIND>> vehiclekindsInDb = GetVehiclekindsFromDatabase(vehiclekindsIdsNotInAudit);
            Dictionary<short?, IEnumerable<VEHICLEKIND>> vehiclekindsInAudit = GetVehiclekindsFromAudit(auditTrails);
            Dictionary<short?, IEnumerable<VEHICLEKIND>> vehiclekinds = new Dictionary<short?, IEnumerable<VEHICLEKIND>>(vehiclekindsInDb);
            // Конкотеначу их.
            foreach (KeyValuePair<short?, IEnumerable<VEHICLEKIND>> vehiclekind in vehiclekindsInDb)
            {
                vehiclekinds.Add(vehiclekind.Key, vehiclekind.Value);
            };
            return vehiclekinds;
        }

        private Dictionary<short?, IEnumerable<VEHICLEKIND>> GetVehiclekindsFromAudit(List<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, IEnumerable<VEHICLEKIND>> vehiclekinds = new Dictionary<short?, IEnumerable<VEHICLEKIND>>();
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroup = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (short? vehiclekindId in audit_TrailsGroup.Keys)
            {
                vehiclekinds.Add(vehiclekindId, _retrievingEntityService.RetrieveEntityVersions<VEHICLEKIND>(auditTrails));
            }
            return vehiclekinds;
        }

        private Dictionary<short?, IEnumerable<VEHICLEKIND>> GetVehiclekindsFromDatabase(IEnumerable<short?> vehiclekindsIdEpsentInAuditTrails)
        {
            IEnumerable<VEHICLEKIND> vehiclekinds = _vehiclekindService.GetAllVehiclekinds(filter: vehiclekind => vehiclekindsIdEpsentInAuditTrails.ToList().Contains(vehiclekind.VEHICLEKINDID));
            if (vehiclekinds == null || vehiclekinds.Count() < 1)
            {
                return new Dictionary<short?, IEnumerable<VEHICLEKIND>>();
            }
            Dictionary<short?, IEnumerable<VEHICLEKIND>> vehiclekindsDictionary = new Dictionary<short?, IEnumerable<VEHICLEKIND>>();
            Dictionary<byte, List<VEHICLEKIND>> vehiclekindsGroup = vehiclekinds.GroupBy(p => p.VEHICLEKINDID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (KeyValuePair<byte, List<VEHICLEKIND>> vehiclekind in vehiclekindsGroup)
            {
                vehiclekindsDictionary.Add(vehiclekind.Key, vehiclekind.Value);
            }
            return vehiclekindsDictionary;
        }
    }
}
