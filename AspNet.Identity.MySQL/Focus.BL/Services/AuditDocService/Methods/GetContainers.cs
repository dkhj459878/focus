﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private string FIELD_CONTAINER_ENTITY_NAME = "CONTAINER";
        private Dictionary<short?, IEnumerable<CONTAINER>> GetContainers(IEnumerable<short?> containerIdentities)
        {
            // Беру все по истории.
            List<AUDIT_TRAIL> auditTrails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == FIELD_CONTAINER_ENTITY_NAME && !containerIdentities.ToList().Contains(audit.ENTITYID)).ToList();
            // Смотрю то, чего нет в истории.
            IEnumerable<short?> containersIdsNotInAudit = containerIdentities.ToList().Intersect(auditTrails.Select(c => c.ENTITYID).Distinct());
            // Беру их bз базы.
            Dictionary<short?, IEnumerable<CONTAINER>> containersInDb = GetContainersFromDatabase(containersIdsNotInAudit);
            Dictionary<short?, IEnumerable<CONTAINER>> containersInAudit = GetContainersFromAudit(auditTrails);
            Dictionary<short?, IEnumerable<CONTAINER>> containers = new Dictionary<short?, IEnumerable<CONTAINER>>(containersInDb);
            // Конкотеначу их.
            foreach (KeyValuePair<short?, IEnumerable<CONTAINER>> container in containersInDb)
            {
                containers.Add(container.Key, container.Value);
            };
            return containers;
        }

        private Dictionary<short?, IEnumerable<CONTAINER>> GetContainersFromAudit(List<AUDIT_TRAIL> auditTrails)
        {
            Dictionary<short?, IEnumerable<CONTAINER>> containers = new Dictionary<short?, IEnumerable<CONTAINER>>();
            Dictionary<short?, List<AUDIT_TRAIL>> audit_TrailsGroup = auditTrails.GroupBy(p => p.ENTITYID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (short? containerId in audit_TrailsGroup.Keys)
            {
                containers.Add(containerId, _retrievingEntityService.RetrieveEntityVersions<CONTAINER>(auditTrails));
            }
            return containers;
        }

        private Dictionary<short?, IEnumerable<CONTAINER>> GetContainersFromDatabase(IEnumerable<short?> containersIdEpsentInAuditTrails)
        {
            IEnumerable<CONTAINER> containers = _containerService.GetAllContainers(filter: container => containersIdEpsentInAuditTrails.ToList().Contains(container.CONTAINERID));
            if (containers == null || containers.Count() < 1)
            {
                return new Dictionary<short?, IEnumerable<CONTAINER>>();
            }
            Dictionary<short?, IEnumerable<CONTAINER>> containersDictionary = new Dictionary<short?, IEnumerable<CONTAINER>>();
            Dictionary<short, List<CONTAINER>> containersGroup = containers.GroupBy(p => p.CONTAINERID).ToDictionary(g => g.Key, g => g.ToList());
            foreach (KeyValuePair<short, List<CONTAINER>> container in containersGroup)
            {
                containersDictionary.Add(container.Key, container.Value);
            }
            return containersDictionary;
        }
    }
}
