﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private Type GetType(string typeName)
        {
            Dictionary<string, Type> typeFromName = new Dictionary<string, Type>();
            typeFromName.Add(typeof(DOC).Name, typeof(DOC));
            typeFromName.Add(typeof(CAR).Name, typeof(CAR));
            typeFromName.Add(typeof(CONTAINER).Name, typeof(CONTAINER));
            typeFromName.Add(typeof(COUNTRY).Name, typeof(COUNTRY));
            typeFromName.Add(typeof(CUSTOMBROCKER).Name, typeof(CUSTOMBROCKER));
            typeFromName.Add(typeof(DEVICE).Name, typeof(DEVICE));
            typeFromName.Add(typeof(DOCUMENT).Name, typeof(DOCUMENT));
            typeFromName.Add(typeof(DOCUMENTKIND).Name, typeof(DOCUMENTKIND));
            typeFromName.Add(typeof(FEATURE).Name, typeof(FEATURE));
            typeFromName.Add(typeof(INSPECTOR).Name, typeof(INSPECTOR));
            typeFromName.Add(typeof(ORGANIZATION).Name, typeof(ORGANIZATION));
            typeFromName.Add(typeof(PERMISSION).Name, typeof(PERMISSION));
            typeFromName.Add(typeof(POST).Name, typeof(POST));
            typeFromName.Add(typeof(STORAGE).Name, typeof(STORAGE));
            typeFromName.Add(typeof(VEHICLE).Name, typeof(VEHICLE));
            typeFromName.Add(typeof(VEHICLEKIND).Name, typeof(VEHICLEKIND));
            typeFromName.Add(typeof(VEHICLETYPE).Name, typeof(VEHICLETYPE));
            return typeFromName.ContainsKey(typeName) ? typeFromName[typeName] : null;
        }
    }
}
