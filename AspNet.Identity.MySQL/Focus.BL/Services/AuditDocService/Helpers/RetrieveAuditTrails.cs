﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private IEnumerable<AUDIT_TRAIL> RetrieveAuditTrails(Dictionary<Type, IEnumerable<short?>> graph)
        {
            if (IsNullOrEmpty(graph))
            {
                return GetEmptyList<AUDIT_TRAIL>();
            }

            IEnumerable<AUDIT_TRAIL> auditTrails = new List<AUDIT_TRAIL>();
            foreach (Type type in graph.Keys)
            {
                List<AUDIT_TRAIL> auditTrailForType = new List<AUDIT_TRAIL>();
                auditTrailForType.AddRange(_audit_TrailService.GetAllAudit_trails(filter: a => (graph.Keys.Select(t => t.Name).Contains(a.ENTITY) && graph[type].Contains(a.ENTITYID))));
            }

            return auditTrails;
        }

    }
}
