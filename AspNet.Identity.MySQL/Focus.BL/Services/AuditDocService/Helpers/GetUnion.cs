﻿using Focus.BL.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        /// <summary>
        /// Get union of two graphs.
        /// </summary>
        /// <param name="graphFirst">First graph.</param>
        /// <param name="graphSecond">Ssecond graph.</param>
        /// <returns>Unioned graph.</returns>
        private Dictionary<Type, IEnumerable<short?>> GetUnion(Dictionary<Type, IEnumerable<short?>> graphFirst, Dictionary<Type, IEnumerable<short?>> graphSecond)
        {
            (bool IsNullOrEmpty, Dictionary<Type, IEnumerable<short?>> ReturningGraph) oneOfGraphsEmptyOrNull = OneOfGraphsEmptyOrNull(graphFirst, graphSecond);
            if (oneOfGraphsEmptyOrNull.IsNullOrEmpty)
            {
                return oneOfGraphsEmptyOrNull.ReturningGraph;
            }
            Dictionary<Type, IEnumerable<short?>> fromFirstGraph = new Dictionary<Type, IEnumerable<short?>>(graphFirst.Where(p => !graphSecond.Keys.ToList().Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value));
            Dictionary<Type, IEnumerable<short?>> fromSecondGraph = new Dictionary<Type, IEnumerable<short?>>(graphSecond.Where(p => !graphFirst.Keys.ToList().Contains(p.Key)).ToDictionary(p => p.Key, p => p.Value));
            Dictionary<Type, IEnumerable<short?>> intersectionGraphs = new Dictionary<Type, IEnumerable<short?>>();
            foreach (Type type in graphFirst.Keys.ToList().Intersect(graphSecond.Keys))
            {
                List<short?> intersectionIds = graphFirst[type].Union(graphSecond[type]).ToList();
                intersectionGraphs.Add(type, intersectionIds);
            }
            Dictionary<Type, IEnumerable<short?>> unionGraph = new Dictionary<Type, IEnumerable<short?>>();
            unionGraph.Union(fromFirstGraph);
            unionGraph.Union(fromSecondGraph);
            unionGraph.Union(intersectionGraphs);

            return unionGraph;
        }

        private (bool IsNullOrEmpty, Dictionary<Type, IEnumerable<short?>> ReturningGraph) OneOfGraphsEmptyOrNull(Dictionary<Type, IEnumerable<short?>> graphFirst, Dictionary<Type, IEnumerable<short?>> graphSecond)
        {
            if (IsNullOrEmpty(graphFirst))
            {
                if (IsNullOrEmpty(graphSecond))
                {
                    return (true, GetEmptyGraph());
                }
                else
                {
                    return (true, graphSecond);
                }
            }
            else
            {
                if (IsNullOrEmpty(graphSecond))
                {
                    return (true, graphFirst);
                }
            }
            return (false, null);
        }
    }
}
