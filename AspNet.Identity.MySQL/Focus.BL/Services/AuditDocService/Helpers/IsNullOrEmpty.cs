﻿using Focus.BL.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocService : IAuditDocService
    {
        private bool IsNullOrEmpty(Dictionary<Type, IEnumerable<short?>> graph)
        {
            if (graph == null || graph.Count() < 1)
            {
                return true;
            }
            return false;
        }

        private bool IsNullOrEmpty<TEntity>(IEnumerable<TEntity> collection)
        {
            if (collection == null || collection.Count() < 1)
            {
                return true;
            }
            return false;
        }

        private bool IsNullOrEmpty<TEntity>(Dictionary<short?, IEnumerable<TEntity>> tree)
        {
            if (tree == null || tree.Count() < 1)
            {
                return true;
            }
            return false;
        }
    }
}
