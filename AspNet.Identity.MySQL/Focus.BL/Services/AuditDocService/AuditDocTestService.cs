﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Focus.BL.Services
{
    public partial class AuditDocTestService : IAuditDocService
    {
        #region Startup.
        private readonly IAudit_trailService _audit_TrailService;
        private readonly IRequestService _requestService;
        private readonly IContainerService _containerService;
        private readonly ICarService _carService;
        private readonly IDeviceService _deviceService;
        private readonly ICountryService _countryService;
        private readonly IRetrievingEntityOptimaService _retrievingEntityOptimaService;
        private readonly IVehicleService _vehicleService;
        private readonly IDevicetypeService _devicetypeService;

        public AuditDocTestService(IAudit_trailService audit_TrailService, IRequestService requestService,
            IContainerService containerService, ICarService carService, IDeviceService deviceService,ICountryService countryService,
            IRetrievingEntityOptimaService retrievingEntityOptimaService,
            IVehicleService vehicleService,
            IDevicetypeService devicetypeService
            )
        {
            _audit_TrailService = audit_TrailService;
            _requestService = requestService;
            _carService = carService;
            _deviceService = deviceService;
            _countryService = countryService;
            _retrievingEntityOptimaService = retrievingEntityOptimaService;
            _vehicleService = vehicleService;
            _devicetypeService = devicetypeService;
            _containerService = containerService;
        }
        #endregion

        public List<DOC> GetDocVersions(short id)
        {
            string entityTypeName = typeof(DOC).Name;

            // Retrieving from database audit records.
            List<AUDIT_TRAIL> audit_Trails = _audit_TrailService.GetAllAudit_trails(filter: audit => audit.ENTITY == entityTypeName && audit.ENTITYID == id).ToList();

            if (audit_Trails == null || audit_Trails.Count < 1)
            {
                return new List<DOC>();
            }

            List<DOC> docs = new List<DOC>(_retrievingEntityOptimaService.RetrieveEntityVersions<DOC>(audit_Trails).ToList());

            // Seed collection elements.
            foreach (DOC doc in docs)
            {
                if (doc.CAR != null && doc.CAR.Count > 0)
                {
                    List<CAR> cars = new List<CAR>(doc.CAR);
                    List<CAR> carsLocal = new List<CAR>();
                    foreach (CAR car in cars)
                    {
                        CAR carLocal = _carService.GetCar(car.CARID);
                        if (carLocal != null)
                        {
                            carsLocal.Add(carLocal);
                        }
                    }
                    doc.CAR = null;
                    doc.CAR = carsLocal;
                }

                if (doc.CONTAINER != null && doc.CONTAINER.Count > 0)
                {
                    List<CONTAINER> containers = new List<CONTAINER>(doc.CONTAINER);
                    List<CONTAINER> containersLocal = new List<CONTAINER>();
                    foreach (CONTAINER container in containers)
                    {
                        CONTAINER containerLocal = _containerService.GetContainer(container.CONTAINERID);
                        if (containerLocal != null)
                        {
                            containersLocal.Add(containerLocal);
                        }
                    }
                    doc.CONTAINER = null;
                    doc.CONTAINER = containersLocal;
                }

                if (doc.DEVICE != null && doc.DEVICE.Count > 0)
                {
                    List<DEVICE> devices = new List<DEVICE>(doc.DEVICE);
                    List<DEVICE> devicesLocal = new List<DEVICE>();
                    foreach (DEVICE device in devices)
                    {
                        DEVICE deviceLocal = _deviceService.GetDevice(device.DEVICEID);
                        if (deviceLocal != null)
                        {
                            devicesLocal.Add(deviceLocal);
                        }
                    }
                    doc.DEVICE = null;
                    doc.DEVICE = devicesLocal;
                }

                if (doc.VEHICLE != null && doc.VEHICLE.Count > 0)
                {
                    List<VEHICLE> vehicles = new List<VEHICLE>(doc.VEHICLE);
                    List<VEHICLE> vehiclesLocal = new List<VEHICLE>();
                    foreach (VEHICLE vehicle in vehicles)
                    {
                        VEHICLE vehicleLocal = _vehicleService.GetVehicle(vehicle.VEHICLEID);
                        if (vehicleLocal != null)
                        {
                            vehiclesLocal.Add(vehicleLocal);
                        }
                    }
                    doc.VEHICLE = null;
                    doc.VEHICLE = vehiclesLocal;
                }
            }

            return docs;
        }
    }
}
