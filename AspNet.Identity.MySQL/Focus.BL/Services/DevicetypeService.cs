﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы с типами ТСТК
    /// в хранилище данных.
    /// </summary>
    public class DevicetypeService : BaseService, IDevicetypeService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="DevicetypeManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="devicetypemanager"> manager </param>
        public DevicetypeService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает все  типы ТСТК
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров  типов ТСТК для пейджинга.</returns>
        public IEnumerable<DEVICETYPE> GetAllDevicetypes(
            Expression<Func<DEVICETYPE, bool>> filter = null,
            Func<IQueryable<DEVICETYPE>, IOrderedQueryable<DEVICETYPE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DevicetypeRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает  типы ТСТК с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями об автомобилях.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр  типа ТСТК.</returns>
        public IEnumerable<DEVICETYPE> GetDevicetypesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DEVICETYPE, bool>> filter = null,
            Func<IQueryable<DEVICETYPE>, IOrderedQueryable<DEVICETYPE>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.DevicetypeRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает  тип ТСТК с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор  типа ТСТК.</param>
        /// <returns>Экземпляр  типа ТСТК.</returns>
        public DEVICETYPE GetDevicetype(byte id)
        {
            return _unitOfWork.DevicetypeRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданный  тип ТСТК в хранилище данных
        /// и возвращает идентификатор добавленного  типа ТСТК.
        /// </summary>
        /// <param name="devicetype">Экземпляр  типа ТСТК для добавления.</param>
        /// <returns>Идентификатор  типа ТСТК.</returns>
        public byte AddDevicetype(DEVICETYPE devicetype,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DevicetypeRepository.Insert(devicetype);
            _unitOfWork.SaveChanges(userId, userInfo);
            return devicetype.DEVICETYPEID;
        }

        /// <summary>
        /// Обновляет информацию заданного  типа ТСТК в хранилище данных.
        /// </summary>
        /// <param name="devicetype">Экземпляр  типа ТСТК для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateDevicetype(DEVICETYPE devicetype,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DevicetypeRepository.UpdateSimpleProperties(devicetype, devicetype.DEVICETYPEID);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет  типа ТСТК с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор  типа ТСТК.</param>
        /// <returns> Task </returns>
        public void DeleteDevicetype(byte id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.DevicetypeRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие типа ТСТК в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска типа ТСТК.</param>
        /// <returns>Возвращает <see langword="true"/>, если  тип ТСТК существует в хранилище данных.</returns>
        public bool DevicetypeExists(Expression<Func<DEVICETYPE, bool>> filter)
        {
            return _unitOfWork.DevicetypeRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}