﻿using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.UoW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Services
{
    /// <summary>
    /// Представляет сервис для работы со странами
    /// в хранилище данных.
    /// </summary>
    public class CountryService : BaseService, ICountryService
    {
        /// <summary>
        /// Создает новый экземпляр <see cref="CountryManagementService"/>
        /// с заданным unit of work.
        /// </summary>
        /// <param name="unitOfWork"> uow </param>
        /// <param name="countrymanager"> manager </param>
        public CountryService(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {
        }

        /// <summary>
        /// Возвращает страны
        /// из хранилища данных.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров стран для пейджинга.</returns>
        public IEnumerable<COUNTRY> GetAllCountries(
            Expression<Func<COUNTRY, bool>> filter = null,
            Func<IQueryable<COUNTRY>, IOrderedQueryable<COUNTRY>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.CountryRepository.Get(filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о странах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о странах.</param>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Экземпляр страны.</returns>
        public IEnumerable<COUNTRY> GetCountriesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<COUNTRY, bool>> filter = null,
            Func<IQueryable<COUNTRY>, IOrderedQueryable<COUNTRY>> orderBy = null,
            string includeProperties = "")
        {
            return _unitOfWork.CountryRepository.GetPage(pageSize, pageNumber, filter, orderBy, includeProperties);
        }

        /// <summary>
        /// Возвращает страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns>Экземпляр страны.</returns>
        public COUNTRY GetCountry(string id)
        {
            return _unitOfWork.CountryRepository.GetById(id);
        }

        /// <summary>
        /// Добавляет заданную страну в хранилище данных
        /// и возвращает идентификатор добавленной страны.
        /// </summary>
        /// <param name="country">Экземпляр страны для добавления.</param>
        /// <returns>Идентификатор страны.</returns>
        public string AddCountry(COUNTRY country,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CountryRepository.Insert(country);
            _unitOfWork.SaveChanges(userId, userInfo);
            return country.ABBR2;
        }

        /// <summary>
        /// Обновляет информацию заданной страны в хранилище данных.
        /// </summary>
        /// <param name="country">Экземпляр страны для обновления.</param>
        /// <returns> Task </returns>
        public void UpdateCountry(COUNTRY country,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CountryRepository.Update(country);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Удаляет страну с заданным идентификатором из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns> Task </returns>
        public void DeleteCountry(string id,
            string userId = "",
            string userInfo = "")
        {
            _unitOfWork.CountryRepository.Delete(id);
            _unitOfWork.SaveChanges(userId, userInfo);
        }

        /// <summary>
        /// Проверяет наличие страны в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска страны.</param>
        /// <returns>Возвращает <see langword="true"/>, если страна существует в хранилище данных.</returns>
        public bool CountryExists(Expression<Func<COUNTRY, bool>> filter)
        {
            return _unitOfWork.CountryRepository.Get(filter: filter)
                      .FirstOrDefault() != null;
        }
    }
}