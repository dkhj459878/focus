﻿using Focus.BL.Common.Services;
using Focus.BL.Common.Validators;
using Focus.BL.Services;
using Focus.BL.Validators;
using Ninject.Modules;

namespace Rocket.BL
{
    public class BLModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IDocService>().To<DocService>();
            Bind<ICarService>().To<CarService>();
            Bind<IContainerService>().To<ContainerService>();
            Bind<IDeviceService>().To<DeviceService>();
            Bind<IDevicetypeService>().To<DevicetypeService>();
            Bind<IVehicleService>().To<VehicleService>();
            Bind<IVehiclekindService>().To<VehiclekindService>();
            Bind<IVehicletypeService>().To<VehicletypeService>();
            Bind<ICountryService>().To<CountryService>();
            Bind<ICustombrockerService>().To<CustombrockerService>();
            Bind<IDocumentkindService>().To<DocumentkindService>();
            Bind<IDocumentService>().To<DocumentService>();
            Bind<IInspectorService>().To<InspectorService>();
            Bind<IOrganizationService>().To<OrganizationService>();
            Bind<IPermissionService>().To<PermissionService>();
            Bind<IPostService>().To<PostService>();
            Bind<IReportService>().To<ReportService>();
            Bind<IStorageService>().To<StorageService>();
            Bind<IFileDocumentService>().To<FileDocumentService>();
            Bind<IStringMaster>().To<StringMaster>();
            Bind<IAdService>().To<AdService>();
            Bind<IAudit_trailService>().To<Audit_trailService>();
            Bind<IVersionService>().To<VersionService>();
            Bind<IFeatureService>().To<FeatureService>();
            Bind<IMapperService>().To<MapperService>();
            Bind<IRetrievingEntityService>().To<RetrievingEntityService>();
            Bind<IRetrievingEntityOptimaService>().To<RetrievingEntityOptimaService>();
            Bind<IRequestService>().To<RequestService>();
            Bind<IUserService>().To<UserService>();
            Bind<IRoleService>().To<RoleService>();
            Bind<IUserRoleService>().To<UserRoleService>();

            // Audit services binding.
            Bind<IAuditCustombrockerService>().To<AuditCustombrockerService>();
            Bind<IAuditDeviceService>().To<AuditDeviceService>();
            Bind<IAuditDevicetypeService>().To<AuditDevicetypeService>();
            Bind<IAuditDocService>().To<AuditDocTestService>();
            Bind<IAuditDocumentService>().To<AuditDocumentService>();
            Bind<IAuditInspectorService>().To<AuditInspectorService>();
            Bind<IAuditOrganizationService>().To<AuditOrganizationService>();
            Bind<IAuditPostService>().To<AuditPostService>();
            Bind<IAuditStorageService>().To<AuditStorageService>();
            Bind<IAuditVehiclekindService>().To<AuditVehiclekindService>();
            Bind<IAuditVehicletypeService>().To<AuditVehicletypeService>();
        }
    }
}