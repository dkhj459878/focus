﻿//using System;

//namespace Focus.DAL.Comparison
//{
//    /// <summary>
//    /// Class that implements the comparison model
//    /// IUser interface 
//    /// </summary>
//    public class Comparison: ICloneable
//    {
//        /// <summary>
//        /// Default constructor 
//        /// </summary>
//        public Comparison()
//        {
//        }

//        /// <summary>
//        /// Custom brocker ID
//        /// </summary>
//        public int CUSTOMBROCKERID { get; set; }

//        /// <summary>
//        /// Custom brocker's last name.
//        /// </summary>
//        public string LASTNAME { get; set; }

//        /// <summary>
//        /// Custom brocker's first name.
//        /// </summary>
//        public string FIRSTNAME { get; set; }

//        /// <summary>
//        /// Custom brocker's farther name.
//        /// </summary>
//        public string FATHERSNAME { get; set; }

//        /// <summary>
//        /// Custom brocker's work place.
//        /// </summary>
//        public string WORKPLACE { get; set; }

//        /// <summary>
//        /// Custom brocker's post.
//        /// </summary>
//        public string POST { get; set; }

//        public string PASSPORTNUM { get; set; }

//        public string PASSPORTNUMBER { get; set; }

//        public string MISTAKE { get; set; }

//        public string PASSPORT2 { get; set; }

//        public string DATE2 { get; set; }

//        public string ISSUED2 { get; set; }

//        public string ADDRESS2 { get; set; }

//        public string PASSPORTISSUED { get; set; }

//        public string PASSPORTBODYISSUE { get; set; }

//        public string PASSPORT { get; set; }

//        public string ADDRESS_FULL { get; set; }

//        public string FIRSTNAMEFROMPASSPORT { get; set; }

//        public string LASTNAMEFROMPASSPORT { get; set; }

//        public string FARTHERNAMEFROMPASSPORT { get; set; }

//        public string ISSUEBODYFROMPASSPORT { get; set; }

//        public string ISSUEDFROMPASSPORT { get; set; }

//        public string ADDRESS { get; set; }

//        public string POST_INDEX { get; set; }

//        public string COUNTRY_ID { get; set; }

//        public string REGION { get; set; }

//        public string DISTRICT { get; set; }

//        public string ADDRESSFROMPASSPORT { get; set; }

//        public string CITY_TYPE { get; set; }

//        public string CITY { get; set; }

//        public string STREET_TYPE { get; set; }

//        public string STREET { get; set; }

//        public string BUILDING { get; set; }

//        public string BUILDING_BLOCK { get; set; }

//        public string OFFICE { get; set; }

//        public string PHONE { get; set; }

//        public string FAX { get; set; }

//        public string EMAIL { get; set; }

//        public string USERID { get; set; }

//        public DateTime LAST_UPDATE { get; set; }

//        public object Clone()
//        {
//            return new Comparison() {
//                CUSTOMBROCKERID = CUSTOMBROCKERID,
//                LASTNAME = LASTNAME,
//                FIRSTNAME = FIRSTNAME,
//                FATHERSNAME = FATHERSNAME,
//                WORKPLACE = WORKPLACE,
//                POST = POST,
//                PASSPORTNUM = PASSPORTNUM,
//                PASSPORTNUMBER = PASSPORTNUMBER,
//                MISTAKE = MISTAKE,
//                PASSPORT2 = PASSPORT2,
//                DATE2 = DATE2,
//                ISSUED2 = ISSUED2,
//                ADDRESS2 = ADDRESS2,
//                PASSPORTISSUED = PASSPORTISSUED,
//                PASSPORTBODYISSUE = PASSPORTBODYISSUE,
//                PASSPORT = PASSPORT,
//                ADDRESS_FULL = ADDRESS_FULL,
//                FIRSTNAMEFROMPASSPORT = FIRSTNAMEFROMPASSPORT,
//                LASTNAMEFROMPASSPORT = LASTNAMEFROMPASSPORT,
//                FARTHERNAMEFROMPASSPORT = FARTHERNAMEFROMPASSPORT,
//                ISSUEBODYFROMPASSPORT = ISSUEBODYFROMPASSPORT,
//                ISSUEDFROMPASSPORT = ISSUEDFROMPASSPORT,
//                ADDRESS = ADDRESS,
//                POST_INDEX = POST_INDEX,
//                COUNTRY_ID = COUNTRY_ID,
//                REGION = REGION,
//                DISTRICT = DISTRICT,
//                ADDRESSFROMPASSPORT = ADDRESSFROMPASSPORT,
//                CITY_TYPE = CITY_TYPE,
//                CITY = CITY,
//                STREET_TYPE = STREET_TYPE,
//                STREET = STREET,
//                BUILDING = BUILDING,
//                BUILDING_BLOCK = BUILDING_BLOCK,
//                OFFICE = OFFICE,
//                PHONE = PHONE,
//                FAX = FAX,
//                EMAIL = EMAIL,
//                UserId = UserId,
//                LAST_UPDATE = LAST_UPDATE
//            };
//        }
//    }
//}
