﻿//using System;
//using System.Collections.Generic;

//namespace Focus.DAL.Comparison
//{
//    /// <summary>
//    /// Class that represents the comparison table in the Access Database
//    /// </summary>
//    public class ComparisonTable 
//    {
//        private AccessDatabase _database;

//        /// <summary>
//        /// Constructor that takes a AccessDatabase instance 
//        /// </summary>
//        /// <param name="database"></param>
//        public ComparisonTable(AccessDatabase database)
//        {
//            _database = database;
//        }

//        /// <summary>
//        /// Deletes a custom borker from the comparison table
//        /// </summary>
//        /// <param name="roleId">The role Id</param>
//        /// <returns></returns>
//        public int Delete(int comperisonId)
//        {
//            string commandText = "Delete from custom brokers where Id = @id";
//            Dictionary<string, object> parameters = new Dictionary<string, object>();
//            parameters.Add("@id", comperisonId);

//            return _database.Execute(commandText, parameters);
//        }

//        /// <summary>
//        /// Inserts a new Role in the Roles table
//        /// </summary>
//        /// <param name="roleName">The role's name</param>
//        /// <returns></returns>
//        public int Insert(Comparison comparison)
//        {
//            string commandText = "Insert into Roles (CUSTOMBROCKERID, LASTNAME, FIRSTNAME, FATHERSNAME, WORKPLACE, POST, PASSPORTNUMBER, PASSPORTISSUED, PASSPORTBODYISSUE, ADDRESS, ADDRESS_FULL, POST_INDEX, COUNTRY_ID, DISTRICT,REGION, CITY_TYPE, CITY, STREET_TYPE, STREET, BUILDING, BUILDING_BLOCK, OFFICE, PHONE, FAX, EMAIL, UserId, LAST_UPDATE) values (@CUSTOMBROCKERID, @LASTNAME, @FIRSTNAME, @FATHERSNAME, @WORKPLACE, @POST, @PASSPORTNUMBER, @PASSPORTISSUED, @PASSPORTBODYISSUE, @ADDRESS, @ADDRESS_FULL, @POST_INDEX, @COUNTRY_ID, @DISTRICTM,REGION, @CITY_TYPE, @CITY, @STREET_TYPE, @STREET, @BUILDING, @BUILDING_BLOCK, @OFFICE, @PHONE, @FAX, @EMAIL, @UserId, @LAST_UPDATE)";
//            Dictionary<string, object> parameters = new Dictionary<string, object>();
//            parameters.Add("@CUSTOMBROCKERID", Convert.ToInt32(comparison.CUSTOMBROCKERID));
//            parameters.Add("@LASTNAME", comparison.LASTNAME);
//            parameters.Add("@FIRSTNAME", comparison.FIRSTNAME);
//            parameters.Add("@FATHERSNAME", comparison.FATHERSNAME);
//            parameters.Add("@WORKPLACE", comparison.WORKPLACE);
//            parameters.Add("@POST", comparison.POST);
//            parameters.Add("@PASSPORTNUMBER", comparison.PASSPORTNUMBER);
//            parameters.Add("@PASSPORTISSUED", comparison.PASSPORTISSUED);
//            parameters.Add("@PASSPORTBODYISSUE", comparison.PASSPORTBODYISSUE);
//            parameters.Add("@ADDRESS", comparison.ADDRESS);
//            parameters.Add("@ADDRESS_FULL", comparison.ADDRESS_FULL);
//            parameters.Add("@POST_INDEX", comparison.POST_INDEX);
//            parameters.Add("@COUNTRY_ID", comparison.COUNTRY_ID);
//            parameters.Add("@DISTRICT", comparison.DISTRICT);
//            parameters.Add("@REGION", comparison.REGION);
//            parameters.Add("@CITY_TYPE", comparison.CITY_TYPE);
//            parameters.Add("@CITY", comparison.CITY);
//            parameters.Add("@STREET_TYPE", comparison.STREET_TYPE);
//            parameters.Add("@STREET", comparison.STREET);
//            parameters.Add("@BUILDING", comparison.BUILDING);
//            parameters.Add("@BUILDING_BLOCK", comparison.BUILDING_BLOCK);
//            parameters.Add("@OFFICE", comparison.OFFICE);
//            parameters.Add("@PHONE", comparison.PHONE);
//            parameters.Add("@OFFICE", comparison.OFFICE);
//            parameters.Add("@FAX", comparison.FAX);
//            parameters.Add("@EMAIL", comparison.FAX);
//            parameters.Add("@UserId", comparison.UserId);
//            parameters.Add("@LAST_UPDATE", comparison.LAST_UPDATE);
//            return _database.Execute(commandText, parameters);
//        }

//        private DateTime ConvertToDateTime(string data)
//        {
//            const int DEFUALT_YEAR = 2001;
//            const int DEFAULT_MONTH = 1;
//            const int DEFAULT_DAY = 1;

//            if (string.IsNullOrEmpty(data))
//            { 
//                return new DateTime(DEFUALT_YEAR, DEFAULT_MONTH, DEFAULT_DAY);
//            }

//            return Convert.ToDateTime(data);
//        }

//        /// <summary>
//        /// Returns a customs brocker with comparison given the roleId
//        /// </summary>
//        /// <param name="roleId">The role Id</param>
//        /// <returns>Role name</returns>
//        public Comparison GetCustomBrockerWithComparison(int ComparisonId)
//        {
//            string commandText = "Select CUSTOMBROCKERID, FIRSTNAME, LASTNAME, FATHERSNAME, " + 
//                "WORKPLACE, POST, PASSPORTNUM, PASSPORTNUMBER, MISTAKE, PASSPORT2, DATE2, ISSUED2, " + 
//                "ADDRESS2, PASSPORTISSUED, PASSPORTBODYISSUE, PASSPORT , ADDRESS_FULL, FIRSTNAMEFROMPASSPORT, LASTNAMEFROMPASSPORT, FARTHERNAMEFROMPASSPORT, ISSUEBODYFROMPASSPORT, ISSUEDFROMPASSPORT, ADDRESS, POST_INDEX, COUNTRY_ID, DISTRICT, REGION , ADDRESSFROMPASSPORT, CITY_TYPE, CITY, STREET_TYPE, STREET, BUILDING, BUILDING_BLOCK, OFFICE, PHONE, FAX, EMAIL, UserId, LAST_UPDATE from customs where Id = @id";
//            Dictionary<string, object> parameters = new Dictionary<string, object>();
//            parameters.Add("@id", ComparisonId);
//            List<Dictionary<string, string>> comparisons = _database.Query(commandText, parameters);
//            Dictionary<string, string> comp;
//            Comparison comparison = new Comparison();
//            if (comparisons.Count == 0)
//            {
//                return null; 
//            }
//            comp = comparisons[0];
//            comparison.CUSTOMBROCKERID = Convert.ToInt32(comp["CUSTOMBROCKERID"]);
//            comparison.FIRSTNAME = comp["FIRSTNAME"];
//            comparison.LASTNAME = comp["LASTNAME"];
//            comparison.FATHERSNAME = comp["FATHERSNAME"];
//            comparison.WORKPLACE = comp["WORKPLACE"];
//            comparison.POST = comp["POST"];
//            comparison.PASSPORTNUM = comp["PASSPORTNUM"];
//            comparison.PASSPORTNUMBER = comp["PASSPORTNUMBER"];
//            comparison.MISTAKE = comp["MISTAKE"];
//            comparison.PASSPORT2 = comp["PASSPORT2"];
//            comparison.DATE2 = comp["DATE2"];
//            comparison.ISSUED2 = comp["ISSUED2"];
//            comparison.ADDRESS2 = comp["ADDRESS2"];
//            comparison.PASSPORTISSUED = comp["PASSPORTISSUED"];
//            comparison.PASSPORTBODYISSUE = comp["PASSPORTBODYISSUE"];
//            comparison.PASSPORT = comp["PASSPORT"];
//            comparison.ADDRESS_FULL = comp["ADDRESS_FULL"];
//            comparison.FIRSTNAMEFROMPASSPORT = comp["FIRSTNAMEFROMPASSPORT"];
//            comparison.LASTNAMEFROMPASSPORT = comp["LASTNAMEFROMPASSPORT"];
//            comparison.FARTHERNAMEFROMPASSPORT = comp["FARTHERNAMEFROMPASSPORT"];
//            comparison.ISSUEBODYFROMPASSPORT = comp["ISSUEBODYFROMPASSPORT"];
//            comparison.ISSUEDFROMPASSPORT = comp["ISSUEDFROMPASSPORT"];
//            comparison.ADDRESS = comp["ADDRESS"];
//            comparison.POST_INDEX = comp["POST_INDEX"];
//            comparison.COUNTRY_ID = comp["COUNTRY_ID"];
//            comparison.DISTRICT = comp["DISTRICT"];
//            comparison.REGION = comp["REGION"];
//            comparison.ADDRESSFROMPASSPORT = comp["ADDRESSFROMPASSPORT"];
//            comparison.CITY_TYPE = comp["CITY_TYPE"];
//            comparison.CITY = comp["CITY"];
//            comparison.STREET_TYPE = comp["STREET_TYPE"];
//            comparison.STREET = comp["STREET"];
//            comparison.BUILDING = comp["BUILDING"];
//            comparison.BUILDING_BLOCK = comp["BUILDING_BLOCK"];
//            comparison.OFFICE = comp["OFFICE"];
//            comparison.PHONE = comp["PHONE"];
//            comparison.FAX = comp["FAX"];
//            comparison.EMAIL = comp["EMAIL"];
//            comparison.UserId = comp["UserId"];
//            comparison.LAST_UPDATE = string.IsNullOrEmpty(comp["LAST_UPDATE"]) ? new DateTime(2001, 1, 1) : Convert.ToDateTime(comp["LAST_UPDATE"]);    

//            return comparison;
//        }

//        /// <summary>
//        /// Gets comparisons.
//        /// </summary>
//        /// <returns></returns>
//        public ICollection<Comparison> GetCustomBrockersWithComparison()
//        {
//            string commandText = "Select CUSTOMBROCKERID, FIRSTNAME, LASTNAME, FATHERSNAME, WORKPLACE, POST, PASSPORTNUM, PASSPORTNUMBER, MISTAKE, PASSPORT2, DATE2, ISSUED2, ADDRESS2, PASSPORTISSUED, PASSPORTBODYISSUE, PASSPORT, ADDRESS_FULL, FIRSTNAMEFROMPASSPORT, LASTNAMEFROMPASSPORT, FARTHERNAMEFROMPASSPORT, ISSUEBODYFROMPASSPORT, ISSUEDFROMPASSPORT, ADDRESS, POST_INDEX, COUNTRY_ID, DISTRICT, REGION, ADDRESSFROMPASSPORT, CITY_TYPE, CITY, STREET_TYPE, STREET, BUILDING, BUILDING_BLOCK, OFFICE, PHONE, FAX, EMAIL, UserId, LAST_UPDATE from customs";
//            ICollection<Comparison> comparisons = new List<Comparison>();
//            Dictionary<string, object> parameters = new Dictionary<string, object>() { };
//            var comparisons2 = _database.Query(commandText, parameters);
//            foreach (var item in comparisons2)
//            {
//                var comparison = new Comparison()
//                {
//                    CUSTOMBROCKERID = Convert.ToInt32(item["CUSTOMBROCKERID"]),
//                    FIRSTNAME = item["FIRSTNAME"].ToString(),
//                    LASTNAME = item["LASTNAME"].ToString(),
//                    FATHERSNAME = item["FATHERSNAME"].ToString(),
//                    WORKPLACE = item["WORKPLACE"].ToString(),
//                    POST = item["POST"].ToString(),
//                    PASSPORTNUM = item["PASSPORTNUM"].ToString(),
//                    PASSPORTNUMBER = item["PASSPORTNUMBER"].ToString(),
//                    MISTAKE = item["MISTAKE"].ToString(),
//                    PASSPORT2 = item["PASSPORT2"].ToString(),
//                    DATE2 = item["DATE2"].ToString(),
//                    ISSUED2 = item["ISSUED2"].ToString(),
//                    ADDRESS2 = item["ADDRESS2"].ToString(),
//                    PASSPORTISSUED = item["PASSPORTISSUED"].ToString(),
//                    PASSPORTBODYISSUE = item["PASSPORTBODYISSUE"].ToString(),
//                    PASSPORT = item["PASSPORT"].ToString(),
//                    ADDRESS_FULL = item["ADDRESS_FULL"].ToString(),
//                    FIRSTNAMEFROMPASSPORT = item["FIRSTNAMEFROMPASSPORT"].ToString(),
//                    LASTNAMEFROMPASSPORT = item["LASTNAMEFROMPASSPORT"].ToString(),
//                    FARTHERNAMEFROMPASSPORT = item["FARTHERNAMEFROMPASSPORT"].ToString(),
//                    ISSUEBODYFROMPASSPORT = item["ISSUEBODYFROMPASSPORT"].ToString(),
//                    ISSUEDFROMPASSPORT = item["ISSUEDFROMPASSPORT"].ToString(),
//                    ADDRESS = item["ADDRESS"].ToString(),
//                    POST_INDEX = item["POST_INDEX"].ToString(),
//                    COUNTRY_ID = item["COUNTRY_ID"].ToString(),
//                    DISTRICT = item["DISTRICT"].ToString(),
//                    REGION = item["REGION"].ToString(),
//                    ADDRESSFROMPASSPORT = item["ADDRESSFROMPASSPORT"].ToString(),
//                    CITY_TYPE = item["CITY_TYPE"].ToString(),
//                    CITY = item["CITY"].ToString(),
//                    STREET_TYPE = item["STREET_TYPE"].ToString(),
//                    STREET = item["STREET"].ToString(),
//                    BUILDING = item["BUILDING"].ToString(),
//                    BUILDING_BLOCK = item["BUILDING_BLOCK"].ToString(),
//                    OFFICE = item["OFFICE"].ToString(),
//                    PHONE = item["PHONE"].ToString(),
//                    FAX = item["FAX"].ToString(),
//                    EMAIL = item["EMAIL"].ToString(),
//                    UserId = item["UserId"].ToString(),
//                    LAST_UPDATE = string.IsNullOrEmpty(item["LAST_UPDATE"]) ? new DateTime(2001, 1, 1) : Convert.ToDateTime(item["LAST_UPDATE"])

//                };
//                comparisons.Add(comparison);
//            }
//            return comparisons;
//        }

//        public int Update(Comparison comparison)
//        {
//            string commandText = "Update customs set FIRSTNAME = @FIRSTNAME, LASTNAME = @LASTNAME, FATHERSNAME = @FATHERSNAME, WORKPLACE = @WORKPLACE, POST = @POST, PASSPORTNUM = @PASSPORTNUM, PASSPORTNUMBER = @PASSPORTNUMBER, MISTAKE = @MISTAKE, PASSPORT2 = @PASSPORT2, DATE2 = @DATE2, ISSUED2 = @ISSUED2, ADDRESS2 = @ADDRESS2, PASSPORTISSUED = @PASSPORTISSUED, PASSPORTBODYISSUE = @PASSPORTBODYISSUE, PASSPORT = @PASSPORT, ADDRESS_FULL = @ADDRESS_FULL, FIRSTNAMEFROMPASSPORT = @FIRSTNAMEFROMPASSPORT, LASTNAMEFROMPASSPORT = @LASTNAMEFROMPASSPORT, FARTHERNAMEFROMPASSPORT = @FARTHERNAMEFROMPASSPORT, ISSUEBODYFROMPASSPORT = @ISSUEBODYFROMPASSPORT, ISSUEDFROMPASSPORT = @ISSUEDFROMPASSPORT, ADDRESS = @ADDRESS, POST_INDEX = @POST_INDEX, COUNTRY_ID = @COUNTRY_ID, DISTRICT = @DISTRICT, REGION = @REGION, ADDRESSFROMPASSPORT = @ADDRESSFROMPASSPORT, CITY_TYPE = @CITY_TYPE, CITY = @CITY, STREET_TYPE = @STREET_TYPE, STREET = @STREET, BUILDING = @BUILDING, BUILDING_BLOCK = @BUILDING_BLOCK, OFFICE = @OFFICE, PHONE = @PHONE, FAX = @FAX, EMAIL = @EMAIL, UserId = @UserId, LAST_UPDATE = @LAST_UPDATE where CUSTOMBROCKERID = @CUSTOMBROCKERID";
//            Dictionary<string, object> parameters = new Dictionary<string, object>();
//            parameters.Add("@CUSTOMBROCKERID", comparison.CUSTOMBROCKERID);
//            parameters.Add("@FIRSTNAME", comparison.FIRSTNAME);
//            parameters.Add("@LASTNAME", comparison.LASTNAME);
//            parameters.Add("@FATHERSNAME", comparison.FATHERSNAME);
//            parameters.Add("@WORKPLACE", comparison.WORKPLACE);
//            parameters.Add("@POST", comparison.POST);
//            parameters.Add("@PASSPORTNUM", comparison.PASSPORTNUM);
//            parameters.Add("@PASSPORTNUMBER", comparison.PASSPORTNUMBER);
//            parameters.Add("@MISTAKE", comparison.MISTAKE);
//            parameters.Add("@PASSPORT2", comparison.PASSPORT2);
//            parameters.Add("@DATE2", comparison.DATE2);
//            parameters.Add("@ISSUED2", comparison.ISSUED2);
//            parameters.Add("@ADDRESS2", comparison.ADDRESS2);
//            parameters.Add("@PASSPORTISSUED", comparison.PASSPORTISSUED);
//            parameters.Add("@PASSPORTBODYISSUE", comparison.PASSPORTBODYISSUE);
//            parameters.Add("@PASSPORT", comparison.PASSPORT);
//            parameters.Add("@ADDRESS_FULL", comparison.ADDRESS_FULL);
//            parameters.Add("@FIRSTNAMEFROMPASSPORT", comparison.FIRSTNAMEFROMPASSPORT);
//            parameters.Add("@LASTNAMEFROMPASSPORT", comparison.LASTNAMEFROMPASSPORT);
//            parameters.Add("@FARTHERNAMEFROMPASSPORT", comparison.FARTHERNAMEFROMPASSPORT);
//            parameters.Add("@ISSUEBODYFROMPASSPORT", comparison.ISSUEBODYFROMPASSPORT);
//            parameters.Add("@ISSUEDFROMPASSPORT", comparison.ISSUEDFROMPASSPORT);
//            parameters.Add("@ADDRESS", comparison.ADDRESS);
//            parameters.Add("@POST_INDEX", comparison.POST_INDEX);
//            parameters.Add("@COUNTRY_ID", comparison.COUNTRY_ID);
//            parameters.Add("@DISTRICT", comparison.DISTRICT);
//            parameters.Add("@REGION", comparison.REGION);
//            parameters.Add("@ADDRESSFROMPASSPORT", comparison.ADDRESSFROMPASSPORT);
//            parameters.Add("@CITY_TYPE", comparison.CITY_TYPE);
//            parameters.Add("@CITY", comparison.CITY);
//            parameters.Add("@STREET_TYPE", comparison.STREET_TYPE);
//            parameters.Add("@STREET", comparison.STREET);
//            parameters.Add("@BUILDING", comparison.BUILDING);
//            parameters.Add("@BUILDING_BLOCK", comparison.BUILDING_BLOCK);
//            parameters.Add("@OFFICE", comparison.OFFICE);
//            parameters.Add("@PHONE", comparison.PHONE);
//            parameters.Add("@FAX", comparison.FAX);
//            parameters.Add("@EMAIL", comparison.EMAIL);
//            parameters.Add("@UserId", comparison.UserId);
//            parameters.Add("@LAST_UPDATE", comparison.LAST_UPDATE);

//            return _database.Execute(commandText, parameters);
//        }
//    }
//}
