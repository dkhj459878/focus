﻿using System;

namespace Focus.Web.ViewModels
{
    public class PageInfo
    {
        public int PageNumber { get; set; } // Number of the current page.
        public int PageSize { get; set; } // Number of the objects at the page.
        public int TotalItems { get; set; } // Whole number of the objects.
        public int TotalPages  // Whole number of the pages.
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }
}