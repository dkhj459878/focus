﻿using AspNet.Identity.MySQL;
using IdentityMySQLDemo.Models;
using Microsoft.AspNet.Identity;
using Focus.Web.Common;
using Microsoft.Owin;
using Owin;
using System;

[assembly: OwinStartupAttribute(typeof(IdentityMySQLDemo.Startup))]
namespace IdentityMySQLDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            PoCStaticMethodInGlobalAsax.SetDateTimeCreation(DateTime.Now);
            string CREATION_CONTEXT_IN_STARTUP_METHOD = "Обьект обновлен в Startup методе.";
            PoCStaticMethodInGlobalAsax.SetCreationContext(CREATION_CONTEXT_IN_STARTUP_METHOD);
            log4net.Config.XmlConfigurator.Configure();
            ConfigureAuth(app);
            CreateRolesandUsers();

        }

        private void CreateRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext("OracleDbContext");

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
            userManager.PasswordValidator = new PasswordValidator() { RequiredLength = 8, RequireNonLetterOrDigit = true };
            userManager.MaxFailedAccessAttemptsBeforeLockout = 3;
            //const string name = "administrator";
            //const string password = "dfkbtd_F.F.05";
            const string roleNameAdmin = "admin";
            const string roleNameUser = "user";
            const string roleNameRedactor = "redactor";
            const string roleNameInspection = "inspection";

            // Create role admin if it does not exist

            if (!roleManager.RoleExists(roleNameAdmin))
            {
                var role = new IdentityRole(roleNameAdmin);
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(roleNameUser))
            {
                var role = new IdentityRole(roleNameUser);
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(roleNameRedactor))
            {
                var role = new IdentityRole(roleNameRedactor);
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists(roleNameInspection))
            {
                var role = new IdentityRole(roleNameInspection);
                roleManager.Create(role);
            }

            //if (userManager.FindByName(name) == null)
            //{
            //    var user = new ApplicationUser { UserName = name, Email = name + "@test.com" };
            //    var result = userManager.Create(user, password);
            //    var rolesForUser = userManager.GetRoles(user.Id);
            //    if (!rolesForUser.Contains(roleNameAdmin))
            //    {
            //        var resultAddUserToRoleAdmin = userManager.AddToRole(user.Id, roleNameAdmin);
            //    }
            //}
        }
    }
}
