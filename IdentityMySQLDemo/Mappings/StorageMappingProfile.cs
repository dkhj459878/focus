﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели СВХ с моделью хранения данных СВХ.
    /// </summary>
    public class StorageMappingProfile : Profile
    {
        public StorageMappingProfile()
        {
            CreateMap<STORAGE, Storage>()
                .MaxDepth(1);
        }
    }
}