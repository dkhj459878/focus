﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели документа, подтверждающего полномочия, с моделью хранения данных документа, подтверждающего полномочия.
    /// </summary>
    public class DocumentMappingProfile : Profile
    {
        public DocumentMappingProfile()
        {
            CreateMap<DOCUMENT, Document>()
                .MaxDepth(1);
        }
    }
}