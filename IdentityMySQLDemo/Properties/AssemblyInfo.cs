﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("IdentityMySQLDemo")]
[assembly: AssemblyDescription("Software for creating customs documents")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Minsk Regional Customs of the State Custom Committee of the Republic of Belarus")]
[assembly: AssemblyProduct("IdentityMySQLDemo")]
[assembly: AssemblyCopyright("Copyright ©  2019")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("351971ab-5f3e-423b-9e97-b3ed7e0ed4ba")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers
// by using the '*' as shown below:
[assembly: AssemblyVersion("0.8.1.2")]
[assembly: AssemblyFileVersion("0.8.1.2")]
