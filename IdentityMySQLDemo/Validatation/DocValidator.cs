﻿using FluentValidation;
using Focus.BL.Common.Services;
using Focus.Web.Models;

namespace Focus.Web.Validation
{
    public class DocValidator: AbstractValidator<Doc>
    {
        readonly IInspectorService _inspectorService;
        readonly IPostService _postService;

        public DocValidator(IInspectorService inspectorService, IPostService postService)
        {
            _inspectorService = inspectorService;

            _postService = postService;

            //const string DOC_DECLARATION_IS_NOT_IN_PROPPER_FORMAT = "Номер декларации на товары не имеет соответствующий формат.";
            //RuleFor(d => d.CUSTOMBROCKERFIRSTNAME).Length(3, 45).WithMessage(DOC_DECLARATION_IS_NOT_IN_PROPPER_FORMAT);
            //RuleFor(d => d.CONSIGNEEID).Must(c => c>0 && c<99999).WithMessage("Требуется выбрать получателя.");
            //RuleFor(d => d.CONSIGNEENAME).NotEmpty().WithMessage("Требуется ввести наименование получателя/отправителя.").MaximumLength(155).WithMessage("Для поля должна не превышать 155 символов.");
            //RuleFor(d => d.CONSIGNEEADDRESS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CONSIGNEETIN).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.TYPE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CUSTOMBROCKERFIRSTNAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.INSPECTORID).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.INSPECTORTITLE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.INSPECTORSHORTNAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.INSPECTORDEVISION).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ID).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CUSTOMBROCKERLASTNAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.FATHERSNAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PASSPORTNUMBER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PASSPORTISSUEDDATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PASSPORTAUTHORITYBODY).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PLACEOFWORK).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.POST).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDRESS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PHONE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.FAX).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PHOTOPAGES).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PHOTOCOPIES).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALNUMBERS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALNAME1).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALPAGES1).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALCOPIES1).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALNAME2).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALPAGES2).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALCOPIES2).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALNAME3).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALPAGES3).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALCOPIES3).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALNAME4).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALPAGES4).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALCOPIES4).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALNAME5).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALPAGES5).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ADDITIONALCOPIES5).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.NUM).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SAMPLINGNUM).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DECISIONNUM).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PERIODSTARTDATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PEROIDFINISHDATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PERIODSTARTTIME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CUSTOMSPLACE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PERIODFINISHTIME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.TRANSFERRINGDATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.TRANSFERRINGTIME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCDECLARATIONONGOODS).NotEmpty().WithMessage(DOC_DECLARATION_IS_NOT_IN_PROPPER_FORMAT);
            //RuleFor(d => d.DOCTRANSITDECLARATION).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCCMRNUM).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCCMRDATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCCARNERNUM).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCCARNETDATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCNOTIFICATIONPLACEMENT).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCPERMISSIONPLACEMENT).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCINVOCENUM).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.DOCINVOICEDATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.GOODSDESCRIPTION).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.GOODSTOTALBRUTTO).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.GOODSARRIVALCOUNTRY).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ONITSOWNWHEELS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PACKAGEINITIALNAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PACKAGENAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PLACESNAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PLACESNUMBER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.QUESTIONS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.GOODSDES).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SAMPLEWEIGHT).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SAMPLECOST).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.TOTALWEIGHT).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.TOTALCOST).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CURRENCYNAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISDANGER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISPERISHABLEPRODUCT).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.KINDOFSAMPLE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SAMPLESNUM).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.EXPERTINSINKIND).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SEALNUM).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.WEIGHINGACTUALNETTO).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.WEIGHINGACTUALBRUTTO).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISACTUAL).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISAVERAGE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISAVERAGEPLACESNUMBER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISAVERAGEFIRST).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISAVERAGESECOND).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISAVERAGETHIRD).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISAVERAGEDESCRIPTION).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISOTHER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISOTHERDESCRIPTION).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISAVERAGE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SIGNSSOFVIOLATIONS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.VIOLATIONSDESCRIPTION).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CUSTOMS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.RADIATIONBACKGROUND).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CUSTOMSRESULT).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.GROUND).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SEALS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SEALSVIOLATED).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.IDENTIFICATIONTOOLS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.IDENTIFICATIONTOOLSNUMBER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PACKEDATOURPRESENCE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.WHEATHER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SAMPLINGPACKAGE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.PROFILENUMBER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CHIEFID).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CHIEFSHORTNAME).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CHIEFPOST).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CHIEFTITLE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CHIEFPHONENUMBER).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.ISCOULDBEPRENTED).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.UserId).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.TIMESTAMP_ON_CREATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.LAST_UPDATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.BASIS).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.TASK).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.REWIEW).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.COMMONRATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.SPEEDUPRATE).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.RATE3).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.RATE5).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.RATE4).NotEmpty().WithMessage("Требуется ввести данные.");
            //RuleFor(d => d.CONTROL).NotEmpty().WithMessage("Требуется ввести данные.");
        }

    }
}