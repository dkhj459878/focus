﻿using FluentValidation;
using Focus.Web.Models;

namespace Focus.Web.Validation
{
    public class VehicleValidator: AbstractValidator<Vehicle>
    {

        public VehicleValidator()
        {
            //RuleFor(c => c.VEHICLEKINDID).NotEmpty().WithMessage("Требуется выбрать вид транспортного средства."); ;
            RuleFor(c => c.MODEL).Length(3, 25).WithMessage("Требуется вве")
                .Length(2, 15).WithMessage("Требуется ввести модель транспортного средства.");
        }
    }
}