﻿using System.Text;
using System.Web.Mvc;

namespace IdentityMySQLDemo.Controllers
{
    public class CreatorCodeController : Controller
    {
        // GET: CreatorCode
        public ActionResult Index()
        {
            string str = Template();
            return View();
        }

        private string Template()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("@foreach (var entity in entityVersions) { if (!string.IsNullOrEmpty(@Html.DisplayFor(modelItem => entity.CONSIGNEENAME).ToString().Trim())) { isFieldEmpty = false; } }");
            builder.Append("@if (!isFieldEmpty)");
            builder.Append("{");
            builder.Append("<tr>");
            builder.Append("<td><div style='with:@firstColumnWidth'>@Html.DisplayNameFor(d => d.CONSIGNEENAME).ToString().Replace('; ', '')</div></td>");
            builder.Append("@for (int i = 0; i < count; i++)");
            builder.Append("{");
            builder.Append("if (i == count - 1)");
            builder.Append("{");
            builder.Append("previousValue = @Html.DisplayFor(modelItem => entityVersions[i].CONSIGNEENAME).ToString().Trim();");
            builder.Append("<td><div style='with:@otherColumnWidth'></div>@ConvertToHtml(previousValue)</td>");
            builder.Append("}");
            builder.Append("else");
            builder.Append("{");
            builder.Append("currentValue = @Html.DisplayFor(modelItem => entityVersions[i].CONSIGNEENAME).ToString().Trim();");
            builder.Append("previousValue = @Html.DisplayFor(modelItem => entityVersions[i + 1].CONSIGNEENAME).ToString().Trim();");
            builder.Append("<td>");
            builder.Append("<div class='@IsBold(previousValue, currentValue) @IsStrikeStart(previousValue, currentValue)' style='color:@GetColor(previousValue, currentValue)'>");
            builder.Append("@GetCurrentValue(previousValue, currentValue)");
            builder.Append("</div>");
            builder.Append("</td>");
            builder.Append("}");
            builder.Append("}");
            builder.Append("@{previousValue = string.Empty; currentValue = string.Empty; isFieldEmpty = true; }");
            builder.Append("</tr>");
            builder.Append("}");
            builder.Append("");
            return builder.ToString();
        }
    }
}