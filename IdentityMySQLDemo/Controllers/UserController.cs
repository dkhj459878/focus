﻿using AspNet.Identity.MySQL;
using Focus.BL.Common.Services;
using IdentityMySQLDemo.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using Focus.DAL.Common.DbModels;
using System.Threading.Tasks;
using System.Web.Mvc;
using Focus.Web.Models;
using Focus.Web.Helpers;
using AutoMapper;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using Messages = IdentityMySQLDemo.Properties.Resources;

namespace IdentityMySQLDemo.Controllers
{
    [Authorize(Roles = "admin, auditor")]
    public class UserController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");
        private ApplicationSignInManager _signInManager;
        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private readonly IInspectorService _inspectorService;
        private readonly IPostService _postService;
        private readonly IRoleService _roleService;
        private readonly IUserRoleService _userRoleService;

        public UserController(IInspectorService inspectorService, IPostService postService, IRoleService roleService, IUserRoleService userRoleService)
        {
            _inspectorService = inspectorService;
            _postService = postService;
            _roleService = roleService;
            _userRoleService = userRoleService;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ActionResult DateUpdate()
        {
            List<ApplicationUser> usersList = userManager.Users.ToList();
            const int SECONDS_BEHIND_COUNT = 5;
            DateTime nowUtc = DateTime.UtcNow.AddSeconds(-SECONDS_BEHIND_COUNT);
            foreach (var user in usersList)
            {
                user.LockoutEndDateUtc = nowUtc;
                userManager.Update(user);
            }

            return View("Index", usersList);
        }

        // GET: User
        public ActionResult Index()
        {

            var users = new List<UsersViewModel>();
            UsersViewModel user = null;
            List<ApplicationUser> usersList = userManager.Users.OrderBy(u => u.UserName).ToList();
            List<POST> posts = _postService.GetAllPosts().ToList();
            List<INSPECTOR> inspectors = _inspectorService.GetAllInspectors().ToList();
            List<ROLES> roles = _roleService.GetAllRoles().ToList();
            List<USERROLES> userRoles = _userRoleService.GetAllUserRoles().ToList();
            foreach (ApplicationUser item in usersList)
            {
                string roleId = userRoles.Where(ur => ur.USERID == item.Id).FirstOrDefault()?.ROLEID;
                string role = roles.Where(r => r.ID == roleId).FirstOrDefault()?.NAME;
                //bool idAdmin = userManager.IsInRole(item.Id, "admin");
                DateTime? lockoutEndDate = item.LockoutEndDateUtc;
                short inspectorId = (short)item.PERSONID;
                INSPECTOR inspector = inspectors.Where(i => i.ID == inspectorId).FirstOrDefault();
                if (inspector != null)
                {
                    string title = inspector.TITLE;
                    string shortName = inspector.SHORTNAME;
                    string phoneNumber = inspector.PHONENUMBER;
                    byte postId = inspector.POSTID ?? 0;
                    POST post = posts.Where(p => p.ID == postId).FirstOrDefault();
                    if (post != null)
                    {
                        string code = post.CODE;
                        string namePto = post.NAME;
                        string division = post.DEVISION;
                        user = new UsersViewModel() { Id = item.Id, UserName = item.UserName, Role = role, ShortName = shortName, CodePTO = code, PTOName = namePto, Division = division, Title = title, PhoneNumber = phoneNumber, LockoutEndDate = lockoutEndDate };
                        users.Add(user);
                        continue;
                    }
                    user = new UsersViewModel() { Id = item.Id, UserName = item.UserName, Role = role, ShortName = shortName, Title = title, PhoneNumber = phoneNumber, LockoutEndDate = lockoutEndDate };
                    users.Add(user);
                    continue;
                }

                user = new UsersViewModel() { Id = item.Id, UserName = item.UserName, Role = role, LockoutEndDate = lockoutEndDate };
                users.Add(user);
            }
            return View(users);
        }

        // GET: User/Edit/5
        public async Task<ActionResult> ChangePassword(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = await userManager.FindByIdAsync(id);
            var userId = applicationUser.Id;
            var role = userManager.GetRoles(userId).FirstOrDefault();
            var user = new RegisterViewModel() { UserName = applicationUser.UserName, Role = role };
            if (applicationUser == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        // POST request.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(RegisterViewModel usermodel)
        {
            var user = await userManager.FindByNameAsync(usermodel.UserName);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user.PasswordHash = userManager.PasswordHasher.HashPassword(usermodel.Password);
            var result = await userManager.UpdateAsync(user);
            if (!result.Succeeded)
            {
                return View(usermodel);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult doesUserNameExist(string UserName)
        {

            var user = userManager.FindByName(UserName);
            if (user == null)
                return Json(true, JsonRequestBehavior.AllowGet);

            string suggestedUID = String.Format(CultureInfo.InvariantCulture,
                "user с имененем '{0}' уже существует. Пожалуйста, введите другое имя пользователя.", UserName);
            return Json(suggestedUID, JsonRequestBehavior.AllowGet);
        }

        // Remote diagnostics.
        // GET: Inspector /NotRepeated/
        [AllowAnonymous]
        public JsonResult NotRepeated(string FIRSTNAME, string LASTNAME, string FATHERNAME)
        {
            if (IsEnique(FIRSTNAME, LASTNAME, FATHERNAME))
            {
                return Json(true,
                    JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteInspectorRepeated,
                        LASTNAME ?? string.Empty,
                        FIRSTNAME ?? string.Empty,
                        FATHERNAME ?? string.Empty),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsEnique(string firstName, string lastName, string fatherName)
        {
            return !_inspectorService.InspectorExists(
                    i => i.FIRSTNAME == (firstName == string.Empty ? null : firstName) &&
                    i.LASTNAME == (lastName == string.Empty ? null : lastName) &&
                    i.FATHERNAME == (fatherName == string.Empty ? null : fatherName));
        }

        public ActionResult LockAccount(string id)
        {
            ApplicationUser user = userManager.FindById(id);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            const int LOCKOUT_MINUTES_COUNT = 15;
            LockAccount(user.UserName, LOCKOUT_MINUTES_COUNT);

            return RedirectToAction("Index");
        }

       
        public ActionResult LockoutAccount(string Id)
        {
            ApplicationUser user = userManager.FindById(Id);
            if (user == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            const int SECONDS_BACKWARD = 5;
            LockoutAccount(user.UserName, -SECONDS_BACKWARD);

            return RedirectToAction("Index");
        }

        private void LockoutAccount(string userName, int seconds = 0)
        {
            var user = userManager.FindByName(userName);

            DateTime lockoutEndDate = DateTime.Now.ToUniversalTime();
            if (seconds == 0)
            {
                userManager.SetLockoutEndDate(user.Id, lockoutEndDate);
            }
            else
            {
                lockoutEndDate = DateTime.Now.AddSeconds(seconds);
                userManager.SetLockoutEndDate(user.Id, lockoutEndDate);
            }
        }

        private void LockAccount(string userName, int minutes = 0)
        {
            ApplicationUser user = userManager.FindByName(userName);

            DateTime lockoutEndDate = DateTime.Now.ToUniversalTime();
            if (minutes == 0)
            {
                const int LOCKOUT_YEARS_COUNT = 100;
                lockoutEndDate = lockoutEndDate.AddYears(LOCKOUT_YEARS_COUNT);
                userManager.SetLockoutEndDate(user.Id, lockoutEndDate);
            }
            else
            {
                lockoutEndDate = lockoutEndDate.AddMinutes(minutes);
                userManager.SetLockoutEndDate(user.Id, lockoutEndDate);
            }
        }

        // GET: User/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = userManager.FindById(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            return View(applicationUser);
        }

        // GET: User/Create
        public ActionResult Create()
        {
            ViewBag.ROLE = new SelectList(RoleManager.Roles.ToList(), "Name", "Name");
            return View();
        }

        [AllowAnonymous]
        [HttpGet, ActionName("asldfkh_slfj_hlSGfgfsdfkfhalskdfj")]
        public ActionResult CreateAdministrator()
        {
            return View("CreateAdministrator");
        }

        // POST: User - Admin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [AllowAnonymous]
        public async Task<ActionResult> CreateAdministrator(RegisterAdministratorViewModel model)
        {
            if (!ModelState.IsValid)
            {
                // If we got this far, something failed, redisplay form
                return View(model);
            }

            // User creation.
            const int SecondBack = 5;
            var userNew = new ApplicationUser { UserName = model.UserName, Email = model.UserName + "@test.com", LockoutEnabled = true, LockoutEndDateUtc = DateTime.UtcNow.AddSeconds(-SecondBack) };
            var creationResult = await userManager.CreateAsync(userNew, model.Password);
            var rolesForUser = userManager.GetRoles(userNew.Id);
            const string AdministratorRoleName = "admin";
            if (!rolesForUser.Contains(AdministratorRoleName))
            {
                var resultAddUserToRoleAdmin = userManager.AddToRole(userNew.Id, "admin");
            }
            const string AdministratorUserName = "administrator";
            var user = userManager.FindByName(AdministratorUserName);
            var userId = user.Id;

            // Add inspector.
            Inspector inspectorAdmin = ExtractInspector(model);
            inspectorAdmin.LAST_UPDATE = DateTime.Now;
            inspectorAdmin.USERID = userId;
            UserInforManager userInforManager = new UserInforManager(inspectorAdmin, user.UserName);
            var userInfo = userInforManager.GetUserInfo();
            var inspectorId = _inspectorService.AddInspector(Mapper.Map<INSPECTOR>(inspectorAdmin), userId: userId ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Fill inspector information with the id of the newly created user.
            user.PERSONID = inspectorId;
            var updated = await userManager.UpdateAsync(user);

            var result = await SignInManager.PasswordSignInAsync(model.UserName, model.Password, false, shouldLockout: true);

            return RedirectToAction("Edit", "Inspector", new { id = inspectorId });
        }

        private Inspector ExtractInspector(RegisterAdministratorViewModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new Inspector()
            {
                LASTNAME = model.LASTNAME,
                FIRSTNAME = model.FIRSTNAME,
                FATHERNAME = model.FATHERNAME,
                TITLE = model.TITLE,
                SHORTNAME = model.SHORTNAME,
                SEAL = model.SEAL,
                LNP = model.LNP,
                PHONENUMBER = model.PHONENUMBER
            };
        }

        // POST: User/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                DateTime now = DateTime.Now.ToUniversalTime();
                var user = new ApplicationUser { UserName = model.UserName, Email = model.UserName + "@test.com", PERSONID = (short)model.PERSONID, LockoutEnabled = true, LockoutEndDateUtc = now };
                var result = await userManager.CreateAsync(user, model.Password);
                //if (!result.Succeeded)
                //{

                //    // For more information on how to enable account confirmation and password reset please visit https://go.microsoft.com/fwlink/?LinkID=320771
                //    // Send an email with this link
                //    // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                //    // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                //    // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");

                //    return RedirectToAction("Index");
                //}
                var rolesForUser = userManager.GetRoles(user.Id);
                if (!rolesForUser.Contains(model.Role))
                {
                    var resultAddUserToRoleAdmin = userManager.AddToRole(user.Id, model.Role);
                }
                return RedirectToAction("Index");
            }

            // If we got this far, something failed, redisplay form
            ViewBag.ROLE = new SelectList(RoleManager.Roles.ToList(), "Name", "Name");
            return View(model);
        }

        // GET: User/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = userManager.FindById(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            var userId = applicationUser.Id;
            var role = userManager.GetRoles(userId).FirstOrDefault();
            var user = new UsersViewModel() { Id = userId, UserName = applicationUser.UserName, Role = role };
            ViewBag.Role = new SelectList(RoleManager.Roles.ToArray(), "Name", "Name", role);
            return View(user);
        }

        // POST: User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UsersViewModel applicationUser)
        {
            if (ModelState.IsValid)
            {
                ViewBag.Role = new SelectList(RoleManager.Roles.ToArray(), "Name", "Name");
                var user = userManager.FindById(applicationUser.Id);
                if (user != null)
                {
                    var userId = applicationUser.Id;
                    var role = userManager.GetRoles(user.Id).FirstOrDefault();
                    userManager.RemoveFromRole(userId, role);
                    userManager.AddToRole(userId, applicationUser.Role);
                }
                return RedirectToAction("Index");
            }
            return View();
        }

        // GET: User/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser applicationUser = userManager.FindById(id);
            if (applicationUser == null)
            {
                return HttpNotFound();
            }
            var userId = applicationUser.Id;
            var role = userManager.GetRoles(applicationUser.Id).FirstOrDefault();
            var user = new UserViewModel() { Id = applicationUser.Id, UserName = applicationUser.UserName, Role = role };
            return View(user);
        }

        // POST: User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            ApplicationUser applicationUser = userManager.FindById(id);
            userManager.Delete(applicationUser);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }

                if (userManager != null)
                {

                    userManager.Dispose();
                    userManager = null;
                }

                if (RoleManager != null)
                {

                    RoleManager.Dispose();
                    RoleManager = null;
                }

            }

            base.Dispose(disposing);
        }
    }
}
