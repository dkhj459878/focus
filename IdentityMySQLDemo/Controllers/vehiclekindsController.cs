﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using log4net;
using Focus.Web.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Messages = IdentityMySQLDemo.Properties.Resources;

namespace Focus.Controllers
{
    [Authorize(Roles = "admin, auditor")]
    public class vehiclekindsController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private IVehiclekindService _vehiclekindService;

        private IVehicletypeService _vehicletypeService;

        private readonly IInspectorService _inspectorService;

        public vehiclekindsController(IVehiclekindService vehiclekindService, IVehicletypeService vehicletypeService, IInspectorService inspectorService)
        {
            _vehiclekindService = vehiclekindService;
            _vehicletypeService = vehicletypeService;
            _inspectorService = inspectorService;
        }

        public ActionResult NotRepeated(string NAME, byte? VEHICLETYPEID, byte? VEHICLEKINDID)
        {
            string vehicleType;
            if (IsUnique(NAME, VEHICLETYPEID, VEHICLEKINDID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            if (VEHICLETYPEID == null)
            {
                vehicleType = string.Empty;
            }
            else
            {
                vehicleType = _vehicletypeService.GetVehicletype((byte)VEHICLETYPEID)?.NAME;
            }

            string vehicleTypeName = _vehicletypeService.GetVehicletype(
                (byte)VEHICLETYPEID).NAME;

            return Json(
                    string.Format(
                        Messages.ErrorRemoteVehiclekindRepeated,
                        vehicleTypeName,
                        NAME
                        ), JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(string name, byte? vehicleTypeId, byte? vehicleKindId)
        {

            if (vehicleKindId == null)
            {
                return !_vehiclekindService.VehiclekindExists
                (filter: v => v.NAME == name &&
                v.VEHICLETYPEID == vehicleTypeId);
            }

            return !_vehiclekindService.VehiclekindExists
                 (filter: v => v.NAME == name &&
                 v.VEHICLETYPEID == vehicleTypeId &&
                 v.VEHICLEKINDID != vehicleKindId);
        }

        // GET: vehiclekinds
        public ActionResult Index()
        {
            var vehiclekinds = _vehiclekindService.GetAllVehiclekinds().ToList();
            return View(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(vehiclekinds));
        }
        

        // GET: vehiclekinds/Details/5
        public ActionResult Details(byte id)
        {
            VEHICLEKIND vehiclekind = _vehiclekindService.GetVehiclekind(id);
            if (vehiclekind == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehiclekind>(vehiclekind));
        }

        // GET: vehiclekinds/Create
        public ActionResult Create()
        {
            ViewBag.VEHICLETYPEID = new SelectList(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(_vehicletypeService.GetAllVehicletypes()), "VEHICLETYPEID", "NAME");
            return View();
        }

        // POST: vehiclekinds/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VEHICLEKINDID,VEHICLETYPEID,NAME,DESCRIPTION,LAST_UPDATE")] Vehiclekind vehiclekind)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                _vehiclekindService.AddVehiclekind(Mapper.Map<VEHICLEKIND>(vehiclekind), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }

            ViewBag.VEHICLETYPEID = new SelectList(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(_vehicletypeService.GetAllVehicletypes()), "VEHICLETYPEID", "NAME");
            return View(vehiclekind);
        }

        // GET: vehiclekinds/Edit/5
        public ActionResult Edit(byte id)
        {
            VEHICLEKIND vehiclekind = _vehiclekindService.GetVehiclekind(id);
            if (vehiclekind == null)
            {
                return HttpNotFound();
            }
            ViewBag.VEHICLETYPEID = new SelectList(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(_vehicletypeService.GetAllVehicletypes()), "VEHICLETYPEID", "NAME", vehiclekind.VEHICLETYPEID);
            return View(Mapper.Map<Vehiclekind>(vehiclekind));
        }

        // POST: vehiclekinds/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VEHICLEKINDID,VEHICLETYPEID,NAME,DESCRIPTION,LAST_UPDATE")] Vehiclekind vehiclekind)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                vehiclekind.LAST_UPDATE = DateTime.Now;

                _vehiclekindService.UpdateVehiclekind(Mapper.Map<VEHICLEKIND>(vehiclekind), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }
            ViewBag.VEHICLETYPEID = new SelectList(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(_vehicletypeService.GetAllVehicletypes()), "VEHICLETYPEID", "NAME", vehiclekind.VEHICLETYPEID);
            return View(vehiclekind);
        }

        // GET: vehiclekinds/Delete/5
        public ActionResult Delete(byte id)
        {
             VEHICLEKIND vehiclekind = _vehiclekindService.GetVehiclekind(id);
            if (vehiclekind == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehiclekind>(vehiclekind));
        }

        // POST: vehiclekinds/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(byte id)
        {
            VEHICLEKIND vehiclekind = _vehiclekindService.GetVehiclekind(id); 

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _vehiclekindService.DeleteVehiclekind(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

        public ActionResult JsonDelete(byte id)
        {
            VEHICLEKIND jsondata = _vehiclekindService.GetVehiclekind(id);

            var vehicleKind = jsondata.NAME;

            var carCount = jsondata.CAR?.Count;
			
			var vehicleCount = jsondata.VEHICLE?.Count;

            return Json(
               new
               {
                   vehicleKindId = id,
                   vehicleKindName = vehicleKind,

                   carCount,
                   cars = jsondata.CAR?.Select(c =>  $"VIN: {c.VIN} {c.COUNTRYCODE}"),

                   vehicleCount,
                   vehicles = jsondata.VEHICLE?.Select(v => $"Рег.номер: {v.REGNUMBER} {v.COUNTRYCODE}")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _vehiclekindService.Dispose();
                _vehicletypeService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
