﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using log4net;
using Microsoft.AspNet.Identity;
using Focus.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Messages = IdentityMySQLDemo.Properties.Resources;

namespace Focus.Controllers
{
    [Authorize(Roles = "admin, auditor")]
    public class devicetypesController : Controller
    {
        protected static readonly ILog _logger =
    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private readonly IDevicetypeService _devicetypeService;
        private readonly IInspectorService _inspectorService;

        public devicetypesController(IDevicetypeService devicetypeService, IInspectorService inspectorService)
        {
            _devicetypeService = devicetypeService;
            _inspectorService = inspectorService;
        }


        // Remote diagnostics.
        // GET: devicetypes /NotRepeated/
        public JsonResult NotRepeated(string name, byte? DEVICETYPEID)
        {
            if (IsUnique(name, DEVICETYPEID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteDevicetypeRepeated, name),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(string name, byte? deviceTypeId)
        {
            if (deviceTypeId == null)
            {
                return !_devicetypeService.DevicetypeExists
                (filter: d => d.NAME == name);
            }
            return !_devicetypeService.DevicetypeExists
                 (filter: d => d.NAME == name &&
                 d.DEVICETYPEID != deviceTypeId);
        }

        // GET: devicetypes
        public ActionResult Index()
        {
            var devicetypes = _devicetypeService.GetAllDevicetypes().ToList();
            return View(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(devicetypes));
        }

        // GET: devicetypes/Details/5
        public ActionResult Details(byte id)
        {
            DEVICETYPE devicetype = _devicetypeService.GetDevicetype(id);
            if (devicetype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Devicetype>(devicetype));
        }

        // GET: devicetypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: devicetypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DEVICETYPEID,NAME,LAST_UPDATE")] Devicetype devicetype)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                devicetype.LAST_UPDATE = DateTime.Now;

                _devicetypeService.AddDevicetype(Mapper.Map<DEVICETYPE>(devicetype), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }

            return View(devicetype);
        }

        // GET: devicetypes/Edit/5
        public ActionResult Edit(byte id)
        {
            DEVICETYPE devicetype = _devicetypeService.GetDevicetype(id);
            if (devicetype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Devicetype>(devicetype));
        }

        // POST: devicetypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DEVICETYPEID,NAME,LAST_UPDATE")] Devicetype devicetype)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                _devicetypeService.UpdateDevicetype(Mapper.Map<DEVICETYPE>(devicetype), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }
            return View(devicetype);
        }

        // GET: devicetypes/Delete/5
        public ActionResult Delete(byte id)
        {
            DEVICETYPE devicetype = _devicetypeService.GetDevicetype(id);
            if (devicetype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Devicetype>(devicetype));
        }

        // POST: devicetypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(byte id)
        {
            DEVICETYPE devicetype = _devicetypeService.GetDevicetype(id);

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _devicetypeService.DeleteDevicetype(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

        // GET: vehicles/JsonDelete/5
        public ActionResult JsonDelete(byte id)
        {
            DEVICETYPE jsondata = _devicetypeService.GetDevicetype(id);

            var devicetypeJSON = $"{jsondata.NAME}";

            var docCount = jsondata.DEVICE?.Count;

            return Json(
               new
               {
                   devicetypeId = id,
                   devicetypeName = devicetypeJSON,

                   docCount,
                   devices = jsondata.DEVICE.Select(d => $"{d.NAME}' {d.MARK} {d.MODEL} s\\n {d.SERIALNUMBER}")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _devicetypeService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
