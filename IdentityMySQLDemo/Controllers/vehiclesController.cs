﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using log4net;
using Focus.Web.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Messages = IdentityMySQLDemo.Properties.Resources;

namespace Focus.Controllers
{
    [Authorize(Roles = "inspection, admin, redactor, auditor")]
    public class vehiclesController : Controller
    {
        protected static readonly ILog _logger =
    LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private IVehicleService _vehicleService;
        private ICountryService _countryService;
        private IVehiclekindService _vehiclekindService;
        private readonly IInspectorService _inspectorService;

        public vehiclesController(IVehicleService vehicleService, ICountryService countryService, IVehiclekindService vehiclekindService, IInspectorService inspectorService)
        {
            _vehicleService = vehicleService;
            _countryService = countryService;
            _vehiclekindService = vehiclekindService;
            _inspectorService = inspectorService;
        }

        public JsonResult NotRepeated(string REGNUMBER, string COUNTRYCODE, int? VEHICLEID)
        {
            string countryName;
            if (IsUnique(REGNUMBER, COUNTRYCODE, VEHICLEID))
            {
                return Json(true,
                    JsonRequestBehavior.AllowGet);
            }

            if (COUNTRYCODE == null)
            {
                countryName = string.Empty;
            }
            else
            {
                countryName = _countryService.GetCountry(COUNTRYCODE)?.NAME;
            }
            return Json(
                    string.Format(
                        Messages.ErrorRemoteVehicleRepeated,
                        countryName, REGNUMBER),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(string regNumber, string countryCode,  int? vehicleId)
        {
            var regNumberForSearch = regNumber == "" ? null : regNumber;
            var countryCodeForSearch = countryCode == "" ? null : countryCode;


            if (vehicleId == null)
            {
                return !_vehicleService.VehicleExists
                (filter: v => v.REGNUMBER == regNumberForSearch &&
                    v.COUNTRYCODE == countryCodeForSearch);
            }

                return !_vehicleService.VehicleExists
                (filter: v => v.REGNUMBER == regNumberForSearch &&
                    v.COUNTRYCODE == countryCodeForSearch &&
                    v.VEHICLEID != vehicleId
                    );

        }

        // GET: vehicles
        public ActionResult Index()
        {
            var vehicles = _vehicleService.GetAllVehicles().ToList();
            return View(Mapper.Map<IEnumerable<VEHICLE>, IEnumerable<Vehicle>>(vehicles));
        }

        // GET: vehicles/Details/5
        public ActionResult Details(short id)
        {
            VEHICLE vehicle = _vehicleService.GetVehicle(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<VEHICLE>(vehicle));
        }

        public JsonResult Json(short id)
        {
            var jsondata = _vehicleService.GetVehicle(id);
            jsondata.COUNTRY = _countryService.GetCountry(jsondata.COUNTRYCODE);
            jsondata.VEHICLEKIND = _vehiclekindService.GetVehiclekind(jsondata.VEHICLEKINDID);

            return Json(
                new
                {
                    id = jsondata?.VEHICLEID,
                    reg = jsondata?.REGNUMBER,
                    desc = jsondata?.DESCRIPTION,
                    vehicleKind = jsondata.VEHICLEKIND?.NAME,
                    vehicleKindId = jsondata.VEHICLEKIND?.VEHICLEKINDID,
                    countryCode = jsondata.COUNTRY?.NAME,
                    countryCode2 = jsondata.COUNTRY?.ABBR2,
                    countryName = jsondata.COUNTRY?.NAME,
                    countryNameFull = jsondata.COUNTRY?.NAMEFULL,
                    countryNameEn = jsondata.COUNTRY?.NAMEEN,
                    countryABBR3 = jsondata.COUNTRY?.ABBR3,
                    countryCodeLetters = jsondata.COUNTRY?.CODELETTERS,
                    countryCodeDigits = jsondata.COUNTRY?.CODEDIGITS,
                    countryCONTINENTS = jsondata.COUNTRY?.CONTINENTS,
                    countryREGIONS = jsondata.COUNTRY?.REGION,
                    countryCode3 = jsondata?.COUNTRYCODE,
                    mark = jsondata?.MARK,
                    model = jsondata?.MODEL,
                    regnumdigits = jsondata?.REGNUMBERDIGITS,
                    regnumberletters = jsondata?.REGNUMBERLETTERS,
                    regnumberdigitsafter = jsondata?.REGNUMBEDIGITSAFTER,
                    lastupdated = jsondata?.LAST_UPDATE
                },
                JsonRequestBehavior.AllowGet);
        }

        public JsonResult JsonOrg(short id)
        {
            var jsondata = _vehicleService.GetVehicle(id);

            return Json(
                new
                {
                    id = jsondata?.VEHICLEID,
                    reg = jsondata?.REGNUMBER,
                    desc = jsondata?.DESCRIPTION,
                    vehicleKind = jsondata?.VEHICLEKIND?.NAME,
                    vehicleKindId = jsondata.VEHICLEKIND?.VEHICLEKINDID,
                    countryCode = jsondata?.COUNTRYCODE,
                    countryCode2 = jsondata.COUNTRY?.ABBR2,
                    countryName = jsondata.COUNTRY?.NAME,
                    countryNameFull = jsondata?.COUNTRYCODE != null ? jsondata.COUNTRY.NAMEFULL : string.Empty,
                    countryNameEn = jsondata?.COUNTRYCODE != null ? jsondata.COUNTRY.NAMEEN : string.Empty,
                    countryABBR3 = jsondata.COUNTRY?.ABBR3,
                    countryCodeLetters = jsondata?.COUNTRYCODE != null ? jsondata.COUNTRY.CODELETTERS : string.Empty,
                    countryCodeDigits = jsondata?.COUNTRYCODE != null ? jsondata.COUNTRY.CODEDIGITS : 0,
                    countryCONTINENTS = jsondata?.COUNTRYCODE != null ? jsondata.COUNTRY.CONTINENTS : string.Empty,
                    countryREGION = jsondata?.COUNTRYCODE != null ? jsondata.COUNTRY.REGION : string.Empty,
                    mark = jsondata?.MARK,
                    model = jsondata?.MODEL,
                    regnumdigits = jsondata?.REGNUMBERDIGITS,
                    regnumberletters = jsondata?.REGNUMBERLETTERS,
                    regnumberdigitsafter = jsondata?.REGNUMBEDIGITSAFTER
                },
                JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Search(string regnumber, int[] vehicleSelectedIndexes)
        {
            if (string.IsNullOrEmpty(regnumber))
            {
                return Content($"<br /><p><b>Поисковая строка не должна быть пустой. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>4</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            if (regnumber.Trim().Length < 5)
            {
                return Content($"<br /><p><b>Поисковая строка должна содержать не менее 5 символов, за исключением пробела. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>4</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            int[] selectedIndexes = vehicleSelectedIndexes;
            IEnumerable<VEHICLE> vehiclesFiltered;
            var vehicles = _vehicleService.GetAllVehicles(filter: o => o.REGNUMBER.Contains(regnumber)).ToList();
            if (vehicles.Count <= 0)
            {
                return Content($"<br /><p><b>Транспортные средства с частью регистрационного номера <span style = 'background-color: #ffff00;'>{regnumber}</span> в базе данных не обнаружены.</b></p>");
            }
            if (selectedIndexes != null)
            {
                vehiclesFiltered = vehicles.Where(x => !vehicleSelectedIndexes.Contains(x.VEHICLEID)).ToArray();
                if (vehiclesFiltered.Count() <= 0)
                {
                    return HttpNotFound();
                }
                return PartialView(Mapper.Map<IEnumerable<VEHICLE>, IEnumerable<Vehicle>>(vehiclesFiltered));
            }
            return PartialView(Mapper.Map<IEnumerable<VEHICLE>, IEnumerable<Vehicle>>(vehicles));
        }


        // POST: vehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult  PartialCreate([Bind(Include = "VEHICLEID,VEHICLEKINDID,COUNTRYCODE,MARK,MODEL,DESCRIPTION,REGNUMBER,REGNUMBERLETTERS,REGNUMBERDIGITS,REGNUMBEDIGITSAFTER,LAST_UPDATE")] Vehicle vehicle)
        {  
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                vehicle.LAST_UPDATE = DateTime.Now;

                vehicle.VEHICLEID = _vehicleService.AddVehicle(Mapper.Map<VEHICLE>(vehicle), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return PartialView("Search", new List<Vehicle>() { vehicle });
            }

            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()),  "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return PartialView(vehicle);
        }

        [HttpGet]
        public ActionResult PartialCreate()
        {
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return PartialView();
        }

        // GET: vehicles/Create
        public ActionResult Create()
        {
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return View();
        }

        // POST: vehicles/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "VEHICLEID,VEHICLEKINDID,COUNTRYCODE,MARK,MODEL,DESCRIPTION,REGNUMBER,REGNUMBERLETTERS,REGNUMBERDIGITS,REGNUMBEDIGITSAFTER,LAST_UPDATE")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                vehicle.LAST_UPDATE = DateTime.Now;

                _vehicleService.AddVehicle(Mapper.Map<VEHICLE>(vehicle), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }

            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return View(vehicle);
        }

        // GET: vehicles/Edit/5
        public ActionResult Edit(short id)
        {
            VEHICLE vehicle = _vehicleService.GetVehicle(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", vehicle.COUNTRYCODE);
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME", vehicle.VEHICLEKINDID);
            return View(Mapper.Map<Vehicle>(vehicle));
        }

        // POST: vehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VEHICLEID,VEHICLEKINDID,COUNTRYCODE,MARK,MODEL,DESCRIPTION,REGNUMBER,REGNUMBERLETTERS,REGNUMBERDIGITS,REGNUMBEDIGITSAFTER,LAST_UPDATE")] Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                vehicle.LAST_UPDATE = DateTime.Now;

                _vehicleService.UpdateVehicle(Mapper.Map<VEHICLE>(vehicle), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME");
            return View(Mapper.Map<Vehicle>(vehicle));
        }

        // GET: vehicles/Edit/5
        public ActionResult PartialEdit(short id)
        {
            VEHICLE vehicle = _vehicleService.GetVehicle(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", vehicle.COUNTRYCODE);
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME", vehicle.VEHICLEKINDID);
            return PartialView(Mapper.Map<Vehicle>(vehicle));
        }

        // POST: vehicles/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialEdit(Vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                vehicle.LAST_UPDATE = DateTime.Now;
                _vehicleService.UpdateVehicle(Mapper.Map<VEHICLE>(vehicle), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return PartialView("VehicleEdited", vehicle);
            }
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", vehicle.COUNTRYCODE);
            ViewBag.VEHICLEKINDID = new SelectList(Mapper.Map<IEnumerable<VEHICLEKIND>, IEnumerable<Vehiclekind>>(_vehiclekindService.GetAllVehiclekinds()), "VEHICLEKINDID", "NAME", vehicle.VEHICLEKINDID);
            return PartialView(vehicle);
        }

        // GET: vehicles/Delete/5
        public ActionResult Delete(short id)
        {
            VEHICLE vehicle = _vehicleService.GetVehicle(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehicle>(vehicle));
        }

        // POST: vehicles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            VEHICLE vehicle = _vehicleService.GetVehicle(id);

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _vehicleService.DeleteVehicle(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

        // GET: vehicles/Delete/5
        public ActionResult PartialDelete(short id)
        {
            VEHICLE vehicle = _vehicleService.GetVehicle(id);
            if (vehicle == null)
            {
                return PartialView(vehicle);
            }
            return PartialView(Mapper.Map<Vehicle>(vehicle));
        }

        // POST: vehicles/Delete/5
        [HttpPost, ActionName("PartialDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult PartialDeleteConfirmed(short id)
        {
            VEHICLE vehicle = _vehicleService.GetVehicle(id);
            if (vehicle == null)
            {
                return new JsonResult();
            }

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _vehicleService.DeleteVehicle(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return PartialView("VehicleDeleted", Mapper.Map<Vehicle>(vehicle));
        }

		// GET: vehicles/JsonDelete/5
		public ActionResult JsonDelete(short id)
        {
            VEHICLE jsondata = _vehicleService.GetVehicle(id);

            var regNumber = jsondata.REGNUMBER;

            var countryName = jsondata.COUNTRYCODE == null ? string.Empty : _countryService.GetCountry(jsondata.COUNTRYCODE)?.NAME ?? string.Empty;

            var JSON = $"Рег. номер: {jsondata.REGNUMBER} {jsondata.COUNTRYCODE}";

            var entityCount = jsondata.DOC?.Count;

            return Json(
               new
               {
                   message = new MvcHtmlString($"<p><b>Данные о транспортном средстве {regNumber}, " +
                        $"зарегистрированные в {countryName} не могут быть удалены, " +
                        $"поскольку от них зависят следующие таможенные документы:</b></p>"),
                   message2 = string.Empty, 
                   id,
                   listLenght = 5,
                   Name = JSON,
                   action = @"\vehicles\Delete\" + id.ToString(),
                   entities2 = new List<string>() { },
                   entities = entityCount > 0 ? jsondata.DOC.Select(d => $"Номер АТД '{d.NUM}' от {d.PERIODSTARTDATE.Value.ToShortDateString()} {d.CONSIGNEENAME}") : new List<string>(),
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _vehicleService.Dispose();
                _countryService.Dispose();
                _vehiclekindService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
