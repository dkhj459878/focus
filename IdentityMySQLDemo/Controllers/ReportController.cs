﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;
using Focus.Web.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using IdentityMySQLDemo.Models;
using AspNet.Identity.MySQL;

namespace Focus.Controllers
{
    public class ReportController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private IReportService _reportService;
        private IInspectorService _inspectorService;

        public ReportController(IInspectorService inspectorService, IReportService reportService)
        {
            _reportService = reportService;
            _inspectorService = inspectorService;
        }

        // GET: reports
        public ActionResult Index()
        {
            var reports = _reportService.GetAllReports().ToList();
            return View(Mapper.Map<IEnumerable<REPORT>, IEnumerable<Report>>(reports));
        }

        // GET: reports/Details/5
        public ActionResult Details(short id)
        {
            REPORT report = _reportService.GetReport(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Report>(report));
        }

        // GET: reports/Create
        public ActionResult Create()
        {
            return View();
        }
        // POST: reports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.7oo,ll,lklkkl
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "REPORTID,PASSPORTNUMBER,CUSTOMBROCKERID,NUMBER,FULLNAME,LAST_UPDATE")] Report report)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                report.LAST_UPDATE = DateTime.Now;
                _reportService.AddReport(Mapper.Map<REPORT>(report), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);
                return RedirectToAction("Index");
            }

            return View(report);
        }

        // GET: reports/Edit/5
        public ActionResult Edit(short id)
        {
            REPORT report = _reportService.GetReport(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Report>(report));
        }

        // POST: reports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "REPORTID,PASSPORTNUMBER,CUSTOMBROCKERID,NUMBER,FULLNAME,LAST_UPDATE")] Report report)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                report.LAST_UPDATE = DateTime.Now;
                _reportService.UpdateReport(Mapper.Map<REPORT>(report), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);
                return RedirectToAction("Index");
            }
            return View(report);
        }

        // GET: reports/Delete/5
        public ActionResult Delete(short id)
        {
            REPORT report = _reportService.GetReport(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Report>(report));
        }

        // POST: reports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            REPORT report = _reportService.GetReport(id);
            _reportService.DeleteReport(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _reportService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
