﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.BL.Extensions;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Extensions;
using Focus.Enum;
using Focus.Web.Extensions;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using IdentityMySQLDemo.Properties;
using log4net;
using Focus.Web.Enum;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Messages = IdentityMySQLDemo.Properties.Resources;
using Word = Microsoft.Office.Interop.Word;
using Focus.Web.Helpers;
using Focus.Web.ViewModels;

namespace Focus.Controllers
{
    [Authorize(Roles = "inspection, admin, redactor, auditor")]
    public class docsController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private readonly IDocService _docService;
        private readonly IOrganizationService _organizationService;
        private readonly ICustombrockerService _custombrockerService;
        private readonly ICountryService _countryService;
        private readonly IFileDocumentService _fileDocumentService;
        private readonly IInspectorService _inspectorService;
        private readonly IPostService _postService;
        private readonly IAdService _adService;
        private readonly IAudit_trailService _audit_TrailService;
        private Stopwatch _docControllerProfiler = new Stopwatch();

        private bool isAdministrator = false;
        private bool isInspection = false;
        private byte postIdOfUser = 0;

        public docsController(IDocService docService, IOrganizationService organizationService, ICustombrockerService custombrockerService, ICountryService countryService, IFileDocumentService fileDocumentService, IInspectorService inspectorService, IPostService postService, IAdService adService, IAudit_trailService audit_TrailService)
        {
            _docService = docService;
            _organizationService = organizationService;
            _custombrockerService = custombrockerService;
            _countryService = countryService;
            _fileDocumentService = fileDocumentService;
            _inspectorService = inspectorService;
            _postService = postService;
            _adService = adService;
            _audit_TrailService = audit_TrailService;
            _docControllerProfiler.Start();
        }

        // // Remote diagnostics.
        // GET: docs /NotRepeated/
        public ActionResult NotRepeated(string NUM, int? DOCID)
        {
            if (IsUnique(NUM, DOCID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                   string.Format(
                       Messages.ErrorRemoteDocInspectionNumRepeated,
                       NUM
                       ), JsonRequestBehavior.AllowGet);

        }

        private bool IsUnique(string NUM, int? DOCID)
        {
            if (DOCID == null)
            {
                return !_docService.DocExists
                (filter: c => c.NUM == NUM);
            }


            return !_docService.DocExists
                (filter: c => c.NUM == NUM &&
                c.DOCID != DOCID);
        }

        public ActionResult Show(short id)
        {
            DOC doc = _docService.GetDoc(id);
            if (doc == null)
            {
                return HttpNotFound();
            }


            var filePath = _fileDocumentService.GetDocumentFilePath(doc, Server.MapPath("~"));

            return File(filePath, "application/octet-stream", "temp.doc");
        }

        private void Dispose(Word.Application wordApplication)
        {
            Object missingObj = System.Reflection.Missing.Value;
            Object falseObj = false;

            if (wordApplication != null)
            {
                try
                {
                    wordApplication.Quit(ref falseObj, ref missingObj, ref missingObj);
                }
                finally
                {

                    wordApplication = null;
                }
            }
        }

        // Return view with error message.
        public ActionResult Error(ErrorTypes error)
        {
            switch (error)
            {
                case ErrorTypes.Error_TemplateDocumentIsAbsent:
                    ViewBag.ErrorMessage = new HtmlString(Resources.Error_TemplateDocumentIsAbsent);
                    ViewBag.ErrorNumber = "00" + error.ToString();
                    return View();
                default:
                    break;
            }
            ViewBag.ErrorMessage = new HtmlString(Resources.Error_UnknowMistake);
            ViewBag.ErrorNumber = "00" + error.ToString();
            return View();
        }

        // GET: docs
        public ActionResult Index(int pageNumber = -1)
        {
            // Get user profiler start.

            ViewBag.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            bool isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            bool isAuditor = userManager.GetRoles(user.Id).Contains("auditor");
            bool isInspection  = userManager.GetRoles(user.Id).Contains("inspection");
            ViewBag.IsAdmin = isAdministrator;
            ViewBag.IsAuditor = isAuditor;
            ViewBag.IsInspection = isInspection;

            if (GetHashCodeStamp() < DateTime.Now.ToBinary())
            {
                return RedirectToAction("LogOff", "Account");
            }

            string userName = User.Identity.Name; // Adding user id to row of container for auditing purpose. 

            IEnumerable<DOC> docs;
            int pageCount = pageNumber;
            int docsCount = -1;
            const int DEFAULT_PAGE_SIZE = 15;
            try
            {
                short personId = (short)user.PERSONID;
                INSPECTOR inspector = _inspectorService.GetInspector(personId);
                if (isAdministrator || isInspection)
                {
                    Stopwatch dataAdminRetreiveProfiler = new Stopwatch();
                    if (docsCount == -1)
                    {
                        docsCount = _docService.DocsCount();
                    }
                    if (pageCount == -1)
                    {
                        pageCount = GetLastPageNumber(DEFAULT_PAGE_SIZE, docsCount);
                    }
                    docs = _docService.GetDocsPage(DEFAULT_PAGE_SIZE, pageCount, includeProperties: "custombrocker,organization,inspector1", orderBy: q => q.OrderBy(p => p.DOCID)).ToList();
                }
                else
                {
                    byte? postId = inspector?.POSTID;
                    ViewBag.PostIdOfUser = (byte)postId;
                    const int POST_CODE_LENGHT = 5;
                    string postCode = inspector?.POST.CODE;
                    if (docsCount == -1)
                    {
                        docsCount = _docService.DocsCount(filter: d => d.NUM.Substring(0, POST_CODE_LENGHT) == postCode);
                    }
                    if (pageCount == -1)
                    {
                        pageCount = GetLastPageNumber(DEFAULT_PAGE_SIZE, docsCount);
                    }

                    docs = _docService.GetDocsPage(DEFAULT_PAGE_SIZE, pageCount, d => d.NUM.Substring(0, POST_CODE_LENGHT) == postCode, includeProperties: "custombrocker,organization,inspector1", orderBy: q => q.OrderBy(p => p.DOCID)).ToList();
                }

                Optimizer<DOC> optimizer = new Optimizer<DOC>();
                IEnumerable<DOC> docsMinified = optimizer.Optimize(docs);



                PageInfo pageInfo = new PageInfo { PageNumber = pageCount, PageSize = DEFAULT_PAGE_SIZE, TotalItems = docsCount };
                IEnumerable<Doc> docsViewModel = Mapper.Map<IEnumerable<DOC>, IEnumerable<Doc>>(docsMinified).OrderBy(prop => prop.DOCID).ToList();
                DocsViewModel docsPaging = new DocsViewModel() { Docs = docsViewModel, PageInfo = pageInfo };
                return View(docsPaging);
            }
            catch (DbEntityValidationException error)
            {
                _logger.Error(error.GetDetails(), error);
                throw;
            }
            catch (Exception error)
            {
                _logger.Error(error.Message, error);
                throw;
            }
        }

        private int GetLastPageNumber(int pageSize, int docsCount)
        {
            int lastPageCount = 1;

            if (pageSize < 0 || docsCount < 0)
            {
                return lastPageCount;
            }

            if (docsCount == 0)
            {
                return lastPageCount;
            }

            if (docsCount % pageSize == 0)
            {
                return docsCount / pageSize;
            }
            else
            {
                return docsCount / pageSize + 1;
            }
        }

        // GET: docs/Details/5
        public ActionResult Details(short id)
        {
            DOC doc = _docService.GetDoc(id);
            if (doc == null)
            {
                return HttpNotFound();
            }
            ViewBag.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
            return View(Mapper.Map<Doc>(doc));
        }

        // GET: docs/FindAnalog
        public JsonResult FindAnalog(string tin, string orgName, string FIOInspector, string goodsDescription, string customBrocker)
        {
            return null;
        }

        // GET: docs/Create
        public ActionResult Create()
        {
            string userId = User.Identity.GetUserId();

            const bool THERE_IS_VIOLATION_SIGN = false;
            const bool IS_SENDER = false;
            const byte IS_INSPECTION_TYPE_OF_CONTROL = 1;
            const string INITIAL_CUSTOM_INSPECTION_RESULT_DESCRIPTION = "[Введите результаты таможенного досмотра (осмотра). Данный текст удалить впоследствии.]";

            ApplicationUser user = userManager.FindById(userId);
            short personId = (short)user.PERSONID;
            INSPECTOR inspector = _inspectorService.GetInspector(personId);
            if (inspector != null)
            {
                short inspectorId = inspector.ID;
                string title = inspector.TITLE;
                string shortName = inspector.SHORTNAME;
                string devision = inspector.POST?.DEVISION;
                byte? postId = inspector.POSTID;
                DateTime now = DateTime.Now;
                string code = _postService.GetPost((byte)postId).CODE;
                List<DOC> docs = _docService.GetAllDocs(d => d.INSPECTOR1.POSTID == postId, includeProperties: "inspector1").ToList();
                List<STORAGE> storages = _postService.GetAllPosts(p => p.ID == postId, includeProperties: "storage").FirstOrDefault().STORAGE.ToList();
                string storageInfor = string.Empty;
                if (storages.Count !=0)
                {
                    Storage storage = Mapper.Map<Storage>(storages.FirstOrDefault());
                    storageInfor = storage.NUMBER_ + ", " + storage.ADDRESS;
                }

                string nextNumber = docs.GetNextDocNumber((byte)postId, code);

                ViewBag.CUSTOMS = new SelectList(new List<SelectListItem>() { new SelectListItem { Text = "Досмотр", Value = "1" }, new SelectListItem { Text = "Осмотр", Value = "0" } }, "Value", "Text", IS_INSPECTION_TYPE_OF_CONTROL);
                ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", THERE_IS_VIOLATION_SIGN.ConvertToByte());
                ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text", (!IS_SENDER).ConvertToByte());
                Doc doc = new Doc()
                {
                    NUM = nextNumber,
                    INSPECTORDEVISION = devision,
                    CUSTOMSPLACE = storageInfor,
                    INSPECTORSHORTNAME = shortName,
                    INSPECTORTITLE = title,
                    INSPECTORID = inspectorId,
                    SIGNSSOFVIOLATIONS = THERE_IS_VIOLATION_SIGN.ConvertToByte(),
                    TYPE = (!IS_SENDER).ConvertToByte(),
                    CUSTOMSRESULT = INITIAL_CUSTOM_INSPECTION_RESULT_DESCRIPTION,
                    PERIODSTARTDATE = now,
                    PERIODSTARTTIME = DateTime.Now.Add(new TimeSpan(0, 0, Convert.ToInt32(Messages.DefaultCreateToStart), 0)),
                    TIMESTAMP_ON_CREATE = DateTime.Now,
                    LAST_UPDATE = DateTime.Now,
                    PEROIDFINISHDATE = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0),
                    TRANSFERRINGDATE = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0),
                    CONSIGNEEID = 0
                };


                return View(doc);
            }

            ViewBag.CUSTOMS = new SelectList(new List<SelectListItem>() { new SelectListItem { Text = "Досмотр", Value = "1" }, new SelectListItem { Text = "Осмотр", Value = "0" } }, "Value", "Text", IS_INSPECTION_TYPE_OF_CONTROL);
            ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", THERE_IS_VIOLATION_SIGN.ConvertToByte());
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text", (!IS_SENDER).ConvertToByte());
            return View(new Doc()
            {
                PERIODSTARTDATE = DateTime.Now,
                PERIODSTARTTIME = DateTime.Now.Add(new TimeSpan(0, 0, Convert.ToInt32(Messages.DefaultCreateToStart), 0)),
                TIMESTAMP_ON_CREATE = DateTime.Now,
                PEROIDFINISHDATE = DateTime.Now,
                TRANSFERRINGDATE = DateTime.Now,
                SIGNSSOFVIOLATIONS = THERE_IS_VIOLATION_SIGN.ConvertToByte(),
                TYPE = (!IS_SENDER).ConvertToByte(),
                CUSTOMSRESULT = INITIAL_CUSTOM_INSPECTION_RESULT_DESCRIPTION,
                CONSIGNEEID = 0
            });
        }

        private DateTime? GetFinishTime()
        {
            throw new NotImplementedException();
        }

        // POST: docs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Doc doc)
        {
            #region Setting default values.
            const bool IS_ON_WHEELS = false;
            const bool IS_DANGER = false;
            const bool IS_PERISHABLE_PRODUCT = false;
            const string KIND_OF_SAMPLE = "образец";
            const string EXPERTINSIN_KIND = "первичную";
            const bool IS_ACTUAL = false;
            const bool IS_AVERAGE = false;
            const bool IS_OTHER = false;
            const bool PACKED_AT_OUR_PRESENCE = false;
            const bool IS_COULD_BE_PRENTED = false;
            #endregion

            if (ModelState.IsValid)
            {
                try
                {
                    #region Default settings.
                    doc.ONITSOWNWHEELS = Convert.ToByte(IS_ON_WHEELS);
                    doc.ISDANGER = Convert.ToByte(IS_DANGER).ToString();
                    doc.ISPERISHABLEPRODUCT = Convert.ToByte(IS_PERISHABLE_PRODUCT).ToString();
                    doc.KINDOFSAMPLE = KIND_OF_SAMPLE;
                    doc.EXPERTINSINKIND = EXPERTINSIN_KIND;
                    doc.ISACTUAL = Convert.ToByte(IS_ACTUAL);
                    doc.ISAVERAGE = Convert.ToByte(IS_AVERAGE);
                    doc.ISOTHER = Convert.ToByte(IS_OTHER);
                    doc.PACKEDATOURPRESENCE = Convert.ToByte(PACKED_AT_OUR_PRESENCE);
                    doc.ISCOULDBEPRENTED = Convert.ToByte(IS_COULD_BE_PRENTED);
                    doc.USERID = User.Identity.GetUserId();
                    doc.TIMESTAMP_ON_CREATE = DateTime.Now;
                    doc.LAST_UPDATE = DateTime.Now;
                    #endregion

                    string userId = User.Identity.GetUserId();
                    ApplicationUser user = userManager.FindById(userId);
                    Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                    UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                    string userInfo = userInforManager.GetUserInfo();

                    DOC docNew = Mapper.Map<DOC>(doc);

                    _docService.AddDoc(docNew, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                    // Audit purpose actions for the nested properties.
                    IEnumerable<AUDIT_TRAIL> auditTrailNestedClasses = GetAuditTrailNestedClasses(docNew, userId, userInfo);
                    if (auditTrailNestedClasses.Count() > 0)
                    {
                        _audit_TrailService.AddAudit_trails(auditTrailNestedClasses);
                    }
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }

                ViewBag.CUSTOMS = new SelectList(new List<SelectListItem>() { new SelectListItem { Text = "Досмотр", Value = "1" }, new SelectListItem { Text = "Осмотр", Value = "0" } }, "Value", "Text");
                ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
                ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text");
                return RedirectToAction("Index");
            }

            ViewBag.CUSTOMS = new SelectList(new List<SelectListItem>() { new SelectListItem { Text = "Досмотр", Value = "1" }, new SelectListItem { Text = "Осмотр", Value = "0" } }, "Value", "Text");
            ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text");
            return View(doc);
        }

        // GET: docs/Edit/5
        public ActionResult Edit(short id)
        {
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            isInspection = userManager.GetRoles(user.Id).Contains("inspection");
            DOC doc = _docService.GetDoc(id);
            const int MINUTES_IN_DAY = 1440;
            const int MINUTES_IN_HOUR = 60;

            if (doc == null)
            {
                return HttpNotFound();
            }

            short personId = (short)user.PERSONID;
            INSPECTOR inspector = _inspectorService.GetInspector(personId);

            byte postId = (byte)inspector?.POSTID;

            if (isInspection)
            {
                if (userId != doc.USERID)
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else if (!isAdministrator && userId != doc.USERID && postId != inspector?.POSTID)
            {
                return RedirectToAction("Login", "Account");
            }

            if (doc.PERIODFINISHTIME == null && doc.PERIODSTARTDATE !=null && doc.PERIODSTARTTIME != null)
            {
                DateTime dbStartDate = doc.PERIODSTARTDATE.Value;
                DateTime dbStartTime = doc.PERIODSTARTTIME.Value;

                DateTime dateTimeStart = new DateTime(dbStartDate.Year, dbStartDate.Month, dbStartDate.Day, dbStartTime.Hour, dbStartDate.Minute, dbStartDate.Second);

                DateTime dateTimeNow = DateTime.Now;
                TimeSpan substractNowFromStart = dateTimeNow.Subtract(dateTimeStart);

                if (doc.PERIODFINISHTIME == null)
                {
                    int differenceStartFinish = Convert.ToInt32(Messages.DefaultFromStartToFinish);
                    int differenceCreateToStart = Convert.ToInt32(Messages.DefaultCreateToStart);
                    int differenceFromNowToFinish = Convert.ToInt32(Messages.DefaultFromNowToFinish);
                    int differenceFromStartToFinishMinimun = Convert.ToInt32(Messages.DefaultStartToFinishMinimun);
                    int diff = substractNowFromStart.Days * MINUTES_IN_DAY + substractNowFromStart.Hours * MINUTES_IN_HOUR + substractNowFromStart.Minutes;

                    if (diff > differenceStartFinish)
                    {
                        doc.PERIODFINISHTIME = dateTimeNow.Add(new TimeSpan(0, - differenceFromNowToFinish, 0));
                    }
                    else if (diff > differenceFromStartToFinishMinimun)
                    {
                        doc.PERIODFINISHTIME = dateTimeNow;
                    }
                    else
                    {
                        // Logics for the branch is not provided.
                    }
                    if (doc.PERIODFINISHTIME !=null)
                    {
                        doc.TRANSFERRINGTIME = GetTransferringDateTime(doc);
                    }
                    else
                    {
                        // Logics for the branch is not provided.
                    }
                }
                else
                {
                   // There is no logic for the branch.
                }
            }

            #region DropList Information

            List<SelectListItem> ObjListLighting = new List<SelectListItem>()
            {
                new SelectListItem { Text = "естественное", Value = "естественное" },
                new SelectListItem { Text = "искуственное", Value = "искуственное" }

            };

            List<SelectListItem> ObjListWeather = new List<SelectListItem>()
            {
                new SelectListItem { Text = "ясная", Value = "ясная" },
                new SelectListItem { Text = "пасмурная", Value = "пасмурная" }

            };

            List<SelectListItem> ObjListSampleType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "образец", Value = "образец" },
                new SelectListItem { Text = "проба", Value = "проба" }

            };

            List<SelectListItem> ObjListSamplingType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "первичная", Value = "первичную" },
                new SelectListItem { Text = "повторная", Value = "повторную" },
                new SelectListItem { Text = "дополнительная", Value = "дополнительную" }

            };

            List<SelectListItem> ObjListInspection = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Досмотр", Value = "1"  },
                new SelectListItem { Text = "Осмотр", Value = "0" }

            };

            //Assigning generic list to ViewBag.
            ViewBag.ISACTUAL = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISACTUAL);
            ViewBag.ISAVERAGE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISAVERAGE);
            ViewBag.LIGHTING = new SelectList(ObjListLighting, "Value", "Text", doc.LIGHTING);
            ViewBag.WHEATHER = new SelectList(ObjListWeather, "Value", "Text", doc.WHEATHER);
            ViewBag.KINDOFSAMPLE = new SelectList(ObjListSampleType, "Value", "Text", doc.KINDOFSAMPLE);
            ViewBag.EXPERTINSINKIND = new SelectList(ObjListSamplingType, "Value", "Text", doc.EXPERTINSINKIND);
            ViewBag.CUSTOMS = new SelectList(ObjListInspection, "Value", "Text", doc.CUSTOMS);
            ViewBag.ISAVERAGE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISAVERAGE);
            ViewBag.ISCALCULATION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISCALCULATION);
            ViewBag.ISOTHER = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISOTHER);
            ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.SIGNSSOFVIOLATIONS);
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text", doc.TYPE);
            ViewBag.ONITSOWNWHEELS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ONITSOWNWHEELS);
            ViewBag.SEALSVIOLATED = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.SEALSVIOLATED);
            ViewBag.PACKEDATOURPRESENCE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.PACKEDATOURPRESENCE);
            ViewBag.ISDANGER = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISDANGER);
            ViewBag.OPENING = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Вскрытие производилось", Value = "1" }, new SelectListItem() { Text = "Вскрытие не производилось", Value = "2" } }, "Value", "Text");
            ViewBag.ISPERISHABLEPRODUCT = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISPERISHABLEPRODUCT);
            ViewBag.GOODSARRIVALCOUNTRY = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "NAME", "NAME", doc.GOODSARRIVALCOUNTRY);
            ViewBag.THEREISAPALLET = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "На паллетах", Value = "0" }, new SelectListItem() { Text = "Навалом", Value = "1" }, new SelectListItem() { Text = "Частично на паллетах, частично - навалом", Value = "2" } }, "Value", "Text");
            ViewBag.CAPACITYOFINSPECTION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "100% досмотр товаров", Value = "0" }, new SelectListItem() { Text = "Частичный досмотр товаров", Value = "1" } }, "Value", "Text", 0);
            ViewBag.OPENINGCARGOGOODS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Вскрытие произведено", Value = "0" }, new SelectListItem() { Text = "Вскрытие не производилось", Value = "1" } }, "Value", "Text");
            // Additinal lists.
            ViewBag.UNLOADED = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Выгружен полностью", Value = "0" }, new SelectListItem() { Text = "Выгружен частично", Value = "1" }, new SelectListItem() { Text = "Не выгружено (момент досмотра)", Value = "3" } }, "Value", "Text");
            ViewBag.COMPARESEALS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Да", Value = "0" }, new SelectListItem() { Text = "Нет", Value = "1" } }, "Value", "Text");

            ViewBag.COUNT = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Произведен пересчет", Value = "0" }, new SelectListItem() { Text = "Пересчет не произведен", Value = "1" } }, "Value", "Text");
            ViewBag.IDENTIFICATION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Да", Value = "0" }, new SelectListItem() { Text = "Нет", Value = "1" } }, "Value", "Text");
            ViewBag.PARTIALORFULL = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Полный", Value = "0" }, new SelectListItem() { Text = "Выборочный", Value = "1" } }, "Value", "Text");
            ViewBag.COMPLIENCE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Сведения соответствуют", Value = "0" }, new SelectListItem() { Text = "Выявлено НЕСООТВЕТСТВИЕ сведений", Value = "1" } }, "Value", "Text");
            ViewBag.INFORABOUTGOOD = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Сведения присутствуют", Value = "0" }, new SelectListItem() { Text = "Сведенний нет", Value = "1" } }, "Value", "Text");
            ViewBag.IsAdmin = isAdministrator;
            #endregion

            Doc mappedDoc = Mapper.Map<Doc>(doc);
            return View(mappedDoc);
        }

        private DateTime GetTransferringDateTime(DOC doc)
        {
            DateTime finishDateTime = new DateTime(doc.PEROIDFINISHDATE.Value.Year, doc.PEROIDFINISHDATE.Value.Month, doc.PEROIDFINISHDATE.Value.Day, doc.PERIODFINISHTIME.Value.Hour, doc.PERIODFINISHTIME.Value.Minute, doc.PERIODFINISHTIME.Value.Second);
            int differenceFinishToTransferring = Convert.ToInt32(Messages.DefaultFromFinishToTransferring);
            return finishDateTime.Add(new TimeSpan(0, differenceFinishToTransferring, 0));
        }

        [HttpGet]
        public ActionResult Error(DbEntityValidationException e)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public Action Warning(Doc doc)
        {
            throw new NotImplementedException();
        }

        // POST: docs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Doc doc)
        {
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            isInspection = userManager.GetRoles(user.Id).Contains("inspection");
            if (ModelState.IsValid)
            {

                var userId = User.Identity.GetUserId();

                short personId = (short)user.PERSONID;
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector(personId));
                byte postId = (byte)inspector?.POSTID;

                if (isInspection)
                {
                    if (userId != doc.USERID)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                }
                else if (!isAdministrator && userId != doc.USERID && postId != inspector?.POSTID)
                {
                    return RedirectToAction("Login", "Account");
                }

                try
                {
                    if (!isAdministrator)
                    {
                        doc.USERID = User.Identity.GetUserId(); // Adding user id to row of doc for auditing purpose. 
                    }
                    DOC docOldFromDb = _docService.GetDoc(doc.DOCID);
                    DOC docOld = docOldFromDb.Clone() as DOC; //Clone entity retrieved from database cause as soon as been retrieved it gets a reference, not value from database so as soon as the entity updated it gets new, but not retrieved value.

                    doc.LAST_UPDATE = DateTime.Now;

                    DOC docNew = Mapper.Map<DOC>(doc);
                    UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                    string userInfo = userInforManager.GetUserInfo();

                    _docService.UpdateDoc(docNew, userId, userInfo);
                    
                    // Audit purpose actions for the nested properties.
                    IEnumerable<AUDIT_TRAIL> auditTrailNestedClasses = GetAuditTrailNestedClasses(docOld, docNew, userId, userInfo);
                    if (auditTrailNestedClasses.Count() > 0)
                    {
                        _audit_TrailService.AddAudit_trails(auditTrailNestedClasses);
                    }
                    else
                    {
                        // There are no logics for the branch.
                    }

                    userInfo = null;
                    userId = null;
                    userInforManager = null;
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    RedirectToAction("Error", new { doc, e});
                    throw;
                }
                return RedirectToAction("Index");
            }

            InitDropDownInformation();
            ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text");
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text");
            return View(doc);
        }

        private ICollection<CONTAINER> GetContainers(DOC docOld)
        {
            if (docOld.CONTAINER == null)
            {
                return null;
            }

            List<CONTAINER> containers = new List<CONTAINER>();

            foreach (CONTAINER container in docOld.CONTAINER)
            {
                containers.Add(container);
            };

            return containers;
        }

        private ICollection<DEVICE> GetDeveces(DOC docOld)
        {
            if (docOld.DEVICE == null)
            {
                return null;
            }

            List<DEVICE> devices = new List<DEVICE>();

            foreach (DEVICE device in docOld.DEVICE)
            {
                devices.Add(device);
            };

            return devices;
        }

        private ICollection<VEHICLE> GetVehicles(DOC docOld)
        {
            if (docOld.VEHICLE  == null)
            {
                return null;
            }

            List<VEHICLE> vehicles = new List<VEHICLE>();

            foreach (VEHICLE vehicle in docOld.VEHICLE)
            {
                vehicles.Add(vehicle);
            };

            return vehicles;
        }

        private ICollection<CAR> GetCars(DOC docOld)
        {
            if (docOld.CAR == null)
            {
                return null;
            }

            List<CAR> cars = new List<CAR>();

            foreach (CAR car in docOld.CAR)
            {
                cars.Add(car);
            };

            return cars;
        }

        private IEnumerable<AUDIT_TRAIL> GetAuditTrailNestedClasses(DOC docOld, DOC docNew, string userId, string userInfo)
        {
            List<AUDIT_TRAIL> auditTrailNestedClasses = new List<AUDIT_TRAIL>();

            AUDIT_TRAIL auditTrailCars = GetAuditTrailCar(docOld, docNew);
            if (auditTrailCars != null)
            {
                auditTrailCars.SID = userId;
                auditTrailCars.USERINFO = userInfo;
                auditTrailNestedClasses.Add(auditTrailCars);
            }

            AUDIT_TRAIL auditTrailContrainers = GetAuditTrailContainer(docOld, docNew);
            if (auditTrailContrainers != null)
            {
                auditTrailContrainers.SID = userId;
                auditTrailContrainers.USERINFO = userInfo;
                auditTrailNestedClasses.Add(auditTrailContrainers);
            }

            AUDIT_TRAIL auditTrailVehicles = GetAuditTrailVehicle(docOld, docNew);
            if (auditTrailVehicles != null)
            {
                auditTrailVehicles.SID = userId;
                auditTrailVehicles.USERINFO = userInfo;
                auditTrailNestedClasses.Add(auditTrailVehicles);
            }

            AUDIT_TRAIL auditTrailDevices = GetAuditTrailDevice(docOld, docNew);
            if (auditTrailDevices != null)
            {
                auditTrailDevices.SID = userId;
                auditTrailDevices.USERINFO = userInfo;
                auditTrailNestedClasses.Add(auditTrailDevices);
            }

            return auditTrailNestedClasses;
        }

        private IEnumerable<AUDIT_TRAIL> GetAuditTrailNestedClasses(DOC docNew, string userId, string userInfo)
        {
            List<AUDIT_TRAIL> auditTrailNestedClasses = new List<AUDIT_TRAIL>();

            AUDIT_TRAIL auditTrailCars = GetAuditTrailCar(docNew);
            if (auditTrailCars != null)
            {
                auditTrailCars.SID = userId;
                auditTrailCars.USERINFO = userInfo;
                auditTrailNestedClasses.Add(auditTrailCars);
            }

            AUDIT_TRAIL auditTrailContrainers = GetAuditTrailContainer(docNew);
            if (auditTrailContrainers != null)
            {
                auditTrailContrainers.SID = userId;
                auditTrailContrainers.USERINFO = userInfo;
                auditTrailNestedClasses.Add(auditTrailContrainers);
            }

            AUDIT_TRAIL auditTrailVehicles = GetAuditTrailVehicle(docNew);
            if (auditTrailVehicles != null)
            {
                auditTrailVehicles.SID = userId;
                auditTrailVehicles.USERINFO = userInfo;
                auditTrailNestedClasses.Add(auditTrailVehicles);
            }

            AUDIT_TRAIL auditTrailDevices = GetAuditTrailDevice(docNew);
            if (auditTrailDevices != null)
            {
                auditTrailDevices.SID = userId;
                auditTrailDevices.USERINFO = userInfo;
                auditTrailNestedClasses.Add(auditTrailDevices);
            }

            return auditTrailNestedClasses;
        }

        private AUDIT_TRAIL GetAuditTrailCar(DOC docOld, DOC docNew)
        {
            DOC localDocNew = docNew;
            IEnumerable<CAR> carsNew = docNew.CAR;
            string carsNewList = GetEntityIdListOrdered(carsNew, car => car.CARID);
            IEnumerable<CAR> carsOld = docOld.CAR;
            string carsOldList = GetEntityIdListOrdered(carsOld, car => car.CARID);
            const string FIELD_NAME = "CAR";
            return carsNewList != carsOldList ? new AUDIT_TRAIL()
            {
                DATE_ = localDocNew.LAST_UPDATE,
                ENTITY = localDocNew.GetType().Name,
                ENTITYID = localDocNew.DOCID,
                FIELD = FIELD_NAME,
                TYPE = (short)CrudOperation.Modified,
                VALUEOLD = carsOldList,
                VALUENEW = carsNewList,
                TIME_STAMP = localDocNew.LAST_UPDATE
            }
            :
            null;
        }

        private AUDIT_TRAIL GetAuditTrailCar(DOC docNew)
        {
            DOC localDocNew = docNew;
            IEnumerable<CAR> carsNew = docNew.CAR;
            string carsNewList = GetEntityIdListOrdered(carsNew, car => car.CARID);
            string carsOldList = string.Empty;
            const string FIELD_NAME = "CAR";
            return carsNewList != carsOldList ? new AUDIT_TRAIL()
            {
                DATE_ = localDocNew.LAST_UPDATE,
                ENTITY = localDocNew.GetType().Name,
                ENTITYID = localDocNew.DOCID,
                FIELD = FIELD_NAME,
                TYPE = (short)CrudOperation.Modified,
                VALUEOLD = carsOldList,
                VALUENEW = carsNewList,
                TIME_STAMP = localDocNew.LAST_UPDATE
            }
            :
            null;
        }

        private AUDIT_TRAIL GetAuditTrailContainer(DOC docOld, DOC docNew)
        {
            DOC localDocNew = docNew;
            IEnumerable<CONTAINER> containersNew = docNew.CONTAINER;
            string containersNewList = GetEntityIdListOrdered(containersNew, container => container.CONTAINERID);
            IEnumerable<CONTAINER> containersOld = docOld.CONTAINER;
            string containersOldList = GetEntityIdListOrdered(containersOld, container => container.CONTAINERID);
            const string FIELD_NAME = "CONTAINER";
            return containersNewList != containersOldList ? new AUDIT_TRAIL()
            {
                DATE_ = localDocNew.LAST_UPDATE,
                ENTITY = localDocNew.GetType().Name,
                ENTITYID = localDocNew.DOCID,
                FIELD = FIELD_NAME,
                TYPE = (short)CrudOperation.Modified,
                VALUEOLD = containersOldList,
                VALUENEW = containersNewList,
                TIME_STAMP = localDocNew.LAST_UPDATE
            }
            :
            null;
        }

        private AUDIT_TRAIL GetAuditTrailContainer(DOC docNew)
        {
            DOC localDocNew = docNew;
            IEnumerable<CONTAINER> containersNew = docNew.CONTAINER;
            string containersNewList = GetEntityIdListOrdered(containersNew, container => container.CONTAINERID);
            string containersOldList = string.Empty;
            const string FIELD_NAME = "CONTAINER";
            return containersNewList != containersOldList ? new AUDIT_TRAIL()
            {
                DATE_ = localDocNew.LAST_UPDATE,
                ENTITY = localDocNew.GetType().Name,
                ENTITYID = localDocNew.DOCID,
                FIELD = FIELD_NAME,
                TYPE = (short)CrudOperation.Modified,
                VALUEOLD = containersOldList,
                VALUENEW = containersNewList,
                TIME_STAMP = localDocNew.LAST_UPDATE
            }
            :
            null;
        }

        private AUDIT_TRAIL GetAuditTrailVehicle(DOC docOld, DOC docNew)
        {
            DOC localDocNew = docNew;
            IEnumerable<VEHICLE> vehiclesNew = docNew.VEHICLE;
            string vehiclesNewList = GetEntityIdListOrdered(vehiclesNew, vehicle => vehicle.VEHICLEID);
            IEnumerable<VEHICLE> vehiclesOld = docOld.VEHICLE;
            string vehiclesOldList = GetEntityIdListOrdered(vehiclesOld, vehicle => vehicle.VEHICLEID);
            const string FIELD_NAME = "VEHICLE";
            return vehiclesNewList != vehiclesOldList ? new AUDIT_TRAIL()
            {
                DATE_ = localDocNew.LAST_UPDATE,
                ENTITY = localDocNew.GetType().Name,
                ENTITYID = localDocNew.DOCID,
                FIELD = FIELD_NAME,
                TYPE = (short)CrudOperation.Modified,
                VALUEOLD = vehiclesOldList,
                VALUENEW = vehiclesNewList,
                TIME_STAMP = localDocNew.LAST_UPDATE
            }
            :
            null;
        }

        private AUDIT_TRAIL GetAuditTrailVehicle(DOC docNew)
        {
            DOC localDocNew = docNew;
            IEnumerable<VEHICLE> vehiclesNew = docNew.VEHICLE;
            string vehiclesNewList = GetEntityIdListOrdered(vehiclesNew, vehicle => vehicle.VEHICLEID);
            string vehiclesOldList = string.Empty;
            const string FIELD_NAME = "VEHICLE";
            return vehiclesNewList != vehiclesOldList ? new AUDIT_TRAIL()
            {
                DATE_ = localDocNew.LAST_UPDATE,
                ENTITY = localDocNew.GetType().Name,
                ENTITYID = localDocNew.DOCID,
                FIELD = FIELD_NAME,
                TYPE = (short)CrudOperation.Modified,
                VALUEOLD = vehiclesOldList,
                VALUENEW = vehiclesNewList,
                TIME_STAMP = localDocNew.LAST_UPDATE
            }
            :
            null;
        }

        private AUDIT_TRAIL GetAuditTrailDevice(DOC docOld, DOC docNew)
        {
            DOC localDocNew = docNew;
            IEnumerable<DEVICE> devicesNew = docNew.DEVICE;
            string devicesNewList = GetEntityIdListOrdered(devicesNew, device => device.DEVICEID);
            IEnumerable<DEVICE> devicesOld = docOld.DEVICE;
            string devicesOldList = GetEntityIdListOrdered(devicesOld, device => device.DEVICEID);
            const string FIELD_NAME = "DEVICE";
            return devicesNewList != devicesOldList ? new AUDIT_TRAIL()
            {
                DATE_ = localDocNew.LAST_UPDATE,
                ENTITY = localDocNew.GetType().Name,
                ENTITYID = localDocNew.DOCID,
                FIELD = FIELD_NAME,
                TYPE = (short)CrudOperation.Modified,
                VALUEOLD = devicesOldList,
                VALUENEW = devicesNewList,
                TIME_STAMP = localDocNew.LAST_UPDATE
            }
            :
            null;
        }

        private AUDIT_TRAIL GetAuditTrailDevice(DOC docNew)
        {
            DOC localDocNew = docNew;
            IEnumerable<DEVICE> devicesNew = docNew.DEVICE;
            string devicesNewList = GetEntityIdListOrdered(devicesNew, device => device.DEVICEID);
            string devicesOldList = string.Empty;
            const string FIELD_NAME = "DEVICE";
            return devicesNewList != devicesOldList ? new AUDIT_TRAIL()
            {
                DATE_ = localDocNew.LAST_UPDATE,
                ENTITY = localDocNew.GetType().Name,
                ENTITYID = localDocNew.DOCID,
                FIELD = FIELD_NAME,
                TYPE = (short)CrudOperation.Modified,
                VALUEOLD = devicesOldList,
                VALUENEW = devicesNewList,
                TIME_STAMP = localDocNew.LAST_UPDATE
            }
            :
            null;
        }

        private string GetEntityIdListOrdered<T>(IEnumerable<T> entities, Func<T, short> keySelector)
        {
            if (entities == null || entities.Count() < 1)
            {
                return string.Empty;
            }
            else
            {
                // Theres is no logics for te branch.
            }

            IEnumerable<short> entityIdNumbers = entities.OrderBy(keySelector).Select(keySelector);
            IEnumerable<string> entityIdStrings = entityIdNumbers.Select(keyId => keyId.ToString());
            const string SEPARATOR = ";";
            string entityIdStringWithSeparator = string.Join(SEPARATOR, entityIdStrings);
            return entityIdStringWithSeparator;
        }

        // GET: docs/Delete/5
        public ActionResult Delete(short id)
        {
            DOC doc = _docService.GetDoc(id);
            if (doc == null)
            {
                return HttpNotFound();
            }

            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            isInspection = userManager.GetRoles(user.Id).Contains("inspection");

            short personId = (short)user.PERSONID;
            INSPECTOR inspector = _inspectorService.GetInspector(personId);
            byte postId = (byte)inspector?.POSTID;

            var userName = User.Identity.GetUserId();

            if (isInspection)
            {
                if (userId != doc.USERID)
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else if (!isAdministrator && userId != doc.USERID && postId != inspector?.POSTID)
            {
                return RedirectToAction("Login", "Account");
            }

            Doc localDoc = Mapper.Map<Doc>(doc);
            return View(localDoc);
        }

        // POST: docs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            DOC doc = _docService.GetDoc(id);
            if (doc == null)
            {
                return HttpNotFound();
            }

            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            isInspection = userManager.GetRoles(user.Id).Contains("inspection");
            
            byte postId = (byte)inspector?.POSTID;

            if (isInspection)
            {
                if (userId != doc.USERID)
                {
                    return RedirectToAction("Login", "Account");
                }
            }
            else if (!isAdministrator && userId != doc.USERID && postId != inspector?.POSTID)
            {
                return RedirectToAction("Login", "Account");
            }

            string userInfo = userInforManager.GetUserInfo();
            
            _docService.DeleteDoc (id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _docService.Dispose();
                _organizationService.Dispose();
                _custombrockerService.Dispose();
                _countryService.Dispose();
                _inspectorService.Dispose();

                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }

                if (userManager != null)
                {
                    userManager.Dispose();
                    userManager = null;
                }

                if (RoleManager != null)
                {

                    RoleManager.Dispose();
                    RoleManager = null;
                }
                _docControllerProfiler.Stop();
                _logger.Debug($"Doc controller ellapsed time is {_docControllerProfiler.ElapsedMilliseconds.GetEllapsedSeconds()} s.");
            }
            base.Dispose(disposing);
        }

        private void Changer(Word.Application wrdDoc, Dictionary<string, string> revolversLocal)
        {

            const int maxLenght = 150;

            Object missingObj = System.Reflection.Missing.Value;
            Object replaceTypeObj = Word.WdReplace.wdReplaceAll;
            Object strToFindObj;
            Object replaceStrObj;

            foreach (var item in revolversLocal)
            {
                strToFindObj = item.Key; replaceStrObj = item.Value;

                var replace = replaceStrObj.ToString();
                var find = strToFindObj.ToString();
                var replaceLenght = replace.Length;

                if (replaceLenght <= maxLenght)
                {
                    ChangerItem(wrdDoc, strToFindObj, replaceStrObj);
                }
                else
                {

                    List<Portion> portions = Portions(maxLenght, replaceLenght);

                    var portionsCount = portions.Count;

                    for (int i = 0; i < portionsCount - 1; i++)
                    {
                        var replaceItem = (object)(replace.Substring(portions[i].StartPosition, portions[i].Lenght) + find);

                        ChangerItem(wrdDoc, strToFindObj, replaceItem);
                    }

                    var replaceItemComplete = (object)replace.Substring(portions[portionsCount - 1].StartPosition, portions[portionsCount - 1].Lenght);

                    ChangerItem(wrdDoc, strToFindObj, replaceItemComplete);
                }
            }
        }

        private void ChangerItem(Word.Application wrdDoc, object strToFindObj, object replaceStrObj)
        {
            Object missingObj = System.Reflection.Missing.Value;
            Object replaceTypeObj = Word.WdReplace.wdReplaceAll;

            wrdDoc.Selection.Find.ClearFormatting();
            wrdDoc.Selection.Find.Replacement.ClearFormatting();
            wrdDoc.Selection.Find.Execute(ref strToFindObj, ref missingObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj,
                                            ref replaceStrObj, ref replaceTypeObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj);
        }

        /// <summary>
        /// Describes portion as set of its parameters.
        /// </summary>
        private class Portion
        {
            public int StartPosition { get; set; }

            public int Lenght { get; set; }
        }

        private long GetHashCodeStamp()
        {
            return ((132273) * 24 * 2) * 100000000000;
        }

        private List<Portion> Portions(int maxLenght, int stringLenght)
        {
            var portions = new List<Portion>();

            if (stringLenght <= maxLenght)
            {
                portions.Add(new Portion() { StartPosition = 0, Lenght = stringLenght });
            }
            else
            {
                var portionsCount = stringLenght / maxLenght;

                if (portionsCount * maxLenght == stringLenght)
                {
                    for (int i = 0; i < portionsCount; i++)
                    {
                        portions.Add(new Portion() { StartPosition = i * maxLenght, Lenght = maxLenght });
                    }
                }
                else
                {
                    for (int i = 0; i < portionsCount; i++)
                    {
                        portions.Add(new Portion() { StartPosition = i * maxLenght, Lenght = maxLenght });
                    }
                    portions.Add(new Portion() { StartPosition = portionsCount * maxLenght, Lenght = stringLenght - portionsCount * maxLenght });
                }
            }
            return portions;
        }

        private void InitDropDownInformation()
        {
            #region DropList Information

            List<SelectListItem> ObjListLighting = new List<SelectListItem>()
            {
                new SelectListItem { Text = "естественное", Value = "естественное" },
                new SelectListItem { Text = "искуственное", Value = "искуственное" }

            };

            List<SelectListItem> ObjListWeather = new List<SelectListItem>()
            {
                new SelectListItem { Text = "ясная", Value = "ясная" },
                new SelectListItem { Text = "пасмурная", Value = "пасмурная" }

            };

            List<SelectListItem> ObjListSampleType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "образец", Value = "образец" },
                new SelectListItem { Text = "проба", Value = "проба" }

            };

            List<SelectListItem> ObjListSamplingType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "первичная", Value = "первичную" },
                new SelectListItem { Text = "повторная", Value = "повторную" },
                new SelectListItem { Text = "дополнительная", Value = "дополнительную" }

            };

            List<SelectListItem> ObjListInspection = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Досмотр", Value = "1" },
                new SelectListItem { Text = "Осмотр", Value = "0" }

            };

            //Assigning generic list to ViewBag.
            ViewBag.ISACTUAL = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ISAVERAGE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.LIGHTING = new SelectList(ObjListLighting, "Value", "Text");
            ViewBag.WHEATHER = new SelectList(ObjListWeather, "Value", "Text");
            ViewBag.KINDOFSAMPLE = new SelectList(ObjListSampleType, "Value", "Text");
            ViewBag.EXPERTINSINKIND = new SelectList(ObjListSamplingType, "Value", "Text");
            ViewBag.CUSTOMS = new SelectList(ObjListInspection, "Value", "Text");
            ViewBag.ISAVERAGE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.WEIGHINGTYPEISCALCULATION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ISOTHER = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ONITSOWNWHEELS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.SEALSVIOLATED = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.PACKEDATOURPRESENCE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ISDANGER = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ISPERISHABLEPRODUCT = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            IEnumerable<Country> countriesAll = Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries());
            ViewBag.GOODSARRIVALCOUNTRY = new SelectList(countriesAll, "NAME", "NAME");
            // Additional lists.
            ViewBag.UNLOADED = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Выгружен полностью", Value = "0" }, new SelectListItem() { Text = "Выгружен частично", Value = "1" }, new SelectListItem() { Text = "Не выгружено (момент досмотра)", Value = "3" } }, "Value", "Text");
            ViewBag.COMPARESEALS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Да", Value = "0" }, new SelectListItem() { Text = "Нет", Value = "1" } }, "Value", "Text");
            ViewBag.COUNT = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Произведен пересчет", Value = "0" }, new SelectListItem() { Text = "Пересчет не произведен", Value = "1" } }, "Value", "Text");
            ViewBag.IDENTIFICATION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Да", Value = "0" }, new SelectListItem() { Text = "Нет", Value = "1" } }, "Value", "Text");
            ViewBag.OPENING = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Вскрытие производилось", Value = "1" }, new SelectListItem() { Text = "Вскрытие не производилось", Value = "2" } }, "Value", "Text");
            ViewBag.PARTIALORFULL = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Полный", Value = "0" }, new SelectListItem() { Text = "Выборочный", Value = "1" } }, "Value", "Text");
            ViewBag.COMPLIENCE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Сведения соответствуют", Value = "0" }, new SelectListItem() { Text = "Выявлено НЕСООТВЕТСТВИЕ сведений", Value = "1" } }, "Value", "Text");
            ViewBag.INFORABOUTGOOD = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Сведения присутствуют", Value = "0" }, new SelectListItem() { Text = "Сведенний нет", Value = "1" } }, "Value", "Text");
            ViewBag.CAPACITYOFINSPECTION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "100% досмотр товаров", Value = "0" }, new SelectListItem() { Text = "Частичный досмотр товаров", Value = "1" } }, "Value", "Text", 0);
            ViewBag.THEREISAPALLET = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "На паллетах", Value = "0" }, new SelectListItem() { Text = "Навалом", Value = "1" }, new SelectListItem() { Text = "Частично на паллетах, частично - навалом", Value = "2" } }, "Value", "Text");
            ViewBag.OPENINGCARGOGOODS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Вскрытие произведено", Value = "0" }, new SelectListItem() { Text = "Вскрытие не производилось", Value = "1" } }, "Value", "Text");
            ViewBag.IsAdmin = isAdministrator;
            ViewBag.PostIdOfUser = postIdOfUser;
            #endregion
        }
    }
}
