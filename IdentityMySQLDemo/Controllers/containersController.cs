﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Helpers;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using Messages = IdentityMySQLDemo.Properties.Resources;

namespace Focus.Controllers
{
    [Authorize(Roles = "inspection, admin, redactor, auditor")]
    public class containersController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private readonly IContainerService _containerService;
        private readonly ICountryService _countryService;
        private readonly IInspectorService _inspectorService;

        public containersController(IContainerService containerService, ICountryService countryService, IInspectorService inspectorService)
        {
            _containerService = containerService;
            _countryService = countryService;
            _inspectorService = inspectorService;
        }


        // Remote diagnostics.
        // GET: Container /NotRepeated/
        [HttpPost]
        public ActionResult NotRepeated(string REGNUMBER, string COUNTRYCODE, int? CONTAINERID)
        {

            if (IsUnique(REGNUMBER, COUNTRYCODE, CONTAINERID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            string countryName = COUNTRYCODE == "" ? "Не определена" : _countryService.GetAllCountries(
                c => c.ABBR2 == COUNTRYCODE).FirstOrDefault()?.NAME;

            return Json(
                    string.Format(
                        Messages.ErrorRemoteContainerRepeated,
                        REGNUMBER,
                        countryName ?? "Не определена"
                        ), JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(string regNumber, string countryCode, int? containerId)
        {
            var countryForSearch = countryCode == "" ? null : countryCode;
            if (containerId == null)
            {
                return !_containerService.ContainerExists
                (filter: c => c.REGNUMBER == regNumber && 
                    c.COUNTRYCODE == countryForSearch);
            }

            return !_containerService.ContainerExists
                (filter: c => c.REGNUMBER == regNumber &&
                    c.COUNTRYCODE == countryForSearch &&
                    c.CONTAINERID != containerId);
        }

        // GET: containers
        public ActionResult Index()
        {
            var containers = _containerService.GetAllContainers(includeProperties: "country").ToList();
            return View(Mapper.Map<IEnumerable<CONTAINER>, IEnumerable<Container>>(containers));
        }

        public JsonResult Json(short id)
        {
            var jsondata = _containerService.GetContainer(id);
            jsondata.COUNTRY = _countryService.GetCountry(jsondata.COUNTRYCODE);

            return Json(
                new
                {
                    id = jsondata?.CONTAINERID,
                    reg = jsondata?.REGNUMBER,
                    desc = jsondata?.NAME,
                    countryCode = jsondata.COUNTRY?.NAME,
                    countryCode2  = jsondata.COUNTRY?.ABBR2,
                    demension = jsondata?.DEMENSIONS
                },
                JsonRequestBehavior.AllowGet); 
        }

        public JsonResult JsonOrg(short id)
        {
            var jsondata = _containerService.GetContainer(id);
            jsondata.COUNTRY = _countryService.GetCountry(jsondata.COUNTRYCODE);

            return Json(
                new
                {
                    id = jsondata?.CONTAINERID,
                    reg = jsondata?.REGNUMBER,
                    desc = jsondata?.NAME,
                    countryCode = jsondata.COUNTRY?.NAME,
                    countryCode2 = jsondata.COUNTRY?.ABBR2,
                    demension = jsondata?.DEMENSIONS
                },
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Search(string regnumber, int[] containerSelectedIndexes)
        {
            if (string.IsNullOrEmpty(regnumber))
            {
                return Content($"<br /><p><b>Поисковая строка не должна быть пустой. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>4</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            if (regnumber.Trim().Length < 5)
            {
                return Content($"<br /><p><b>Поисковая строка должна содержать не менее 5 символов, за исключением пробела. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>4</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }


            int[] selectedIndexes = containerSelectedIndexes;
            IEnumerable<CONTAINER> containersFiltered;
            var containers = _containerService.GetAllContainers(o => o.REGNUMBER.Contains(regnumber)).ToList();
            if (containers.Count <= 0)
            {
                return Content($"<br /><p><b>Контейнеры с частью регистрационного номера <span style = 'background-color: #ffff00;'>{regnumber}</span> в базе данных не обнаружены.</b></p>");
            }
            if (selectedIndexes != null)
            {
                containersFiltered = containers.Where(x => !containerSelectedIndexes.Contains(x.CONTAINERID)).ToArray();
                if (containersFiltered.Count() <= 0)
                {
                    return HttpNotFound();
                }
                return PartialView(Mapper.Map<IEnumerable<CONTAINER>, IEnumerable<Container>>(containersFiltered));
            }
            return PartialView(Mapper.Map<IEnumerable<CONTAINER>, IEnumerable<Container>>(containers));
        }


        // GET: containers/Details/5
        public ActionResult Details(short id)
        {
            CONTAINER container = _containerService.GetContainer(id);
            if (container == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Container>(container));
        }

        // GET: containers/Create
        public ActionResult PartialCreate()
        {
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return PartialView();
        }
        
        // POST: containers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialCreate([Bind(Include = "CONTAINERID,REGNUMBER,NAME,COUNTRYCODE,DEMENSIONS,LAST_UPDATE")] Container container)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    // Block for audit. Setting needed variables and objects.
                    string userId = User.Identity.GetUserId();
                    ApplicationUser user = userManager.FindById(userId);
                    Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                    UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                    string userInfo = userInforManager.GetUserInfo();

                    // Set last update date of the entity.
                    container.LAST_UPDATE = DateTime.Now;

                    container.CONTAINERID = _containerService.AddContainer(Mapper.Map<CONTAINER>(container), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                    // Set null for unused variables.
                    userId = null;
                    user = null;
                    inspector = null;
                    userInforManager = null;
                    userInfo = null;

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }
                return PartialView("Search", new List<Container>() { container });
            }

            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return PartialView(container);
        }

        // GET: containers/Create
        public ActionResult Create()
        {
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return View();
        }

        // POST: containers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CONTAINERID,REGNUMBER,NAME,COUNTRYCODE,DEMENSIONS,LAST_UPDATE")] Container container)
        {
            if (ModelState.IsValid)
            {    
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                container.LAST_UPDATE = DateTime.Now;

                _containerService.AddContainer(Mapper.Map<CONTAINER>(container), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }

            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return View(container);
        }


        // GET: containers/Edit/5
        public ActionResult PartialEdit(short id)
        {
            CONTAINER container = _containerService.GetContainer(id);
            if (container == null)
            {
                return HttpNotFound();
            }
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", container.COUNTRYCODE);
            return PartialView(Mapper.Map<Container>(container));
        }

        // POST: containers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialEdit([Bind(Include = "CONTAINERID,REGNUMBER,NAME,COUNTRYCODE,DEMENSIONS,LAST_UPDATE")] Container container)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                container.LAST_UPDATE = DateTime.Now;

                _containerService.UpdateContainer(Mapper.Map<CONTAINER>(container), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return PartialView("ContainerEdited", container);
            }
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return PartialView(container);
        }

        // GET: containers/Edit/5
        public ActionResult Edit(short id)
        {
            CONTAINER container = _containerService.GetContainer(id);
            if (container == null)
            {
                return HttpNotFound();
            }
            container.USERID = User.Identity.GetUserId();
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", container.COUNTRYCODE);
            return View(Mapper.Map<Container>(container));
        }

        // POST: containers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CONTAINERID,REGNUMBER,NAME,COUNTRYCODE,DEMENSIONS,LAST_UPDATE")] Container container)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                container.LAST_UPDATE = DateTime.Now;

                _containerService.UpdateContainer(Mapper.Map<CONTAINER>(container), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }
            ViewBag.COUNTRYCODE = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            
            return View(container);
        }

        // GET: containers/Delete/5
        public ActionResult PartialDelete(short id)
        {
            CONTAINER container = _containerService.GetContainer(id);
            if (container == null)
            {
                return new JsonResult();
            }
            return PartialView(Mapper.Map<Container>(container));
        }

        // POST: containers/Delete/5
        [HttpPost, ActionName("PartialDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult PartialDeleteConfirmed(short id)
        {
            CONTAINER container = _containerService.GetContainer(id);
            if (container == null)
            {
                return new JsonResult();
            }

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _containerService.DeleteContainer(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return PartialView("ContainerDeleted", Mapper.Map<Container>(container));
        }

        // GET: containers/Delete/5
        public ActionResult Delete(short id)
        {
            CONTAINER container = _containerService.GetContainer(id);
            if (container == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Container>(container));
        }

        // POST: containers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _containerService.DeleteContainer(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

		// GET: vehicles/JsonDelete/5
		public ActionResult JsonDelete(short id)
        {
            CONTAINER jsondata = _containerService.GetContainer(id);

            var countryName = jsondata.COUNTRYCODE ?? string.Empty;

            countryName = string.IsNullOrEmpty(countryName) ? "" : _countryService.GetCountry(jsondata.COUNTRYCODE).NAME;

            var anonArray = new[] { new {
                message = new MvcHtmlString($"<p><b>Данные о контейнере с рег. номером '{jsondata.REGNUMBER ?? string.Empty}', " +
                        $"зарегистрированном в '{countryName}' не могут быть удалены, " +
                        $"поскольку от них зависят следующие таможенные документы:</b></p>"),
                name = $"Рег. номер: {jsondata.REGNUMBER} {jsondata.COUNTRYCODE}",
                action =  @"\Car\Delete\" + id.ToString(),
                enitities  = jsondata.DOC.Select(d => $"Номер АТД '{d.NUM}' от {d.PERIODSTARTDATE.Value.ToShortDateString()} {d.CONSIGNEENAME}").Take(5)}
            };

            return Json(
               new
               {
                   anonArray
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _containerService.Dispose();
                _countryService.Dispose();
    }
            base.Dispose(disposing);
        }
    }
}
