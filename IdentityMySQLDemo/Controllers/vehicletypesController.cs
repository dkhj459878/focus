﻿using AspNet.Identity.MySQL;
using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.Web.Models;
using IdentityMySQLDemo.Models;
using log4net;
using Focus.Web.Helpers;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Messages = IdentityMySQLDemo.Properties.Resources;

namespace Focus.Controllers
{
    [Authorize(Roles = "admin, auditor")]
    public class vehicletypesController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ApplicationDbContext db = new ApplicationDbContext("OracleDbContext");

        UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("OracleDbContext")));
        RoleManager<IdentityRole> RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext("OracleDbContext")));

        private IVehicletypeService _vehicletypeService;
        private readonly IInspectorService _inspectorService;

        public vehicletypesController(IVehicletypeService vehicletypeService, IInspectorService inspectorService)
        {
            _vehicletypeService = vehicletypeService;
            _inspectorService = inspectorService;
        }

        // Remote diagnostics.
        // GET: vehicletypes /NotRepeated/
        public JsonResult NoRepeated(string name, byte? VEHICLETYPEID)
        {
            if (IsUnique(name, VEHICLETYPEID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteVehicletypeRepeated,
                        name),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(string name, byte? vehicelTypeId)
        {
            if (vehicelTypeId == null)
            {
                return !_vehicletypeService.VehicletypeExists
                (filter: v => v.NAME == name);
            }
            return !_vehicletypeService.VehicletypeExists
                 (filter: v => v.NAME == name &&
                 v.VEHICLETYPEID != vehicelTypeId);
        }

        // GET: vehicletypes
        public ActionResult Index()
        {
            var vehicletypes = _vehicletypeService.GetAllVehicletypes().ToList();
            return View(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(vehicletypes));
        }

        // GET: vehicletypes/Details/5 
        public ActionResult Details(byte id)
        {
            VEHICLETYPE vehicletype = _vehicletypeService.GetVehicletype(id);
            if (vehicletype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehicletype>(vehicletype));
        }

        // GET: vehicletypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: vehicletypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Vehicletype vehicletype)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                vehicletype.LAST_UPDATE = DateTime.Now;

                _vehicletypeService.AddVehicletype(Mapper.Map<VEHICLETYPE>(vehicletype), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }

            return View(vehicletype);
        }

        // GET: vehicletypes/Edit/5
        public ActionResult Edit(byte id)
        {
            VEHICLETYPE vehicletype = _vehicletypeService.GetVehicletype(id);
            if (vehicletype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehicletype>(vehicletype));
        }

        // POST: vehicletypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VEHICLETYPEID,NAME,DESCRIPTION,LAST_UPDATE")] Vehicletype vehicletype)
        {
            if (ModelState.IsValid)
            {
                // Block for audit. Setting needed variables and objects.
                string userId = User.Identity.GetUserId();
                ApplicationUser user = userManager.FindById(userId);
                Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
                UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
                string userInfo = userInforManager.GetUserInfo();

                // Set last update date of the entity.
                vehicletype.LAST_UPDATE = DateTime.Now;

                _vehicletypeService.UpdateVehicletype(Mapper.Map<VEHICLETYPE>(vehicletype), userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

                // Set null for unused variables.
                userId = null;
                user = null;
                inspector = null;
                userInforManager = null;
                userInfo = null;

                return RedirectToAction("Index");
            }
            return View(vehicletype);
        }

        // GET: vehicletypes/Delete/5
        public ActionResult Delete(byte id)
        {
            var vehicletype = _vehicletypeService.GetVehicletype(id);
            if (vehicletype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehicletype>(vehicletype));
        }

        // POST: vehicletypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(byte id)
        {
            VEHICLETYPE vehicletype = _vehicletypeService.GetVehicletype(id);

            // Block for audit. Setting needed variables and objects.
            string userId = User.Identity.GetUserId();
            ApplicationUser user = userManager.FindById(userId);
            Inspector inspector = Mapper.Map<Inspector>(_inspectorService.GetInspector((short)user.PERSONID));
            UserInforManager userInforManager = new UserInforManager(inspector, user.UserName);
            string userInfo = userInforManager.GetUserInfo();

            _vehicletypeService.DeleteVehicletype(id, userId: User.Identity.GetUserId() ?? string.Empty, userInfo: userInfo ?? string.Empty);

            // Set null for unused variables.
            userId = null;
            user = null;
            inspector = null;
            userInforManager = null;
            userInfo = null;

            return RedirectToAction("Index");
        }

        public ActionResult JsonDelete(byte id)
        {
            VEHICLETYPE jsondata = _vehicletypeService.GetVehicletype(id);

            var vehicleType = jsondata.NAME;

            var vehicleTypeCount = jsondata.VEHICLEKIND?.Count;

            return Json(
               new
               {
                   vehicleTypeId = id,
                   vehicleTypeName = vehicleType,
                   vehicleTypeCount,
                   vehicleKinds = jsondata.VEHICLEKIND.Select(vk => vk.NAME)
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _vehicletypeService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
