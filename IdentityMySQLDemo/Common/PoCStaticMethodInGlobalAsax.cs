﻿using System;

namespace Focus.Web.Common
{
    public static class PoCStaticMethodInGlobalAsax
    {
        private static DateTime dateTimeCreation;
        private static string creationContext;

        public static string GetCreationContext()
        {
            return creationContext;
        }

        public static void SetCreationContext(string value)
        {
            creationContext = value;
        }

        public static DateTime GetDateTimeCreation()
        {
            return dateTimeCreation;
        }

        public static void SetDateTimeCreation(DateTime value)
        {
            dateTimeCreation = value;
        }
    }
}