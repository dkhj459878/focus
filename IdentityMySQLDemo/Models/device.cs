﻿namespace Focus.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Device
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Device()
        {
        }

        [Display(Name = "Код ")]
        public short DEVICEID { get; set; }
        [Display(Name = "Тип")]
        [Required(ErrorMessage = "Требуется выбрать тип ТСТК.")]
        public byte DEVICETYPEID { get; set; }
        [Required(ErrorMessage = "Требуется ввести наименование ТСТК.")]
        [Display(Name = "Наименование")]
        [MaxLength(45)]
        public string NAME { get; set; }
        [Display(Name = "Марка")]
        [Remote("NotRepeated", "Device", HttpMethod = "POST", AdditionalFields = "DEVICETYPEID, MODEL, SERIALNUMBER, DEVICEID")]
        [Required(ErrorMessage = "Требуется ввести марку ТСТК.")]
        [MaxLength(45)]
        public string MARK { get; set; }
        [Required(ErrorMessage = "Требуется ввести модель ТСТК.")]
        [Display(Name = "Модель")]
        [MaxLength(45)]
        public string MODEL { get; set; }
        [Display(Name = "Серийный номер")]
        [MaxLength(10)]
        public string SERIALNUMBER { get; set; }
        [Display(Name = "Дата поверки весов (ИДК)")]
        public DateTime? CALLIBRATION { get; set; }
        [Display(Name = "Параметры весов")]
        [MaxLength(150)]
        public string PARAMETERSOFSCALES { get; set; }
        [Display(Name = "Параметры ИДК")]
        [MaxLength(150)]
        public string PARAMETERSXRAYCOMPLEXES { get; set; }
        [Display(Name = "Параметры фотоаппарата / видеокамеры")]
        [MaxLength(120)]
        public string PARAMETERSOFCAMERA { get; set; }
        [Display(Name = "Телефон сервиса")]
        [MaxLength(250)]
        public string PHONE { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime LAST_UPDATE { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        public virtual Devicetype DEVICETYPE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc> DOC { get; set; }
    }
}
