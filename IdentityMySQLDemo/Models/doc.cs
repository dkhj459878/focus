﻿namespace Focus.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Doc
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Doc()
        {
        }

        [Display(Name = "Код документа")]
        public short DOCID { get; set; }
        [Range(1, 99999, ErrorMessage = "Требуется выбрать получателя/отправителя")]
        [Required(ErrorMessage = "Требуется выбрать получателя.")]
        [Display(Name = "Код получателя")]
        public short? CONSIGNEEID { get; set; }
        [Required(ErrorMessage = "Требуется ввести наименование получателя/отправителя.")]
        [Display(Name = "Наименование")]
        [MaxLength(155)]
        public string CONSIGNEENAME { get; set; }
        [Display(Name = "Адрес")]
        [MaxLength(255)]
        public string CONSIGNEEADDRESS { get; set; }
        [Required(ErrorMessage = "Требуется ввести УНП получателя/отправителя.")]
        [Display(Name = "УНП")]
        [MaxLength(255)]
        public string CONSIGNEETIN { get; set; }
        [Required(ErrorMessage = "Требуется ввести имя представителя.")]
        [Display(Name = "Имя")]
        [MaxLength(45)]
        public string FIRSTNAME { get; set; }
        [Required(ErrorMessage = "Требуется выбрать инспектора.")]
        [Display(Name = "Код инспектора")]
        public short? INSPECTORID { get; set; }
        [Required(ErrorMessage = "Требуется ввести должность инспектора.")]
        [Display(Name = "Должность инспектора")]
        [MaxLength(55)]
        public string INSPECTORTITLE { get; set; }
        [Required(ErrorMessage = "Требуется ввести фамилию и инициалы инспектора.")]
        [Display(Name = "Фамилия, инициалы")]
        [MaxLength(45)]
        public string INSPECTORSHORTNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести подразделение инспектора.")]
        [Display(Name = "Отдел (подразделение) инспектора")]
        [MaxLength(45)]
        public string INSPECTORDEVISION { get; set; }
        [Display(Name = "Код представителя")]
        [Required(ErrorMessage = "Требуется выбрать уполномоченного представителя.")]
        public short? ID { get; set; }
        [Required(ErrorMessage = "Требуется ввести фамилию представителя.")]
        [Display(Name = "Фамилия")]
        [MaxLength(45)]
        public string LASTNAME { get; set; }
        //[Required(ErrorMessage = "Требуется ввести отчество представителя.")]
        [Display(Name = "Отчество")]
        [MaxLength(45)]
        public string FATHERSNAME { get; set; }
        [Required(ErrorMessage = "Требуется ввести номер паспорта представителя.")]
        [Display(Name = "Номер")]
        [MaxLength(255)]
        public string PASSPORTNUMBER { get; set; }
        [Required(ErrorMessage = "Требуется ввести дату выдачи паспорта представителя.")]
        [Display(Name = "Дата")]
        [MaxLength(45)]
        public string PASSPORTISSUEDDATE { get; set; }
        [Required(ErrorMessage = "Требуется ввести орган выдачи паспорта представителя.")]
        [Display(Name = "Выдан")]
        [MaxLength(255)]
        public string PASSPORTAUTHORITYBODY { get; set; }
        [Required(ErrorMessage = "Требуется ввести место работы представителя.")]
        [Display(Name = "Место работы")]
        [MaxLength(255)]
        public string PLACEOFWORK { get; set; }
        [Required(ErrorMessage = "Требуется ввести должность представителя.")]
        [Display(Name = "Должность")]
        [MaxLength(130)]
        public string POST { get; set; }
        [Display(Name = "Получатель/отправитель")]
        public byte? TYPE { get; set; }
        [Display(Name = "Адрес")]
        [MaxLength(500)]
        public string ADDRESS { get; set; }
        [Display(Name = "Телефон")]
        [MaxLength(45)]
        public string PHONE { get; set; }
        [Display(Name = "Факс")]
        [MaxLength(45)]
        public string FAX { get; set; }
        [Display(Name = "Кол-во стр. фотографий")]
        [MaxLength(45)]
        public string PHOTOPAGES { get; set; }
        [Display(Name = "Кол-во зкз. фотографий")]
        [MaxLength(45)]
        public string PHOTOCOPIES { get; set; }
        [Display(Name = "Кол-во доп. приложений")]
        [MaxLength(45)]
        public string ADDITIONALNUMBERS { get; set; }
        [Display(Name = "Наим. прил. 1")]
        [MaxLength(45)]
        public string ADDITIONALNAME1 { get; set; }
        [Display(Name = "Кол-во стр. прил. 1")]
        [MaxLength(45)]
        public string ADDITIONALPAGES1 { get; set; }
        [Display(Name = "Кол-во экз. прил. 1")]
        [MaxLength(45)]
        public string ADDITIONALCOPIES1 { get; set; }
        [Display(Name = "Наим. прил. 2")]
        [MaxLength(45)]
        public string ADDITIONALNAME2 { get; set; }
        [Display(Name = "Кол-во страниц прил. 2")]
        [MaxLength(45)]
        public string ADDITIONALPAGES2 { get; set; }
        [Display(Name = "Кол-во экз. прил. 2")] 
        [MaxLength(45)]
        public string ADDITIONALCOPIES2 { get; set; }
        [Display(Name = "Телефон 2")]
        [MaxLength(450)]
        public string CHIEFPHONENUMBER { get; set; }
        [Display(Name = "Наиме. прил. 3")]
        [MaxLength(45)]
        public string ADDITIONALNAME3 { get; set; }
        [Display(Name = "Кол-во страниц прил. 3")]
        [MaxLength(45)]
        public string ADDITIONALPAGES3 { get; set; }
        [Display(Name = "Кол-во экз. прил. 3")]
        [MaxLength(45)]
        public string ADDITIONALCOPIES3 { get; set; }
        [Display(Name = "Наим. прил. 4")]
        [MaxLength(45)]
        public string ADDITIONALNAME4 { get; set; }
        [Display(Name = "Кол-во страниц прил. 4")]
        [MaxLength(45)]
        public string ADDITIONALPAGES4 { get; set; }
        [Display(Name = "Кол-во экз. прил. 4")]
        [MaxLength(45)]
        public string ADDITIONALCOPIES4 { get; set; }
        [Display(Name = "Наим. прил. 5")]
        [MaxLength(45)]
        public string ADDITIONALNAME5 { get; set; }
        [Display(Name = "Кол-во страниц прил. 5")]
        [MaxLength(45)]
        public string ADDITIONALPAGES5 { get; set; }
        [Display(Name = "Кол-во экз. прил. 5")]
        [MaxLength(45)]
        public string ADDITIONALCOPIES5 { get; set; }
        [Display(Name = "Номер АТД")]
        [Remote("NotRepeated", "docs", HttpMethod = "POST", AdditionalFields = "DOCID")]
        [MaxLength(45)]
        public string NUM { get; set; }
        [Display(Name = "Номер акта отбора проб и образцов")]
        [MaxLength(45)]
        public string SAMPLINGNUM { get; set; }
        [Display(Name = "Номер решения о назначении таможенной экспертизы")]
        [MaxLength(45)]
        public string DECISIONNUM { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Дата")]
        public DateTime? PERIODSTARTDATE { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Дата")]
        public DateTime? PEROIDFINISHDATE { get; set; }
        [DataType(DataType.Time)]
        [Display(Name = "Время")]
        public DateTime? PERIODSTARTTIME { get; set; }
        [Required(ErrorMessage = "Требуется указать место проведения таможенного досмотра (осмотра).")]
        [Display(Name = "Место таможенного досмотра (осмотра)")]
        [MaxLength(350)]
        public string CUSTOMSPLACE { get; set; }
        [DataType(DataType.Time)]
        [Display(Name = "Время")]
        public DateTime? PERIODFINISHTIME { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Дата")]
        public DateTime? TRANSFERRINGDATE { get; set; }
        [DataType(DataType.Time)]
        [Display(Name = "Время")]
        public DateTime? TRANSFERRINGTIME { get; set; }
        [Display(Name = "ДТ")]
        [MaxLength(450)]
        public string DOCDECLARATIONONGOODS { get; set; }
        [Display(Name = "ТД")]
        [MaxLength(450)]
        public string DOCTRANSITDECLARATION { get; set; }
        [Display(Name = "Номер")]
        [MaxLength(450)]
        public string DOCCMRNUM { get; set; }
        [Display(Name = "Дата")]
        [MaxLength(45)]
        public string DOCCMRDATE { get; set; }
        [Display(Name = "Номер")]
        [MaxLength(450)]
        public string DOCCARNERNUM { get; set; }
        [Display(Name = "Дата")]
        [MaxLength(45)]
        public string DOCCARNETDATE { get; set; }
        [Display(Name = "Уведомление о размещении в зоне таможенного контроля")]
        [MaxLength(450)]
        public string DOCNOTIFICATIONPLACEMENT { get; set; }
        [Display(Name = "Разрешение о помещении под таможенный контроль")]
        [MaxLength(450)]
        public string DOCPERMISSIONPLACEMENT { get; set; }
        [Display(Name = "Номер")]
        [MaxLength(450)]
        public string DOCINVOCENUM { get; set; }
        [Display(Name = "Дата")]
        [MaxLength(45)]
        public string DOCINVOICEDATE { get; set; }
        [Display(Name = "Описание товара")]
        [MaxLength(65536)]
        public string GOODSDESCRIPTION { get; set; }
        [Display(Name = "Общий вес брутто, кг")]
        public decimal? GOODSTOTALBRUTTO { get; set; }
        [Display(Name = "Страна отправления")]
        [MaxLength(45)]
        public string GOODSARRIVALCOUNTRY { get; set; }
        [Display(Name = "ТС, как товар, перемещается своим ходом")]
        public byte? ONITSOWNWHEELS { get; set; }
        [Display(Name = "Наименование первичной упаковки")]
        [MaxLength(45)]
        public string PACKAGEINITIALNAME { get; set; }
        [Display(Name = "Наименование упаковки")]
        [MaxLength(45)]
        public string PACKAGENAME { get; set; }
        [Display(Name = "Наименование грузовых мест")]
        [MaxLength(45)]
        public string PLACESNAME { get; set; }
        [Display(Name = "Кол-во грузовых мест")]
        [MaxLength(45)]
        public string PLACESNUMBER { get; set; }
        [Display(Name = "Вопросы")]
        [MaxLength(2000)]
        public string QUESTIONS { get; set; }
        [Display(Name = "Наименования объектов, отобраных в качестве проб и (или) образцов")]
        [MaxLength(255)]
        public string GOODSDES { get; set; }
        [Display(Name = "Вес образца (пробы)")]
        public decimal? SAMPLEWEIGHT { get; set; }
        [Display(Name = "Стоимость образца (пробы)")]
        public decimal? SAMPLECOST { get; set; }
        [Display(Name = "Общий вес образцов (проб)")]
        public decimal? TOTALWEIGHT { get; set; }
        [Display(Name = "Общая стоимость образцов (проб)")]
        public decimal? TOTALCOST { get; set; }
        [Display(Name = "Наименование валюты")]
        [MaxLength(45)]
        public string CURRENCYNAME { get; set; }
        [Display(Name = "Отобранные пробы и (или) образцы опасные")]
        [MaxLength(45)]
        public string ISDANGER { get; set; }
        [Display(Name = "Отобранные пробы и (или) образцы скоропортящиеся")]
        [MaxLength(45)]
        public string ISPERISHABLEPRODUCT { get; set; }
        [Display(Name = "Пробы/образцы")]
        [MaxLength(45)]
        public string KINDOFSAMPLE { get; set; }
        [Display(Name = "Кол-во проб (образцов)")]
        [MaxLength(45)]
        public string SAMPLESNUM { get; set; }
        [Display(Name = "Вид экспертизы")]
        [MaxLength(45)]
        public string EXPERTINSINKIND { get; set; }
        [Display(Name = "Номер пломбы")]
        [MaxLength(45)]
        public string SEALNUM { get; set; }
        [Display(Name = "Факт. вес нетто, кг")]
        public decimal? WEIGHINGACTUALNETTO { get; set; }
        [Display(Name = "Фак. вес брутто, кг")]
        public decimal? WEIGHINGACTUALBRUTTO { get; set; }
        [Display(Name = "Метод фактического взвешивания")]
        public byte? ISACTUAL { get; set; }
        [Display(Name = "Метод определения среднего")]
        public byte? ISAVERAGE { get; set; }
        [Display(Name = "Кол-во взвешиваний")]
        public short? ISAVERAGEPLACESNUMBER { get; set; }
        [Display(Name = "Результаты 1-ого взвешивания")]
        public decimal? ISAVERAGEFIRST { get; set; }
        [Display(Name = "Результаты 2-ого взвешивания")]
        public decimal? ISAVERAGESECOND { get; set; }
        [Display(Name = "Результаты 3-его взвешивания")]
        public decimal? ISAVERAGETHIRD { get; set; }
        [Display(Name = "Данные расчета")]
        [MaxLength(255)]
        public string ISCALCULATIONDESCRIPTION { get; set; }
        [Display(Name = "Иной метод")]
        public byte? ISOTHER { get; set; }
        [Display(Name = "Способ расчета (для иного метода)")]
        [MaxLength(255)]
        public string ISOTHERDESCRIPTION { get; set; }
        [Display(Name = "Расчетный метод")]
        public sbyte? ISCALCULATION { get; set; }
        [Display(Name = "Обнаружены признаки право-нарушений")]
        [Required(ErrorMessage = "Требуется ввести сведения о наличии признаков правонарушений.")]
        public byte SIGNSSOFVIOLATIONS { get; set; }
        [Display(Name = "Описание признаков право-нарушений")]
        [MaxLength(255)]
        public string VIOLATIONSDESCRIPTION { get; set; }
        [Display(Name = "Форма таможенного контроля")]
        public byte? CUSTOMS { get; set; }
        [Display(Name = "Уровень радиационного фона")]
        [MaxLength(45)]
        public string RADIATIONBACKGROUND { get; set; }
        [Required(ErrorMessage = "Требуется ввести результаты таможенного досмотра (таможенного осмотра).")]
        [Display(Name = "Результаты таможенного досмотра (осмотра)")]
        [MaxLength(65536)]
        public string CUSTOMSRESULT { get; set; }
        [Display(Name = "Обновлен")]
        public DateTime LAST_UPDATE { get; set; }
        [Display(Name = "Радиационный фон")]
        [MaxLength(45)]
        public string GROUND { get; set; }
        [Display(Name = "Описание средства идентификации")]
        public string SEALS { get; set; }
        [Display(Name = "Пломбы (др. средства идентификации) нарушены")]
        public byte? SEALSVIOLATED { get; set; }
        [Display(Name = "Наложены средства идентификации")]
        [MaxLength(45)]
        public string IDENTIFICATIONTOOLS { get; set; }
        [Display(Name = "Кол-во наложенных средств идентификации")]
        public short? IDENTIFICATIONTOOLSNUMBER { get; set; }
        [Display(Name = "Грузовые места упакованы в нашем присутствии")]
        public byte? PACKEDATOURPRESENCE { get; set; }
        [Display(Name = "Погода при отборе проб и (или) образцов товаров")]
        [MaxLength(45)]
        public string WHEATHER { get; set; }
        [Display(Name = "Освещение при отборе проб и (или) образцов товаров")]
        [MaxLength(45)]
        public string LIGHTING { get; set; }
        [Display(Name = "Упаковка проб и (или) образцов товаров")]
        [MaxLength(45)]
        public string SAMPLINGPACKAGE { get; set; }
        [Display(Name = "Основания для проведения таможенной экспертизы (№ профиля)")]
        [MaxLength(45)]
        public string PROFILENUMBER { get; set; }
        [Display(Name = "Код должностного лица, принявшего решение о назначении таможенной экспертизы)")]
        public short? CHIEFID { get; set; }
        [Display(Name = "ФИО")]
        [MaxLength(45)]
        public string CHIEFSHORTNAME { get; set; }
        [Display(Name = "Подразделение")]
        [MaxLength(45)]
        public string CHIEFPOST { get; set; }
        [Display(Name = "Должность")]
        [MaxLength(45)]
        public string CHIEFTITLE { get; set; }
        [Display(Name = "Основание (профиль, индикатор)")]
        [MaxLength(500)]
        public string BASIS { get; set; }
        [Display(Name = "Основания и конфигурации проверок")]
        [MaxLength(450)]
        public string CONTROL { get; set; }
        [Display(Name = "Задание")]
        [MaxLength(400)]
        public string TASK { get; set; }
        [Display(Name = "Отзыв")]
        [MaxLength(500)]
        public string REWIEW { get; set; }
        [Display(Name = "Общая оценка")]
        public byte? COMMONRATE { get; set; }
        [Display(Name = "Оценка за ускорение")]
        public byte? SPEEDUPRATE { get; set; }
        public byte? RATE3 { get; set; }
        public byte? RATE5 { get; set; }
        public byte? RATE4 { get; set; }
        [Display(Name = "Создан")]
        public DateTime TIMESTAMP_ON_CREATE { get; set; }
        public byte? ISCOULDBEPRENTED { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "SID")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        public virtual Custombrocker CUSTOMBROCKER { get; set; }
        public virtual Inspector INSPECTOR { get; set; }
        public virtual Inspector INSPECTOR1 { get; set; }
        public virtual Organization ORGANIZATION { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Car> CAR { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Container> CONTAINER { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Device> DEVICE { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Vehicle> VEHICLE { get; set; }
    }
}
