﻿namespace Focus.Web.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Organization
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Organization()
        {
        }

        [Display(Name = "Код")]
        public short ORGANIZATIONID { get; set; }
        [Required(ErrorMessage = "Требуется ввести УНП организации.")]
        [Remote("NotRepeatedITN", "organizations", HttpMethod = "POST",
            AdditionalFields = "ORGANIZATIONID,COUNTRYID")]
        [Display(Name = "УНП")]
        public string UNP { get; set; }
        [Required(ErrorMessage = "Требуется ввести наименование организации.")]
        [Remote("NotRepeatedName", "organizations", HttpMethod = "POST",
            AdditionalFields = "ORGANIZATIONID,COUNTRYID")]
        [Display(Name = "Название")]
        [MaxLength(255)]
        public string NAME { get; set; }
        [Display(Name = "Полный адрес")]
        [MaxLength(500)]
        public string ADDRESSFULL { get; set; }
        [Required(ErrorMessage = "Требуется ввести адрес организации.")]
        [Display(Name = "Адрес")]
        [MaxLength(255)]
        public string ADDRESS { get; set; }
        [Display(Name = "Страна")]
        [MaxLength(3)]
        [Required(ErrorMessage = "Требуется ввести страну.")]
        public string COUNTRYID { get; set; }
        [Display(Name = "Индекс")]
        [MaxLength(255)]
        public string POST_INDEX { get; set; }
        [Display(Name = "Регион")]
        [MaxLength(255)]
        public string REGION { get; set; }
        [Display(Name = "Район")]
        [MaxLength(255)]
        public string DISTRICT { get; set; }
        [Display(Name = "Тип населенного пункта")]
        [MaxLength(15)]
        public string CITY_TYPE { get; set; }
        [Display(Name = "Наименование населенного пункта")]
        [MaxLength(255)]
        public string CITY { get; set; }
        [Display(Name = "Тип улицы")]
        [MaxLength(255)]
        public string STREET_TYPE { get; set; }
        [Display(Name = "Улица")]
        [MaxLength(255)]
        public string STREET { get; set; }
        [Display(Name = "Номер строения / здания")]
        [MaxLength(255)]
        public string BUILDING { get; set; }
        [Display(Name = "Номер корпуса")]
        [MaxLength(255)]
        public string BUILDING_BLOCK { get; set; }
        [Display(Name = "Номер офиса")]
        [MaxLength(255)]
        public string OFFICE { get; set; }
        [Display(Name = "Тел.")]
        [MaxLength(255)]
        public string PHONE { get; set; }
        [Display(Name = "Факс")]
        [MaxLength(255)]
        public string FAX { get; set; }
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Формат записи не соответствует формату адреса электронной почты.")]
        [MaxLength(255)]
        public string EMAIL { get; set; }
        [Display(Name = "Код зоны деятельности")]
        [MaxLength(255)]
        public string CUSTOM_ZONE { get; set; }
        [Display(Name = "Пользователь")]
        [MaxLength(256)]
        public string USERID { get; set; }
        [Display(Name = "Обновлен")]
        public System.DateTime LAST_UPDATE { get; set; }
        [Display(Name = "Обновлен")]
        public string SID { get; set; }
        [Display(Name = "Сведения о пользователе")]
        public string USERINFO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc> DOC { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Document> DOCUMENT { get; set; }
    }
}
