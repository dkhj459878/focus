﻿namespace Focus.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class Permission
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Permission()
        {
        }

            [Display(Name = "Код разрешения")]
            public short PERMISSIONID { get; set; }
            [Display(Name = "Код документа")]
            public Nullable<short> DOCUMENTID { get; set; }
            [Required(ErrorMessage = "Требуется ввести дату начала действия полномочий.")]
            [DataType(DataType.Date)]
            [Display(Name = "Дата начала")]
            public Nullable<System.DateTime> DATESTART { get; set; }
            [Required(ErrorMessage = "Требуется ввести дату завершения действия полномочий.")]
            [Display(Name = "Дата завершения")]
            [DataType(DataType.Date)]
            public Nullable<System.DateTime> DATEFINISH { get; set; }
            [Display(Name = "Разрешение действует")]
            public Nullable<byte> ISVALID { get; set; }
            [Display(Name = "Пользователь")]
            [MaxLength(256)]
            public string USERID { get; set; }
            [Display(Name = "Обновлен")]
            public System.DateTime LAST_UPDATE { get; set; }
            [Display(Name = "SID")]
            public string SID { get; set; }
            [Display(Name = "Сведения о пользователе")]
            public string USERINFO { get; set; }

        public virtual Document DOCUMENT { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Custombrocker> CUSTOMBROCKER { get; set; }
    }
}
