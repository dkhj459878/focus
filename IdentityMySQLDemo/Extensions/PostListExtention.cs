﻿using Focus.Web.Models;
using IdentityMySQLDemo.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace IdentityMySQLDemo.Extensions
{
    internal static class PostListExtention
    {
        internal static IEnumerable<DropDownListView> ToCustomList(this IEnumerable<Post> posts)
        {
            var dropDownList = new List<DropDownListView>();

            //Check on null.
            if (posts == null || posts.Count() == 0)
            {
                return dropDownList;
            }

            foreach (var post in posts)
            {
                dropDownList.Add(new DropDownListView() { ID = post.ID, CODE = $"{post.CODE} {post.NAME}" });
            }

            return dropDownList;
        }
    }
}