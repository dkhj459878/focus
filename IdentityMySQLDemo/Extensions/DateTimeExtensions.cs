﻿using System;

namespace Focus.Web.Extensions
{
    public static class DateTimeExtentions
    {
        public static TimeSpan GetTimeSpanValue(this DateTime date)
        {
            if (date == null)
            {
                return new TimeSpan(0,0,0);
            }

            int hours = date.Hour;
            int minutes = date.Minute;
            int seconds = date.Second;

            return new TimeSpan(hours, minutes, seconds);
        }

        public static string GetEllapsedSeconds(this long ellapseTime)
        {
            if (ellapseTime <=0)
            {
                return string.Empty;
            }

            const long MILLISECONDS_IN_SECOND = 1000;
            const char ZERO_CHAR = '0';

            return $"{ellapseTime / MILLISECONDS_IN_SECOND},{(ellapseTime % MILLISECONDS_IN_SECOND).ToString().PadLeft(3, ZERO_CHAR)}";
        }
    }
}