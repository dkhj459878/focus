﻿function Disabling(string) {
    var element = document.getElementById(string);
    element.setAttribute('disabled', 'true');
}

function Enabling(string) {
    var element = document.getElementById(string);
    element.removeAttribute('disabled');
}

function DeleteElementById(id) {
    if (id == null) {
        return ;
    }
    var element = document.getElementById(id);

    if (element == null) {
        return ;
    }
    element.remove();
}

function UpdateStartCheck() {
    //alert('UpdateStartCheck');
    var checkboxes = document.getElementsByClassName('checkboxes');
    if (checkboxes == null) {
        return; // Exclude null reference exception situation.
    }
    var allCheckBoxSelected = true;
    for (var i = 0; i < checkboxes.length; i++) {
        if (!checkboxes[i].checked) {
            allCheckBoxSelected = false;
            break; 
        }
    }
    //alert('allCheckBoxSelected ' + allCheckBoxSelected);
    if (allCheckBoxSelected) {
        document.getElementById('startCheckBox').checked = true;
    }
    else {
        //alert('Ok');
        document.getElementById('startCheckBox').checked = false;
    }
}

function SelectAll() {
    //alert('SelectAll');
    var startCheckBox = document.getElementById('startCheckBox');
    if (startCheckBox == null) {
        return; // Exclude null reference exception situation.
    }
    if (startCheckBox.checked) {
        SelectAllCheckBoxes();
    }
    else {
        SelectSomeFirstCheckBoxes();
    }
    
}

function SelectAllCheckBoxes() {
    //alert('SelectAllCheckBoxes');
    var checkboxes = document.getElementsByClassName("checkboxes");
    if (checkboxes == null) {
        return; // Exclude null reference exception situation.
    }
    $('.hiddenFields').remove();
    for (var i = 0; i < checkboxes.length; i++) {
        checkboxes[i].checked = true;
        var field = "<input name='markup' class='hiddenFields' value='" + checkboxes[i].id + "' type='hidden' id = '" + checkboxes[i].id + "'>";
        $('#anchorFor').before(field);
        
    }
    if (checkboxes.length < 2) {
        Disabling('submitbutton');
    }
    else {
        Enabling('submitbutton');
    }
}

function SelectSomeFirstCheckBoxes() {
    var checkboxes = document.getElementsByClassName('checkboxes');
    if (checkboxes == null) {
        return; // Exclude null reference exception situation.
    }
    var checkboxesChecked = 2;
    var checkboxesChecked2 = 0;
    $('.hiddenFields').remove();
    for (var i = 0; i < checkboxes.length; i++) {
        if ((i < checkboxesChecked)) {
            checkboxes[i].checked = true;
            var field = "<input name='markup' class='hiddenFields' value='" + checkboxes[i].id + "' type='hidden' id = '" + checkboxes[i].id + "'>";
            $('#anchorFor').before(field);
            checkboxesChecked2++;
        }
        else {
            checkboxes[i].checked = false;
        }
    }
    //alert('checkboxesChecked2 = ' + checkboxesChecked2);
    if (checkboxesChecked2 > 1) {
        Enabling('submitbutton');
    }
    else {
        Disabling('submitbutton');
    }
}

function AddHiddenFields() {
    //alert('AddHiddenFields');
    var hiddenFields = document.getElementsByClassName('hiddenFields');
    if (hiddenFields == null) {
        return; // Exclude null reference exception situation.
    }
    var checkboxes = document.getElementsByClassName('checkboxes');
    if (checkboxes == null) {
        return; // Exclude null reference exception situation.
    }
    var checkboxesCheckedNumber = 0;
    //alert(checkboxes);
    // Delete all the hidden fields.
    for (var i = 0; i < hiddenFields.length; i++) {
        $('.hiddenFields').remove();
    }

    // Go through all check boxes.
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].checked) {
            var field = "<input name='markup' class='hiddenFields' value='" + checkboxes[i].id + "' type='hidden' id = '" + checkboxes[i].id + "'>";
            $('#anchorFor').before(field);
            checkboxesCheckedNumber++;
        }
    }
    //alert(checkboxesCheckedNumber);
    if (checkboxesCheckedNumber >= 2) {
        Enabling('submitbutton')
    }
    else {
        Disabling('submitbutton');
    }

    // Update start check box state. If all check boxes are checked it would be checked
    // otherwise it would not checked.
    UpdateStartCheck();
}
