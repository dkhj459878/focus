﻿namespace Focus.SSO.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Web.Mvc;

    public partial class Car
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Car()
        {
        }

        [Display(Name = "Код")]
        public short CARID { get; set; }
        [Display(Name = "Вид")]
        [Required(ErrorMessage = "Требуется выбрать вид транспортного средства.")]
        public byte VEHICLEKINDID { get; set; }
        [Display(Name = "Страна")]
        [MaxLength(2)]
        public string COUNTRYCODE { get; set; }
        [Required(ErrorMessage = "Требуется ввести VIN номер.")]
        [Remote("NotRepeatedVIN", "Car", HttpMethod = "POST", AdditionalFields = "CARID")]
        [Display(Name = "VIN")]
        [MaxLength(17)]
        public string VIN { get; set; }
        [Display(Name = "Описание")]
        [MaxLength(150)]
        public string DESCRIPTION { get; set; }
        [Required(ErrorMessage = "Требуется ввести марку транспортного средства.")]
        [Display(Name = "Марка")]
        [MaxLength(45)]
        public string GOODSMARK { get; set; }
        [Required(ErrorMessage = "Требуется ввести модель транспортного средства.")]
        [Display(Name = "Модель")]
        [MaxLength(45)]
        public string GOODSMODEL { get; set; }
        [Display(Name = "Тип двигателя")]
        [MaxLength(15)]
        public string MOTORFUELTYPE { get; set; }
        [Display(Name = "Объем двигателя")]
        public Nullable<short> ENGINECAPACITY { get; set; }
        [Display(Name = "Обновлен")]
        [MaxLength(256)]
        public string USERID { get; set; }
        public System.DateTime LAST_UPDATE { get; set; }

        public virtual Country COUNTRY { get; set; }
        public virtual Vehiclekind VEHICLEKIND { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc> DOC { get; set; }
    }
}
