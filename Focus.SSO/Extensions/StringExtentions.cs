﻿namespace Focus.Web.Extensions
{
    public static class StringExtentions
    {
        public static string AddValue(this string str, string addition, string delimeter = ", ")
        {
            if (string.IsNullOrEmpty(str))
            {
                return addition;
            }

            if (string.IsNullOrEmpty(addition))
            {
                return str;
            }

            return str + delimeter + addition;
        }
    }
}