﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели вида транспортного средства с моделью хранения данных вида транспортного средства.
    /// </summary>
    public class VehiclekindMappingProfile : Profile
    {
        public VehiclekindMappingProfile()
        {
            CreateMap<VEHICLEKIND, Vehiclekind>()
                .ForMember(cfg => cfg.VEHICLE, act => act.Ignore())
                .MaxDepth(1);
        }
    }
}