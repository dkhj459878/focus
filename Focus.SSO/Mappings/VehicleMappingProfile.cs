﻿using AutoMapper;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;

namespace Focus.Web.Mappings
{
    /// <summary>
    /// Профиль сопоставления веб модели транспортного средства с моделью хранения данных транспортного средства.
    /// </summary>
    public class VehicleMappingProfile : Profile
    {
        public VehicleMappingProfile()
        {
            CreateMap<VEHICLE, Vehicle>()
                .ForMember(cfg => cfg.DOC, act => act.Ignore())
                .MaxDepth(1);
            ;
        }
    }
}