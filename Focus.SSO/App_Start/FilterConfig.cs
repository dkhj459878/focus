﻿using log4net;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Focus.Web.Common;
using System.Web.Mvc;
using System.Web.Routing;
using Focus.BL.Services;
using System.Security.Principal;

namespace IdentityMySQLDemo
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CustomExceptionHandler());
            filters.Add(new CustomActionfilterAttribute());

        }
    }

    //public class ETagAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        filterContext.HttpContext.Response.Filter = new ETagFilter(filterContext.HttpContext.Response, filterContext.RequestContext.HttpContext.Request);
    //    }
    //}

    //public class ETagFilter : MemoryStream
    //{
    //    private HttpResponseBase _response = null;
    //    private HttpRequestBase _request;
    //    private Stream _filter = null;

    //    public ETagFilter(HttpResponseBase response, HttpRequestBase request)
    //    {
    //        _response = response;
    //        _request = request;
    //        _filter = response.Filter;
    //    }

    //    private string GetToken(Stream stream)
    //    {
    //        var checksum = new byte[0];
    //        checksum = MD5.Create().ComputeHash(stream);
    //        return Convert.ToBase64String(checksum, 0, checksum.Length);
    //    }

    //    public override void Write(byte[] buffer, int offset, int count)
    //    {
    //        var data = new byte[count];

    //        Buffer.BlockCopy(buffer, offset, data, 0, count);

    //        var token = GetToken(new MemoryStream(data));
    //        var clientToken = _request.Headers["If-None-Match"];

    //        if (token != clientToken)
    //        {
    //            _response.AddHeader("ETag", token);
    //            _filter.Write(data, 0, count);
    //        }
    //        else
    //        {
    //            _response.SuppressContent = true;
    //            _response.StatusCode = 304;
    //            _response.StatusDescription = "Not Modified";
    //            _response.AddHeader("Content-Length", "0");
    //        }
    //    }
    //}

    //public class WhitespaceFilterAttribute : ActionFilterAttribute
    //{
    //    public override void OnActionExecuted(ActionExecutedContext filterContext)
    //    {
    //        var response = filterContext.HttpContext.Response;

    //        // If it's a sitemap, just return.
    //        if (filterContext.HttpContext.Request.RawUrl == "/sitemap.xml") return;

    //        if (response.ContentType != "text/html" || response.Filter == null) return;

    //        response.Filter = new HelperClass(response.Filter);
    //    }

    //    private class HelperClass : Stream
    //    {
    //        private readonly Stream _base;
    //        StringBuilder _s = new StringBuilder();

    //        public HelperClass(Stream responseStream)
    //        {
    //            if (responseStream == null)
    //                throw new ArgumentNullException("responseStream");
    //            _base = responseStream;
    //        }

    //        public override void Write(byte[] buffer, int offset, int count)
    //        {
    //            var html = Encoding.UTF8.GetString(buffer, offset, count);
    //            var reg = new Regex(@"(?<=\s)\s+(?![^<>]*</pre>)");
    //            html = reg.Replace(html, string.Empty);

    //            buffer = Encoding.UTF8.GetBytes(html);
    //            _base.Write(buffer, 0, buffer.Length);
    //        }

    //        #region Other Members

    //        public override int Read(byte[] buffer, int offset, int count)
    //        {
    //            throw new NotSupportedException();
    //        }

    //        public override bool CanRead { get { return false; } }

    //        public override bool CanSeek { get { return false; } }

    //        public override bool CanWrite { get { return true; } }

    //        public override long Length { get { throw new NotSupportedException(); } }

    //        public override long Position
    //        {
    //            get { throw new NotSupportedException(); }
    //            set { throw new NotSupportedException(); }
    //        }

    //        public override void Flush()
    //        {
    //            _base.Flush();
    //        }

    //        public override long Seek(long offset, SeekOrigin origin)
    //        {
    //            throw new NotSupportedException();
    //        }

    //        public override void SetLength(long value)
    //        {
    //            throw new NotSupportedException();
    //        }

    //        #endregion
    //    }
    //}

    //public class CompressFilter : ActionFilterAttribute
    //{
    //    public override void OnActionExecuting(ActionExecutingContext filterContext)
    //    {
    //        bool allowCompression = false;
    //        bool.TryParse(ConfigurationManager.AppSettings["Compression"], out allowCompression);

    //        if (allowCompression)
    //        {
    //            HttpRequestBase request = filterContext.HttpContext.Request;

    //            string acceptEncoding = request.Headers["Accept-Encoding"];

    //            if (string.IsNullOrEmpty(acceptEncoding)) return;

    //            acceptEncoding = acceptEncoding.ToUpperInvariant();

    //            HttpResponseBase response = filterContext.HttpContext.Response;

    //            if (acceptEncoding.Contains("GZIP"))
    //            {
    //                response.AppendHeader("Content-encoding", "gzip");
    //                response.Filter = new GZipStream(response.Filter, CompressionMode.Compress);
    //            }
    //            else if (acceptEncoding.Contains("DEFLATE"))
    //            {
    //                response.AppendHeader("Content-encoding", "deflate");
    //                response.Filter = new DeflateStream(response.Filter, CompressionMode.Compress);
    //            }
    //        }
    //    }
    //}

    public class CustomExceptionHandler : HandleErrorAttribute
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);

            string method = filterContext.RouteData.Values["action"] as string;
            string type = filterContext.RouteData.Values["controller"] as string;
            string ip = filterContext.HttpContext.Request.UserHostAddress;
            string user = filterContext.HttpContext.User.Identity.Name;
            string errorMessage = filterContext.Exception.Message;
            string stackTrace = filterContext.Exception.StackTrace;

            string message = string.Empty;
            message = $"{type} {method} {ip} {user} {errorMessage} {stackTrace}";
            _logger.Error(message);
        }
    }

    public class CustomActionfilterAttribute : IActionFilter
    {
        private readonly AdService _adService;

        public CustomActionfilterAttribute()
        {
            var user = HttpContext.Current.User;
            if (user != null)
            {
                _adService = new AdService(user.Identity?.Name, string.Empty);
            }

        }

        protected static readonly log4net.ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Stopwatch _stopwatch;
        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            _stopwatch.Stop();
            Log(filterContext.RouteData, filterContext.HttpContext, _stopwatch.ElapsedMilliseconds, "AfterAction");
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {

            _stopwatch = new Stopwatch();
            var user = HttpContext.Current.User;
            if (user != null)
            {
            }
            Log(filterContext.RouteData, filterContext.HttpContext, 0, "BeforAction");
            _stopwatch.Start();
        }

        private void Log(RouteData routeData, HttpContextBase httpContextBase, long timeSpent, string eventCase)
        {
            string method = routeData.Values["action"] as string;
            string type = routeData.Values["controller"] as string;
            string ip = httpContextBase.Request.UserHostAddress;
            string user = httpContextBase.User.Identity.Name;

            string message = string.Empty;
            message = $"{type} {method} {ip} Elapsed time:{timeSpent} ms {user} {eventCase}";
            _logger.Info(message);
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class PreventDuplicateRequestAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Request["__RequestVerificationToken"] == null)
                return;

            var currentToken = HttpContext.Current.Request["__RequestVerificationToken"].ToString();

            if (HttpContext.Current.Session["LastProcessedToken"] == null)
            {
                HttpContext.Current.Session["LastProcessedToken"] = currentToken;
                return;
            }

            lock (HttpContext.Current.Session["LastProcessedToken"])
            {
                var lastToken = HttpContext.Current.Session["LastProcessedToken"].ToString();

                if (lastToken == currentToken)
                {
                    filterContext.Controller.ViewData.ModelState.AddModelError("", "Looks like you accidentally tried to double post.");
                    return;
                }

                HttpContext.Current.Session["LastProcessedToken"] = currentToken;
            }
        }
    }

    //public class DecimalModelBinder : DefaultModelBinder
    //{
    //    public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
    //    {
    //        var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

    //        return valueProviderResult == null ? base.BindModel(controllerContext, bindingContext) : Convert.ToDecimal(valueProviderResult.AttemptedValue);
    //        // of course replace with your custom conversion logic
    //    }
    //}
}
