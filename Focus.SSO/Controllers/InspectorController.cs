﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class InspectorController : Controller
    {
        private readonly IInspectorService _inspectorService;
        private readonly IPostService _postService;

        public InspectorController(IInspectorService inspectorService, IPostService postService)
        {
            _inspectorService = inspectorService;
            _postService = postService;
        }

        // Remote diagnostics.
        // GET: Inspector /NotRepeated/
        public JsonResult NotRepeated(string FIRSTNAME, string LASTNAME, string FATHERNAME, int? ID)
        {
            if (IsEnique(FIRSTNAME, LASTNAME, FATHERNAME, ID))
            { 
                return Json(true,
                    JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteInspectorRepeated,
                        LASTNAME ?? string.Empty,
                        FIRSTNAME ?? string.Empty,
                        FATHERNAME ?? string.Empty),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsEnique(string firstName, string lastName, string fatherName, int? id)
        {
            if (id == null)
            {
                return !_inspectorService.InspectorExists(
                    i => i.FIRSTNAME == (firstName == string.Empty ? null: firstName) &&
                    i.LASTNAME == (lastName == string.Empty ? null : lastName) &&
                    i.FATHERNAME == (fatherName == string.Empty ? null : fatherName));
            }

            return !_inspectorService.InspectorExists(
                    i => i.FIRSTNAME == (firstName == string.Empty ? null : firstName) &&
                    i.LASTNAME == (lastName == string.Empty ? null : lastName) &&
                    i.FATHERNAME == (fatherName == string.Empty ? null : fatherName) &&
                    i.ID != id);

        }


        // GET: Inspector
        public ActionResult Index()
        {
            var inspector =  _inspectorService.GetAllInspectors(includeProperties: "post");
            return View(Mapper.Map<IEnumerable<INSPECTOR>, IEnumerable<Inspector>>(inspector.ToList()));
        }

        // GET: Inspector/Details/5
        public ActionResult Details(short id)
        {
            INSPECTOR inspector = _inspectorService.GetInspector(id);
            if (inspector == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Inspector>(inspector));
        }

        public JsonResult JsonOrg(short id)
        {
            var jsondata = _inspectorService.GetInspector(id);
            jsondata.POST = _postService.GetPost(jsondata.POST.ID);

            return Json(
                new
                {
                    id = jsondata?.ID,
                    lastname = jsondata.LASTNAME,
                    firstname = jsondata.FIRSTNAME,
                    fathername = jsondata.FATHERNAME,
                    code = jsondata.POST.CODE,
                    postname = jsondata.POST.NAME,
                    phonenumber = jsondata.PHONENUMBER,
                    title = jsondata?.TITLE,
                    name = jsondata?.SHORTNAME,
                    devision = jsondata.POST?.DEVISION
                },
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Search(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Content($"<br /><p><b>Поисковая строка не должна быть пустой. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>2</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }
            if (name.Trim().Length < 3)
            {
                return Content($"<br /><p><b>Поисковая строка должна содержать не менее 3 символов, за исключением пробела. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>2</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            var inspectors = _inspectorService.GetAllInspectors(filter: o => o.SHORTNAME.Contains(name)).ToList();
            if (inspectors.Count <= 0)
            {
                return Content($"<br /><p><b>Инспектора с частью фамилии <span style = 'background-color: #ffff00;'>{name}</span> в базе данных не обнаружены.</b></p>");
            }
            return PartialView(Mapper.Map<IEnumerable<INSPECTOR>, IEnumerable<Inspector>>(inspectors));
        }


        [HttpPost]
        public ActionResult ChiefSearch(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Content($"<br /><p><b>Поисковая строка не должна быть пустой. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>2</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }
            if (name.Trim().Length < 3)
            {
                return Content($"<br /><p><b>Поисковая строка должна содержать не менее 3 символов, за исключением пробела. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>2</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            var inspectors = _inspectorService.GetAllInspectors(filter: o => o.SHORTNAME.Contains(name)).ToList();
            if (inspectors.Count <= 0)
            {
                return Content($"<br /><p><b>Инспектора с частью фамилии <span style = 'background-color: #ffff00;'>{name}</span> в базе данных не обнаружены.</b></p>");
            }
            return PartialView(Mapper.Map<IEnumerable<INSPECTOR>, IEnumerable<Inspector>>(inspectors));
        }

        // GET: Inspector/Create
        public ActionResult Create()
        {
            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
            return View();
        }

        // POST: Inspector/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,POSTID,FIRSTNAME,LASTNAME,FATHERNAME,SHORTNAME,TITLE,LNP,SEAL,PHONENUMBER,LAST_UPDATE")] Inspector inspector)
        {
            if (ModelState.IsValid)
            {
                inspector.LAST_UPDATE = DateTime.Now;
                _inspectorService.AddInspector(Mapper.Map<INSPECTOR>(inspector));
                return RedirectToAction("Index");
            }

            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
            return View(inspector);
        }

        // GET: Inspector/Create
        public ActionResult PartialCreate()
        {
            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
            return PartialView();
        }

        // POST: Inspector/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialCreate([Bind(Include = "ID,POSTID,FIRSTNAME,LASTNAME,FATHERNAME,SHORTNAME,TITLE,LNP,SEAL,PHONENUMBER,LAST_UPDATE")] Inspector inspector)
        {
            if (ModelState.IsValid)
            {
                inspector.LAST_UPDATE = DateTime.Now;
                short _inspectorId = _inspectorService.AddInspector(Mapper.Map<INSPECTOR>(inspector));
                inspector.ID = _inspectorId;
                return PartialView("Search", new List<Inspector>() { inspector });
            }

            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
            return PartialView(inspector);
        }

        // POST: Inspector/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChiefPartialCreate([Bind(Include = "ID,POSTID,FIRSTNAME,LASTNAME,FATHERNAME,SHORTNAME,TITLE,LNP,SEAL,PHONENUMBER,LAST_UPDATE")] Inspector inspector)
        {
            if (ModelState.IsValid)
            {
                inspector.LAST_UPDATE = DateTime.Now;
                short _inspectorId = _inspectorService.AddInspector(Mapper.Map<INSPECTOR>(inspector));
                inspector.ID = _inspectorId;
                return PartialView("ChiefSearch", new List<Inspector>() { inspector });
            }

            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
            return PartialView(inspector);
        }

        // GET: Inspector/Edit/5
        public ActionResult Edit(short id)
        {
            INSPECTOR inspector = _inspectorService.GetInspector(id);
            if (inspector == null)
            {
                return HttpNotFound();
            }
            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE", inspector.POSTID);
            return View(Mapper.Map<Inspector>(inspector));
        }

        // POST: Inspector/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,POSTID,FIRSTNAME,LASTNAME,FATHERNAME,SHORTNAME,TITLE,LNP,SEAL,PHONENUMBER,LAST_UPDATE")] Inspector inspector)
        {
            if (ModelState.IsValid)
            {
                inspector.LAST_UPDATE = DateTime.Now;
                _inspectorService.UpdateInspector(Mapper.Map<INSPECTOR>(inspector));
                return RedirectToAction("Index");
            }
            ViewBag.POSTID = new SelectList(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(_postService.GetAllPosts()), "ID", "CODE");
            return View(inspector);
        }

        // GET: Inspector/Delete/5
        public ActionResult Delete(short id)
        {
            INSPECTOR inspector = _inspectorService.GetInspector(id);
            if (inspector == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Inspector>(inspector));
        }

        // POST: Inspector/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            INSPECTOR inspector = _inspectorService.GetInspector(id);
            _inspectorService.DeleteInspector(id);
            return RedirectToAction("Index");
        }

        // GET: vehicles/JsonDelete/5
        public ActionResult JsonDelete(short id)
        {
            INSPECTOR jsondata = _inspectorService.GetInspector(id);

            var inspectorJSON = $"{jsondata.LASTNAME} {jsondata.FIRSTNAME} {jsondata.FATHERNAME}";

            var docCount = jsondata.DOC?.Count;

            return Json(
               new
               {
                   inspectorId = id,
                   inspectorName = inspectorJSON,

                   docCount,
                   docs = jsondata.DOC.Select(o => $"Номер АТД '{o.NUM}' от {o.PERIODSTARTDATE.Value.ToShortDateString()} {o.CONSIGNEENAME}")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _inspectorService.Dispose();
                _postService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
