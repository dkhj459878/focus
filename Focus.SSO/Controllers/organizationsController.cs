﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Extensions;
using Focus.SSO.Models;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class organizationsController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private IOrganizationService _organizationService;
        private ICountryService _countryService;

        public organizationsController(IOrganizationService organizationService, ICountryService countryService)
        {
            _organizationService = organizationService;
            _countryService = countryService;
        }

        // GET: organizations/NotRepeatedITN/
        public JsonResult NotRepeatedITN(string UNP, int? ORGANIZATIONID, string COUNTRYID)
        {
            // Check on organization number lenght.
            (bool IsTrue, string Error) result;
            result = IsCorrectLenght(UNP, COUNTRYID);

            if (!result.IsTrue)
            {
                return Json(result.Error, JsonRequestBehavior.AllowGet);
            }

            if (IsEniqueITN(UNP, ORGANIZATIONID, COUNTRYID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(string.Format(Messages.ErrorRemoteOrganizationUNPRepeated,
                UNP), JsonRequestBehavior.AllowGet);
        }

        private bool IsEniqueITN(string itn, int? organizationId, string COUNTRYID)
        {
            if (organizationId == null)
            {
                return !_organizationService.OrganizationExists(
                    o => o.UNP == (itn == string.Empty ? null : itn) && o.COUNTRYID == COUNTRYID);
            }

            return !_organizationService.OrganizationExists(
                o => o.UNP == (itn == string.Empty ? null : itn) && o.COUNTRYID == COUNTRYID &&
                o.ORGANIZATIONID != organizationId);
        }

        private (bool IsCorrect, string Error) IsCorrectLenght(string UNP, string COUNTRYID)
        {
            if (string.IsNullOrEmpty(COUNTRYID))
            {
                return (true, string.Empty);
            }

            Match match;


            switch (COUNTRYID)
            {
                case "BY":
                    Regex regexBLR = new Regex(@"^[1-9]{1}[0-9]{8}$");
                    match = regexBLR.Match(UNP);
                    if (!match.Success)
                    {
                        return (false, "УНН организации должен состоять из 9 цифр и не начинаться с 0");
                    }
                    break;
                case "RU":
                    Regex regexRUS = new Regex(@"^[1-9]{1}[0-9]{9}$");
                    match = regexRUS.Match(UNP);
                    if (!match.Success)
                    {
                        return (false, "ИНН организации, зарегистрированной в Российской Федерации, должен состоять из 10 цифр и не начинаться с 0");
                    }
                    break;
                case "KZ":
                    Regex regexKAZ = new Regex(@"^[1-9]{1}[0-9]{11}$");
                    match = regexKAZ.Match(UNP);
                    if (!match.Success)
                    {
                        return (false, "РНН организации, зарегистрированной в Республики Казахстан, должен состоять из 12 цифр и не начинаться с 0");
                    }
                    break;
                case "KG":
                    Regex regexKGZ = new Regex(@"^[1-9]{1}[0-9]{13}$");
                    match = regexKGZ.Match(UNP);
                    if (!match.Success)
                    {
                        return (false, "ИНН организации, зарегистрированной в Республики Кыргызстан, должен состоять из 14 цифр и не начинаться с 0");
                    }
                    break;
                default:
                    break;
            }

            return (true, string.Empty);
        }

        // GET: organizations/NotR epeatedName/
        public JsonResult NotRepeatedName(string NAME, int? ORGANIZATIONID, string COUNTRYID)
        {
            if (IsEnuqueName(NAME, ORGANIZATIONID, COUNTRYID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(string.Format(Messages.ErrorRemoteOrganizationNameRepeated,
                NAME), JsonRequestBehavior.AllowGet);
        }


        private bool IsEnuqueName(string name, int? organizationId, string COUNTRYID)
        {
            if (organizationId == null)
            {
                return !_organizationService.OrganizationExists(
                    o => o.NAME == (name == string.Empty ? null : name) && o.COUNTRYID == COUNTRYID);
            }

            return !_organizationService.OrganizationExists(
                o => o.NAME == (name == string.Empty ? null : name) && o.COUNTRYID == COUNTRYID &&
                o.ORGANIZATIONID != organizationId);
        }

        // GET: organizations
        public ActionResult Index()
        {
            var organizations = _organizationService.GetAllOrganizations().ToList();
            return View(Mapper.Map<IEnumerable<ORGANIZATION>, IEnumerable<Organization>>(organizations));
        }

        [HttpPost]
        public ActionResult Search(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                return Content($"<br /><p><b>Поисковая строка не должна быть пустой. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>3</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            if (name.Trim().Length < 4)
            {
                return Content($"<br /><p><b>Поисковая строка должна содержать не менее 4 символов, за исключением пробела. Введите в поисковую строку тест, содержащий более <span style = 'background-color: #ffff00; color: Red;'>3</span> символов, исключая пробелы, и вновь нажмите клавишу «Ввод» либо кнопку «Поиск».</b></p>");
            }

            string nameUpperCase = name.ToUpper();
            var orgs = _organizationService.GetAllOrganizations(filter: o => o.NAME.ToUpper().Contains(nameUpperCase)).ToList();
            if (orgs.Count <= 0)
            {
                return Content($"<br /><p><b>Организации (физические лица) с частями наименований (фамилий) <span style = 'background-color: #ffff00;'>{name}</span> в базе данных не обнаружены.</b></p>");
            }
            return PartialView(Mapper.Map<IEnumerable<ORGANIZATION>, IEnumerable<Organization>>(orgs));
        }
        
        public ActionResult PartialCreate()
        {
            ViewBag.COUNTRYID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", "BY");
            return PartialView();
        }

        // POST: organizations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult  PartialCreate([Bind(Include = "ORGANIZATIONID,UNP,NAME,ADDRESSFULL,ADDRESS,COUNTRYID,POST_INDEX,REGION,DISTRICT,CITY_TYPE,CITY,STREET_TYPE,STREET,BUILDING,BUILDING_BLOCK,OFFICE,PHONE,FAX,EMAIL,CUSTOM_ZONE,LAST_UPDATE")] Organization organization)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    organization.USERID = User.Identity.GetUserId();
                    organization.LAST_UPDATE = DateTime.Now;
                    organization.ORGANIZATIONID = _organizationService.AddOrganization(Mapper.Map<ORGANIZATION>(organization));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                return PartialView("Search", new List<Organization>() { organization });
            }
            ViewBag.COUNTRYID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return PartialView(organization);
        }

        public JsonResult JsonOrg(short id)
        {
            var jsondata = _organizationService.GetOrganization(id);
            return Json(
                new
                {
                    id = jsondata?.ORGANIZATIONID,
                    name = jsondata?.NAME,
                    tin = jsondata?.UNP,    
                    country = jsondata?.COUNTRYID,    
                    addr = jsondata?.ADDRESS
                },
                JsonRequestBehavior.AllowGet);
        }


        // GET: organizations/Details/5
        public ActionResult Details(short id)
        {
            ORGANIZATION organization = _organizationService.GetOrganization(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Organization>(organization));
        }

        // GET: organizations/Create
        public ActionResult Create()
        {
            ViewBag.COUNTRYID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", "BY");
            return View();
        }

        // POST: organizations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ORGANIZATIONID,UNP,NAME,ADDRESSFULL,ADDRESS,COUNTRYID,POST_INDEX,REGION,DISTRICT,CITY_TYPE,CITY,STREET_TYPE,STREET,BUILDING,BUILDING_BLOCK,OFFICE,PHONE,FAX,EMAIL,CUSTOM_ZONE,LAST_UPDATE")] Organization organization)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    organization.USERID = User.Identity.GetUserId();
                    organization.LAST_UPDATE = DateTime.Now;
                    _organizationService.AddOrganization(Mapper.Map<ORGANIZATION>(organization));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }

                return RedirectToAction("Index");
            }
            ViewBag.COUNTRYID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return View(organization);
        }

        // GET: organizations/Edit/5
        public ActionResult Edit(short id)
        {
            ORGANIZATION organization = _organizationService.GetOrganization(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            ViewBag.COUNTRYID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME", organization.COUNTRYID);
            return View(Mapper.Map<Organization>(organization));
        }

        // POST: organizations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ORGANIZATIONID,UNP,NAME,ADDRESSFULL,ADDRESS,COUNTRYID,POST_INDEX,REGION,DISTRICT,CITY_TYPE,CITY,STREET_TYPE,STREET,BUILDING,BUILDING_BLOCK,OFFICE,PHONE,FAX,EMAIL,CUSTOM_ZONE,LAST_UPDATE")] Organization organization)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    organization.USERID = User.Identity.GetUserId();
                    organization.LAST_UPDATE = DateTime.Now;
                    _organizationService.UpdateOrganization(Mapper.Map<ORGANIZATION>(organization));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }

                return RedirectToAction("Index");
            }
            ViewBag.COUNTRYID = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "ABBR2", "NAME");
            return View(organization);
        }

        // GET: organizations/Delete/5
        public ActionResult Delete(short id)
        {
            ORGANIZATION organization = _organizationService.GetOrganization(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Organization>(organization));
        }

        // GET: organizations/Delete/5
        public ActionResult PartialDelete(short id)
        {
            ORGANIZATION organization = _organizationService.GetOrganization(id);
            if (organization == null)
            {
                return HttpNotFound();
            }
            return PartialView(Mapper.Map<Organization>(organization));
        }

        // POST: organizations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            _organizationService.DeleteOrganization(id);
            return RedirectToAction("Index");
        }

        // GET: vehicles/JsonDelete/5
        public ActionResult JsonDelete(short id)
        {
            ORGANIZATION jsondata = _organizationService.GetOrganization(id);

            var organizationJSON = $"УНП: {jsondata.UNP} {jsondata.NAME}";

            var docCount = jsondata.DOC?.Count;

            return Json(
               new
               {
                   organizationId = id,
                   organizationName = organizationJSON,

                   docCount,
                   docs = jsondata.DOC.Select(o => $"Номер АТД '{o.NUM}' от {o.PERIODSTARTDATE.Value.ToShortDateString()} {o.CONSIGNEENAME}")
               },
               JsonRequestBehavior.AllowGet);
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _organizationService.Dispose();
                _countryService.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}
