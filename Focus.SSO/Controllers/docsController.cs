﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.BL.Extensions;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Extensions;
using Focus.Enum;
using Focus.Web.Extensions;
using Focus.SSO.Models;
using Focus.SSO.Properties;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;
using Word = Microsoft.Office.Interop.Word;

namespace Focus.Controllers
{
    public class docsController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly IDocService _docService;
        private readonly IOrganizationService _organizationService;
        private readonly ICustombrockerService _custombrockerService;
        private readonly ICountryService _countryService;
        private readonly IFileDocumentService _fileDocumentService;
        private readonly IInspectorService _inspectorService;
        private readonly IPostService _postService;
        private readonly IAdService _adService;
        private Stopwatch _docControllerProfiler = new Stopwatch();

        private bool isAdministrator = false;
        private byte postIdOfUser = 0;

        public docsController(IDocService docService, IOrganizationService organizationService, ICustombrockerService custombrockerService, ICountryService countryService, IFileDocumentService fileDocumentService, IInspectorService inspectorService, IPostService postService, IAdService adService)
        {
            _docService = docService;
            _organizationService = organizationService;
            _custombrockerService = custombrockerService;
            _countryService = countryService;
            _fileDocumentService = fileDocumentService;
            _inspectorService = inspectorService;
            _postService = postService;
            _adService = adService;
            _docControllerProfiler.Start();
        }

        // // Remote diagnostics.
        // GET: docs /NotRepeated/
        public ActionResult NotRepeated(string NUM, int? DOCID)
        {
            if (IsUnique(NUM, DOCID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                   string.Format(
                       Messages.ErrorRemoteDocInspectionNumRepeated,
                       NUM
                       ), JsonRequestBehavior.AllowGet);

        }

        private bool IsUnique(string NUM, int? DOCID)
        {
            if (DOCID == null)
            {
                return !_docService.DocExists
                (filter: c => c.NUM == NUM);
            }


            return !_docService.DocExists
                (filter: c => c.NUM == NUM &&
                c.DOCID != DOCID);
        }

        public ActionResult Show(short id)
        {
            DOC doc = _docService.GetDoc(id);
            if (doc == null)
            {
                return HttpNotFound();
            }


            var filePath = _fileDocumentService.GetDocumentFilePath(doc, Server.MapPath("~"));

            return File(filePath, "application/octet-stream", "temp.doc");
        }

        private void Dispose(Word.Application wordApplication)
        {
            Object missingObj = System.Reflection.Missing.Value;
            Object falseObj = false;

            if (wordApplication != null)
            {
                try
                {
                    wordApplication.Quit(ref falseObj, ref missingObj, ref missingObj);
                }
                finally
                {

                    wordApplication = null;
                }
            }
        }

        // Return view with error message.
        public ActionResult Error(ErrorTypes error)
        {
            switch (error)
            {
                case ErrorTypes.Error_TemplateDocumentIsAbsent:
                    ViewBag.ErrorMessage = new HtmlString(Resources.Error_TemplateDocumentIsAbsent);
                    ViewBag.ErrorNumber = "00" + error.ToString();
                    return View();
                default:
                    break;
            }
            ViewBag.ErrorMessage = new HtmlString(Resources.Error_UnknowMistake);
            ViewBag.ErrorNumber = "00" + error.ToString();
            return View();
        }

        // GET: docs
        public ActionResult Index()
        {
            // Get user profiler start.

            //ViewBag.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
            ////ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            //bool isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            bool IS_ADMINISTRATOR_FOR_TEST_PURPOSES = true;
            //bool isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            bool isAdministrator = IS_ADMINISTRATOR_FOR_TEST_PURPOSES;
            ViewBag.IsAdmin = isAdministrator;
            
            string userName = User.Identity.Name; // Adding user id to row of container for auditing purpose. 

            IEnumerable<DOC> docs;

            try
            {
                if (isAdministrator)
                {
                    Stopwatch dataAdminRetreiveProfiler = new Stopwatch();

                    docs = _docService.GetAllDocs(includeProperties: "custombrocker,organization,inspector1").ToList();
                }
                else
                {
                    const short PERSON_ID_FOR_TEST_PURPOSES = 9; // It's person id of administrator.
                    //short personId = (short)user.PERSONID;
                    short personId = PERSON_ID_FOR_TEST_PURPOSES;
                    INSPECTOR inspector = _inspectorService.GetInspector(personId);
                    byte? postId = inspector?.POSTID;
                    ViewBag.PostIdOfUser = (byte)postId;
                    const int POST_CODE_LENGHT = 5;
                    string postCode = inspector?.POST.CODE;
                    docs = _docService.GetAllDocs(d => d.NUM.Substring(0, POST_CODE_LENGHT) == postCode, includeProperties: "custombrocker,organization,inspector1").OrderBy(prop => prop.DOCID).ToList();
                }

                Optimizer<DOC> optimizer = new Optimizer<DOC>();
                IEnumerable<DOC> docsMinified = optimizer.Optimize(docs);

                IEnumerable<Doc> docsViewModel = Mapper.Map<IEnumerable<DOC>, IEnumerable<Doc>>(docsMinified).OrderBy(prop => prop.DOCID).ToList();
                return View(docsViewModel);
            }
            catch (DbEntityValidationException error)
            {
                _logger.Error(error.GetDetails(), error);
                throw;
            }
            catch (Exception error)
            {
                _logger.Error(error.Message, error);
                throw;
            }
        }



        // GET: docs/Details/5
        public ActionResult Details(short id)
        {
            DOC doc = _docService.GetDoc(id);
            if (doc == null)
            {
                return HttpNotFound();
            }
            ViewBag.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
            return View(Mapper.Map<Doc>(doc));
        }

        // GET: docs/FindAnalog
        public JsonResult FindAnalog(string tin, string orgName, string FIOInspector, string goodsDescription, string customBrocker)
        {
            return null;
        }

        // GET: docs/Create
        public ActionResult Create()
        {
            string userId = User.Identity.GetUserId();

            const bool THERE_IS_VIOLATION_SIGN = false;
            const bool IS_SENDER = false;
            const byte IS_INSPECTION_TYPE_OF_CONTROL = 1;
            const string INITIAL_CUSTOM_INSPECTION_RESULT_DESCRIPTION = "[Введите результаты таможенного досмотра (осмотра). Данный текст удалить впоследствии.]";

            //ApplicationUser user = userManager.FindById(userId);
            //short personId = (short)user.PERSONID;
            const short PERSON_ID_FOR_TEST = 9;
            INSPECTOR inspector = _inspectorService.GetInspector(PERSON_ID_FOR_TEST);
            if (inspector != null)
            {
                short inspectorId = inspector.ID;
                string title = inspector.TITLE;
                string shortName = inspector.SHORTNAME;
                string devision = inspector.POST.DEVISION;
                byte? postId = inspector.POSTID;
                string code = _postService.GetPost((byte)postId).CODE;
                List<DOC> docs = _docService.GetAllDocs(d => d.INSPECTOR1.POSTID == postId, includeProperties: "inspector1").ToList();
                List<STORAGE> storages = _postService.GetAllPosts(p => p.ID == postId, includeProperties: "storage").FirstOrDefault().STORAGE.ToList();
                string storageInfor = string.Empty;
                if (storages.Count !=0)
                {
                    Storage storage = Mapper.Map<Storage>(storages.FirstOrDefault());
                    storageInfor = storage.NUMBER_ + ", " + storage.ADDRESS;
                }

                string nextNumber = docs.GetNextDocNumber((byte)postId, code);
                


                ViewBag.CUSTOMS = new SelectList(new List<SelectListItem>() { new SelectListItem { Text = "Досмотр", Value = "1" }, new SelectListItem { Text = "Осмотр", Value = "0" } }, "Value", "Text", IS_INSPECTION_TYPE_OF_CONTROL);
                ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", THERE_IS_VIOLATION_SIGN.ConvertToByte());
                ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text", (!IS_SENDER).ConvertToByte());
                Doc doc = new Doc()
                {
                    NUM = nextNumber,
                    INSPECTORDEVISION = devision,
                    CUSTOMSPLACE = storageInfor,
                    INSPECTORSHORTNAME = shortName,
                    INSPECTORTITLE = title,
                    INSPECTORID = inspectorId,
                    SIGNSSOFVIOLATIONS = THERE_IS_VIOLATION_SIGN.ConvertToByte(),
                    TYPE = (!IS_SENDER).ConvertToByte(),
                    CUSTOMSRESULT = INITIAL_CUSTOM_INSPECTION_RESULT_DESCRIPTION,
                    PERIODSTARTDATE = DateTime.Now,
                    PERIODSTARTTIME = DateTime.Now.Add(new TimeSpan(0, 0, Convert.ToInt32(Messages.MinTimeSpanInMinutes), 0)),
                    TIMESTAMP_ON_CREATE = DateTime.Now,
                    LAST_UPDATE = DateTime.Now,
                    PEROIDFINISHDATE = DateTime.Now,
                    TRANSFERRINGDATE = DateTime.Now,
                    CONSIGNEEID = 0
                };


                return View(doc);
            }

            ViewBag.CUSTOMS = new SelectList(new List<SelectListItem>() { new SelectListItem { Text = "Досмотр", Value = "1" }, new SelectListItem { Text = "Осмотр", Value = "0" } }, "Value", "Text", IS_INSPECTION_TYPE_OF_CONTROL);
            ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", THERE_IS_VIOLATION_SIGN.ConvertToByte());
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text", (!IS_SENDER).ConvertToByte());
            return View(new Doc()
            {
                PERIODSTARTDATE = DateTime.Now,
                PERIODSTARTTIME = DateTime.Now.Add(new TimeSpan(0, 0, Convert.ToInt32(Messages.MinTimeSpanInMinutes), 0)),
                TIMESTAMP_ON_CREATE = DateTime.Now,
                PEROIDFINISHDATE = DateTime.Now,
                TRANSFERRINGDATE = DateTime.Now,
                SIGNSSOFVIOLATIONS = THERE_IS_VIOLATION_SIGN.ConvertToByte(),
                TYPE = (!IS_SENDER).ConvertToByte(),
                CUSTOMSRESULT = INITIAL_CUSTOM_INSPECTION_RESULT_DESCRIPTION,
                CONSIGNEEID = 0
            });
        }

        // POST: docs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Doc doc)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    doc.USERID = User.Identity.GetUserId();
                    doc.TIMESTAMP_ON_CREATE = DateTime.Now;
                    doc.LAST_UPDATE = DateTime.Now;
                    _docService.AddDoc(Mapper.Map<DOC>(doc));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }

                ViewBag.CUSTOMS = new SelectList(new List<SelectListItem>() { new SelectListItem { Text = "Досмотр", Value = "1" }, new SelectListItem { Text = "Осмотр", Value = "0" } }, "Value", "Text");
                ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
                ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text");
                return RedirectToAction("Index");
            }

            ViewBag.CUSTOMS = new SelectList(new List<SelectListItem>() { new SelectListItem { Text = "Досмотр", Value = "1" }, new SelectListItem { Text = "Осмотр", Value = "0" } }, "Value", "Text");
            ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text");
            return View(doc);
        }

        // GET: docs/Edit/5
        public ActionResult Edit(short id)
        {
            var userName = User.Identity.GetUserId();
            //ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            //isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            const bool IS_ADMINISTRATOR_VALUE_FOR_TEST_PURPOSES = true;
            isAdministrator = IS_ADMINISTRATOR_VALUE_FOR_TEST_PURPOSES;
            DOC doc = _docService.GetDoc(id);
            const int MINUTES_IN_DAY = 1440;
            const int MINUTES_IN_HOUR = 60;

            if (doc == null)
            {
                return HttpNotFound();
            }

            //short personId = (short)user.PERSONID;
            const short PERSON_ID_FOR_TEST_PURPOSES = 9; // Administrator person id.
            INSPECTOR inspector = _inspectorService.GetInspector(PERSON_ID_FOR_TEST_PURPOSES);

            byte postId = (byte)inspector?.POSTID;

            if (userName != doc.USERID && !isAdministrator && postId != inspector?.POSTID)
            {
                return RedirectToAction("Login", "Account");
            }

            if (doc.PERIODFINISHTIME == null && doc.PERIODSTARTDATE !=null && doc.PERIODSTARTTIME != null)
            {
                DateTime databaseStartDate = (DateTime)doc.PERIODSTARTDATE;
                TimeSpan databaseStartTime = doc.PERIODSTARTTIME.Value.GetTimeSpanValue();
                DateTime startDate = databaseStartDate.Add(databaseStartTime);

                DateTime currentDate = DateTime.Now;
                TimeSpan substruct = currentDate.Subtract(startDate);

                int difference = Convert.ToInt32(Messages.TimeSpanBetweenStartAndFinish);
                int minutes = Convert.ToInt32(Messages.MinTimeSpanInMinutes);
                int diff = substruct.Days* MINUTES_IN_DAY + substruct.Hours* MINUTES_IN_HOUR + substruct.Minutes;
                
                if (diff > difference + minutes)
                {
                    doc.PERIODFINISHTIME = currentDate.Add(new TimeSpan(0, 0, -minutes,0));
                }
                else
                {
                    doc.PERIODFINISHTIME = currentDate;
                }
            }

            if (doc.PEROIDFINISHDATE == null)
            {
                doc.PEROIDFINISHDATE = DateTime.Now;
            }

            if (doc.TRANSFERRINGDATE == null)
            {
                doc.TRANSFERRINGDATE = DateTime.Now;

            }

            if (doc.TRANSFERRINGTIME == null)    
            {
                int timeSpanTransferring = Convert.ToInt32(Messages.TransfferAfterMinutes);
                doc.TRANSFERRINGTIME = (DateTime.Now).Add(new TimeSpan(0, 0, timeSpanTransferring, 0));
            }

            #region DropList Information

            List<SelectListItem> ObjListLighting = new List<SelectListItem>()
            {
                new SelectListItem { Text = "естественное", Value = "естественное" },
                new SelectListItem { Text = "искуственное", Value = "искуственное" }

            };

            List<SelectListItem> ObjListWeather = new List<SelectListItem>()
            {
                new SelectListItem { Text = "ясная", Value = "ясная" },
                new SelectListItem { Text = "пасмурная", Value = "пасмурная" }

            };

            List<SelectListItem> ObjListSampleType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "образец", Value = "образец" },
                new SelectListItem { Text = "проба", Value = "проба" }

            };

            List<SelectListItem> ObjListSamplingType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "первичная", Value = "первичную" },
                new SelectListItem { Text = "повторная", Value = "повторную" },
                new SelectListItem { Text = "дополнительная", Value = "дополнительную" }

            };

            List<SelectListItem> ObjListInspection = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Досмотр", Value = "1"  },
                new SelectListItem { Text = "Осмотр", Value = "0" }

            };

            //Assigning generic list to ViewBag.
            ViewBag.ISACTUAL = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISACTUAL);
            ViewBag.ISAVERAGE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISAVERAGE);
            ViewBag.LIGHTING = new SelectList(ObjListLighting, "Value", "Text", doc.LIGHTING);
            ViewBag.WHEATHER = new SelectList(ObjListWeather, "Value", "Text", doc.WHEATHER);
            ViewBag.KINDOFSAMPLE = new SelectList(ObjListSampleType, "Value", "Text", doc.KINDOFSAMPLE);
            ViewBag.EXPERTINSINKIND = new SelectList(ObjListSamplingType, "Value", "Text", doc.EXPERTINSINKIND);
            ViewBag.CUSTOMS = new SelectList(ObjListInspection, "Value", "Text", doc.CUSTOMS);
            ViewBag.ISAVERAGE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISAVERAGE);
            ViewBag.ISCALCULATION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISCALCULATION);
            ViewBag.ISOTHER = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISOTHER);
            ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.SIGNSSOFVIOLATIONS);
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text", doc.TYPE);
            ViewBag.ONITSOWNWHEELS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ONITSOWNWHEELS);
            ViewBag.SEALSVIOLATED = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.SEALSVIOLATED);
            ViewBag.PACKEDATOURPRESENCE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.PACKEDATOURPRESENCE);
            ViewBag.ISDANGER = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISDANGER);
            ViewBag.OPENING = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Вскрытие производилось", Value = "1" }, new SelectListItem() { Text = "Вскрытие не производилось", Value = "2" } }, "Value", "Text");
            ViewBag.ISPERISHABLEPRODUCT = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text", doc.ISPERISHABLEPRODUCT);
            ViewBag.GOODSARRIVALCOUNTRY = new SelectList(Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries()), "NAME", "NAME", doc.GOODSARRIVALCOUNTRY);
            ViewBag.THEREISAPALLET = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "На паллетах", Value = "0" }, new SelectListItem() { Text = "Навалом", Value = "1" }, new SelectListItem() { Text = "Частично на паллетах, частично - навалом", Value = "2" } }, "Value", "Text");
            ViewBag.CAPACITYOFINSPECTION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "100% досмотр товаров", Value = "0" }, new SelectListItem() { Text = "Частичный досмотр товаров", Value = "1" } }, "Value", "Text", 0);
            ViewBag.OPENINGCARGOGOODS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Вскрытие произведено", Value = "0" }, new SelectListItem() { Text = "Вскрытие не производилось", Value = "1" } }, "Value", "Text");
            // Additinal lists.
            ViewBag.UNLOADED = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Выгружен полностью", Value = "0" }, new SelectListItem() { Text = "Выгружен частично", Value = "1" }, new SelectListItem() { Text = "Не выгружено (момент досмотра)", Value = "3" } }, "Value", "Text");
            ViewBag.COMPARESEALS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Да", Value = "0" }, new SelectListItem() { Text = "Нет", Value = "1" } }, "Value", "Text");

            ViewBag.COUNT = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Произведен пересчет", Value = "0" }, new SelectListItem() { Text = "Пересчет не произведен", Value = "1" } }, "Value", "Text");
            ViewBag.IDENTIFICATION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Да", Value = "0" }, new SelectListItem() { Text = "Нет", Value = "1" } }, "Value", "Text");
            ViewBag.PARTIALORFULL = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Полный", Value = "0" }, new SelectListItem() { Text = "Выборочный", Value = "1" } }, "Value", "Text");
            ViewBag.COMPLIENCE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Сведения соответствуют", Value = "0" }, new SelectListItem() { Text = "Выявлено НЕСООТВЕТСТВИЕ сведений", Value = "1" } }, "Value", "Text");
            ViewBag.INFORABOUTGOOD = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Сведения присутствуют", Value = "0" }, new SelectListItem() { Text = "Сведенний нет", Value = "1" } }, "Value", "Text");
            ViewBag.IsAdmin = isAdministrator;
            #endregion
            return View(Mapper.Map<Doc>(doc));
        }

        [HttpGet]
        public ActionResult Error(DbEntityValidationException e)
        {
            throw new NotImplementedException();
        }

        [HttpGet]
        public Action Warning(Doc doc)
        {
            throw new NotImplementedException();
        }

        // POST: docs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Doc doc)
        {
            //ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            //isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            const bool IS_ADMINISTRATOR_VALUE_FOR_TEST_PURPOSES = true;
            isAdministrator = IS_ADMINISTRATOR_VALUE_FOR_TEST_PURPOSES;
            if (ModelState.IsValid)
            {

                var userName = User.Identity.GetUserId();

                //short personId = (short)user.PERSONID;
                const short PERSON_ID_FOR_TEST_PURPOSES = 9;
                INSPECTOR inspector = _inspectorService.GetInspector(PERSON_ID_FOR_TEST_PURPOSES);

                byte postId = (byte)inspector?.POSTID;

                if (userName != doc.USERID && !isAdministrator && postId != inspector?.POSTID)
                {
                    return RedirectToAction("Login", "Account");
                }
                
                try
                {
                    if (!isAdministrator)
                    {
                        doc.USERID = User.Identity.GetUserId(); // Adding user id to row of doc for auditing purpose. 
                    }
                    doc.LAST_UPDATE = DateTime.Now;
                    _docService.UpdateDoc(Mapper.Map<DOC>(doc));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    RedirectToAction("Error", new { doc, e});
                    throw;
                }
                return RedirectToAction("Index");
            }

            InitDropDownInformation();
            ViewBag.SIGNSSOFVIOLATIONS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text");
            ViewBag.TYPE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Получатель", Value = "1" }, new SelectListItem() { Text = "Отправитель", Value = "0" } }, "Value", "Text");
            return View(doc);
        }

        // GET: docs/Delete/5
        public ActionResult Delete(short id)
        {
            DOC doc = _docService.GetDoc(id);
            if (doc == null)
            {
                return HttpNotFound();
            }

            //ApplicationUser user = userManager.FindById(User.Identity.GetUserId());
            //isAdministrator = userManager.GetRoles(user.Id).Contains("admin");
            const bool IS_ADMINISTRATOR_VALUE_FOR_TEST_PURPOSES = true;
            isAdministrator = IS_ADMINISTRATOR_VALUE_FOR_TEST_PURPOSES;
            var userName = User.Identity.GetUserId();
            if (userName != doc.USERID && !isAdministrator)
            {
                return RedirectToAction("LogOff", "Account");
            }
            Doc localDoc = Mapper.Map<Doc>(doc);
            return View(localDoc);
        }

        // POST: docs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            _docService.DeleteDocWithUserIdMarking(id, User.Identity.GetUserId());
            //_docService.DeleteDoc (id);

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _docService.Dispose();
                _organizationService.Dispose();
                _custombrockerService.Dispose();
                _countryService.Dispose();
                _inspectorService.Dispose();
                _docControllerProfiler.Stop();
                _logger.Debug($"Doc controller ellapsed time is {_docControllerProfiler.ElapsedMilliseconds.GetEllapsedSeconds()} s.");
            }
            base.Dispose(disposing);
        }

        private void Changer(Word.Application wrdDoc, Dictionary<string, string> revolversLocal)
        {

            const int maxLenght = 150;

            Object missingObj = System.Reflection.Missing.Value;
            Object replaceTypeObj = Word.WdReplace.wdReplaceAll;
            Object strToFindObj;
            Object replaceStrObj;

            foreach (var item in revolversLocal)
            {
                strToFindObj = item.Key; replaceStrObj = item.Value;

                var replace = replaceStrObj.ToString();
                var find = strToFindObj.ToString();
                var replaceLenght = replace.Length;

                if (replaceLenght <= maxLenght)
                {
                    ChangerItem(wrdDoc, strToFindObj, replaceStrObj);
                }
                else
                {

                    List<Portion> portions = Portions(maxLenght, replaceLenght);

                    var portionsCount = portions.Count;

                    for (int i = 0; i < portionsCount - 1; i++)
                    {
                        var replaceItem = (object)(replace.Substring(portions[i].StartPosition, portions[i].Lenght) + find);

                        ChangerItem(wrdDoc, strToFindObj, replaceItem);
                    }

                    var replaceItemComplete = (object)replace.Substring(portions[portionsCount - 1].StartPosition, portions[portionsCount - 1].Lenght);

                    ChangerItem(wrdDoc, strToFindObj, replaceItemComplete);
                }
            }
        }

        private void ChangerItem(Word.Application wrdDoc, object strToFindObj, object replaceStrObj)
        {
            Object missingObj = System.Reflection.Missing.Value;
            Object replaceTypeObj = Word.WdReplace.wdReplaceAll;

            wrdDoc.Selection.Find.ClearFormatting();
            wrdDoc.Selection.Find.Replacement.ClearFormatting();
            wrdDoc.Selection.Find.Execute(ref strToFindObj, ref missingObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj,
                                            ref replaceStrObj, ref replaceTypeObj, ref missingObj,
                                            ref missingObj, ref missingObj, ref missingObj);
        }

        /// <summary>
        /// Describes portion as set of its parameters.
        /// </summary>
        private class Portion
        {
            public int StartPosition { get; set; }

            public int Lenght { get; set; }
        }

        private List<Portion> Portions(int maxLenght, int stringLenght)
        {
            var portions = new List<Portion>();

            if (stringLenght <= maxLenght)
            {
                portions.Add(new Portion() { StartPosition = 0, Lenght = stringLenght });
            }
            else
            {
                var portionsCount = stringLenght / maxLenght;

                if (portionsCount * maxLenght == stringLenght)
                {
                    for (int i = 0; i < portionsCount; i++)
                    {
                        portions.Add(new Portion() { StartPosition = i * maxLenght, Lenght = maxLenght });
                    }
                }
                else
                {
                    for (int i = 0; i < portionsCount; i++)
                    {
                        portions.Add(new Portion() { StartPosition = i * maxLenght, Lenght = maxLenght });
                    }
                    portions.Add(new Portion() { StartPosition = portionsCount * maxLenght, Lenght = stringLenght - portionsCount * maxLenght });
                }
            }
            return portions;
        }

        private void InitDropDownInformation()
        {
            #region DropList Information

            List<SelectListItem> ObjListLighting = new List<SelectListItem>()
            {
                new SelectListItem { Text = "естественное", Value = "естественное" },
                new SelectListItem { Text = "искуственное", Value = "искуственное" }

            };

            List<SelectListItem> ObjListWeather = new List<SelectListItem>()
            {
                new SelectListItem { Text = "ясная", Value = "ясная" },
                new SelectListItem { Text = "пасмурная", Value = "пасмурная" }

            };

            List<SelectListItem> ObjListSampleType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "образец", Value = "образец" },
                new SelectListItem { Text = "проба", Value = "проба" }

            };

            List<SelectListItem> ObjListSamplingType = new List<SelectListItem>()
            {
                new SelectListItem { Text = "первичная", Value = "первичную" },
                new SelectListItem { Text = "повторная", Value = "повторную" },
                new SelectListItem { Text = "дополнительная", Value = "дополнительную" }

            };

            List<SelectListItem> ObjListInspection = new List<SelectListItem>()
            {
                new SelectListItem { Text = "Досмотр", Value = "1" },
                new SelectListItem { Text = "Осмотр", Value = "0" }

            };

            //Assigning generic list to ViewBag.
            ViewBag.ISACTUAL = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ISAVERAGE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.LIGHTING = new SelectList(ObjListLighting, "Value", "Text");
            ViewBag.WHEATHER = new SelectList(ObjListWeather, "Value", "Text");
            ViewBag.KINDOFSAMPLE = new SelectList(ObjListSampleType, "Value", "Text");
            ViewBag.EXPERTINSINKIND = new SelectList(ObjListSamplingType, "Value", "Text");
            ViewBag.CUSTOMS = new SelectList(ObjListInspection, "Value", "Text");
            ViewBag.ISAVERAGE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.WEIGHINGTYPEISCALCULATION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ISOTHER = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ONITSOWNWHEELS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.SEALSVIOLATED = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.PACKEDATOURPRESENCE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ISDANGER = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            ViewBag.ISPERISHABLEPRODUCT = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Нет", Value = "0" }, new SelectListItem() { Text = "Да", Value = "1" } }, "Value", "Text");
            IEnumerable<Country> countriesAll = Mapper.Map<IEnumerable<COUNTRY>, IEnumerable<Country>>(_countryService.GetAllCountries());
            ViewBag.GOODSARRIVALCOUNTRY = new SelectList(countriesAll, "NAME", "NAME");
            // Additional lists.
            ViewBag.UNLOADED = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Выгружен полностью", Value = "0" }, new SelectListItem() { Text = "Выгружен частично", Value = "1" }, new SelectListItem() { Text = "Не выгружено (момент досмотра)", Value = "3" } }, "Value", "Text");
            ViewBag.COMPARESEALS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Да", Value = "0" }, new SelectListItem() { Text = "Нет", Value = "1" } }, "Value", "Text");
            ViewBag.COUNT = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Произведен пересчет", Value = "0" }, new SelectListItem() { Text = "Пересчет не произведен", Value = "1" } }, "Value", "Text");
            ViewBag.IDENTIFICATION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Да", Value = "0" }, new SelectListItem() { Text = "Нет", Value = "1" } }, "Value", "Text");
            ViewBag.OPENING = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Вскрытие производилось", Value = "1" }, new SelectListItem() { Text = "Вскрытие не производилось", Value = "2" } }, "Value", "Text");
            ViewBag.PARTIALORFULL = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Полный", Value = "0" }, new SelectListItem() { Text = "Выборочный", Value = "1" } }, "Value", "Text");
            ViewBag.COMPLIENCE = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Сведения соответствуют", Value = "0" }, new SelectListItem() { Text = "Выявлено НЕСООТВЕТСТВИЕ сведений", Value = "1" } }, "Value", "Text");
            ViewBag.INFORABOUTGOOD = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Сведения присутствуют", Value = "0" }, new SelectListItem() { Text = "Сведенний нет", Value = "1" } }, "Value", "Text");
            ViewBag.CAPACITYOFINSPECTION = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "100% досмотр товаров", Value = "0" }, new SelectListItem() { Text = "Частичный досмотр товаров", Value = "1" } }, "Value", "Text", 0);
            ViewBag.THEREISAPALLET = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "На паллетах", Value = "0" }, new SelectListItem() { Text = "Навалом", Value = "1" }, new SelectListItem() { Text = "Частично на паллетах, частично - навалом", Value = "2" } }, "Value", "Text");
            ViewBag.OPENINGCARGOGOODS = new SelectList(new List<SelectListItem>() { new SelectListItem() { Text = "Вскрытие произведено", Value = "0" }, new SelectListItem() { Text = "Вскрытие не производилось", Value = "1" } }, "Value", "Text");
            ViewBag.IsAdmin = isAdministrator;
            ViewBag.PostIdOfUser = postIdOfUser;
            #endregion
        }
    }
}
