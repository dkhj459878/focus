﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Views
{
    public class DeviceController : Controller
    {
        private readonly IDeviceService _deviceService;
        private readonly ICountryService _countryService;
        private readonly IDevicetypeService _devicetypeService;

        public DeviceController(IDeviceService deviceService, ICountryService countryService, IDevicetypeService devicetypeService)
        {
            _deviceService = deviceService;
            _countryService = countryService;
            _devicetypeService = devicetypeService;
        }

        // Remote diagnostics.
        // GET: Device /NotRepeated/
        public JsonResult NotRepeated(byte? DEVICETYPEID, string MARK, string MODEL, string SERIALNUMBER, int? DEVICEID)
        {
            string deviceType;
            if (IsUnique(DEVICETYPEID, MARK, MODEL, SERIALNUMBER, DEVICEID))
            {
                return Json(true, 
                    JsonRequestBehavior.AllowGet);
            }

            if (DEVICETYPEID == null)
            {
                deviceType = string.Empty;
            }
            else
            {
                deviceType = _devicetypeService.GetDevicetype((byte)DEVICETYPEID)?.NAME;
            }
            return Json(
                    string.Format(
                        Messages.ErrorRemoteDeviceRepeated,
                        deviceType, MARK ?? string.Empty,
                        MODEL ?? string.Empty,
                        SERIALNUMBER ?? string.Empty),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(byte? deviceType, string mark, string model, string serialNumber, int? deviceId)
        {
            var markForSearch = mark == "" ? null : mark;
            var modelForSearch = model == "" ? null : model;
            var serialNumberForSearch = serialNumber == "" ? null : serialNumber;


            if (deviceId == null)
            {
                return !_deviceService.DeviceExists
                (filter: c => c.MARK == markForSearch &&
                    c.MODEL == modelForSearch &&
                    c.SERIALNUMBER == serialNumber &&
                    c.DEVICETYPEID == deviceType);
            }

            return !_deviceService.DeviceExists
                (filter: c => c.MARK == markForSearch &&
                    c.MODEL == modelForSearch &&
                    c.SERIALNUMBER == serialNumberForSearch &&
                    c.DEVICETYPEID == deviceType &&
                    c.DEVICEID != deviceId);
        }

        // GET: Device
        public ActionResult Index()
        {
            var devices = _deviceService.GetAllDevices(includeProperties: "devicetype");
            return View(Mapper.Map<IEnumerable<DEVICE>, IEnumerable<Device>>(devices.ToList()));
        }

        // GET: Device/Details/5
        public ActionResult Details(short id)
        {
            DEVICE device = _deviceService.GetDevice(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Device>(device));
        }

        public JsonResult Json(short id)
        {
            var jsondata = _deviceService.GetDevice(id);
            jsondata.DEVICETYPE = _devicetypeService.GetDevicetype(jsondata.DEVICETYPEID);

            return Json(
                new
                {
                    id = jsondata?.DEVICEID,
                    serialnumber = jsondata?.SERIALNUMBER,
                    name = jsondata?.NAME,
                    type = jsondata.DEVICETYPE?.NAME,
                    devicetypeid = jsondata.DEVICETYPE?.DEVICETYPEID,
                    callibration = jsondata?.CALLIBRATION,
                    mark = jsondata?.MARK,
                    model = jsondata?.MODEL,
                    parametersofcamera = jsondata?.PARAMETERSOFCAMERA,
                    parametersofscales = jsondata?.PARAMETERSOFSCALES,
                    parametersxrayinspectioncomplexes = jsondata?.PARAMETERSXRAYCOMPLEXES,
                    phone = jsondata?.PHONE
                },
                JsonRequestBehavior.AllowGet);
        }

        public JsonResult JsonOrg(short id)
        {
            var jsondata = _deviceService.GetDevice(id);
            jsondata.DEVICETYPE = _devicetypeService.GetDevicetype(jsondata.DEVICETYPEID);

            return Json(
                new
                {
                    id = jsondata?.DEVICEID,
                    serialnumber = jsondata?.SERIALNUMBER,
                    name = jsondata?.NAME,
                    type = jsondata.DEVICETYPE?.NAME,
                    devicetypeid = jsondata.DEVICETYPE?.DEVICETYPEID,
                    callibration = jsondata?.CALLIBRATION,
                    mark = jsondata?.MARK,
                    model = jsondata?.MODEL,
                    parametersofcamera = jsondata?.PARAMETERSOFCAMERA,
                    parametersofscales = jsondata?.PARAMETERSOFSCALES,
                    parametersxrayinspectioncomplexes = jsondata?.PARAMETERSXRAYCOMPLEXES,
                    phone = jsondata?.PHONE
                },
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Search(string name, int[] deviceSelectedIndexes)
        {
            int[] selectedIndexes = deviceSelectedIndexes;
            IEnumerable<DEVICE> devicesFiltered;
            var devices = _deviceService.GetAllDevices(filter: o => o.NAME.Contains(name)).ToList();
            if (devices.Count <= 0)
            {
                return HttpNotFound();
            }
            if (selectedIndexes != null)
            {
                devicesFiltered = devices.Where(x => !deviceSelectedIndexes.Contains(x.DEVICEID)).ToArray();
                if (devicesFiltered.Count() <= 0)
                {
                    return HttpNotFound();
                }
                return PartialView(Mapper.Map<IEnumerable<DEVICE>, IEnumerable<Device>>(devicesFiltered));
            }
            return PartialView(Mapper.Map<IEnumerable<DEVICE>, IEnumerable<Device>>(devices));
        }

        // GET: Device/Create
        public ActionResult PartialCreate()
        {
            ViewBag.DEVICETYPEID = new SelectList(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(_devicetypeService.GetAllDevicetypes()), "DEVICETYPEID", "NAME");
            return PartialView();
        }

        // GET: Device/Create
        public ActionResult Create()
        {
            ViewBag.DEVICETYPEID = new SelectList(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(_devicetypeService.GetAllDevicetypes()), "DEVICETYPEID", "NAME");
            return View();
        }

        // POST: Device/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialCreate([Bind(Include = "DEVICEID,DEVICETYPEID,NAME,MARK,MODEL,CALLIBRATION,PARAMETERSOFSCALES,PARAMETERSXRAYCOMPLEXES,PARAMETERSOFCAMERA,PHONE")] Device device)
        {
            if (ModelState.IsValid)
            {
                device.LAST_UPDATE = DateTime.Now;
                device.DEVICEID = _deviceService.AddDevice(Mapper.Map<DEVICE>(device));
                return PartialView("Search", new List<Device>() { device });
            }

            ViewBag.DEVICETYPEID = new SelectList(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(_devicetypeService.GetAllDevicetypes()), "DEVICETYPEID", "NAME");
            return PartialView(device);
        }


        // POST: Device/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DEVICEID,DEVICETYPEID,NAME,MARK,MODEL,CALLIBRATION,PARAMETERSOFSCALES,PARAMETERSXRAYCOMPLEXES,PARAMETERSOFCAMERA,PHONE")] Device device)
        {
            if (ModelState.IsValid)
            {
                device.LAST_UPDATE = DateTime.Now;
                _deviceService.AddDevice(Mapper.Map<DEVICE>(device));
                return RedirectToAction("Index");
            }

            ViewBag.DEVICETYPEID = new SelectList(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(_devicetypeService.GetAllDevicetypes()), "DEVICETYPEID", "NAME");
            return View(device);
        }


        // GET: Device/Edit/5
        public ActionResult Edit(short id)
        {
            DEVICE device = _deviceService.GetDevice(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.DEVICETYPEID = new SelectList(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(_devicetypeService.GetAllDevicetypes()), "DEVICETYPEID", "NAME", device.DEVICETYPEID);
            return View(Mapper.Map<Device>(device));
        }

        // POST: Device/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PartialEdit([Bind(Include = "DEVICEID,DEVICETYPEID,NAME,MARK,MODEL,CALLIBRATION,PARAMETERSOFSCALES,PARAMETERSXRAYCOMPLEXES,PARAMETERSOFCAMERA,PHONE")] Device device)
        {
            if (ModelState.IsValid)
            {
                device.LAST_UPDATE = DateTime.Now;
                _deviceService.UpdateDevice(Mapper.Map<DEVICE>(device));
                return PartialView("DeviceEdited", device);
            }
            ViewBag.DEVICETYPEID = new SelectList(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(_devicetypeService.GetAllDevicetypes()), "DEVICETYPEID", "NAME");
            return PartialView(device);
        }


        // GET: Device/Edit/5
        public ActionResult PartialEdit(short id)
        {
            DEVICE device = _deviceService.GetDevice(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            ViewBag.DEVICETYPEID = new SelectList(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(_devicetypeService.GetAllDevicetypes()), "DEVICETYPEID", "NAME", device.DEVICETYPEID);
            return PartialView(Mapper.Map<Device>(device));
        }

        // POST: Device/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DEVICEID,DEVICETYPEID,NAME,MARK,MODEL,CALLIBRATION,PARAMETERSOFSCALES,PARAMETERSXRAYCOMPLEXES,PARAMETERSOFCAMERA,PHONE")] Device device)
        {
            if (ModelState.IsValid)
            {
                device.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
                device.LAST_UPDATE = DateTime.Now;
                _deviceService.UpdateDevice(Mapper.Map<DEVICE>(device));
                return RedirectToAction("Index");
            }
            ViewBag.DEVICETYPEID = new SelectList(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(_devicetypeService.GetAllDevicetypes()), "DEVICETYPEID", "NAME");
            return View(Mapper.Map<Device>(device));
        }

        // GET: Device/Delete/5
        public ActionResult PartialDelete(short id)
        {
            DEVICE device = _deviceService.GetDevice(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return PartialView(Mapper.Map<Device>(device));
        }

        // POST: Device/Delete/5
        [HttpPost, ActionName("PartialDelete")]
        [ValidateAntiForgeryToken]
        public ActionResult PartialDeleteConfirmed(short id)
        {
            DEVICE device = _deviceService.GetDevice(id);
            _deviceService.DeleteDevice(id);
            return PartialView("DeviceDeleted", Mapper.Map<Device>(device));
        }

        // GET: Device/Delete/5
        public ActionResult Delete(short id)
        {
            DEVICE device = _deviceService.GetDevice(id);
            if (device == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Device>(device));
        }

        // POST: Device/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            DEVICE device = _deviceService.GetDevice(id);
            if (device == null)
            {
                return new JsonResult();
            }
            _deviceService.DeleteDevice(id);
            return RedirectToAction("Index");
        }

		// GET: vehicles/JsonDelete/5
		public ActionResult JsonDelete(short id)
        {
            DEVICE jsondata = _deviceService.GetDevice(id);

            var deviceNameJson = $"{_devicetypeService.GetDevicetype(jsondata.DEVICETYPEID).NAME} {jsondata.MODEL} {jsondata.MODEL} {jsondata.SERIALNUMBER}";

            var docCount = jsondata.DOC?.Count;

            return Json(
               new
               {
                   deviceId = id,
                   deviceName = deviceNameJson,
				   
                   docCount,
                   docs = jsondata.DOC.Select(d => $"Номер АТД '{d.NUM}' от {d.PERIODSTARTDATE.Value.ToShortDateString()} {d.CONSIGNEENAME}")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _deviceService.Dispose();
                _countryService.Dispose();
                _devicetypeService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
