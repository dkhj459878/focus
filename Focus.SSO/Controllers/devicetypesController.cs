﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class devicetypesController : Controller
    {
        private readonly IDevicetypeService _devicetypeService;

        public devicetypesController(IDevicetypeService devicetypeService)
        {
            _devicetypeService = devicetypeService;
        }


        // Remote diagnostics.
        // GET: devicetypes /NotRepeated/
        public JsonResult NotRepeated(string name, byte? DEVICETYPEID)
        {
            if (IsUnique(name, DEVICETYPEID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteDevicetypeRepeated, name),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(string name, byte? deviceTypeId)
        {
            if (deviceTypeId == null)
            {
                return !_devicetypeService.DevicetypeExists
                (filter: d => d.NAME == name);
            }
            return !_devicetypeService.DevicetypeExists
                 (filter: d => d.NAME == name &&
                 d.DEVICETYPEID != deviceTypeId);
        }

        // GET: devicetypes
        public ActionResult Index()
        {
            var devicetypes = _devicetypeService.GetAllDevicetypes().ToList();
            return View(Mapper.Map<IEnumerable<DEVICETYPE>, IEnumerable<Devicetype>>(devicetypes));
        }

        // GET: devicetypes/Details/5
        public ActionResult Details(byte id)
        {
            DEVICETYPE devicetype = _devicetypeService.GetDevicetype(id);
            if (devicetype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Devicetype>(devicetype));
        }

        // GET: devicetypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: devicetypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "DEVICETYPEID,NAME,LAST_UPDATE")] Devicetype devicetype)
        {
            if (ModelState.IsValid)
            {
                devicetype.LAST_UPDATE = DateTime.Now;
                _devicetypeService.AddDevicetype(Mapper.Map<DEVICETYPE>(devicetype));
                return RedirectToAction("Index");
            }

            return View(devicetype);
        }

        // GET: devicetypes/Edit/5
        public ActionResult Edit(byte id)
        {
            DEVICETYPE devicetype = _devicetypeService.GetDevicetype(id);
            if (devicetype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Devicetype>(devicetype));
        }

        // POST: devicetypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "DEVICETYPEID,NAME,LAST_UPDATE")] Devicetype devicetype)
        {
            if (ModelState.IsValid)
            {
                devicetype.USERID = User.Identity.GetUserId(); // Adding user id to row of container for auditing purpose. 
                _devicetypeService.UpdateDevicetype(Mapper.Map<DEVICETYPE>(devicetype));
                return RedirectToAction("Index");
            }
            return View(devicetype);
        }

        // GET: devicetypes/Delete/5
        public ActionResult Delete(byte id)
        {
            DEVICETYPE devicetype = _devicetypeService.GetDevicetype(id);
            if (devicetype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Devicetype>(devicetype));
        }

        // POST: devicetypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(byte id)
        {
            DEVICETYPE devicetype = _devicetypeService.GetDevicetype(id);
            _devicetypeService.DeleteDevicetype(id);
            return RedirectToAction("Index");
        }

        // GET: vehicles/JsonDelete/5
        public ActionResult JsonDelete(byte id)
        {
            DEVICETYPE jsondata = _devicetypeService.GetDevicetype(id);

            var devicetypeJSON = $"{jsondata.NAME}";

            var docCount = jsondata.DEVICE?.Count;

            return Json(
               new
               {
                   devicetypeId = id,
                   devicetypeName = devicetypeJSON,

                   docCount,
                   devices = jsondata.DEVICE.Select(d => $"{d.NAME}' {d.MARK} {d.MODEL} s\\n {d.SERIALNUMBER}")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _devicetypeService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
