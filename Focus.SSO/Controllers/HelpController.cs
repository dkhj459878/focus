﻿using System.Web.Mvc;

namespace IdentityMySQLDemo.Controllers
{
    public class HelpController : Controller
    {
        // GET: Help
        public ActionResult PartialDocCreate()
        {
            return PartialView();
        }
        
        // GET: Help
        public ActionResult DocCreate()
        {
            return View();
        }

        // GET: Help
        public ActionResult PartialSmallDocCreate()
        {
            return PartialView();
        }
         
        // GET: Help
        public ActionResult SmallDocCreate()
        {
            return View();
        }

        // GET: Help
        public ActionResult PartialSmallCustomBrockersCreate()
        {
            return PartialView();
        }
        
        // GET: Help
        public ActionResult SmallCustomBrockersCreate()
        {
            return View();
        }

        // GET: Help
        public ActionResult NewDocs()
        {
            return View();
        }        
        
        // GET: Help
        public ActionResult PartialNewDocs()
        {
            return PartialView();
        }

        // GET: Help
        public ActionResult SmallCreateCompany()
        {
            return PartialView();
        }

        // GET: Help
        public ActionResult CreateCompany()
        {
            return View();
        }

        // GET: Help
        public ActionResult SmallCreateResolution()
        {
            return PartialView("SmallCreateCompany");
        }

        // GET: Help
        public ActionResult Version()
        {
            return View();
        }

        // GET: Help
        public ActionResult ListHelp()
        {
            return View();
        }

        // GET: Help
        public ActionResult CreateResolution()
        {
            return View();
        }

        // GET: Help
        public ActionResult Description()
        {
            return View();
        }
    }
}