﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.DAL.Extensions;
using Focus.SSO.Models;
using log4net;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class postController : Controller
    {
        protected static readonly ILog _logger =
            LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        private readonly IPostService _postService;

        public postController(IPostService postService)
        {
            _postService = postService;
        }

        // Remote diagnostics.
        // GET: post /NotRepeated/
        public JsonResult NotRepeated(string CODE, string NAME, int? ID)
        {
            if (IsEnique(CODE, NAME, ID))
            {
                return Json(true,
                    JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemotePostRepeated,
                        CODE ?? string.Empty,
                        NAME ?? string.Empty),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsEnique(string code, string name, int? id)
        {
            if (id == null)
            {
                return !_postService.PostExists(
                    i => i.CODE == (code == string.Empty ? null : code) &&
                    i.NAME == (name == string.Empty ? null : name));
            }

            return !_postService.PostExists(
                    i => i.CODE == (code == string.Empty ? null : code) &&
                    i.NAME == (name == string.Empty ? null : name) &&
                    i.ID != id);

        }


        // GET: Inspector
        public ActionResult Index()
        {
            var post = _postService.GetAllPosts();
            return View(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(post.ToList()));
        }

        // GET: Inspector/Details/5
        public ActionResult Details(byte id)
        {
            POST post = _postService.GetPost(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Post>(post));
        }

        public JsonResult JsonOrg(byte id)
        {
            var jsondata = _postService.GetPost(id);

            return Json(
                new
                {
                    id = jsondata?.ID,
                    postcode = jsondata?.CODE,
                    postname = jsondata?.NAME,
                    postdevision = jsondata?.DEVISION,
                    postaddress = jsondata?.ADDRESS
                },
                JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Search(string name)
        {
            List<POST> posts = _postService.GetAllPosts(filter: o => o.NAME.Contains(name)).ToList();
            if (posts.Count == 0)
            {
                return HttpNotFound();
            }
            return PartialView(Mapper.Map<IEnumerable<POST>, IEnumerable<Post>>(posts));
        }

        // GET: post/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inspector/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,CODE,NAME,DEVISION,ADDRESS,LAST_UPDATE")] Post post)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    post.USERID = User.Identity.GetUserId(); // Adding user id to row of post for auditing purpose. 
                    post.LAST_UPDATE = DateTime.Now;
                    _postService.AddPost(Mapper.Map<POST>(post));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }

                return RedirectToAction("Index");
            }
            
            return View(post);
        }

        // GET: Inspector/Edit/5
        public ActionResult Edit(byte id)
        {
            POST post = _postService.GetPost(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Post>(post));
        }

        // POST: Inspector/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,CODE,NAME,DEVISION,ADDRESS,LAST_UPDATE")] Post post)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    post.USERID = User.Identity.GetUserId(); // Adding user id to row of post for auditing purpose. 
                    post.LAST_UPDATE = DateTime.Now;
                    _postService.UpdatePost(Mapper.Map<POST>(post));
                }
                catch (DbEntityValidationException e)
                {
                    _logger.Error(e.GetDetails(), e);
                    throw;
                }
                return RedirectToAction("Index");
            }
            return View(post);
        }

        // GET: Inspector/Delete/5
        public ActionResult Delete(byte id)
        {
            POST post = _postService.GetPost(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Post>(post));
        }

        // POST: Inspector/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(byte id)
        {
            POST post = _postService.GetPost(id);
            _postService.DeletePost(id);
            return RedirectToAction("Index");
        }

        // GET: vehicles/JsonDelete/5
        public ActionResult JsonDelete(byte id)
        {
            POST jsondata = _postService.GetPost(id);

            var postJSON = $"{jsondata.CODE} {jsondata.NAME} {jsondata.DEVISION}";

            var storageCount = jsondata.STORAGE?.Count;

            return Json(
               new
               {
                   storageId = id,
                   storageName = postJSON,

                   storageCount,
                   storages = jsondata.STORAGE.Select(o => $"Склад временного хранения с номером '{o.NUMBER_}'")
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _postService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
