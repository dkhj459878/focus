﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Focus.Controllers
{
    public class reportsController : Controller
    {
        private IReportService _reportService;

        public reportsController(IReportService reportService)
        {
            _reportService = reportService;
        }

        // GET: reports
        public ActionResult Index()
        {
            var reports = _reportService.GetAllReports().ToList();
            return View(Mapper.Map<IEnumerable<REPORT>, IEnumerable<Report>>(reports));
        }

        // GET: reports/Details/5
        public ActionResult Details(short id)
        {
            REPORT report = _reportService.GetReport(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Report>(report));
        }

        // GET: reports/Create
        public ActionResult Create()
        {
            return View();
        }
        // POST: reports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.7oo,ll,lklkkl
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "REPORTID,PASSPORTNUMBER,CUSTOMBROCKERID,NUMBER,FULLNAME,LAST_UPDATE")] Report report)
        {
            if (ModelState.IsValid)
            {
                report.LAST_UPDATE = DateTime.Now;
                _reportService.AddReport(Mapper.Map<REPORT>(report), "", "");
                return RedirectToAction("Index");
            }

            return View(report);
        }

        // GET: reports/Edit/5
        public ActionResult Edit(short id)
        {
            REPORT report = _reportService.GetReport(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Report>(report));
        }

        // POST: reports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "REPORTID,PASSPORTNUMBER,CUSTOMBROCKERID,NUMBER,FULLNAME,LAST_UPDATE")] Report report)
        {
            if (ModelState.IsValid)
            {
                report.LAST_UPDATE = DateTime.Now;
                _reportService.UpdateReport(Mapper.Map<REPORT>(report), "", "");
                return RedirectToAction("Index");
            }
            return View(report);
        }

        // GET: reports/Delete/5
        public ActionResult Delete(short id)
        {
            REPORT report = _reportService.GetReport(id);
            if (report == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Report>(report));
        }

        // POST: reports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            REPORT report = _reportService.GetReport(id);
            _reportService.DeleteReport(id, "", "");
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _reportService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
