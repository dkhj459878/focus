﻿using AutoMapper;
using Focus.BL.Common.Services;
using Focus.DAL.Common.DbModels;
using Focus.SSO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Messages = Focus.SSO.Properties.Resources;

namespace Focus.Controllers
{
    public class vehicletypesController : Controller
    {
        private IVehicletypeService _vehicletypeService;

        public vehicletypesController(IVehicletypeService vehicletypeService)
        {
            _vehicletypeService = vehicletypeService;
        }

        // Remote diagnostics.
        // GET: vehicletypes /NotRepeated/
        public JsonResult NoRepeated(string name, byte? VEHICLETYPEID)
        {
            if (IsUnique(name, VEHICLETYPEID))
            {
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(
                    string.Format(
                        Messages.ErrorRemoteVehicletypeRepeated,
                        name),
                        JsonRequestBehavior.AllowGet);
        }

        private bool IsUnique(string name, byte? vehicelTypeId)
        {
            if (vehicelTypeId == null)
            {
                return !_vehicletypeService.VehicletypeExists
                (filter: v => v.NAME == name);
            }
            return !_vehicletypeService.VehicletypeExists
                 (filter: v => v.NAME == name &&
                 v.VEHICLETYPEID != vehicelTypeId);
        }

        // GET: vehicletypes
        public ActionResult Index()
        {
            var vehicletypes = _vehicletypeService.GetAllVehicletypes().ToList();
            return View(Mapper.Map<IEnumerable<VEHICLETYPE>, IEnumerable<Vehicletype>>(vehicletypes));
        }

        // GET: vehicletypes/Details/5 
        public ActionResult Details(byte id)
        {
            VEHICLETYPE vehicletype = _vehicletypeService.GetVehicletype(id);
            if (vehicletype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehicletype>(vehicletype));
        }

        // GET: vehicletypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: vehicletypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Vehicletype vehicletype)
        {
            if (ModelState.IsValid)
            {
                vehicletype.LAST_UPDATE = DateTime.Now;
                _vehicletypeService.AddVehicletype(Mapper.Map<VEHICLETYPE>(vehicletype));
                return RedirectToAction("Index");
            }

            return View(vehicletype);
        }

        // GET: vehicletypes/Edit/5
        public ActionResult Edit(byte id)
        {
            VEHICLETYPE vehicletype = _vehicletypeService.GetVehicletype(id);
            if (vehicletype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehicletype>(vehicletype));
        }

        // POST: vehicletypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "VEHICLETYPEID,NAME,DESCRIPTION,LAST_UPDATE")] Vehicletype vehicletype)
        {
            if (ModelState.IsValid)
            {
                vehicletype.LAST_UPDATE = DateTime.Now;
                _vehicletypeService.UpdateVehicletype(Mapper.Map<VEHICLETYPE>(vehicletype));
                return RedirectToAction("Index");
            }
            return View(vehicletype);
        }

        // GET: vehicletypes/Delete/5
        public ActionResult Delete(byte id)
        {
            var vehicletype = _vehicletypeService.GetVehicletype(id);
            if (vehicletype == null)
            {
                return HttpNotFound();
            }
            return View(Mapper.Map<Vehicletype>(vehicletype));
        }

        // POST: vehicletypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(byte id)
        {
            VEHICLETYPE vehicletype = _vehicletypeService.GetVehicletype(id);
            _vehicletypeService.DeleteVehicletype(id);
            return RedirectToAction("Index");
        }

        public ActionResult JsonDelete(byte id)
        {
            VEHICLETYPE jsondata = _vehicletypeService.GetVehicletype(id);

            var vehicleType = jsondata.NAME;

            var vehicleTypeCount = jsondata.VEHICLEKIND?.Count;

            return Json(
               new
               {
                   vehicleTypeId = id,
                   vehicleTypeName = vehicleType,
                   vehicleTypeCount,
                   vehicleKinds = jsondata.VEHICLEKIND.Select(vk => vk.NAME)
               },
               JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _vehicletypeService.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
