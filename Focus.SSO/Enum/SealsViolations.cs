﻿using System;
namespace Focus.Enum
{
    [Flags]
    public enum SealsViolations
    {
        ZERO   = 0,
        OPENING_YES = 4,
        OPENING_NO = 8,
        UNLOADED_FULL = 16,
        UNLOADED_PARTLY = 32,
        Other = 64
    }
}