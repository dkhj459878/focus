﻿using Focus.Web;
using Ninject;
using Ninject.Web.Common.WebHost;
using System;
using Focus.Web.Common;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace IdentityMySQLDemo
{
    public class MvcApplication : NinjectHttpApplication
    {
        protected override void OnApplicationStarted()
        {
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new RazorViewEngine());

            log4net.Config.XmlConfigurator.Configure();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            MapperConfig.Initialize();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            PoCStaticMethodInGlobalAsax.SetDateTimeCreation(DateTime.Now);
            string CREATION_CONTEXT_IN_GLOBAL_ASAX = "Обьект обновлен в Global.asax.";
            PoCStaticMethodInGlobalAsax.SetCreationContext(CREATION_CONTEXT_IN_GLOBAL_ASAX);
        }

        protected override IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(new[] { "Focus.BL*", "Focus.DAL*", "Focus.BL.Tests*" });

            return kernel;
        }
    }

    public abstract class FloatingPointModelBinderBase<T> : DefaultModelBinder
    {
        protected abstract Func<string, IFormatProvider, T> ConvertFunc { get; }

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (valueProviderResult == null) return base.BindModel(controllerContext, bindingContext);
            try
            {
                return ConvertFunc.Invoke(valueProviderResult.AttemptedValue, CultureInfo.CurrentUICulture);
            }
            catch (FormatException)
            {
                // If format error then fallback to InvariantCulture instead of current UI culture
                return ConvertFunc.Invoke(valueProviderResult.AttemptedValue, CultureInfo.InvariantCulture);
            }
        }
    }
}
