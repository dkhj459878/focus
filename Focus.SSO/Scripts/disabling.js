﻿function Disabling(string) {
    var element = document.getElementById(string);
    element.setAttribute('disabled', 'true');
}
function Enabling(string) {
    var element = document.getElementById(string);
    element.removeAttribute('disabled');
}
function DeleteElementById(id) {
    if (id == null) {
        return ;
    }
    var element = document.getElementById(id);

    if (element == null) {
        return ;
    }
    element.remove();
}