﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с документами.
    /// </summary>
    public interface IDocumentService : IDisposable
    {
        /// <summary>
        /// Возвращает все документы
        /// из хранилища данных.   
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекцию всех документов.</returns>
        IEnumerable<DOCUMENT> GetAllDocuments(
            Expression<Func<DOCUMENT, bool>> filter = null,
            Func<IQueryable<DOCUMENT>, IOrderedQueryable<DOCUMENT>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает документы
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="pageSize">Количество сведений о документах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документах.</param>
        /// <returns>Коллекция экземпляров документов для пейджинга.</returns>
        IEnumerable<DOCUMENT> GetDocumentsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DOCUMENT, bool>> filter = null,
            Func<IQueryable<DOCUMENT>, IOrderedQueryable<DOCUMENT>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает документы с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        DOCUMENT GetDocument(short id);

        /// <summary>
        /// Добавляет заданный документ в хранилище данных
        /// и возвращает идентификатор добавленного документа.
        /// </summary>
        /// <param name="user">Экземпляр документа.</param>
        /// <returns>Уникальный идентификатор документа.</returns>
        short AddDocument(DOCUMENT document,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного документа в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр документа.</param>
        /// <returns> Task </returns>
        void UpdateDocument(DOCUMENT document,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет документ с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns> Task </returns>
        void DeleteDocument(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие документа в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска документа.</param>
        /// <returns>Возвращает <see langword="true"/>, если документ существует в хранилище данных.</returns>
        bool DocumentExists(Expression<Func<DOCUMENT, bool>> filter);
    }
}