﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditVehiclekindService
    {
        List<VEHICLEKIND> GetVehiclekindVersions(short id);
    }
}
