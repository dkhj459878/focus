﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с ПТО.
    /// </summary>
    public interface IPostService : IDisposable
    {
        /// <summary>
        /// Возвращает ПТО
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров ПТО для пейджинга.</returns>
        IEnumerable<POST> GetAllPosts(
            Expression<Func<POST, bool>> filter = null,
            Func<IQueryable<POST>, IOrderedQueryable<POST>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает ПТО с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о ПТО.</param>
        /// <returns>Экземпляр ПТО.</returns>
        IEnumerable<POST> GetPostsPage(
           int pageSize,
           int pageNumber,
           Expression<Func<POST, bool>> filter = null,
           Func<IQueryable<POST>, IOrderedQueryable<POST>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает ПТО с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор ПТО.</param>
        /// <returns>Экземпляр ПТО.</returns>
        POST GetPost(byte id);

        /// <summary>
        /// Добавляет заданный ПТО в хранилище данных
        /// и возвращает идентификатор добавленного ПТО.
        /// </summary>
        /// <param name="user">Экземпляр ПТО.</param>
        /// <returns>Уникальный идентификатор ПТО.</returns>
        byte AddPost(POST post,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного ПТО в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр ПТО.</param>
        /// <returns> Task </returns>
        void UpdatePost(POST post,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет ПТО с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор ПТО.</param>
        /// <returns> Task </returns>
        void DeletePost(byte id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие ПТО в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска ПТО.</param>
        /// <returns>Возвращает <see langword="true"/>, если ПТО существует в хранилище данных.</returns>
        bool PostExists(Expression<Func<POST, bool>> filter);
    }
}