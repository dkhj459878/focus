﻿using System;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IRetrieveEntitiesVersions<TEntity> where TEntity : class
    {
        Dictionary<short?, Dictionary<DateTime, TEntity>> GetEntitiesVersions(IAudit_trailService audit_TrailService, IRetrievingEntityOptimaService retrievingEntityService);
    }
}
