﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditDocumentService
    {
        List<DOCUMENT> GetDocumentVersions(short id);
    }
}
