﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с контейнерами.
    /// </summary>
    public interface IContainerService : IDisposable
    {
        /// <summary>
        /// Возвращает контейнеры
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров контейнеров для пейджинга.</returns>
        IEnumerable<CONTAINER> GetAllContainers(
            Expression<Func<CONTAINER, bool>> filter = null,
            Func<IQueryable<CONTAINER>, IOrderedQueryable<CONTAINER>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает контейнеры с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документе.</param>
        /// <returns>Экземпляр контейнера.</returns>
        IEnumerable<CONTAINER> GetContainersPage(
           int pageSize,
           int pageNumber,
           Expression<Func<CONTAINER, bool>> filter = null,
           Func<IQueryable<CONTAINER>, IOrderedQueryable<CONTAINER>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает контейнер с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        CONTAINER GetContainer(short id);

        /// <summary>
        /// Добавляет заданный контейнер в хранилище данных
        /// и возвращает идентификатор добавленного контейнера.
        /// </summary>
        /// <param name="user">Экземпляр контейнера.</param>
        /// <returns>Уникальный идентификатор контейнера.</returns>
        short AddContainer(CONTAINER container,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного контейнера в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр контейнера.</param>
        /// <returns> Task </returns>
        void UpdateContainer(CONTAINER container,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет контейнер с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор контейнера.</param>
        /// <returns> Task </returns>
        void DeleteContainer(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие контейнера в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска контейнера.</param>
        /// <returns>Возвращает <see langword="true"/>, если контейнер существует в хранилище данных.</returns>
        bool ContainerExists(Expression<Func<CONTAINER, bool>> filter);
    }
}