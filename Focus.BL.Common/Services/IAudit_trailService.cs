﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с автомобилями.
    /// </summary>
    public interface IAudit_trailService : IDisposable
    {
        /// <summary>
        /// Возвращает автомобили, как товары,
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекция экземпляров автомобилей для пейджинга.</returns> 
        IEnumerable<AUDIT_TRAIL> GetAllAudit_trails(
            Expression<Func<AUDIT_TRAIL, bool>> filter = null,
            Func<IQueryable<AUDIT_TRAIL>, IOrderedQueryable<AUDIT_TRAIL>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает автомобили, как товары, с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документе.</param>
        /// <returns>Экземпляр автомобиля.</returns>
        IEnumerable<AUDIT_TRAIL> GetAudit_trailsPage(
           int pageSize,
           int pageNumber,
           Expression<Func<AUDIT_TRAIL, bool>> filter = null,
           Func<IQueryable<AUDIT_TRAIL>, IOrderedQueryable<AUDIT_TRAIL>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает автомобиль, как товар, с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        AUDIT_TRAIL GetAudit_trail(long id);

        /// <summary>
        /// Добавляет заданный автомобиль в хранилище данных
        /// и возвращает идентификатор добавленного автомобиля.
        /// </summary>
        /// <param name="user">Экземпляр автомобиля.</param>
        /// <returns>Уникальный идентификатор автомобиля.</returns>
        long AddAudit_trail(AUDIT_TRAIL audit_trail);

        /// <summary>
        /// Добавляет заданный автомобиль в хранилище данных
        /// и возвращает идентификатор добавленного автомобиля.
        /// </summary>
        /// <param name="user">Экземпляр автомобиля.</param>
        /// <returns>Уникальный идентификатор автомобиля.</returns>
        int AddAudit_trails(IEnumerable<AUDIT_TRAIL> audit_trails);

        /// <summary>
        /// Обновляет информацию заданного автомобиля в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр автомобиля.</param>
        /// <returns> Task </returns>
        void UpdateAudit_trail(AUDIT_TRAIL audit_trail);

        /// <summary>
        /// Удаляет автомобиль с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор автомобиля.</param>
        /// <returns> Task </returns>
        void DeleteAudit_trail(long id);

        /// <summary>
        /// Проверяет наличие автомобиля в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска автомобиля.</param>
        /// <returns>Возвращает <see langword="true"/>, если автомобиль существует в хранилище данных.</returns>
        bool Audit_trailExists(Expression<Func<AUDIT_TRAIL, bool>> filter);
    }
}