﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с транспортными средствами.
    /// </summary>
    public interface IVehicleService : IDisposable
    {
        /// <summary>
        /// Возвращает все транспортные средства,
        /// из хранилища данных.  
         /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекцию всех экземпляров транспортных средств.</returns>
        IEnumerable<VEHICLE> GetAllVehicles(
            Expression<Func<VEHICLE, bool>> filter = null,
            Func<IQueryable<VEHICLE>, IOrderedQueryable<VEHICLE>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает транспортные средства
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="pageSize">Количество сведений о транспортных средствах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о транспортном средстве.</param>
        /// <returns>Коллекция экземпляров транспортных средства для пейджинга.</returns>
        IEnumerable<VEHICLE> GetVehiclesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<VEHICLE, bool>> filter = null,
            Func<IQueryable<VEHICLE>, IOrderedQueryable<VEHICLE>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает транспортные средства с заданным идентификатором,
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор контейнера.</param>
        /// <returns>Экземпляр контейнера.</returns>
        VEHICLE GetVehicle(short id);

        /// <summary>
        /// Добавляет заданный контейнер в хранилище данных
        /// и возвращает идентификатор добавленного контейнера.
        /// </summary>
        /// <param name="user">Экземпляр контейнера.</param>
        /// <returns>Уникальный идентификатор контейнера.</returns>
        short AddVehicle(VEHICLE vehicle,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного контейнера в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр контейнера.</param>
        /// <returns> Task </returns>
        void UpdateVehicle(VEHICLE vehicle,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет контейнер с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор контейнера.</param>
        /// <returns> Task </returns>
        void DeleteVehicle(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие контейнера в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска контейнера.</param>
        /// <returns>Возвращает <see langword="true"/>, если контейнер существует в хранилище данных.</returns>
        bool VehicleExists(Expression<Func<VEHICLE, bool>> filter);
    }
}