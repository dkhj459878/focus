﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с типами ТСТК.
    /// </summary>
    public interface IDevicetypeService : IDisposable
    {
        /// <summary>
        /// Возвращает типы ТСТК
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекция типов ТСТК автомобилей для пейджинга.</returns> 
        IEnumerable<DEVICETYPE> GetAllDevicetypes(
            Expression<Func<DEVICETYPE, bool>> filter = null,
            Func<IQueryable<DEVICETYPE>, IOrderedQueryable<DEVICETYPE>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает тип ТСТК с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о типах ТСТК, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о типах ТСТК.</param>
        /// <returns>Экземпляр типа ТСТК.</returns>
        IEnumerable<DEVICETYPE> GetDevicetypesPage(
           int pageSize,
           int pageNumber,
           Expression<Func<DEVICETYPE, bool>> filter = null,
           Func<IQueryable<DEVICETYPE>, IOrderedQueryable<DEVICETYPE>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает тип ТСТК с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор типа ТСТК.</param>
        /// <returns>Экземпляр типа ТСТК.</returns>
        DEVICETYPE GetDevicetype(byte id);

        /// <summary>
        /// Добавляет заданный тип ТСТК в хранилище данных
        /// и возвращает идентификатор добавленного типа ТСТК.
        /// </summary>
        /// <param name="user">Экземпляр типа ТСТК.</param>
        /// <returns>Уникальный идентификатор типа ТСТК.</returns>
        byte AddDevicetype(DEVICETYPE devicetype,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного типа ТСТК в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр типа ТСТК.</param>
        /// <returns> Task </returns>
        void UpdateDevicetype(DEVICETYPE devicetype,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет тип ТСТК с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор типа ТСТК.</param>
        /// <returns> Task </returns>
        void DeleteDevicetype(byte id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие типа ТСТК в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска типа ТСТК.</param>
        /// <returns>Возвращает <see langword="true"/>, если тип ТСТК существует в хранилище данных.</returns>
        bool DevicetypeExists(Expression<Func<DEVICETYPE, bool>> filter);
    }
}