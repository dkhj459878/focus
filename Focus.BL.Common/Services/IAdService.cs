﻿using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Service for managing user attributes' information from 
    /// Microsoft Active directory.
    /// </summary>
    public interface IAdService
    {
        string UserName { get; set; }

        string UserDomain { get; set; }

        bool IsInitialized { get; set; }

        /// <summary>
        /// Return all groups from active directory
        /// for some user belongs to.
        /// </summary>
        /// <returns></returns>
        IEnumerable<string> GetGroupsName();

        /// <summary>
        /// Check weither specified user belongs 
        /// to the certain group in active directory.
        /// </summary>
        /// <param name="groupName">Name of group.</param>
        /// <returns>True if specified user belongs to the certain group
        /// in an other case returns false.</returns>
        bool IsUserInGroup(string groupName);

        /// <summary>
        /// Gets the name of the departments
        /// user works in.
        /// </summary>
        /// <returns>Identificator (code) of the devision
        /// user works in.</returns>
        string GetUserDepartment();

        /// <summary>
        /// Create group for certain user.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        void CreateGroup(string groupName);

        /// <summary>
        /// Delete group for certain user.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        void DeleteGroup(string groupName);
    }
}
