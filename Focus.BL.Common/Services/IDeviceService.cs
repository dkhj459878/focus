﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с ТСТК.
    /// </summary>
    public interface IDeviceService : IDisposable
    {
        /// <summary>
        /// Возвращает все ТСТК, как товары,
        /// из хранилища данных.   
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекцию всех экземпляров ТСТК.</returns>
        IEnumerable<DEVICE> GetAllDevices(
            Expression<Func<DEVICE, bool>> filter = null,
            Func<IQueryable<DEVICE>, IOrderedQueryable<DEVICE>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает ТСТК
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документе.</param>
        /// <returns>Коллекция экземпляров ТСТК для пейджинга.</returns>
        IEnumerable<DEVICE> GetDevicesPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DEVICE, bool>> filter = null,
            Func<IQueryable<DEVICE>, IOrderedQueryable<DEVICE>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает ТСТК, как товары, с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор ТСТК.</param>
        /// <returns>Экземпляр ТСТК.</returns>
        DEVICE GetDevice(short id);

        /// <summary>
        /// Добавляет заданный ТСТК в хранилище данных
        /// и возвращает идентификатор добавленного ТСТК.
        /// </summary>
        /// <param name="user">Экземпляр ТСТК.</param>
        /// <returns>Уникальный идентификатор ТСТК.</returns>
        short AddDevice(DEVICE device,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного ТСТК в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр ТСТК.</param>
        /// <returns> Task </returns>
        void UpdateDevice(DEVICE device,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет ТСТК с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор ТСТК.</param>
        /// <returns> Task </returns>
        void DeleteDevice(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие ТСТК в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска ТСТК.</param>
        /// <returns>Возвращает <see langword="true"/>, если ТСТК существует в хранилище данных.</returns>
        bool DeviceExists(Expression<Func<DEVICE, bool>> filter);
    }
}