﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы со странами.
    /// </summary>
    public interface IUserRoleService : IDisposable
    {
        /// <summary>
        /// Возвращает страны
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекция экземпляров стран для пейджинга.</returns> 
        IEnumerable<USERROLES> GetAllUserRoles(
            Expression<Func<USERROLES, bool>> filter = null,
            Func<IQueryable<USERROLES>, IOrderedQueryable<USERROLES>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает страны
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений о странах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документе.</param>
        /// <returns>Экземпляр страны.</returns>
        IEnumerable<USERROLES> GetUserRolesPage(
           int pageSize,
           int pageNumber,
           Expression<Func<USERROLES, bool>> filter = null,
           Func<IQueryable<USERROLES>, IOrderedQueryable<USERROLES>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает страны
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns>Экземпляр страны.</returns>
        USERROLES GetUserRole(string id);

        /// <summary>
        /// Добавляет заданную страну в хранилище данных
        /// и возвращает идентификатор добавленной страны.
        /// </summary>
        /// <param name="user">Экземпляр страны.</param>
        /// <returns>Уникальный идентификатор страны.</returns>
        string AddUserRole(USERROLES userRole,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданной страны в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр страны.</param>
        /// <returns> Task </returns>
        void UpdateUserRole(USERROLES userRole,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет страну с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор страны.</param>
        /// <returns> Task </returns>
        void DeleteUserRole(string id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие страны в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска страны.</param>
        /// <returns>Возвращает <see langword="true"/>, если страна существует в хранилище данных.</returns>
        bool UserRoleExists(Expression<Func<USERROLES, bool>> filter);
    }
}