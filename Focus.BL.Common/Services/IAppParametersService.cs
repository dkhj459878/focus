﻿using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с параметрами приложения с использованием
    /// файла конфигурации.
    /// </summary>
    public interface IAppParametersService
    {
        /// <summary>
        /// Возвращает значение параметра приложения.
        /// </summary>
        /// <param name="parameterName">Наименование параметра приложения.</param>
        /// <returns>Значение параметра приложения.</returns>
        object GetParameter(string parameterName);

        /// <summary>
        /// Задает значение параметра приложения.
        /// </summary>
        /// <param name="parameterName">Наименование параметра приложения.</param>
        /// <param name="parameterValue">Устанавливаемое значение параметра приложения./<param>
        void SetParameter(string parameterName, string parameterValue);

        /// <summary>
        /// Возвращает библиотеку параметров приложения.
        /// Ключ библиотеки соответствует наименованию параметра приложения, а значение - 
        /// его значение соответственно.
        /// </summary>
        /// <returns>Библиотеку линейных параметров приложения.</returns>
        Dictionary<string, string> GetParameters();

        /// <summary>
        /// Задает все параметры приложения, не включенные в коллекцию.
        /// </summary>
        /// <param name="parameters">Библиотека параметров приложения.</param>
        void SetParameters(Dictionary<string, string> collecitonParameters);

        /// <summary>
        /// Возвращает библиотеку коллекций-параметров приложения.
        /// Ключ библиотеки соответствует наименованию коллекции-параметра
        /// приложения, значение - коллекция-параметр.
        /// </summary>
        /// <returns>Библиотеку коллекций линейных параметров.</returns>
        Dictionary<string, IEnumerable<string>> GetCollectionsParameters();
        
        /// <summary>
        /// Возвращает коллекцию-параметр приложения.
        /// </summary>
        /// <param name="collectionParameterName">Наименование коллекции-параметра приложения.</param>
        /// <returns>Коллекция-параметр приложения.</returns>
        IEnumerable<object> GetCollectionParameters(string collectionParameterName);

        /// <summary>
        /// Устанавлиет коллекцию-параметр приложения.
        /// </summary>
        /// <param name="collectionParameterName">Наименование коллекции-параметра приложения.</param>
        /// <param name="collectionParameters">Коллекция параметров приложения.</param>
        void SetCollectionParameters(string collectionParameterName, IEnumerable<object> collectionParameters);

        /// <summary>
        /// Задает библиотеку коллекций-параметров приложения.
        /// </summary>
        /// <param name="collectionParameter">Библиотека коллекций-параметров приложения.</param>
        void SetCollectionsParameters(Dictionary<string, IEnumerable<object>> collectionsParameters);
    }
}
