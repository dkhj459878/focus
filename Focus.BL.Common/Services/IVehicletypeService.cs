﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с типами транспортных средств.
    /// </summary>
    public interface IVehicletypeService : IDisposable
    {
        /// <summary>
        /// Возвращает типы транспортных средств
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// <returns>Коллекция экземпляров типов транспортных средств для пейджинга.</returns>
        IEnumerable<VEHICLETYPE> GetAllVehicletypes(
            Expression<Func<VEHICLETYPE, bool>> filter = null,
            Func<IQueryable<VEHICLETYPE>, IOrderedQueryable<VEHICLETYPE>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает тип транспортного средства с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документе.</param>
        /// <returns>Экземпляр типа транспортного средства.</returns>
        IEnumerable<VEHICLETYPE> GetVehicletypesPage(
           int pageSize,
           int pageNumber,
           Expression<Func<VEHICLETYPE, bool>> filter = null,
           Func<IQueryable<VEHICLETYPE>, IOrderedQueryable<VEHICLETYPE>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает тип транспортного средства с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства.</param>
        /// <returns>Экземпляр документа.</returns>
        VEHICLETYPE GetVehicletype(byte id);

        /// <summary>
        /// Добавляет заданный тип транспортного средства в хранилище данных
        /// и возвращает идентификатор добавленного типа транспортного средства.
        /// </summary>
        /// <param name="user">Экземпляр типа транспортного средства.</param>
        /// <returns>Уникальный идентификатор типа транспортного средства.</returns>
        byte AddVehicletype(VEHICLETYPE vehicletype,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного типа транспортного средства в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр типа транспортного средства.</param>
        /// <returns> Task </returns>
        void UpdateVehicletype(VEHICLETYPE vehicletype,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет тип транспортного средства с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор типа транспортного средства.</param>
        /// <returns> Task </returns>
        void DeleteVehicletype(byte id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие типа транспортного средства в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска типа транспортного средства.</param>
        /// <returns>Возвращает <see langword="true"/>, если тип транспортного средства существует в хранилище данных.</returns>
        bool VehicletypeExists(Expression<Func<VEHICLETYPE, bool>> filter);
    }
}