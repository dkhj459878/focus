﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с транспортными средствами.
    /// </summary>
    public interface IInspectorService : IDisposable
    {
        /// <summary>
        /// Возвращает всех инспекторов,
        /// из хранилища данных.  
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекцию всех экземпляров транспортных средств.</returns>
        IEnumerable<INSPECTOR> GetAllInspectors(
            Expression<Func<INSPECTOR, bool>> filter = null,
            Func<IQueryable<INSPECTOR>, IOrderedQueryable<INSPECTOR>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает инспекторов
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="pageSize">Количество сведений о транспортных средствах, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о транспортном средстве.</param>
        /// <returns>Коллекция экземпляров транспортных средства для пейджинга.</returns>
        IEnumerable<INSPECTOR> GetInspectorsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<INSPECTOR, bool>> filter = null,
            Func<IQueryable<INSPECTOR>, IOrderedQueryable<INSPECTOR>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает инспекторов с заданным идентификатором,
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор инспектора.</param>
        /// <returns>Экземпляр инспектора.</returns>
        INSPECTOR GetInspector(short id);

        /// <summary>
        /// Добавляет заданного инспектора в хранилище данных
        /// и возвращает идентификатор добавленного инспектора.
        /// </summary>
        /// <param name="user">Экземпляр инспектора.</param>
        /// <returns>Уникальный идентификатор инспектора.</returns>
        short AddInspector(INSPECTOR inspector,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного инспектора в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр инспектора.</param>
        /// <returns> Task </returns>
        void UpdateInspector(INSPECTOR inspector,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет инспектора с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор инспектора.</param>
        /// <returns> Task </returns>
        void DeleteInspector(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие инспектора в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска инспектора.</param>
        /// <returns>Возвращает <see langword="true"/>, если инспектор существует в хранилище данных.</returns>
        bool InspectorExists(Expression<Func<INSPECTOR, bool>> filter);
    }
}