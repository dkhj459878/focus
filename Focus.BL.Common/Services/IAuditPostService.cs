﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditPostService
    {
        List<POST> GetPostVersions(short id);
    }
}
