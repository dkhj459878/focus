﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditCustombrockerService
    {
        List<CUSTOMBROCKER> GetCustombrockerVersions(short id);
    }
}
