﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IRetrieveVehicleKindService
    {
        Dictionary<short?, Dictionary<DateTime, VEHICLEKIND>> GetVehicleKindVersions(
            IRetrieveVehicleTypeService retrieveVehicleTypeService,
            IAudit_trailService audit_TrailService,
            IVehiclekindService vehiclekindService, 
            IRetrievingEntityOptimaService retrievingEntityService,
            IRetrieveEntitiesVersions<VEHICLEKIND> retrieveEntitiesVersions);
    }
}
