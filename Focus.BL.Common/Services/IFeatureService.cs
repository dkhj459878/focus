﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с автомобилями.
    /// </summary>
    public interface IFeatureService : IDisposable
    {
        /// <summary>
        /// Возвращает автомобили, как товары,
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекция экземпляров автомобилей для пейджинга.</returns> 
        IEnumerable<FEATURE> GetAllFeatures(
            Expression<Func<FEATURE, bool>> filter = null,
            Func<IQueryable<FEATURE>, IOrderedQueryable<FEATURE>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает автомобили, как товары, с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="pageSize">Количество сведений об автомобилях, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о документе.</param>
        /// <returns>Экземпляр автомобиля.</returns>
        IEnumerable<FEATURE> GetFeaturesPage(
           int pageSize,
           int pageNumber,
           Expression<Func<FEATURE, bool>> filter = null,
           Func<IQueryable<FEATURE>, IOrderedQueryable<FEATURE>> orderBy = null,
           string includeProperties = "");

        /// <summary>
        /// Возвращает автомобиль, как товар, с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор документа.</param>
        /// <returns>Экземпляр документа.</returns>
        FEATURE GetFeature(short id);

        /// <summary>
        /// Добавляет заданный автомобиль в хранилище данных
        /// и возвращает идентификатор добавленного автомобиля.
        /// </summary>
        /// <param name="user">Экземпляр автомобиля.</param>
        /// <returns>Уникальный идентификатор автомобиля.</returns>
        short AddFeature(FEATURE feature,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного автомобиля в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр автомобиля.</param>
        /// <returns> Task </returns>
        void UpdateFeature(FEATURE feature,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет автомобиль с заданным идентификатором
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор автомобиля.</param>
        /// <returns> Task </returns>
        void DeleteFeature(short id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие автомобиля в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска автомобиля.</param>
        /// <returns>Возвращает <see langword="true"/>, если автомобиль существует в хранилище данных.</returns>
        bool FeatureExists(Expression<Func<FEATURE, bool>> filter);
    }
}