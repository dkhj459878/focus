﻿using Focus.DAL.Common.DbModels;
using System.Collections.Generic;

namespace Focus.BL.Common.Services
{
    public interface IAuditDeviceService
    {
        List<DEVICE> GetDeviceVersions(short id);
    }
}
