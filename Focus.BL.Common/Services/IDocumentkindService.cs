﻿using Focus.DAL.Common.DbModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Focus.BL.Common.Services
{
    /// <summary>
    /// Представляет сервис для работы с видами документов.
    /// </summary>
    public interface IDocumentkindService : IDisposable
    {
        /// <summary>
        /// Возвращает все виды документов
        /// из хранилища данных.  
         /// <param name="filter">Лямбда-выражение определяющее фильтрацию экземпляров.</param>
        /// <param name="orderBy">Лямбда-выражение определяющее сортировку экземпляров.</param>
        /// <param name="includeProperties">Список связанных свойств экземпляров, разделенный запятыми.</param>
        /// </summary>
        /// <returns>Коллекцию всех экземпляров виды документов.</returns>
        IEnumerable<DOCUMENTKIND> GetAllDocumentkinds(
            Expression<Func<DOCUMENTKIND, bool>> filter = null,
            Func<IQueryable<DOCUMENTKIND>, IOrderedQueryable<DOCUMENTKIND>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает виды документов
        /// из хранилища данных для пейджинга.
        /// </summary>
        /// <param name="pageSize">Количество сведений о видах документов, выводимых на страницу.</param>
        /// <param name="pageNumber">Номер выводимой страницы со сведениями о видах документов.</param>
        /// <returns>Коллекция экземпляров видов документов для пейджинга.</returns>
        IEnumerable<DOCUMENTKIND> GetDocumentkindsPage(
            int pageSize,
            int pageNumber,
            Expression<Func<DOCUMENTKIND, bool>> filter = null,
            Func<IQueryable<DOCUMENTKIND>, IOrderedQueryable<DOCUMENTKIND>> orderBy = null,
            string includeProperties = "");

        /// <summary>
        /// Возвращает вид документа с заданным идентификатором,
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор вида документа.</param>
        /// <returns>Экземпляр вида документа.</returns>
        DOCUMENTKIND GetDocumentkind(byte id);

        /// <summary>
        /// Добавляет заданный вид документа в хранилище данных
        /// и возвращает идентификатор добавленного вида документа .
        /// </summary>
        /// <param name="user">Экземпляр вида документа .</param>
        /// <returns>Уникальный идентификатор вида документа .</returns>
        byte AddDocumentkind(DOCUMENTKIND documentkind,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Обновляет информацию заданного вида документа  в хранилище данных.
        /// </summary>
        /// <param name="user">Экземпляр вида документа .</param>
        /// <returns> Task </returns>
        void UpdateDocumentkind(DOCUMENTKIND documentkind,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Удаляет контейнер с заданным видом документа 
        /// из хранилища данных.
        /// </summary>
        /// <param name="id">Идентификатор вида документа .</param>
        /// <returns> Task </returns>
        void DeleteDocumentkind(byte id,
            string userId = "",
            string userInfo = "");

        /// <summary>
        /// Проверяет наличие вида документа в хранилище данных
        /// соответствующего заданному фильтру.
        /// </summary>
        /// <param name="filter">Лямбда-выражение определяющее фильтр для поиска вида документа .</param>
        /// <returns>Возвращает <see langword="true"/>, если вид документа существует в хранилище данных.</returns>
        bool DocumentkindExists(Expression<Func<DOCUMENTKIND, bool>> filter);
    }
}