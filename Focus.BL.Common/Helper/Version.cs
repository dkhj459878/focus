﻿using System;

namespace Focus.BL.Common.Helpers
{
    public class Version<TType>: IVersion<TType> where TType : Type
    {
        public DateTime VersionDateTime { get; set; }

        public TType Entity { get; set; }

        public short Id { get; set; }
    }
}
