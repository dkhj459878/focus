﻿using System;
using System.Collections.Generic;

namespace Focus.BL.Common.Helpers
{
    public interface ITree<TType> where TType : Type
    {
        short? Id { get; set; }

        IEnumerable<Version<TType>> Versions { get; set; }
    }
}
