﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbCarRepository : BaseRepository<CAR>, IDbCarRepository
    {
        public DbCarRepository(Entities focusContext) : 
            base(focusContext)
        {
        }
    }
}
