﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbVehicletypeRepository : BaseRepository<VEHICLETYPE>, IDbVehicletypeRepository
    {
        public DbVehicletypeRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}