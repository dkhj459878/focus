﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbCountryRepository : BaseRepository<COUNTRY>, IDbCountryRepository
    {
        public DbCountryRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}