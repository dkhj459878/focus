﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbAudit_trailRepository : BaseRepository<AUDIT_TRAIL>, IDbAudit_trailRepository
    {
        public DbAudit_trailRepository(Entities focusContext) : 
            base(focusContext)
        {
        }
    }
}
