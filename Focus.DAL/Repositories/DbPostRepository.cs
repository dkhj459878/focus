﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbPostRepository : BaseRepository<POST>, IDbPostRepository
    {
        public DbPostRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}