﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbVersionRepository : BaseRepository<VERSION>, IDbVersionRepository
    {
        public DbVersionRepository(Entities focusContext) : 
            base(focusContext)
        {
        }
    }
}
