﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbOrganizationRepository : BaseRepository<ORGANIZATION>, IDbOrganizationRepository
    {
        public DbOrganizationRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}