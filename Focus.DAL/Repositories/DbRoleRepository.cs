﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbRoleRepository : BaseRepository<ROLES>, IDbRoleRepository
    {
        public DbRoleRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}