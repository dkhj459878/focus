﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbVehicleRepository : BaseRepository<VEHICLE>, IDbVehicleRepository
    {
        public DbVehicleRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}