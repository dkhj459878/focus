﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;

namespace Focus.DAL.Repositories
{
    public class DbStorageRepository : BaseRepository<STORAGE>, IDbStorageRepository
    {
        public DbStorageRepository(Entities focusContext) : base(focusContext)
        {
        }
    }
}