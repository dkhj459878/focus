﻿namespace Focus.DAL.Enum
{
    public enum CrudOperation
    {
        Added,
        Modified,
        Deleted
    }
}