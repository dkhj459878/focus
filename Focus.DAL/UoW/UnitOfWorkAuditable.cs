﻿using Focus.DAL.Common.DbModels;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Focus.DAL.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Focus.DAL.UoW
{
    /// <summary>
    /// Представляет собой паттерн UnitOfWork.
    /// в хранилище данных.
    /// </summary>
    public class UnitOfWorkAuditable : UnitOfWork
    {
        public UnitOfWorkAuditable(
            Entities focusContext,
            IDbCarRepository carRepository,
            IDbContainerRepository containerRepository,
            IDbCountryRepository countryRepository,
            IDbCustombrockerRepository custombrockerRepository,
            IDbDeviceRepository deviceRepository,
            IDbDevicetypeRepository devicetypeRepository,
            IDbDocRepository docRepository,
            IDbDocumentkindRepository documentkindRepository,
            IDbDocumentRepository documentRepository,
            IDbInspectorRepository inspectorRepository,
            IDbOrganizationRepository organizationRepository,
            IDbPermissionRepository permissionRepository,
            IDbPostRepository postRepository,
            IDbReportRepository reportRepository,
            IDbStorageRepository storageRepository,
            IDbVehiclekindRepository vehiclekindRepository,
            IDbVehicleRepository vehicleRepositor,
            IDbVehicletypeRepository vehicletypeRepository,
            IDbAudit_trailRepository audit_trailRepository,
            IDbFeatureRepository featureRepository,
            IDbUserRepository userRepository,
            IDbVersionRepository versionRepository,
            IDbRoleRepository roleRepository,
            IDbUserRoleRepository userRoleRepository) :
            base
            (
             focusContext,
             carRepository,
             containerRepository,
             countryRepository,
             custombrockerRepository,
             deviceRepository,
             devicetypeRepository,
             docRepository,
             documentkindRepository,
             documentRepository,
             inspectorRepository,
             organizationRepository,
             permissionRepository,
             postRepository,
             reportRepository,
             storageRepository,
             vehiclekindRepository,
             vehicleRepositor,
             vehicletypeRepository,
             audit_trailRepository,
             featureRepository,
             userRepository,
             versionRepository,
             roleRepository,
             userRoleRepository
            )
        {
        }

        private string KeyEntityFieldName { get; set; } = string.Empty;

        ~UnitOfWorkAuditable()
        {
            Dispose(false);
        }

        /// <summary>
        /// Сохранение изменений
        /// </summary>
        /// <returns> int </returns>
        public override int SaveChanges(string userId = "", string userInfo = "")
        {
            // Get all Added/Deleted/Modified entities (not Unmodified or Detached)

            foreach (DbEntityEntry entityEntry in _focusContext.ChangeTracker.Entries().Where(p => p.State == EntityState.Added || p.State == EntityState.Deleted || p.State == EntityState.Modified))
            {
                // For each changed record, get the audit record entries and add them
                string tableName = GetTableName(entityEntry);
                const string TABLE_NAME_USED_FOR_THE_AUDIT_PURPOSES = "AUDIT_TRAIL";
                if (tableName == TABLE_NAME_USED_FOR_THE_AUDIT_PURPOSES)
                {
                    continue; // There is no need to audit the entity used for the audit purposes itself.
                }

                KeyEntityFieldName = GetKeyName(entityEntry, tableName);
                short entityId = 0;
                IEnumerable<AUDIT_TRAIL> entryCollection = GetAuditRecordsForChange(entityEntry, entityId, userId, userInfo);
                if (entityEntry.State == EntityState.Added)
                {
                    _focusContext.SaveChanges();
                    entityId = short.Parse(entityEntry.CurrentValues.GetValue<object>(KeyEntityFieldName).ToString());
                    foreach (var auditTrail in entryCollection)
                    {
                        if (auditTrail.FIELD == KeyEntityFieldName)
                        {
                            auditTrail.VALUENEW = entityId.ToString();
                        }
                        auditTrail.ENTITYID = entityId;
                    }
                }
                else
                {
                    // There is no logic branch.
                }


                Audit_trailRepository.InsertRange(entryCollection);
            }

            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return _focusContext.SaveChanges();
        }
               

        private List<AUDIT_TRAIL> GetAuditRecordsForChange(DbEntityEntry dbEntry, short entityId, string userId, string userInfo)
        {
            List<AUDIT_TRAIL> changesEntity = new List<AUDIT_TRAIL>();
            DateTime changeTime = DateTime.Now;
            const string LAST_UPDATE_FIELD_NAME = "LAST_UPDATE";
            const string SID_FIELD_NAME = "SID";
            const string USER_ID_FIELD_NAME = "USERID";
            const string USER_INFO_FIELD_NAME = "USERINFO";


            // Get primary key name.
            string tableName = GetTableName(dbEntry);
            KeyEntityFieldName = GetKeyName(dbEntry, tableName);

            if (dbEntry.State == EntityState.Added)
            {
                // For Inserts, just add the whole record

                DateTime last_update = Convert.ToDateTime(dbEntry.CurrentValues.GetValue<object>(LAST_UPDATE_FIELD_NAME) == null ? null : dbEntry.CurrentValues.GetValue<object>(LAST_UPDATE_FIELD_NAME).ToString());

                foreach (string propertyName in dbEntry.CurrentValues.PropertyNames)
                {
                    if (propertyName == SID_FIELD_NAME || propertyName == USER_ID_FIELD_NAME || propertyName == USER_INFO_FIELD_NAME) // Some properties we ignore in audit process.
                    {
                        continue;
                    }

                    string valueNew = dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString();

                    if (string.IsNullOrEmpty(valueNew))
                    {
                        continue;
                    }

                    changesEntity.Add(new AUDIT_TRAIL()
                    {
                        SID = userId,
                        USERINFO = userInfo,
                        TIME_STAMP = changeTime,
                        TYPE = (short)CrudOperation.Added,    // Added
                        ENTITY = tableName,
                        ENTITYID = entityId,
                        DATE_ = last_update,
                        FIELD = propertyName,
                        VALUENEW = valueNew
                    }
                    );
                }
                AUDIT_TRAIL last_update_item = changesEntity.Where(p => p.FIELD == LAST_UPDATE_FIELD_NAME).FirstOrDefault();
                if (last_update_item !=null)
                {
                    changesEntity.Remove(last_update_item);
                }
            }
            else if (dbEntry.State == EntityState.Deleted)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    if (propertyName == SID_FIELD_NAME || propertyName == USER_ID_FIELD_NAME || propertyName == USER_INFO_FIELD_NAME) // Some properties we ignore in audit process.
                    {
                        continue;
                    }
                    string valuOld = dbEntry.OriginalValues.GetValue<object>(propertyName) == null ? null : dbEntry.OriginalValues.GetValue<object>(propertyName).ToString();

                    if (string.IsNullOrEmpty(valuOld))
                    {
                        continue;
                    }

                    DateTime last_update = Convert.ToDateTime(dbEntry.OriginalValues.GetValue<object>(LAST_UPDATE_FIELD_NAME)?.ToString());
                    short entityIdDeleted = short.Parse(dbEntry.OriginalValues.GetValue<object>(KeyEntityFieldName)?.ToString());
                    changesEntity.Add(new AUDIT_TRAIL()
                    {
                        SID = userId,
                        USERINFO = userInfo,
                        TIME_STAMP = changeTime,
                        TYPE = (short)CrudOperation.Deleted,    // Added
                        ENTITY = tableName,
                        ENTITYID = entityIdDeleted,
                        DATE_ = last_update,
                        FIELD = propertyName,
                        VALUENEW = string.Empty,
                        VALUEOLD = valuOld
                    }
                            );
                }
                AUDIT_TRAIL last_update_item = changesEntity.Where(p => p.FIELD == LAST_UPDATE_FIELD_NAME).FirstOrDefault();
                if (last_update_item != null)
                {
                    changesEntity.Remove(last_update_item);
                }

            }
            else if (dbEntry.State == EntityState.Modified)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    if (propertyName == SID_FIELD_NAME || propertyName == USER_ID_FIELD_NAME || propertyName == USER_INFO_FIELD_NAME) // Some property we ignore in audit process.
                    {
                        continue;
                    }
                    // For updates, we only want to capture the columns that actually changed
                    var valueOld = dbEntry.OriginalValues.GetValue<object>(propertyName);
                    var valueNew = dbEntry.CurrentValues.GetValue<object>(propertyName);

                    DateTime last_update = Convert.ToDateTime(dbEntry.CurrentValues.GetValue<object>(LAST_UPDATE_FIELD_NAME) == null ? null : dbEntry.CurrentValues.GetValue<object>(LAST_UPDATE_FIELD_NAME).ToString());

                    if (!object.Equals(valueOld, valueNew))
                    {
                        changesEntity.Add(new AUDIT_TRAIL()
                        {
                            SID = userId,
                            USERINFO = userInfo,
                            TIME_STAMP = changeTime,
                            DATE_ = last_update,
                            TYPE = (short)CrudOperation.Modified,    // Modified
                            ENTITY = tableName,
                            ENTITYID = short.Parse(dbEntry.OriginalValues.GetValue<object>(KeyEntityFieldName).ToString()),
                            FIELD = propertyName,
                            VALUEOLD = dbEntry.OriginalValues.GetValue<object>(propertyName)?.ToString() ?? string.Empty,
                            VALUENEW = dbEntry.CurrentValues.GetValue<object>(propertyName)?.ToString() ?? string.Empty,
                        }
                                        );
                    }
                }
                AUDIT_TRAIL last_update_item = changesEntity.Where(p => p.FIELD == LAST_UPDATE_FIELD_NAME).FirstOrDefault();
                if (last_update_item != null)
                {
                    changesEntity.Remove(last_update_item);
                }
            }
            // Otherwise, don't do anything, we don't care about Unchanged or Detached entities

            return changesEntity;
        }

        private string GetKeyName(DbEntityEntry dbEntry,  string tableName)
        {
            const string USUAL_ENTITY_IDENTIFICATOR = "ID";

            string localTableName = tableName;

            string keyNameFull = localTableName + USUAL_ENTITY_IDENTIFICATOR;
            string keyNameShort = USUAL_ENTITY_IDENTIFICATOR;
            List<string> propertyNames = new List<string>();
            DbEntityEntry localDbEntity = dbEntry;
            switch (dbEntry.State)
            {
                case EntityState.Added:
                    propertyNames = localDbEntity.CurrentValues.PropertyNames.ToList();
                    break;
                case EntityState.Deleted:
                    propertyNames = localDbEntity.OriginalValues.PropertyNames.ToList();
                    break;
                case EntityState.Modified:
                    propertyNames = localDbEntity.CurrentValues.PropertyNames.ToList();
                    break;
                default:
                    break;
            }

            bool FullEntityIdentificatorExists = propertyNames.Any(p => p.Equals(keyNameFull, StringComparison.OrdinalIgnoreCase));

            return FullEntityIdentificatorExists ?
                propertyNames.Where(p => p.Equals(keyNameFull, StringComparison.OrdinalIgnoreCase)).FirstOrDefault() :
                propertyNames.Where(p => p.Equals(keyNameShort, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            
        }

        private string GetTableName(DbEntityEntry dbEntry)
        {
            // Get the Table() attribute, if one exists
            //TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), false).SingleOrDefault() as TableAttribute;

            TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), true).SingleOrDefault() as TableAttribute;

            Type typeOfEntity = dbEntry.Entity.GetType();

            // Get table name (if it has a Table attribute, use that, otherwise get the pluralized name)
            return tableAttr != null ? tableAttr.Name : (dbEntry.State == EntityState.Added ? typeOfEntity.Name : (typeOfEntity.BaseType.Name == "Object" ? typeOfEntity.Name : typeOfEntity.BaseType.Name));
        }
    }
}
