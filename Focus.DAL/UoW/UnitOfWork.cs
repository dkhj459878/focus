﻿using System;
using Focus.DAL.Common.Repositories;
using Focus.DAL.Common.UoW;
using Focus.DAL.Common.DbModels;

namespace Focus.DAL.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        protected Entities _focusContext;
        private bool _disposed;

        /// <summary>
        /// Unit of Work для FocusContext
        /// </summary>
        /// <param name="focusContext">Контекст данных</param>
        /// <param name="carRepository">Репозиторий для автомобилей, как товара.</param>
        /// <param name="containerRepository">Репозиторий для контейнеров.</param>
        /// <param name="countryRepository">Репозиторий стран.</param>
        /// <param name="custombrockerRepository">Репозиторий для уполномоченных представителей (декларантов).</param>
        /// <param name="deviceRepository">Репозиторий для технических средств таможенного контроля.</param>
        /// <param name="devicetypeRepository">Репозиторий для типов технических средств таможенного контроля.</param>
        /// <param name="docRepository">Репозиторий для документов.</param>
        /// <param name="documentkindRepository">Репозиторий для типов документов, содержащих сведения о полномочиях таможенных представителей.</param>
        /// <param name="documentRepository">Репозиторий для документов, содержащих сведения о полномочиях таможенных представителей.</param>
        /// <param name="inspectorRepository">Репозиторий для должностных лиц таможни.</param>
        /// <param name="organizationRepository">Репозиторий для организаций.</param>
        /// <param name="permissionRepository.">Репозиторий для разрешений.</param>
        /// <param name="postRepository">Репозиторий для пунктов таможенного оформления.</param>
        /// <param name="reportRepository">Репозиторий для отчетов (протоколирования действий пользователей)..</param>
        /// <param name="storageRepository">Репозиторий для складов временного хранения.</param>
        /// <param name="vehiclekindRepository">Репозиторий для видов транспортных средств.</param>
        /// <param name="vehicleRepositor">Репозиторий для транспортных средств.</param>
        /// <param name="vehicletypeRepository">Репозиторий для типов транспортных средств.</param>
        /// <param name="trackingAuditRepository">Репозиторий для аудита.</param>
        /// <param name="userRepository">Репозиторий для аудита.</param>

        public UnitOfWork(
            Entities focusContext,
            IDbCarRepository carRepository,
            IDbContainerRepository containerRepository,
            IDbCountryRepository countryRepository,
            IDbCustombrockerRepository custombrockerRepository,
            IDbDeviceRepository deviceRepository,
            IDbDevicetypeRepository devicetypeRepository,
            IDbDocRepository docRepository,
            IDbDocumentkindRepository documentkindRepository,
            IDbDocumentRepository documentRepository,
            IDbInspectorRepository inspectorRepository,
            IDbOrganizationRepository organizationRepository,
            IDbPermissionRepository permissionRepository,
            IDbPostRepository postRepository,
            IDbReportRepository reportRepository,
            IDbStorageRepository storageRepository,
            IDbVehiclekindRepository vehiclekindRepository,
            IDbVehicleRepository vehicleRepositor,
            IDbVehicletypeRepository vehicletypeRepository,
            IDbAudit_trailRepository audit_trailRepository,
            IDbFeatureRepository featureRepository,
            IDbUserRepository userRepository,
            IDbVersionRepository versionRepository,
            IDbRoleRepository roleRepository,
            IDbUserRoleRepository userRoleRepository)
        {
            _focusContext = focusContext;
            CarRepository = carRepository;
            ContainerRepository = containerRepository;
            CountryRepository = countryRepository;
            CustombrockerRepository = custombrockerRepository;
            DeviceRepository = deviceRepository;
            DevicetypeRepository = devicetypeRepository;
            DocRepository = docRepository;
            DocumentkindRepository = documentkindRepository;
            DocumentRepository = documentRepository;
            InspectorRepository = inspectorRepository;
            OrganizationRepository = organizationRepository;
            PermissionRepository = permissionRepository;
            PostRepository = postRepository;
            ReportRepository = reportRepository;
            StorageRepository = storageRepository;
            VehiclekindRepository = vehiclekindRepository;
            VehicleRepository = vehicleRepositor;
            VehicletypeRepository = vehicletypeRepository;
            Audit_trailRepository = audit_trailRepository;
            FeatureRepository = featureRepository;
            VersionRepository = versionRepository;
            UserRepository = userRepository;
            RoleRepository = roleRepository;
            UserRoleRepository = userRoleRepository;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }

        /// <summary>
        /// Возвращает репозиторий для автомобилей, как товара.
        /// </summary>
        public IDbCarRepository CarRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для контейнеров.
        /// </summary>
        public IDbContainerRepository ContainerRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для автомобилей, как товара.
        /// </summary>
        public IDbCountryRepository CountryRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для пользователей.
        /// </summary>
        public IDbUserRepository UserRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для автомобилей, как товара.
        /// </summary>
        public IDbCustombrockerRepository CustombrockerRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для автомобилей, как товара.
        /// </summary>
        public IDbDeviceRepository DeviceRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для автомобилей, как товара.
        /// </summary>
        public IDbDevicetypeRepository DevicetypeRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для документов.
        /// </summary>
        public IDbDocRepository DocRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для типов документов, содержащих сведения о полномочиях таможенных представителей.
        /// </summary>
        public IDbDocumentkindRepository DocumentkindRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для документов, содержащих сведения о полномочиях таможенных представителей.
        /// </summary>
        public IDbDocumentRepository DocumentRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для должностных лиц таможни.
        /// </summary>
        public IDbInspectorRepository InspectorRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для организаций.
        /// </summary>
        public IDbOrganizationRepository OrganizationRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для разрешений.
        /// </summary>
        public IDbPermissionRepository PermissionRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для пунктов таможенного оформления.
        /// </summary>
        public IDbPostRepository PostRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для отчетов (протоколирования действий пользователей).
        /// </summary>
        public IDbReportRepository ReportRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для складов временного хранения.
        /// .
        /// </summary>
        public IDbStorageRepository StorageRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для видов транспортных средств.
        /// </summary>
        public IDbVehiclekindRepository VehiclekindRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для транспортных средств.
        /// </summary>
        public IDbVehicleRepository VehicleRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для типов транспортных средств.
        /// </summary>
        public IDbVehicletypeRepository VehicletypeRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для типов транспортных средств.
        /// </summary>
        public IDbAudit_trailRepository Audit_trailRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для типов транспортных средств.
        /// </summary>
        public IDbFeatureRepository FeatureRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для типов транспортных средств.
        /// </summary>
        public IDbVersionRepository VersionRepository{ get; }

        /// <summary>
        /// Возвращает репозиторий для типов транспортных средств.
        /// </summary>
        public IDbRoleRepository RoleRepository { get; }

        /// <summary>
        /// Возвращает репозиторий для типов транспортных средств.
        /// </summary>
        public IDbUserRoleRepository UserRoleRepository { get; }

        /// <summary>
        /// Освобождает управляемые ресурсы.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Сохранение изменений
        /// </summary>
        /// <returns> int </returns>
        public virtual int SaveChanges()
        {
            return _focusContext.SaveChanges();
        }

        /// <summary>
        /// Сохранение изменений
        /// </summary>
        /// <returns> int </returns>
        public virtual int SaveChanges(string userId = "", string userInfo = "")
        {
            return _focusContext.SaveChanges();
        }

        /// <summary>
        /// Освобождает управляемые ресурсы.
        /// </summary>
        /// <param name="disposing">Указывает вызван ли этот метод из метода Dispose() или из финализатора.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (_disposed) return;

            if (disposing)
            {
                GC.SuppressFinalize(this);
            }

            _focusContext?.Dispose();
            _focusContext = null;
            _disposed = true;
        }
    }
}
